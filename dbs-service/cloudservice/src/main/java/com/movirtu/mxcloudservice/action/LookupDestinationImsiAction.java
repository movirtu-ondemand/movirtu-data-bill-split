package com.movirtu.mxcloudservice.action;

import org.apache.log4j.Logger;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.dao.UserDetails;
import com.movirtu.mxcloudservice.dao.manager.UserDetailsManager;
import com.movirtu.mxcloudservice.service.Session;
import com.movirtu.mxcloudservice.service.machine.MachineEvent;
import com.movirtu.mxcloudservice.service.sms.SmsSession;

public class LookupDestinationImsiAction extends CallBaseAction{

	Logger logger = Logger.getLogger(LookupDestinationImsiAction.class);
	private MachineEvent failureEvent;
	
	public boolean perform(Session session) throws Exception, RestException {
		// TODO Auto-generated method stub
		UserDetails ud = UserDetailsManager.getInstance().getUserDetailsFromImsi(session.getDestinationImsi());
		//logger.info("");
		if(ud==null) {
			if(proceedEvenWithFailure()) {
				return true;
			} else {
				onError(session);
			}
		}
		session.setCdpn(ud.getPhoneNumber());
		session.setOrigCdpn(ud.getPhoneNumber());
		
		if(session instanceof SmsSession) {
			((SmsSession) session).setCalledPartySipUser(ud.getSipUser());
			
		}
		return true;
	}

	public String getName() {
		// TODO Auto-generated method stub
		return "LookupDestinationImsiAction";
	}

	public MachineEvent getFailureEvent() {
		return failureEvent;
	}

	public void setFailureEvent(MachineEvent failureEvent) {
		this.failureEvent = failureEvent;
	}

}
