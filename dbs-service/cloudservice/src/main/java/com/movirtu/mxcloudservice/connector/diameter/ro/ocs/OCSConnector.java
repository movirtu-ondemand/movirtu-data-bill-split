package com.movirtu.mxcloudservice.connector.diameter.ro.ocs;

import java.io.File;
import java.io.FileInputStream;
import java.net.URI;

import org.apache.log4j.Logger;
import org.jdiameter.api.Message;

import com.movirtu.diameter.charging.apps.B2BGw.client.B2BGwCCRClient;
import com.movirtu.diameter.charging.apps.B2BGw.client.B2BGwCCRController;
import com.movirtu.diameter.charging.apps.common.PrepaidServiceProcessingDiameterMessages;
import com.movirtu.diameter.charging.apps.common.TransactionInfo;
import com.movirtu.mxcloudservice.connector.GenericConsumer;
import com.movirtu.mxcloudservice.connector.cache.GenericCache;
import com.movirtu.mxcloudservice.connector.diameter.ro.OnlineChargingConnector;
import com.movirtu.mxcloudservice.connector.event.DiameterEvent;
import com.movirtu.mxcloudservice.service.machine.MachineEvent;

public class OCSConnector extends OnlineChargingConnector implements PrepaidServiceProcessingDiameterMessages {

	B2BGwCCRController client;
	
	private GenericCache<String, String> cache;
	
	Logger logger = Logger.getLogger(OCSConnector.class);
	

	protected void createClient() {
		// TODO Auto-generated method stub
		actionConsumer = new GenericConsumer(outQueue, this, getActionExecutor(),new OCSConnectorActionConsumer(this));
	}


	protected void createServer() throws Exception {
		// TODO Auto-generated method stub
		client = new B2BGwCCRController();
		client.clientNode = new B2BGwCCRClient(this);
		client.clientNode.init(new FileInputStream(new File(new URI("file:"+getConfigFileName()))),"CLIENT");
		//client.clientNode.init(getClass().getResourceAsStream(getConfigFileName()), "CLIENT");
		client.clientNode.start();
		//client.setUp(super.getConfigFileName());
		setCache(new GenericCache<String,String>());
		eventConsumer = new GenericConsumer(inQueue, this, getEventExecutor() , new OCSConnectorEventConsumer(this));
	}

	public void serviceProcessingCCR(TransactionInfo obj) {
		// TODO Auto-generated method stub
		
	}

	public void serviceProcessingCCA(TransactionInfo obj) {
		// TODO Auto-generated method stub
		logger.info("Recieved CCA from OCS");
		getEventQueue().add(new DiameterEvent("", MachineEvent.CCA, obj));
	}

	public void sessionSupervisionTimerExpiredNotification(String sessionId) {
		// TODO Auto-generated method stub
		logger.info("ignored : Received sessionSupervisionTimerExpiredNotification sessionId="+sessionId);
		
	}

	public GenericCache<String, String> getCache() {
		return cache;
	}

	public void setCache(GenericCache<String, String> cache) {
		this.cache = cache;
	}


	public void serviceProcessingRAR(TransactionInfo obj) {
		// TODO Auto-generated method stub
		logger.info("ignored : Received serviceProcessingRAR");
	}


	public void serviceProcessingRAA(TransactionInfo obj) {
		// TODO Auto-generated method stub
		logger.info("ignored : Received serviceProcessingRAA");
	}


	public void sessionSupervisionTimerStartedNotification(String sessionId) {
		// TODO Auto-generated method stub
		logger.info("ignored : Received sessionSupervisionTimerStartedNotification sessionId="+sessionId);
	}


	public void sessionSupervisionTimerRestartedNotification(String sessionId) {
		// TODO Auto-generated method stub
		logger.info("ignored : sessionSupervisionTimerRestartedNotification sessionId = "+sessionId);
	}


	public void sessionSupervisionTimerStoppedNotification(String sessionId) {
		// TODO Auto-generated method stub
		logger.info("ignored : received sessionSupervisionTimerStoppedNotification sessionId = "+sessionId);
	}


	public void txTimerExpiredNotification(String sessionId) {
		// TODO Auto-generated method stub
		logger.info("txTimerExpiredNotification received on sessionId="+sessionId);
	}


	public void grantAccessOnDeliverFailureNotification(String sessionId,
			Message request) {
		// TODO Auto-generated method stub
		logger.info("grantAccessOnDeliverFailureNotification received on sessionId="+sessionId);
		getEventQueue().add(new DiameterEvent(sessionId, MachineEvent.ALLOW_FAILOVER_DELIVERY_FAILURE, request));
	}


	public void denyAccessOnDeliverFailureNotification(String sessionId,
			Message request) {
		// TODO Auto-generated method stub
		logger.info("denyAccessOnDeliverFailureNotification received on sessionId="+sessionId);
		getEventQueue().add(new DiameterEvent(sessionId, MachineEvent.DENY_FAILOVER_DELIVERY_FAILURE, request));
	}


	public void grantAccessOnTxExpireNotification(String sessionId) {
		// TODO Auto-generated method stub
		logger.info("grantAccessOnTxExpireNotification received on sessionId="+sessionId);
		getEventQueue().add(new DiameterEvent(sessionId, MachineEvent.ALLOW_FAILOVER_TIMEOUT));
	}

	public void denyAccessOnTxExpireNotification(String sessionId) {
		// TODO Auto-generated method stub
		logger.info("denyAccessOnTxExpireNotification sessionId="+sessionId);
		getEventQueue().add(new DiameterEvent(sessionId, MachineEvent.DENY_FAILOVER_TIMEOUT));
	}


	public void grantAccessOnFailureMessageNotification(String sessionId) {
		// TODO Auto-generated method stub
		logger.info("grantAccessOnFailureMessageNotification sessionId="+sessionId);
		getEventQueue().add(new DiameterEvent(sessionId, MachineEvent.ALLOW_FAILOVER_FAILURE_MESSAGE));
	}


	public void denyAccessOnFailureMessageNotification(String sessionId) {
		// TODO Auto-generated method stub
		logger.info("denyAccessOnFailureMessageNotification sessionId="+sessionId);
		getEventQueue().add(new DiameterEvent(sessionId, MachineEvent.DENY_FAILOVER_FAILURE_MESSAGE));
	}


	public void indicateServiceErrorNotification(String sessionId) {
		// TODO Auto-generated method stub
		logger.info("indicateServiceErrorNotification sessionId="+sessionId);
	}

}
