package com.movirtu.mxcloudservice.connector.thrift.map.perform;

import java.util.HashMap;

import org.apache.thrift.TException;

import com.movirtu.mxcloudservice.connector.action.ConnectorAction;
import com.movirtu.mxcloudservice.connector.thrift.map.mapjavainterface.Client;
import com.movirtu.mxcloudservice.service.sms.SmsSession;

public class ReqOutgoingSms extends AbstractMapPerformer{

	public ReqOutgoingSms(Client client) {
		super(client);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void perform(ConnectorAction action) throws TException {
		// TODO Auto-generated method stub
		SmsSession session = (SmsSession) action.getSession();
		logger.info("sending reqOutgoingSmsData : SMS "+
					"Service Id = "+session.getServiceId()+
					",Connector Session Id = "+action.getConnecterSessionId()+
					",CGPN="+session.getCgpn()+
					",CDPN="+session.getCdpn()+
					",EncodingScheme = "+session.getEncodingScheme()+
					",MessageContent = "+session.getMessageContent());

		
			client.reqOutgoingSmsData(session.getServiceId(), 
					  action.getConnecterSessionId(), 
					  session.getCgpn(), 
					  session.getCdpn(), 
					  session.getEncodingScheme(), 
					  session.getMessageContent(), 
					  new HashMap<String, String>());
	}

}
