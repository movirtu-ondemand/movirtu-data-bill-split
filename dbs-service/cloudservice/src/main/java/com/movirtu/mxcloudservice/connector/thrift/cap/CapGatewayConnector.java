package com.movirtu.mxcloudservice.connector.thrift.cap;

import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.server.TServer.Args;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransportException;

import com.movirtu.mxcloudservice.connector.GenericConsumer;
import com.movirtu.mxcloudservice.connector.thrift.BaseThriftConnector;
import com.movirtu.mxcloudservice.service.SessionFactory;

public class CapGatewayConnector extends BaseThriftConnector {
	
	private CapActionConsumer capActionConsumer;
	private CapEventConsumer capEventConsumer;
	
	public void recreateClient() throws TTransportException {
		transport = new TSocket(getSenderIpAddress(),getSenderPort());
		transport.open();
		protocol = new TBinaryProtocol(transport);
		mxcap.Client client = new mxcap.Client(protocol);
		capActionConsumer.setClient(client);// = new MapActionConsumer(client);
		((GenericConsumer)actionConsumer).setConsumable(capActionConsumer);
	}
	
	public void createClient() throws TTransportException {
		transport = new TSocket(getSenderIpAddress(),getSenderPort());
		transport.open();
		
		protocol = new TBinaryProtocol(transport);
		mxcap.Client client = new mxcap.Client(protocol);
		
		capActionConsumer = new CapActionConsumer(client, this);
		actionConsumer = new GenericConsumer(outQueue, this, getActionExecutor(),capActionConsumer);
	}
	
	public void createServer() throws TTransportException {
		serverTransport = new TServerSocket(getReceiverPort());
		
		CapGatewayHandler handler = new CapGatewayHandler(this);
		
		mxcapservice.Processor processor = new mxcapservice.Processor(handler); 
		
		server = new TSimpleServer(new Args(serverTransport).processor(processor));
		//
		//capEventConsumer = new CapEventConsumer(getService(), this);
		//eventConsumer = new GenericConsumer(inQueue, this, getEventExecutor() , capEventConsumer);

	}
}
