package com.movirtu.mxcloudservice.action.sms;

import org.apache.log4j.Logger;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.action.ari.BaseAriAction;
import com.movirtu.mxcloudservice.action.sms.util.SmsDirection;
import com.movirtu.mxcloudservice.action.sms.util.SmsState;
import com.movirtu.mxcloudservice.dao.manager.SmsDetailsManager;
import com.movirtu.mxcloudservice.dao.manager.UserDetailsManager;
import com.movirtu.mxcloudservice.service.Session;
import com.movirtu.mxcloudservice.service.sms.SmsSession;

public class SaveOutgoingSmsToDBAction extends BaseAriAction {

	private Logger logger = Logger.getLogger(ParseAndCreateOutgoingSmsAction.class);

	public boolean perform(Session session) throws Exception, RestException {
		System.out.println("Inside "+getName());

		SmsSession smsSession = (SmsSession)session;

		SmsDetailsManager.getInstance().saveSms(smsSession.getMessageId(), 
				SmsDirection.OUTGOING.getDirection(), 
				SmsState.PENDING.getState(), 
				smsSession.getCgpn(), 
				smsSession.getCdpn(), 
				smsSession.getSqlTimestamp(), 
				smsSession.getMessageContent());

		return true;
	}

}
