package com.movirtu.mxcloudservice.action.diameter;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.connector.action.ConnectorActionType;
import com.movirtu.mxcloudservice.connector.action.DiameterConnectorAction;
import com.movirtu.mxcloudservice.service.DiameterSession;

public class SendCCAToCTF extends BaseDiameterAction{

	@Override
	public boolean perform(DiameterSession session) throws Exception,
			RestException {
		// TODO Auto-generated method stub
		//session.getTransactionInfo1().result = session.getTransactionInfo2().result;
		super.getConnector().getActionQueue().add(new DiameterConnectorAction(session, ConnectorActionType.SEND_CCA, session.getTransactionInfo()));
		return true;
	}

}