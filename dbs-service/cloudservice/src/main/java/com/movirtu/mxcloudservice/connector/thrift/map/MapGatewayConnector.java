package com.movirtu.mxcloudservice.connector.thrift.map;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;

import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocolFactory;
import org.apache.thrift.server.TServer.Args;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransportException;
import org.apache.thrift.transport.TTransportFactory;

import com.movirtu.mxcloudservice.connector.GenericConsumer;
import com.movirtu.mxcloudservice.connector.thrift.BaseThriftConnector;

public class MapGatewayConnector extends BaseThriftConnector{
	
	private MapActionConsumer mapActionConsumer;
	private MapEventConsumer mapEventConsumer;
	
	public void recreateClient() throws TTransportException {
		transport = new TFramedTransport(new TSocket(getSenderIpAddress(),getSenderPort()));
		//transport = new TSocket(getSenderIpAddress(),getSenderPort());
		transport.open();
		protocol = new TBinaryProtocol(transport);
		mapjavainterface.Client client = new mapjavainterface.Client(protocol);
		mapActionConsumer.setClient(client);// = new MapActionConsumer(client);
		((GenericConsumer)actionConsumer).setConsumable(mapActionConsumer);
	}
	
	public void createClient() throws TTransportException {
		transport = new TFramedTransport(new TSocket(getSenderIpAddress(),getSenderPort()));
		//transport = new TSocket(getSenderIpAddress(),getSenderPort());
		transport.open();
		
		protocol = new TBinaryProtocol(transport);
		mapjavainterface.Client client = new mapjavainterface.Client(protocol);
		
		mapActionConsumer = new MapActionConsumer(client, this);
		actionConsumer = new GenericConsumer(outQueue, this, getActionExecutor(),mapActionConsumer);
	}
	
	public void createServer() throws TTransportException, UnknownHostException, IOException {
		
		TProtocolFactory protocolFactory = new TBinaryProtocol.Factory();
		TTransportFactory transportFactory = new TFramedTransport.Factory();
		serverTransport = new TServerSocket(getReceiverPort());
		
		MapGatewayHandler handler = new MapGatewayHandler(this);
		
		mapjavagw.Processor processor = new mapjavagw.Processor(handler); 
		
		server = new TSimpleServer(new Args(serverTransport).processor(processor).transportFactory(transportFactory).protocolFactory(protocolFactory));
		//
		mapEventConsumer = new MapEventConsumer(getService(), this);
		eventConsumer = new GenericConsumer(inQueue, this, getEventExecutor() , mapEventConsumer);

	}

	public void createServer2() throws UnknownHostException, IOException {
		  
		TProtocolFactory protocolFactory = new TBinaryProtocol.Factory();
		TTransportFactory transportFactory = new TFramedTransport.Factory();
		MapGatewayHandler handler = new MapGatewayHandler(this);
		mapjavagw.Processor processor = new mapjavagw.Processor(handler); 

		ServerSocket serverSocket_ = new ServerSocket(getReceiverPort(), 10,
		        InetAddress.getAllByName(null)[0]);
		    // Prevent 2MSL delay problem on server restarts
		serverSocket_.setReuseAddress(true);
		serverTransport = new TServerSocket(serverSocket_, 100);
		TThreadPoolServer.Args serverArgs = new TThreadPoolServer.Args(serverTransport);
		serverArgs.processor(processor).transportFactory(transportFactory)
		      .protocolFactory(protocolFactory);
		server = new TThreadPoolServer(serverArgs);   
		
		mapEventConsumer = new MapEventConsumer(getService(), this);
		eventConsumer = new GenericConsumer(inQueue, this, getEventExecutor() , mapEventConsumer);

		}

}
