package com.movirtu.mxcloudservice.connector.ami;

public class AmiMessage {

	private String callingParty;
	private String calledParty;
	private String messageContent;

	public AmiMessage(String callingParty, String calledParty, String messageContent) {
		this.callingParty=callingParty;
		this.calledParty=calledParty;
		this.messageContent=messageContent;	
	}

	public String getCallingParty() {
		return callingParty;
	}
	public void setCallingParty(String callingParty) {
		this.callingParty = callingParty;
	}
	public String getCalledParty() {
		return calledParty;
	}
	public void setCalledParty(String calledParty) {
		this.calledParty = calledParty;
	}
	public String getMessageContent() {
		return messageContent;
	}
	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("AmiMessage- callingParty=");
		sb.append(callingParty);
		sb.append(" calledParty=");
		sb.append(calledParty);
		sb.append(" messageContent=");
		sb.append(messageContent);
		return sb.toString();
	}
}
