package com.movirtu.mxcloudservice;


import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.movirtu.mxcloudservice.ApplicationState.State;
import com.movirtu.mxcloudservice.connector.ConnectorManager;


public class Application {
	
	private static Logger logger = Logger.getLogger(Application.class);
	private static ApplicationContext context;
	
	public static ApplicationContext getContext() {
		return context;
	}
	
	private static String xmlPath = "";
	private static String xmlConfigFile = "movirtu.xml";
	
	public static void main( String[] args )
    {
		logger.info("Application started");
		if(args.length>0 && args[0]!=null) {
			//logger.info("Load XML path = "+args[0]);
			//setXmlPath(args[0]);
			if(args.length>1 && args[1]!=null && args[1].compareTo("local")==0)
				xmlConfigFile+=".local";
			setXmlPath(args[0]);
			System.out.println("Reading configuration from "+getXmlPath()+"/"+xmlConfigFile);
			context = new FileSystemXmlApplicationContext(getXmlPath()+"/"+getXmlConfigFile());
		} else 
			context = new ClassPathXmlApplicationContext(getXmlPath()+"/"+getXmlConfigFile());
		
		ApplicationState.getInstance().changeState(State.RUN);
		
		logger.info("starting connector manager");
		ConnectorManager connectorManager = (ConnectorManager) context.getBean("connectorManager");
		connectorManager.startMonitor();
		//Spring3HelloWorld bean = (Spring3HelloWorld) context.getBean("firstBean");
        //bean.sayHello();
    }

	public static String getXmlPath() {
		return xmlPath;
	}

	public static void setXmlPath(String xmlPath) {
		Application.xmlPath = xmlPath;
	}

	public static String getXmlConfigFile() {
		return xmlConfigFile;
	}

	public static void setXmlConfigFile(String xmlConfigFile) {
		Application.xmlConfigFile = xmlConfigFile;
	}
	
}
