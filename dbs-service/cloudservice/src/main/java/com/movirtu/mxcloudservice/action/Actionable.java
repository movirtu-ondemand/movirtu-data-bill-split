package com.movirtu.mxcloudservice.action;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.service.BaseSession;
import com.movirtu.mxcloudservice.service.Session;

public interface Actionable {
	public boolean perform(BaseSession session) throws Exception, RestException;
	public void onError(BaseSession session);
	public boolean proceedEvenWithFailure();
	public String getName();
}
