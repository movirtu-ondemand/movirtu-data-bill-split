package com.movirtu.mxcloudservice.service;

import java.util.concurrent.Future;

import com.movirtu.mxcloudservice.dao.UserDetails;
import com.movirtu.mxcloudservice.service.machine.MachineState;

public class Session extends BaseSession{
	private String channelId;
	private String cgpn;
	private String cdpn;
	private String callingPartySipUser;
	private String calledPartySipUser;
	private String callerId;

	private String context;
	private long priority;


	private String destinationImsi;
	private String srcImsi;
	private int serviceId;
	private Future<UserDetails> cgpnProfileFuture;
	private Future<UserDetails> cdpnProfileFuture;
	private UserDetails cgpnProfile=null, cdpnProfile=null;
	private String origCgpn, origCdpn;
	
	public Session() {
		channelId = "";
		cgpn = "";
		cdpn = "";
		callerId = "Unknown";
		super.setState(MachineState.NULL);
	}

	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	public String getCgpn() {
		return cgpn;
	}
	public void setCgpn(String cgpn) {
		this.cgpn = cgpn;
	}
	public String getCdpn() {
		return cdpn;
	}
	public void setCdpn(String cdpn) {
		this.cdpn = cdpn;
	}
	public String getCallerId() {
		return callerId;
	}
	public void setCallerId(String callerId) {
		this.callerId = callerId;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public long getPriority() {
		return priority;
	}

	public void setPriority(long priority) {
		this.priority = priority;
	}


	public String getCallingPartySipUser() {
		System.out.println("Inside getCallingPartySipUser "+callingPartySipUser);
		return callingPartySipUser;
	}

	public void setCallingPartySipUser(String callingPartySipUser) {
		System.out.println("Inside setCallingPartySipUser "+callingPartySipUser);
		this.callingPartySipUser = callingPartySipUser;
	}

	public String getCalledPartySipUser() {
		System.out.println("Inside getCalledPartySipUser "+calledPartySipUser);
		return calledPartySipUser;
	}

	public void setCalledPartySipUser(String calledPartySipUser) {
		System.out.println("Inside setCalledPartySipUser "+calledPartySipUser);
		this.calledPartySipUser = calledPartySipUser;
	}

	public String getDestinationImsi() {
		return destinationImsi;
	}

	public void setDestinationImsi(String destinationImsi) {
		this.destinationImsi = destinationImsi;
	}

	public String getSrcImsi() {
		return srcImsi;
	}

	public void setSrcImsi(String srcImsi) {
		this.srcImsi = srcImsi;
	}

	public int getServiceId() {
		return serviceId;
	}

	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	public Future<UserDetails> getCgpnProfileFuture() {
		return cgpnProfileFuture;
	}

	public void setCgpnProfileFuture(Future<UserDetails> cgpnProfileFuture) {
		this.cgpnProfileFuture = cgpnProfileFuture;
	}

	public Future<UserDetails> getCdpnProfileFuture() {
		return cdpnProfileFuture;
	}

	public void setCdpnProfileFuture(Future<UserDetails> cdpnProfileFuture) {
		this.cdpnProfileFuture = cdpnProfileFuture;
	}

	public UserDetails getCgpnProfile() {
		return cgpnProfile;
	}

	public void setCgpnProfile(UserDetails cgpnProfile) {
		this.cgpnProfile = cgpnProfile;
	}

	public UserDetails getCdpnProfile() {
		return cdpnProfile;
	}

	public void setCdpnProfile(UserDetails cdpnProfile) {
		this.cdpnProfile = cdpnProfile;
	}

	public String getOrigCdpn() {
		return origCdpn;
	}

	public void setOrigCdpn(String origCdpn) {
		this.origCdpn = origCdpn;
	}

	public String getOrigCgpn() {
		return origCgpn;
	}

	public void setOrigCgpn(String origCgpn) {
		this.origCgpn = origCgpn;
	}

}
