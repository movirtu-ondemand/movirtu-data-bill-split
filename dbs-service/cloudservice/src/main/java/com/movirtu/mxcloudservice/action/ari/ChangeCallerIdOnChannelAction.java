package com.movirtu.mxcloudservice.action.ari;

import org.apache.log4j.Logger;

import ch.loway.oss.ari4java.tools.AriCallback;
import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.service.Session;

public class ChangeCallerIdOnChannelAction extends BaseAriAction{

	private Logger logger = Logger.getLogger(ChangeCallerIdOnChannelAction.class);
	public boolean perform(final Session session) throws Exception, RestException {
		// TODO Auto-generated method stub
		logger.info("channel="+session.getChannelId()+",callerId="+session.getCallerId());
		getAriConnector().getARI().channels().setChannelVar(session.getChannelId(), 
														"CALLERID(num)", session.getCallerId(), 
														new AriCallback<Void>() {

            public void onSuccess(Void result) {
                    // TODO Auto-generated method stub
            	logger.info("callerId changed successfully ");
            }

            public void onFailure(RestException e) {
                    // TODO Auto-generated method stub
                    e.printStackTrace();
                    logger.warn(e);
                    onError(session);
            }
            
    	});
		return true;
	}

}
