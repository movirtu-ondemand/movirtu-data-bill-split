package com.movirtu.mxcloudservice.connector;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.thrift.TException;

import com.movirtu.mxcloudservice.connector.thrift.BaseThriftConnector;

public class GenericConsumer implements Runnable{
	
	Consummable consummable;
	BlockingQueue queue;
	Connector connector;
	ExecutorService executor;
	Logger logger = Logger.getLogger(GenericConsumer.class);
	
	public static class EndCommand {
		
	}
	
	public class Worker implements Runnable {

		Object obj;
		
		public Worker(Object obj) {
			this.obj = obj;
		}
		
		public void run() {
			// TODO Auto-generated method stub
			try {
				consummable.onEvent(obj);
			} catch (TException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				connector.onConnectorException(e, obj);
			}
		}		
	}
	
	public GenericConsumer(BlockingQueue queue, Connector connector, ExecutorService executor, Consummable consummable) {
		this.consummable  = consummable;
		this.queue = queue;
		this.connector = connector;
		this.executor = executor;
		logger.info("Generic consumer created queue="+queue
				+",connector="+connector
				+"consummable="+consummable);
	}
	
	public void setConsumable(Consummable consummable) {
		this.consummable = consummable;
	}
	
	public void run() {
			Object obj=null;
			try {
				while(connector.isRunning()) {
					//logger.info("consummable="+consummable+" waiting for further events");
					obj = queue.poll(100000, TimeUnit.MILLISECONDS);
					if(obj!=null) {
						if(obj instanceof EndCommand) {
							logger.info("consummable="+consummable+" queue has been terminated");
							throw new Exception("Queue has been terminated.");
						} else {
							//logger.info("consummable="+consummable+" dequeued object="+obj);
							executor.submit(new Worker(obj));
						}
					}
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				connector.onConnectorException(e, obj);
				//e.printStackTrace();
			} catch (Exception e) {
				//connector.onConnectorException(e);
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
}
