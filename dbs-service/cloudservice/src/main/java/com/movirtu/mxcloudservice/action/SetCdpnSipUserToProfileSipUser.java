package com.movirtu.mxcloudservice.action;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.service.Session;

public class SetCdpnSipUserToProfileSipUser extends CallBaseAction{

	@Override
	public boolean perform(Session session) throws Exception, RestException {
		// TODO Auto-generated method stub
			String sipUser = null;
			if(session.getCdpnProfile()!=null) {
				sipUser = session.getCdpnProfile().getSipUser();
			}
			if(sipUser!=null) {
				session.setCalledPartySipUser(sipUser);;
			} else {
				if(proceedEvenWithFailure())
					return true;
				else
					return false;
			}
			return true;
		}
}