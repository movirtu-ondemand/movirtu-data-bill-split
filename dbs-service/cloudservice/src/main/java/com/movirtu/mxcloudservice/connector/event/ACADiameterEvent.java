package com.movirtu.mxcloudservice.connector.event;

import com.movirtu.diameter.charging.apps.common.TransactionInfo;
import com.movirtu.mxcloudservice.service.machine.MachineEvent;

public class ACADiameterEvent extends DiameterEvent{

	public ACADiameterEvent( TransactionInfo info) {
		super("", MachineEvent.ACA, info);
		// TODO Auto-generated constructor stub
	}

}
