package com.movirtu.mxcloudservice.action.diameter;



import org.apache.log4j.Logger;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.service.DiameterSession;

public class SetErrorCode extends BaseDiameterAction{

	private int errorCode;
	private String errorCodeDescription;
	
	Logger logger = Logger.getLogger(SetErrorCode.class);
	
	private void emptyOtherParameters(DiameterSession session) {
		session.getTransactionInfo().serviceContextId=null;
		session.getTransactionInfo().calledStationId=null;
		session.getTransactionInfo().userName=null;
		session.getTransactionInfo().multipleServiceInd=null;
		session.getTransactionInfo().multipleServiceCreditControl=null;
		session.getTransactionInfo().framedIpAddress=null;
		session.getTransactionInfo().imsi=null;
		session.getTransactionInfo().chargingId=null;
		session.getTransactionInfo().pdpType=null;
		session.getTransactionInfo().gprsQosNeg=null;
		session.getTransactionInfo().sgsnAddr=null;
		session.getTransactionInfo().ggsnAddr=null;
		session.getTransactionInfo().imsiMccMnc=null;
		session.getTransactionInfo().ggsnMccMnc=null;
		session.getTransactionInfo().nsapi=null;
		session.getTransactionInfo().selectionMode=null;
		session.getTransactionInfo().chargingCharacteristics=null;
		session.getTransactionInfo().userLocationInformation=null;
		session.getTransactionInfo().rat=null;
		session.getTransactionInfo().contextType=null;
		session.getTransactionInfo().bearerUsage=null;
		session.getTransactionInfo().imeiSv=null;		
	}
	
	@Override
	public boolean perform(DiameterSession session) throws Exception,
			RestException {
		// TODO Auto-generated method stub
		emptyOtherParameters(session);
		session.getTransactionInfo().result = getErrorCode();
		logger.debug("Returning errorCode="+getErrorCode()+",desc="+getErrorCodeDescription());
		return true;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorCodeDescription() {
		return errorCodeDescription;
	}

	public void setErrorCodeDescription(String errorCodeDescription) {
		this.errorCodeDescription = errorCodeDescription;
	}

}
