package com.movirtu.mxcloudservice.action.thrift.map;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.action.thrift.BaseThriftAction;
import com.movirtu.mxcloudservice.connector.action.ConnectorAction;
import com.movirtu.mxcloudservice.connector.action.ConnectorActionType;
import com.movirtu.mxcloudservice.service.BaseSession;
import com.movirtu.mxcloudservice.service.Session;
import com.movirtu.mxcloudservice.service.machine.MachineEvent;

public class SendIncomingSmsResponseAction extends BaseThriftAction{

	private boolean success;

	public boolean perform(BaseSession session) throws Exception, RestException {
		if(checkConstraints(session)) {
			ConnectorAction action = new ConnectorAction(success ?ConnectorActionType.INCOMING_SMS_RESPONSE_SUCCESS:ConnectorActionType.INCOMING_SMS_RESPONSE_FAILURE);
			action.setSession(session);
			action.setConnecterSessionId(getConnector().getCache().getConnectorSessionIdFromServiceSessionId(session.getSessionId()));
			getConnector().getActionQueue().add(action);
			return true;
		}
		return false;
	}

	public boolean isSuccess() {
		return success;
	}



	public void setSuccess(boolean success) {
		this.success = success;
	}

}
