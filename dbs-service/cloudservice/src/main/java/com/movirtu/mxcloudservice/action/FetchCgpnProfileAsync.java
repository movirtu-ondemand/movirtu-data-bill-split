package com.movirtu.mxcloudservice.action;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.dao.manager.UserDetailsManager;
import com.movirtu.mxcloudservice.service.Session;

public class FetchCgpnProfileAsync extends CallBaseAsyncAction{

	private String columnName;
	
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	
	public String getColumnName() {
		return columnName;
	}
	
	@Override
	public boolean perform(Session session) throws Exception, RestException {
		// TODO Auto-generated method stub
		if(columnName==null) {
			logger.error("columnName is null.");
			return proceedEvenWithFailure();
		}
		session.setCgpnProfileFuture(UserDetailsManager.getInstance().fetchUserDetailsAsync(columnName, 
																					  session.getCgpn(), 
																					  session, 
																					  getSuccessEvent(), 
																					  getFailureEvent()));
		return true;
	}

}
