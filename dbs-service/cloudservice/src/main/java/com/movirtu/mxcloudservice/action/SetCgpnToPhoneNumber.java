package com.movirtu.mxcloudservice.action;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.service.Session;

public class SetCgpnToPhoneNumber extends CallBaseAction{

	@Override
	public boolean perform(Session session) throws Exception, RestException {
		// TODO Auto-generated method stub
			String phoneNumber = null;
			if(session.getCgpnProfile()!=null) {
					phoneNumber = session.getCgpnProfile().getPhoneNumber();
			}
			if(phoneNumber!=null) {
				session.setCgpn(phoneNumber);
			} else {
				if(proceedEvenWithFailure())
					return true;
				else
					return false;
			}
			return true;
		}
}

