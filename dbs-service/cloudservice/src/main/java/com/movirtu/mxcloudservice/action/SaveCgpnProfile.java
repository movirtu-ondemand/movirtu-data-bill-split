package com.movirtu.mxcloudservice.action;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.service.Session;

public class SaveCgpnProfile extends CallBaseAction{

	@Override
	public boolean perform(Session session) throws Exception, RestException {
		// TODO Auto-generated method stub
		session.setCgpnProfile(session.getCgpnProfileFuture().get());
		return true;
	}

}
