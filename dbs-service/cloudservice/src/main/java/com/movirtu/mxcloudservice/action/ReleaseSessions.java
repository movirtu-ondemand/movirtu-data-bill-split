package com.movirtu.mxcloudservice.action;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.service.BaseSession;
import com.movirtu.mxcloudservice.service.Session;
import com.movirtu.mxcloudservice.service.SessionManager;

public class ReleaseSessions extends BaseAction{

	private String connectorNames;
	private String connectorNameList[];
	public boolean perform(BaseSession session) throws Exception, RestException {
		// TODO Auto-generated method stub
		for(int i=0;i<connectorNameList.length;i++)
			SessionManager.getInstance().releaseSessionsStartingWith(connectorNameList[i]);
		return true;
	}
	public String getConnectorNames() {
		return connectorNames;
	}
	public void setConnectorNames(String connectorNames) {
		this.connectorNames = connectorNames;
		connectorNameList = connectorNames.split(";");
	}

}
