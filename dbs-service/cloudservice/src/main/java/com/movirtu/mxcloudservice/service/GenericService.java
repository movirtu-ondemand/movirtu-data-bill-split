package com.movirtu.mxcloudservice.service;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.action.Actionable;
import com.movirtu.mxcloudservice.connector.Connector;
import com.movirtu.mxcloudservice.connector.Connector.State;
import com.movirtu.mxcloudservice.connector.ConnectorStateListener;
import com.movirtu.mxcloudservice.service.machine.MachineEvent;
import com.movirtu.mxcloudservice.service.machine.MachineState;
import com.movirtu.mxcloudservice.service.machine.StateMachine;
import com.movirtu.mxcloudservice.service.machine.StateMachineInTuple;
import com.movirtu.mxcloudservice.service.machine.StateMachineOutTuple;
import com.movirtu.mxcloudservice.service.machine.factory.StateMachineInTupleFactory;
import com.movirtu.mxcloudservice.util.AnyOne;
import com.movirtu.mxcloudservice.util.ExpireHandler;

public class GenericService implements Service,ConnectorStateListener,ExpireHandler {
	private StateMachine stateMachine;
	private Logger logger = Logger.getLogger(GenericService.class);
	private ExecutorService executor = null;
	private int threadPoolSize;
	private String dependencyConnectors;
	private AnyOne state = null;
	private String name;

	private class Worker implements Runnable {

		private BaseSession session;
		private MachineEvent event;
		public Worker(BaseSession session, MachineEvent event) {
			this.session = session;
			this.event = event;
		}
		public void run() {
			
			if(session==null)
				return;
			
			StateMachineInTuple in = StateMachineInTupleFactory.getInstance().getStateMachineInTuple(session.getState(), event);
			
			if(in==null)
				return;
			
			StateMachineOutTuple out = getStateMachine().execute(in);
			logger.info((new Date())+":StateMachine Transition ===> SessionId=["+session.getSessionId()+"]:["+session.getState()+","+event+"]=["+out.getState()+"]");
			
			synchronized(session) {
				session.setState(out.getState());
				onSessionStateChange(session, in.getState(),out.getState());
				//SessionManager.getInstance().putTimeout(session.getSessionId(), stateMachine.getStateTimeout(session.getState()), stateMachine.getStateTimeoutEvent(session.getState()), this);
				//stateMachine.g
				boolean ret = true;
				for(int i=0;out.getActionsList()!=null && i<out.getActionsList().size() && ret;i++) {
					Actionable actionable = out.getActionsList().get(i);
					try {
						logger.info("Action started "+actionable.getName() +" "+actionable);
						ret = actionable.perform(session);
						logger.info("Action finished "+actionable.getName()+" result = "+ret);
					} catch (RestException e) {
						// TODO Auto-generated catch block
						logger.error(e.getStackTrace());
						//e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						logger.error(e.getStackTrace());;
					}
				}
			}

		}
		
	}
	
	public void setThreadPoolSize(int threadPoolSize) {
		this.threadPoolSize = threadPoolSize;
		if(executor==null) {
			executor = Executors.newFixedThreadPool(threadPoolSize);
		} else {
			executor.shutdownNow();
			executor = Executors.newFixedThreadPool(threadPoolSize);
		}
	}
	
	public int getThreadPoolSize() {
		return threadPoolSize;
	}
	
	public StateMachine getStateMachine() {
		return stateMachine;
	}

	public void setStateMachine(StateMachine stateMachine) {
		this.stateMachine = stateMachine;
	}
	
	public void execute(BaseSession session, MachineEvent event) {
		
		if(event!=MachineEvent.INIT && event!=MachineEvent.DESTROY && event!=MachineEvent.INIT_ACK) {
			if(!isActive()) {
				logger.error(getName()+" service is not active");
				return;
			}
		}
		
		Runnable runnable = new Worker(session, event);
		if(executor!=null)
			executor.execute(runnable);
		else
			runnable.run();
	}

	public void init() {
		// TODO Auto-generated method stub
		stateMachine.init();
	}


	public void onConnectorStateChange(Connector connector, State fromState,
			State toState) {
		if(state!=null) {
			if(toState!=Connector.State.RUNNING)
				state.setDown(connector.getName());
			else
				state.setUp(connector.getName());
			//logger.info("service active = "+state.isActive());
		}
	}

	public String getDependencyConnectors() {
		return dependencyConnectors;
	}

	public void setDependencyConnectors(String dependencyConnectors) throws Exception {
		this.dependencyConnectors = dependencyConnectors;
		state = new AnyOne(dependencyConnectors.split(";"));
	}

	public boolean isActive() {
		return state==null || (state!=null && state.isActive());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void onSessionStateChange(BaseSession session, MachineState prevState, MachineState nextState) {
		long timeout = stateMachine.getStateTimeout(nextState);
		if(timeout>0) {
			SessionManager.getInstance().putTimeout(session.getSessionId(), 
														timeout, 
														stateMachine.getStateTimeoutEvent(nextState), 
														this);		
		} else {
			timeout = stateMachine.getStateTimeout(prevState);
			if(timeout>0)
				SessionManager.getInstance().removeTimeout(session.getSessionId());
		}
	}
	

	public void onExpire(String id,Object obj) {
		// TODO Auto-generated method stub
		if(obj instanceof MachineEvent) {
			MachineEvent event = (MachineEvent) obj;
			BaseSession session = SessionManager.getInstance().getSession(id);
			logger.info("Session state timeout for sessionid="+id+",trigger event="+event);
			if(session!=null)
				execute(session, event);
		}
	}
}
