package com.movirtu.mxcloudservice.service;

import java.util.concurrent.Future;

import org.jdiameter.api.Request;
import org.jdiameter.api.cca.ServerCCASession;

import com.movirtu.diameter.charging.apps.common.TransactionInfo;
import com.movirtu.mxcloudservice.dao.DataUserProfile;

public class DiameterSession extends BaseSession implements ProfileAble{
	private TransactionInfo transactionInfo;
	private String subscriberId;
	private int subscriberIdType;
	private String apn;
	private DataUserProfile profile;
	private Future<DataUserProfile> futureProfile;
	private Request ccrRequest;
	private ServerCCASession serverCCASession;
	
	// 1 : physicalProfile
	// 2 : virtualProfile
	// anything else : not decided/don't care
	private int typeOfProfile;
	
	public void setUnknownProfile() {
		typeOfProfile=0;
	}
	
	public boolean isVirtualProfile() {
		return typeOfProfile==2;
	}
	
	public boolean isPhysicalProfile() {
		return typeOfProfile==1;
	}
	
	public boolean isImsiBased() {
		return subscriberIdType==1;
	}
	
	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}
	
	public String getSubscriberId() {
		return subscriberId;
	}
	
	public void setApn(String apn) {
		this.apn = apn;
	}
	
	public String getApn() {
		return apn;
	}
	
	public void setFutureProfile(Future<DataUserProfile> futureProfile) {
		this.futureProfile = futureProfile;
	}
	
	public Future<DataUserProfile> getFutureProfile() {
		return futureProfile;
	}
	
	public DataUserProfile getProfile() {
		return profile;
	}
	
	public void setProfile(DataUserProfile profile) {
		this.profile = profile;
	}

	public int getSubscriberIdType() {
		return subscriberIdType;
	}

	public void setSubscriberIdType(int subscriberIdType) {
		this.subscriberIdType = subscriberIdType;
	}

	public TransactionInfo getTransactionInfo() {
		return transactionInfo;
	}

	public void setTransactionInfo(TransactionInfo transactionInfo) {
		this.transactionInfo = transactionInfo;//transactionInfo.ser
	}

	public Request getCcrRequest() {
		return ccrRequest;
	}

	public void setCcrRequest(Request ccrRequest) {
		this.ccrRequest = ccrRequest;
	}

	public ServerCCASession getServerCCASession() {
		return serverCCASession;
	}

	public void setServerCCASession(ServerCCASession serverCCASession) {
		this.serverCCASession = serverCCASession;
	}

	public void setVirtualProfile() {
		// TODO Auto-generated method stub
		typeOfProfile=2;
	}

	public void setPhysicalProfile() {
		// TODO Auto-generated method stub
		typeOfProfile=1;
	}

}
