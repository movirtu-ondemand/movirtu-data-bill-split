package com.movirtu.mxcloudservice.connector.diameter.rf.cdf;

import com.movirtu.diameter.charging.apps.B2BGw.client.B2BGwSendFromServiceInvocationWrapperForRequest;
import com.movirtu.mxcloudservice.connector.Consummable;
import com.movirtu.mxcloudservice.connector.action.ConnectorActionType;
import com.movirtu.mxcloudservice.connector.action.DiameterConnectorAction;

public class CDFConnectorActionConsumer implements Consummable{

	private B2BGwSendFromServiceInvocationWrapperForRequest client;
	
	public CDFConnectorActionConsumer(B2BGwSendFromServiceInvocationWrapperForRequest client) {
		this.client = client;
	}
	
	public void onEvent(Object obj)  {
		// TODO Auto-generated method stub
		if(obj instanceof DiameterConnectorAction) {
			DiameterConnectorAction action = (DiameterConnectorAction) obj;
			if(action.getType()==ConnectorActionType.SEND_ACR) {
				try {
					client.sendACRWrapper(action.getTransactionInfo());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if(action.getType()==ConnectorActionType.SEND_ACA) {
				// do nothing
			}
		}
	}

}
