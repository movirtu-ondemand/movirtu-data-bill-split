package com.movirtu.mxcloudservice.connector.cache;

import java.util.concurrent.ConcurrentHashMap;

public class GenericCache<T1,T2> {
	protected ConcurrentHashMap<T1,T2> oneToTwoMap = new ConcurrentHashMap<T1,T2>();
	protected ConcurrentHashMap<T2,T1> twoToOneMap = new ConcurrentHashMap<T2,T1>();
	
	public void put(T1 t1, T2 t2) {
		oneToTwoMap.put(t1, t2);
		twoToOneMap.put(t2, t1);
	}

	public T1 getSession1From2(T2 t2) {
		return twoToOneMap.get(t2);
	}
	
	public T2 getSession2From1(T1 t1) {
		return oneToTwoMap.get(t1);
	}
	
	public void remove(T1 t1, T2 t2) {
		oneToTwoMap.remove(t1);
		twoToOneMap.remove(t2);
	}
	
	public void clear() {
		oneToTwoMap.clear();
		twoToOneMap.clear();
	}
	
	public long size() {
		return oneToTwoMap.size()+twoToOneMap.size();
	}
}
