package com.movirtu.mxcloudservice.connector.thrift.map;

import java.sql.Timestamp;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.movirtu.mxcloudservice.connector.Consummable;
import com.movirtu.mxcloudservice.connector.event.Event;
import com.movirtu.mxcloudservice.connector.event.SmsEvent;
import com.movirtu.mxcloudservice.connector.event.SmsResponseEvent;
import com.movirtu.mxcloudservice.connector.thrift.BaseThriftConnector;
import com.movirtu.mxcloudservice.service.Service;
import com.movirtu.mxcloudservice.service.SessionManager;
import com.movirtu.mxcloudservice.service.machine.MachineEvent;
import com.movirtu.mxcloudservice.service.machine.MachineState;
import com.movirtu.mxcloudservice.service.sms.SmsSession;

public class MapEventConsumer implements Consummable{

	Service service;

	BaseThriftConnector connector;
	
	Logger logger = Logger.getLogger(MapEventConsumer.class);
	
	public MapEventConsumer(Service service, BaseThriftConnector connector) {
		this.service = service;
		this.connector = connector;
	}
	
	public void onEvent(Object eventObj) {
		if(eventObj instanceof SmsResponseEvent) {
			SmsResponseEvent event= (SmsResponseEvent) eventObj;
			event.setSessionId(connector.getCache().getServiceSessionIdFromConnectorSessionId(event.getConnectorSessionId()));
			if(event.getSessionId()!=null) {
				SmsSession session = (SmsSession) SessionManager.getInstance().getSession(event.getSessionId());//getOrCreateSession(event.getSessionId(), connector.getSessionFactory(), service);
				session.getService().execute(session, event.getEvent());
			} else {
				logger.info("SessionId for connectorSessionId = "+event.getConnectorSessionId()+" might have been deleted.");
			}
			//event.setSessionId(connector.getCache().generateServiceSessionId(connector.getName(), event.getMapSessionId()));
			
		} else if(eventObj instanceof SmsEvent) {
			SmsEvent event = (SmsEvent) eventObj;
			event.setSessionId(connector.getCache().generateServiceSessionId(connector.getName(), event.getMapSessionId()));
			SmsSession session = (SmsSession) SessionManager.getInstance().getOrCreateSession(event.getSessionId(), connector.getSessionFactory(), service);
			session.setCgpn(event.getOrigPartyAddress());
			session.setMessageContent(event.getDataString());
			session.setCdpn(event.getImsi());
			session.setDestinationImsi(event.getImsi());
			session.setState(MachineState.NULL);
			session.setServiceId(event.getServiceId());
			session.setCallingPartySipUser(event.getOrigPartyAddress());
			session.setCgpn(event.getOrigPartyAddress());
			session.setOrigCgpn(session.getCgpn());
			session.setLogTime(event.getTimeStamp());
			session.setSqlTimestamp(new Timestamp(System.currentTimeMillis()));
			session.setMessageId(UUID.randomUUID().toString().replaceAll("-", ""));
			service.execute(session, MachineEvent.INCOMING_SMS);
		} else if (eventObj instanceof Event) {
			Event event = (Event) eventObj;
			if(event.getEvent()==MachineEvent.INIT) {
				SmsSession session = (SmsSession) SessionManager.getInstance().getOrCreateSession(event.getSessionId(), connector.getSessionFactory(), service);
				service.init();
				service.execute(session, event.getEvent());
			} else if(event.getEvent()==MachineEvent.DESTROY) {
				SmsSession session = (SmsSession) SessionManager.getInstance().getSession(event.getSessionId());
				service.execute(session, event.getEvent());
			} else if(event.getEvent()==MachineEvent.REINIT) {
				SmsSession session = (SmsSession) SessionManager.getInstance().getSession(event.getSessionId());
				service.execute(session, event.getEvent());
			} else if(event.getEvent()==MachineEvent.INIT_ACK) {
				SmsSession session = (SmsSession) SessionManager.getInstance().getSession(event.getSessionId());
				service.execute(session, event.getEvent());
			}
		}
	}
}
