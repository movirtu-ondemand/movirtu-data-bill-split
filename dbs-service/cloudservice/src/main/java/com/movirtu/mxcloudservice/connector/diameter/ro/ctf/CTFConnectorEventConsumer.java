package com.movirtu.mxcloudservice.connector.diameter.ro.ctf;

import org.apache.log4j.Logger;
import org.apache.thrift.TException;

import com.movirtu.mxcloudservice.connector.Consummable;
import com.movirtu.mxcloudservice.connector.event.DiameterEvent;
import com.movirtu.mxcloudservice.service.DiameterSession;
import com.movirtu.mxcloudservice.service.SessionManager;
import com.movirtu.mxcloudservice.service.machine.MachineEvent;

public class CTFConnectorEventConsumer implements Consummable {

	private CTFConnector connector;
	private Logger logger = Logger.getLogger(CTFConnectorEventConsumer.class);
	
	public CTFConnectorEventConsumer(CTFConnector ctfConnector) {
		// TODO Auto-generated constructor stub
		this.connector = ctfConnector;
	}

	public void onEvent(Object obj) throws TException {
		// TODO Auto-generated method stub
		logger.info("Received an event "+obj);
		if(obj instanceof DiameterEvent) {
			DiameterEvent event = (DiameterEvent) obj;
			
			if(event.getEvent()==MachineEvent.CCR_INIT)
				handleCCR_Init(event);
			else if(event.getEvent()==MachineEvent.CCR_INTERIM)
				handleCCR_Interim(event);
			else if(event.getEvent()==MachineEvent.CCR_TERM)
				handleCCR_Term(event);		
		}
	}

	private void handleCCR_Term(DiameterEvent event) {
		// TODO Auto-generated method stub
		DiameterSession session = (DiameterSession) SessionManager.getInstance().getSession(connector.createSessionId(event.getInfo().calledStationId,event.getInfo().key));
		if(session!=null) {
			updateSession(session, event);
			connector.getService().execute(session, event.getEvent());	
		} else {
			logger.info("Diameter session for apn="+event.getInfo().calledStationId+",key="+event.getInfo().key+" does not exist");
		}		
	}

	private void handleCCR_Interim(DiameterEvent event) {
		// TODO Auto-generated method stub
		DiameterSession session = (DiameterSession) SessionManager.getInstance().getSession(connector.createSessionId(event.getInfo().calledStationId,event.getInfo().key));
		if(session!=null) {
			updateSession(session, event);
			connector.getService().execute(session, event.getEvent());	
		} else {
			logger.info("Diameter session for apn="+event.getInfo().calledStationId+",key="+event.getInfo().key+" does not exist");
		}
	}

	private void handleCCR_Init(DiameterEvent event) {
		// TODO Auto-generated method stub
		DiameterSession session = (DiameterSession) SessionManager.getInstance().getOrCreateSession(connector.createSessionId(event.getInfo().calledStationId,event.getInfo().key), connector.getSessionFactory(), connector.getService());
		logger.info("DiameterSession is created with sessionId="+session.getSessionId());
		updateSession(session, event);
		connector.getService().execute(session, event.getEvent());
	}
	
	private void updateSession(DiameterSession session, DiameterEvent event) {
		if(event.getInfo().request!=null) session.setCcrRequest(event.getInfo().request);
		if(event.getInfo().serverCCASession!=null) session.setServerCCASession(event.getInfo().serverCCASession);
		session.setApn(event.getInfo().calledStationId);
		session.setSubscriberId(event.getInfo().subscriptionIdDataValue);
		session.setSubscriberIdType(event.getInfo().subscriptionIdTypeValue);
		session.setTransactionInfo(event.getInfo());
	}
}
