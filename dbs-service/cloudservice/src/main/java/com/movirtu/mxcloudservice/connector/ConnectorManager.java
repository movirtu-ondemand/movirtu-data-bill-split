package com.movirtu.mxcloudservice.connector;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.apache.log4j.Logger;

import com.movirtu.mxcloudservice.Application;
import com.movirtu.mxcloudservice.ApplicationState;
import com.movirtu.mxcloudservice.service.SessionManager;

public class ConnectorManager {
	
	private int pingDuration;
	
	private List<Connector> connectorList=null;
	private String connectors;
	
	private List<ConnectorStateListener> connectorStateListeners=null;
	private String stateListeners;
	
	private Logger logger = Logger.getLogger(ConnectorManager.class);
	public int getPingDuration() {
		return pingDuration;
	}

	public void setPingDuration(int pingDuration) {
		this.pingDuration = pingDuration;
	}

	public List<Connector> getConnectorList() {
		return connectorList;
	}

	public void setConnectorList(List<Connector> connectorList) {
		this.connectorList = connectorList;
	}
	
	public void init() {
		if(connectorList==null && connectors!=null && !connectors.isEmpty()) {
			String[] list = connectors.split(";");
			if(list.length>0) {
				connectorList = new ArrayList<Connector>();
				for(int i=0;i<list.length;i++) {
					connectorList.add((Connector) Application.getContext().getBean(list[i]));
				}
			}
		}
		if(connectorStateListeners==null && stateListeners!=null && !stateListeners.isEmpty()){
			String[] list = stateListeners.split(";");
			if(list.length>0) {
				connectorStateListeners = new ArrayList<ConnectorStateListener>();
				for(int i=0;i<list.length;i++) {
					connectorStateListeners.add((ConnectorStateListener) Application.getContext().getBean(list[i]));
				}
			}
		}
	}
	
	public void startMonitor() {
		
		init();
		
        while(ApplicationState.getInstance().isRunning()) {
        	try {
        		monitorConnectors();
        		logger.info("ApplicationState = "+ApplicationState.getInstance().getApplicationState()+", sessions="+SessionManager.getInstance().size());
				Thread.currentThread().sleep(getPingDuration() * 1000);
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				logger.info(e.getMessage());
			}
        }
	}
	
	private void notifyStateListeners(Connector connector) {
		if(connectorStateListeners==null)
			return;
		Iterator<ConnectorStateListener> iterator = connectorStateListeners.iterator();
		while(iterator.hasNext()) {
			ConnectorStateListener listener = (ConnectorStateListener)iterator.next();
			listener.onConnectorStateChange(connector, Connector.State.NULL, connector.getState());
		}
	}
	
	private void monitorConnectors() {

        ListIterator<Connector> listIterator = (ListIterator<Connector>) connectorList.listIterator();
        
		while(listIterator.hasNext()){
        	Connector connector = listIterator.next();
        	notifyStateListeners(connector);
        	if(!connector.isRunning()) {
        		logger.info(connector.getName() + " Connector is not running");
        		connector.start();
        		logger.info(connector.getName()+ " Connector is started");
        	} else {
        		logger.info("Connector = "+connector.getName()+","+connector.getState());
        		connector.ping();
        	}
        }
	}

	public String getConnectors() {
		return connectors;
	}

	public void setConnectors(String connectors) {
		this.connectors = connectors;
	}

	public String getStateListeners() {
		return stateListeners;
	}

	public void setStateListeners(String stateListeners) {
		this.stateListeners = stateListeners;
	}

	public List<ConnectorStateListener> getConnectorStateListeners() {
		return connectorStateListeners;
	}

	public void setConnectorStateListeners(List<ConnectorStateListener> connectorStateListeners) {
		this.connectorStateListeners = connectorStateListeners;
	}
}
