package com.movirtu.mxcloudservice.action.sms;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;

import org.apache.log4j.Logger;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.action.ari.BaseAriAction;
import com.movirtu.mxcloudservice.service.Session;
import com.movirtu.mxcloudservice.service.sms.SmsSession;

public class ConvertToSqlTimestampAction extends BaseAriAction {

	private Logger logger = Logger.getLogger(ParseAndCreateOutgoingSmsAction.class);

	public boolean perform(Session session) throws Exception, RestException {
		System.out.println("ClearSmsChannelAction : Hello sms state machine");
		SmsSession smsSession = (SmsSession)session;
		convertToSqlTimestamp(smsSession.getLogTime());
		return true;
	}

	public Timestamp convertToSqlTimestamp(String timestamp) {
		Timestamp sqlTimestamp=null;
		try {
			sqlTimestamp = new Timestamp(convertToLong(timestamp));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return sqlTimestamp;
	}

	public long convertToLong(final String iso8601string) throws ParseException {
		//DateTime dt = new DateTime();
		DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
		Date date = new Date(fmt.parseMillis(iso8601string));
		return fmt.parseMillis(iso8601string);
	}
}
