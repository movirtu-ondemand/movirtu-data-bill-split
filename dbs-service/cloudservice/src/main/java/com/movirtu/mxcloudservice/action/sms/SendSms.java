package com.movirtu.mxcloudservice.action.sms;

import org.apache.log4j.Logger;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.action.ari.BaseAriAction;
import com.movirtu.mxcloudservice.action.sms.handler.SmsSenderManager;
import com.movirtu.mxcloudservice.service.Session;
import com.movirtu.mxcloudservice.service.sms.SmsSession;

public class SendSms extends BaseAriAction {

	private Logger logger = Logger.getLogger(SendSms.class);

	public boolean perform(Session session) throws Exception, RestException {
		System.out.println("SendSms: Hello sms state machine");		
		SmsSenderManager.getInstance().sendSms((SmsSession) session);
		return true;
	}

}
