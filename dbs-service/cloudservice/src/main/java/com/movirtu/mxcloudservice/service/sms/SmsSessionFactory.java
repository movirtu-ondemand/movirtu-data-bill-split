package com.movirtu.mxcloudservice.service.sms;

import com.movirtu.mxcloudservice.service.Session;
import com.movirtu.mxcloudservice.service.SessionFactory;

public class SmsSessionFactory implements SessionFactory {

	public Session createSession() {
		return new SmsSession();
	}

}
