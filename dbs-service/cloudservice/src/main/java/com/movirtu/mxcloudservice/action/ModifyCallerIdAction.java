package com.movirtu.mxcloudservice.action;

import org.apache.log4j.Logger;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.service.Session;

public class ModifyCallerIdAction extends CallBaseAction{

	private String prefixToAdd;
	private String prefixToRemove;
	private String suffixToAdd;
	private String suffixToRemove;
	private boolean addSuffix;
	private boolean addPrefix;
	private boolean removeSuffix;
	private boolean removePrefix;
	
	Logger logger = Logger.getLogger(ModifyCdpnAction.class);
	public boolean perform(Session session) throws Exception, RestException {
		// TODO Auto-generated method stub
		String str = session.getCallerId();
		if(str == null || str.isEmpty()) {
			logger.info("CallerId is empty");
			return true;
		}
		logger.info("callerId = "+str);
		
		if(removeSuffix) {
			if(str.endsWith(suffixToRemove)) {
				int i = str.lastIndexOf(suffixToRemove);
				str = str.substring(0, i);
				logger.info("Removing suffix ("+suffixToRemove+"), new callerId = "+str);
			}
		}
		
		if(addSuffix) {
			str += suffixToAdd;
			logger.info("Adding suffix ("+suffixToAdd+"), new callerId = "+str);
		}
		
		if(removePrefix) {
			if(str.startsWith(prefixToRemove)) {
				int i = prefixToRemove.length();//str.lastIndexOf(prefixToRemove);
				//str = str.substring(0, i);
				str = str.substring(i);
				logger.info("Removing prefix ("+prefixToRemove+"), new callerId = "+str);
			}
		}
		
		if(addPrefix) {
			str = prefixToAdd + str;
			logger.info("Adding prefix ("+prefixToAdd+"), new calerId = "+str);
		}
		
		session.setCallerId(str);
		logger.info("callerId ="+session.getCallerId());
		return true;
	}

	public String getName() {
		// TODO Auto-generated method stub
		return "ModifyCdpnAction";
	}

	public String getPrefixToAdd() {
		return prefixToAdd;
	}

	public void setPrefixToAdd(String prefixToAdd) {
		this.prefixToAdd = prefixToAdd;
	}

	public String getPrefixToRemove() {
		return prefixToRemove;
	}

	public void setPrefixToRemove(String prefixToRemove) {
		this.prefixToRemove = prefixToRemove;
	}

	public String getSuffixToAdd() {
		return suffixToAdd;
	}

	public void setSuffixToAdd(String suffixToAdd) {
		this.suffixToAdd = suffixToAdd;
	}

	public String getSuffixToRemove() {
		return suffixToRemove;
	}

	public void setSuffixToRemove(String suffixToRemove) {
		this.suffixToRemove = suffixToRemove;
	}

	public boolean isAddSuffix() {
		return addSuffix;
	}

	public void setAddSuffix(boolean addSuffix) {
		this.addSuffix = addSuffix;
	}

	public boolean isAddPrefix() {
		return addPrefix;
	}

	public void setAddPrefix(boolean addPrefix) {
		this.addPrefix = addPrefix;
	}

	public boolean isRemoveSuffix() {
		return removeSuffix;
	}

	public void setRemoveSuffix(boolean removeSuffix) {
		this.removeSuffix = removeSuffix;
	}

	public boolean isRemovePrefix() {
		return removePrefix;
	}

	public void setRemovePrefix(boolean removePrefix) {
		this.removePrefix = removePrefix;
	}

}
