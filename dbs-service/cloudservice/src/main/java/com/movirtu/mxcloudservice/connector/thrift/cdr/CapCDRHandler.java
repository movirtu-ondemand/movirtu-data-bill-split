package com.movirtu.mxcloudservice.connector.thrift.cdr;

import java.util.Map;

import com.movirtu.mxcloudservice.dao.CallDataRecord;

public class CapCDRHandler {
	public static CallDataRecord createCDR(Map<String,String> nvp) {
		CallDataRecord cdr = new CallDataRecord();
		int type=0;
		//cdr.setId(nvp.get("one_id"));
		if(nvp.get("callType").compareToIgnoreCase("originating")==0) {
			cdr.setMsisdn(nvp.get("cgpn"));
			cdr.setOtherParty(nvp.get("cdpn"));
			type=1;
		} else {
			cdr.setMsisdn(nvp.get("cdpn"));
			cdr.setOtherParty(nvp.get("cgpn"));
			type=2;
		}
		cdr.setImei(nvp.get("imei"));
		cdr.setImsi(nvp.get("imsi"));
		cdr.setChargingDuration(nvp.get("duration"));
		cdr.setSequenceNumber(nvp.get("session_id"));
		cdr.setStartOfChargingTime(nvp.get("callAnsweredTime"));
		cdr.setTransactionTime(nvp.get("transactionTime"));
		cdr.setBasicService(nvp.get("service"));
		if(nvp.get("service").compareTo("sms")==0)
			type +=30;
		cdr.setTransactionType(type);
		return cdr;
	}
}
