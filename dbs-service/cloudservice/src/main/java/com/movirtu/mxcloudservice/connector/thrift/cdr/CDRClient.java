package com.movirtu.mxcloudservice.connector.thrift.cdr;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.movirtu.mxcloudservice.connector.thrift.cdr.RemoteCDR.Client;

import org.apache.log4j.Logger;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransportException;

import com.movirtu.mxcloudservice.connector.thrift.map.mapjavainterface;

public class CDRClient {

	//private static String host="192.168.1.36";
	//private static int port=16150;
	private static String host="127.0.0.1";
	private static int port=9999;
	private static CDRClient instance=null;
	TFramedTransport transport;
	TBinaryProtocol protocol;
	private static RemoteCDR.Client client;
	private static ExecutorService executor = Executors.newFixedThreadPool(1);
	
	static Logger logger = Logger.getLogger(CDRClient.class.getName());
	
	static class CDRTask implements Runnable {

		Map<String,String> nvp;
		public CDRTask(Map<String,String> nvp) {
			this.nvp=nvp;
		}
		public void run() {
			// TODO Auto-generated method stub
			_sendCDR(nvp);
		}
		
	}
	
	private CDRClient() throws TTransportException {
		transport = new TFramedTransport(new TSocket(getHost(),getPort()));
		//transport = new TSocket(getSenderIpAddress(),getSenderPort());
		transport.open();
		
		protocol = new TBinaryProtocol(transport);
		client = new RemoteCDR.Client(protocol);
	}
	
	private static void createInstance() {
		if(instance==null) {
			try {
				instance = new CDRClient();
			} catch (TTransportException e) {
				// TODO Auto-generated catch block
				logger.debug(""+e);
				instance = null;
			}
		}
	}
	
	public static String getHost() {
		return host;
	}
	public static void setHost(String host) {
		CDRClient.host = host;
	}
	public static int getPort() {
		return port;
	}
	public static void setPort(int port) {
		CDRClient.port = port;
	}
	
	synchronized public static void sendCDR(Map<String,String> nvp) {
		logger.debug("CDR NVP submitted for execution");
		executor.submit(new CDRTask(nvp));
	}
	
	private static void _sendCDR(Map<String,String> nvp) {
		createInstance();
		if(instance!=null) {
			try {
				client.sendCDR(nvp);
			} catch (TException e) {
				// TODO Auto-generated catch block
				logger.debug(""+e);
				instance=null;
				createInstance();
				if(instance!=null) {
					try {
						client.sendCDR(nvp);
					} catch (TException e1) {
						// TODO Auto-generated catch block
						logger.debug(""+e1);
						instance = null;
					}
				} else {
					printNVP(nvp);
				}
			}
		} else {
			printNVP(nvp);
		}
	}

	private static  void printNVP(Map<String, String> nvp) {
		// TODO Auto-generated method stub
		logger.info("################ NVP Received ##################");
		Iterator it = nvp.entrySet().iterator();
		String str="";
		while(it.hasNext()) {
			Entry<String,String> entry = (Entry<String, String>) it.next();
			str += (entry.getKey()+"="+entry.getValue());
			if(it.hasNext())
				str+=",";
			//logger.info(entry.getKey()+"="+entry.getValue());
		}
		logger.info(str);
	}
}
