package com.movirtu.mxcloudservice.connector.diameter;

import com.movirtu.mxcloudservice.connector.AsyncIOConnector;
import com.movirtu.mxcloudservice.service.DiameterSessionFactory;

public abstract class DiameterConnector extends AsyncIOConnector{

	private String configFileName;
	private DiameterSessionFactory sessionFactory;
	
	public String getConfigFileName() {
		return configFileName;
	}

	public void setConfigFileName(String configFileName) {
		this.configFileName = configFileName;
	}

	@Override
	protected void closeClient() {
		// TODO Auto-generated method stub
		// do nothing
	}

	public String createSessionId(String apn, String tid) {
		return apn+tid;
	}

	public DiameterSessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(DiameterSessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
}
