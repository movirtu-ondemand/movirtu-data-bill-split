package com.movirtu.mxcloudservice.action.diameter;

import java.util.Date;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.service.DiameterSession;

public class PutTimestamp extends BaseDiameterAction{

	private String key;
	
	public String getKey() {
		return key;
	}
	
	public void setKey(String key) {
		this.key = key;
	}
	
	@Override
	public boolean perform(DiameterSession session) throws Exception,
			RestException {
		// TODO Auto-generated method stub
		session.getNvp().put(getKey(), (new Date()).toString());
		return true;
	}

}
