package com.movirtu.mxcloudservice.action.diameter;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.dao.manager.DataUserProfileDaoManager;
import com.movirtu.mxcloudservice.service.DiameterSession;

public class FetchDataUserProfileGeneric extends BaseDiameterActionAsync{

	private boolean fetchActiveProfileEvenForPhysicalAPN;
	
	@Override
	public boolean perform(DiameterSession session) throws Exception,
			RestException {
		// TODO Auto-generated method stub
		String[] columns=null;
		Object[] values=null;
		boolean virtualProfile=false;
		boolean imsiBased=false;
		
		if(session.isPhysicalProfile()) {
			if(this.isFetchActiveProfileEvenForPhysicalAPN()) {
				String[] columns0={session.isImsiBased()?"physicalImsi":"physicalSubscriptionId","active"};
				Object[] values0={session.getSubscriberId(),true};
				columns = columns0;
				values = values0;
				virtualProfile=false;
				imsiBased = session.isImsiBased();
			} else {
				String[] columns0={session.isImsiBased()?"physicalImsi":"physicalSubscriptionId"};
				Object[] values0={session.getSubscriberId()};
				columns = columns0;
				values = values0;
				virtualProfile=false;
				imsiBased = session.isImsiBased();
				/*session.getService().execute(session, super.getSuccessEvent());
				return true;*/
			}
		} else if(session.isVirtualProfile()) {
			
				String[] columns0={session.isImsiBased()?"physicalImsi":"physicalSubscriptionId"};
				Object[] values0={session.getSubscriberId()};
				columns = columns0;
				values = values0;
				virtualProfile=true;
				imsiBased = session.isImsiBased();
		} else {
				String[] columns0={session.isImsiBased()?"physicalImsi":"physicalSubscriptionId","active"};
				Object[] values0={session.getSubscriberId(), true};
				columns = columns0;
				values = values0;
				virtualProfile=false;
				imsiBased = session.isImsiBased();
				
				session.setFutureProfile(DataUserProfileDaoManager.getInstance().getProfileAsync(columns, 
						values,
						session, 
						getSuccessEvent(), 
						getFailureEvent()));
				return true;
		}
		
		session.setFutureProfile(DataUserProfileDaoManager.getInstance().getProfileAsync(columns, 
				values,
				virtualProfile,
				imsiBased,
				session, 
				getSuccessEvent(), 
				getFailureEvent()));
		return true;
	}

	public boolean isFetchActiveProfileEvenForPhysicalAPN() {
		return fetchActiveProfileEvenForPhysicalAPN;
	}

	public void setFetchActiveProfileEvenForPhysicalAPN(
			boolean fetchActiveProfileEvenForPhysicalAPN) {
		this.fetchActiveProfileEvenForPhysicalAPN = fetchActiveProfileEvenForPhysicalAPN;
	}

}
