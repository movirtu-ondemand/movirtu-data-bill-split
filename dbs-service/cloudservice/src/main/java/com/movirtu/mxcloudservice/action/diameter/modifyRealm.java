package com.movirtu.mxcloudservice.action.diameter;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.service.DiameterSession;

public class modifyRealm extends BaseDiameterAction{

	private String realm;
	private String destHost;
	@Override
	public boolean perform(DiameterSession session) throws Exception,
			RestException {
		// TODO Auto-generated method stub
		if(session.getTransactionInfo()!=null) {
			session.getTransactionInfo().destRealmStr = realm;
			session.getTransactionInfo().destHostStr = destHost;
			logger.info("Session transactionInfo destRealmStr="+realm+",destHostStr="+destHost);
		} else {
			logger.error("Cannot modify realm and destHost because transactionInfo object is null");
		}
		return true;
	}
	public String getRealm() {
		return realm;
	}
	public void setRealm(String realm) {
		this.realm = realm;
	}
	public String getDestHost() {
		return destHost;
	}
	public void setDestHost(String destHost) {
		this.destHost = destHost;
	}

}
