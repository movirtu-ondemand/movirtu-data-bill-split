package com.movirtu.mxcloudservice.connector.ari.sms;

import java.sql.Timestamp;
import java.util.StringTokenizer;
import java.util.UUID;

import org.apache.log4j.Logger;

import ch.loway.oss.ari4java.generated.Message;
import ch.loway.oss.ari4java.generated.StasisStart;

import com.movirtu.mxcloudservice.connector.ari.AriConnector;
import com.movirtu.mxcloudservice.service.SessionManager;
import com.movirtu.mxcloudservice.service.machine.MachineEvent;
import com.movirtu.mxcloudservice.service.machine.MachineState;
import com.movirtu.mxcloudservice.service.sms.SmsSession;

public class AriSmsConnector extends AriConnector {

	private Logger logger = Logger.getLogger(AriSmsConnector.class);
	private static volatile int counter =0;

	@Override
	public String generateSessionId(String id) {
		return getName()+id+counter++;
	}

	public void onEvent(Message m) {
		System.out.println("Inside AriSmsConnector :: onEvent");
		
		if(m instanceof StasisStart) {
			System.out.println("Session:: dialplan: "+((StasisStart) m).getChannel());
			System.out.println("Session:: cdSipParty: "+((StasisStart) m).getChannel().getDialplan().getExten());
			System.out.println("Session:: cgpn: "+((StasisStart) m).getChannel().getCaller().getNumber());

			SmsSession session = (SmsSession) SessionManager.getInstance().getOrCreateSession(generateSessionId(((StasisStart) m).getChannel().getId()), getSessionFactory(), service);
			session.setChannelId(((StasisStart) m).getChannel().getId());
			session.setPriority(((StasisStart) m).getChannel().getDialplan().getPriority());
			session.setContext(((StasisStart) m).getChannel().getDialplan().getContext());
			session.setCdpn(((StasisStart) m).getChannel().getDialplan().getExten());
			session.setOrigCdpn(session.getCdpn());
			
			String from = (String) ((StasisStart) m).getArgs().get(0);
			String callerNumber = parseParam("from" , from);
			System.out.println("callerNumber" +callerNumber);
			session.setCgpn(callerNumber);
			session.setOrigCgpn(session.getCgpn());
			
			String content = (String) ((StasisStart) m).getArgs().get(1);
			String messageContent = parseParam("content" , content);
			System.out.println("memberContent" +messageContent);
			session.setMessageContent(messageContent);

			System.out.println("Session:: cdpn: "+session.getCdpn());
			System.out.println("Session:: cgpn: "+session.getCgpn());
			System.out.println("Session:: cgpn: "+session.getMessageContent());

			session.setState(MachineState.NULL);
			
			session.setSqlTimestamp(new Timestamp(System.currentTimeMillis()));
			session.setMessageId(UUID.randomUUID().toString().replaceAll("-", ""));
			getService().execute(session, MachineEvent.SEND_OUTGOING_SMS);
		}
		
		System.out.println("Leaving AriSmsConnector:onEvent ffffffffffffffffff");
	}

	// This tokenizer is directly linked to the asterisk dial plan configuration.
	// Asterisk does not send the caller name and number in json string for SMS. It sends
	//  "caller":{"name":"","number":""} which does not contain the value.
	// So we have to make changes in the asterisk dialplan file to send caller name and number
	// inside the body. Following is the value to be added in the asterisk dialplan file:
	// same => n,Stasis(hello,from\":\"${CUT(MESSAGE(from),<,1)},content\":\"${MESSAGE(body)})
	private static String parseParam(String paramName, String dataStr) {
		String paramValue = null;
		String tokenValue = null;
		StringTokenizer strings = new StringTokenizer(dataStr,":\"");
		while (strings.hasMoreElements()) {
			tokenValue = strings.nextElement().toString().trim();
			if(tokenValue.equals(paramName)) {
				continue;
			}
			paramValue=tokenValue;
		}
		//System.out.println("Returning paramValue="+paramValue +" for paramName="+paramName);
		return paramValue;
	}

}
