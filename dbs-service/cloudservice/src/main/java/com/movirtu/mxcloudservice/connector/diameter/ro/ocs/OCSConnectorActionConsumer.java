package com.movirtu.mxcloudservice.connector.diameter.ro.ocs;

import org.apache.log4j.Logger;
import org.apache.thrift.TException;

import com.movirtu.diameter.charging.apps.B2BGw.client.B2BGwCCRBuilder;
import com.movirtu.mxcloudservice.connector.Consummable;
import com.movirtu.mxcloudservice.connector.action.ConnectorActionType;
import com.movirtu.mxcloudservice.connector.action.DiameterConnectorAction;

public class OCSConnectorActionConsumer implements Consummable{

	private OCSConnector connector;
	private Logger logger = Logger.getLogger(OCSConnectorActionConsumer.class);

	public OCSConnectorActionConsumer(OCSConnector ocsConnector) {
		// TODO Auto-generated constructor stub
		this.connector = ocsConnector;
	}

	private String getOrCreateConnectorSessionId(DiameterConnectorAction action) {
		String connectorSessionId = connector.getCache().getSession2From1(action.getSession().getSessionId());
		if(connectorSessionId==null) {
			logger.info("createSessionIdForSecondLeg called for string="+action.getSession().getSessionId());
			connectorSessionId = connector.client.clientNode.createSessionIdForSecondLeg(action.getSession().getSessionId());
			logger.info("createSessionIdForSecondLeg returned");
			connector.getCache().put(action.getSession().getSessionId(), connectorSessionId);
		}
		logger.info("getOrCreateConnectorSessionId connectorSessionId="+connectorSessionId);
		return connectorSessionId;
	}
	
	public void onEvent(Object obj) throws TException {
		// TODO Auto-generated method stub
		logger.info("DiameterConnectorAction received "+obj);
		if(obj instanceof DiameterConnectorAction) {
			DiameterConnectorAction action = (DiameterConnectorAction) obj;
			int type = action.getTransactionInfo().recordType;
			logger.info("Action type="+action.getType()+",Transaction type = "+type);
			String connectorSessionId = getOrCreateConnectorSessionId(action);
			action.getTransactionInfo().key = connectorSessionId;
			
			
			if(action.getType()==ConnectorActionType.SEND_CCR) {
				try {
					
					if(type==B2BGwCCRBuilder.CC_REQUEST_TYPE_INITIAL) {
						connector.client.sendInitial(action.getTransactionInfo());
					} else if(type==B2BGwCCRBuilder.CC_REQUEST_TYPE_INTERIM) {
						connector.client.sendInterim(action.getTransactionInfo());
					} else if(type==B2BGwCCRBuilder.CC_REQUEST_TYPE_TERMINATE) {
						connector.client.sendTerminate(action.getTransactionInfo());
					}
				} catch(Exception e) {
					e.printStackTrace();
					connector.onConnectorException(e, obj);
				}
			}
		}
	}
}

