package com.movirtu.mxcloudservice.connector.thrift.map.types;

/**
 * Autogenerated by Thrift
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 */

import java.util.Map;
import java.util.HashMap;
import org.apache.thrift.TEnum;

public enum callStateType implements org.apache.thrift.TEnum {
  CallState_connecting(0),
  CallState_connected(1),
  CallState_disconnected(2);

  private final int value;

  private callStateType(int value) {
    this.value = value;
  }

  /**
   * Get the integer value of this enum value, as defined in the Thrift IDL.
   */
  public int getValue() {
    return value;
  }

  /**
   * Find a the enum type by its integer value, as defined in the Thrift IDL.
   * @return null if the value is not found.
   */
  public static callStateType findByValue(int value) { 
    switch (value) {
      case 0:
        return CallState_connecting;
      case 1:
        return CallState_connected;
      case 2:
        return CallState_disconnected;
      default:
        return null;
    }
  }
}
