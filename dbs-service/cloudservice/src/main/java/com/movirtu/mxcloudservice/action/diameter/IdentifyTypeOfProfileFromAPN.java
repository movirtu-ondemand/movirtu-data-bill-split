package com.movirtu.mxcloudservice.action.diameter;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.service.DiameterSession;

public class IdentifyTypeOfProfileFromAPN extends BaseDiameterAction{

	private String virtualApn;
	private String physicalApn;
	private boolean treatUnknownApnAsPhysicalProfile;
	private boolean treatEmptyApnAsPhysicalProfile;
	private boolean compareApnWithIgnoringCase;

	
	@Override
	public boolean perform(DiameterSession session) throws Exception,
			RestException {
		// TODO Auto-generated method stub
		if(session.getApn()==null || session.getApn().isEmpty()) {
			if(this.isTreatEmptyApnAsPhysicalProfile())
				session.setPhysicalProfile();
			else
				session.setUnknownProfile();
		} else {
			if(compareApn(session.getApn(),getVirtualApn())==0)
				session.setVirtualProfile();
			else if(compareApn(session.getApn(),getPhysicalApn())==0)
				session.setPhysicalProfile();
			else if(isTreatUnknownApnAsPhysicalProfile())
				session.setPhysicalProfile();
			else
				session.setUnknownProfile();
		}
		return true;
	}

	private int compareApn(String apn0, String apn1) {
		if(compareApnWithIgnoringCase)
			return apn0.compareToIgnoreCase(apn1);
		else
			return apn0.compareTo(apn1);
	}

	public String getVirtualApn() {
		return virtualApn;
	}


	public void setVirtualApn(String virtualApn) {
		this.virtualApn = virtualApn;
	}


	public String getPhysicalApn() {
		return physicalApn;
	}


	public void setPhysicalApn(String physicalApn) {
		this.physicalApn = physicalApn;
	}


	public boolean isTreatUnknownApnAsPhysicalProfile() {
		return treatUnknownApnAsPhysicalProfile;
	}


	public void setTreatUnknownApnAsPhysicalProfile(
			boolean treatUnknownApnAsPhysicalProfile) {
		this.treatUnknownApnAsPhysicalProfile = treatUnknownApnAsPhysicalProfile;
	}


	public boolean isTreatEmptyApnAsPhysicalProfile() {
		return treatEmptyApnAsPhysicalProfile;
	}


	public void setTreatEmptyApnAsPhysicalProfile(
			boolean treatEmptyApnAsPhysicalProfile) {
		this.treatEmptyApnAsPhysicalProfile = treatEmptyApnAsPhysicalProfile;
	}

	public void setCompareApnWithIgnoringCase(boolean compareApnWithIgnoringCase) {
		this.compareApnWithIgnoringCase = compareApnWithIgnoringCase;
	}
}
