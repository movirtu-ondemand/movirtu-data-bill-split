package com.movirtu.mxcloudservice.action;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.service.CallSession;
import com.movirtu.mxcloudservice.service.Session;

public class SetMsrnToDestinationNumber extends CallBaseAction{

	@Override
	public boolean perform(Session session) throws Exception, RestException {
		// TODO Auto-generated method stub
		if(session instanceof CallSession) {
			((CallSession) session).setMsrn(session.getCdpn());
		}
		return true;
	}

}
