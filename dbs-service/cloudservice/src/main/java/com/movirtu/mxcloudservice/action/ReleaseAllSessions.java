package com.movirtu.mxcloudservice.action;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.service.BaseSession;
import com.movirtu.mxcloudservice.service.Session;
import com.movirtu.mxcloudservice.service.SessionManager;

public class ReleaseAllSessions extends BaseAction{

	public boolean perform(BaseSession session) throws Exception, RestException {
		// TODO Auto-generated method stub
		SessionManager.getInstance().releaseAllSessions();
		return true;
	}

	public String getName() {
		// TODO Auto-generated method stub
		return "ReleaseAllSessions";
	}

}
