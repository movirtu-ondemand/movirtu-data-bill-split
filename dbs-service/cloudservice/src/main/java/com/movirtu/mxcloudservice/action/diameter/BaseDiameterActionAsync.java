package com.movirtu.mxcloudservice.action.diameter;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.action.BaseAsyncAction;
import com.movirtu.mxcloudservice.connector.diameter.DiameterConnector;
import com.movirtu.mxcloudservice.service.BaseSession;
import com.movirtu.mxcloudservice.service.DiameterSession;

public abstract class BaseDiameterActionAsync extends BaseAsyncAction{
	
	private DiameterConnector connector;

	public DiameterConnector getConnector() {
		return connector;
	}

	public void setConnector(DiameterConnector connector) {
		this.connector = connector;
	}
	
	public abstract boolean perform(DiameterSession session) throws Exception, RestException;
	
	public boolean perform(BaseSession session) throws Exception, RestException {
		// TODO Auto-generated method stub
		return perform((DiameterSession) session);
	}
}
