package com.movirtu.mxcloudservice.action.diameter;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.dao.manager.DataUserProfileDaoManager;
import com.movirtu.mxcloudservice.service.DiameterSession;

public abstract class FetchDataUserProfileAsync extends BaseDiameterActionAsync{

	protected abstract String[] getColumns(DiameterSession session);
	protected abstract Object[] getValues(DiameterSession session);
	
	@Override
	public boolean perform(DiameterSession session) throws Exception,
			RestException {
		// TODO Auto-generated method stub
		session.setFutureProfile(DataUserProfileDaoManager.getInstance().getProfileAsync(getColumns(session), 
																getValues(session), 
																session, 
																getSuccessEvent(), 
																getFailureEvent()));
		return true;
	}

}
