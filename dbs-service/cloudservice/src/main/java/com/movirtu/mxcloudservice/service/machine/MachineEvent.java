package com.movirtu.mxcloudservice.service.machine;

public enum MachineEvent {

	STASIS_START(0),
	RINGING(1),
	ANSWERED(2),
	NO_ANSWER(3),
	BUSY(4),
	HANGUP(5),
	DESTROYED(6),
	UNDEFINED(7),
	ERROR(8),
	INIT(9),
	DESTROY(10),
	

	// SMS specific events
	SEND_OUTGOING_SMS(11),
	SMS_SENT_SUCCESSFULLY(12),
	SMS_SENDING_FAILED(13),
	SMS_DELIVERY_RECEIPT(14),
	INIT_ACK(15),
	INIT_TIMEOUT(16),
	
	INCOMING_SMS(17),
	REINIT(18),
	
	PROFILE_SUCCESS(19),
	PROFILE_FAILURE(20),
	CDR(21),
	ACR(22),
	ACA(23),
	APN_BASED_SERVICE(24),
	ACTIVE_PROFILE_BASED_SERVICE(25),
	CCR_INIT(26),
	CCR_INTERIM(27),
	CCR_TERM(28),
	CCA(29),
	FAILOVER(30),
	ALLOW_FAILOVER_TIMEOUT(31),
	DENY_FAILOVER_TIMEOUT(32),
	ALLOW_FAILOVER_DELIVERY_FAILURE(33),
	DENY_FAILOVER_DELIVERY_FAILURE(34),
	ALLOW_FAILOVER_FAILURE_MESSAGE(35),
	DENY_FAILOVER_FAILURE_MESSAGE(36),
	PROFILE_SWITCH_DETECTED(37),
	PROFILE_OK(38),
	PROFILE_NOT_CHANGED(39),
	PROFILE_CHANGED(40)
	;

	private int value;
	private static String name[] = {
			"STASIS_START",
			"RINGING",
			"ANSWERED",
			"NO_ANSWER",
			"BUSY",
			"HANGUP",
			"DESTROYED",
			"UNDEFINED",
			"ERROR",
			"INIT",
			"DESTROY",

			//SMS specific events
			"SEND_OUTGOING_SMS",
			"SMS_SENT_SUCCESSFULLY",
			"SMS_SENDING_FAILED",
			"SMS_DELIVERY_RECEIPT",
			"INIT_ACK",
			"INIT_TIMEOUT",
			"INCOMING_SMS",
			"REINIT",
			"PROFILE_SUCCESS",
			"PROFILE_FAILURE",
			"CDR",
			"ACR",
			"ACA",
			"APN_BASED_SERVICE",
			"ACTIVE_PROFILE_BASED_SERVICE",
			"CCR_INIT",
			"CCR_INTERIM",
			"CCR_TERM",
			"CCA",
			"FAILOVER",
			"ALLOW_FAILOVER_TIMEOUT",
			"DENY_FAILOVER_TIMEOUT",
			"ALLOW_FAILOVER_DELIVERY_FAILURE",
			"DENY_FAILOVER_DELIVERY_FAILURE",
			"ALLOW_FAILOVER_FAILURE_MESSAGE",
			"DENY_FAILOVER_FAILURE_MESSAGE",
			"PROFILE_SWITCH_DETECTED",
			"PROFILE_OK",
			"PROFILE_NOT_CHANGED",
			"PROFILE_CHANGED"
	};

	private MachineEvent(int value) {
		this.value = value;
	}

	public int getValue(){
		return value;
	}

	public String toString() {
		return name[value];
	}
	

	public static MachineEvent fromString(String text) {
		text=text.trim();
	    if (text != null) {
	      for (MachineEvent b : MachineEvent.values()) {
	        if (text.equalsIgnoreCase(name[b.value])) {
	          return b;
	        }
	      }
	    }
	    System.out.println("MachineEvent :"+text+" does not exist");
	    return null;
	  }
}
