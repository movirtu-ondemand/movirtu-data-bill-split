package com.movirtu.mxcloudservice.action.sms.handler;

import java.io.IOException;

import org.asteriskjava.manager.AuthenticationFailedException;
import org.asteriskjava.manager.DefaultManagerConnection;
import org.asteriskjava.manager.ManagerEventListener;
import org.asteriskjava.manager.TimeoutException;
import org.asteriskjava.manager.action.MessageSendAction;
import org.asteriskjava.manager.event.ManagerEvent;

import com.movirtu.mxcloudservice.service.sms.SmsSession;

public class AmiSmsSender implements SmsSender {

	private static volatile AmiSmsSender client;
	private DefaultManagerConnection dmc; 

	private String ipAddress;
	private int port;
	private String userName;
	private String password;

	public static AmiSmsSender getInstance() {
		if(client==null) {
			client = new AmiSmsSender();
		}
		return client;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public AmiSmsSender() {
		System.out.println("AmiSmsSender userName "+userName);
		System.out.println("AmiSmsSender password "+password);
		System.out.println("AmiSmsSender ipAddress "+ipAddress);
		System.out.println("AmiSmsSender port "+port);

		dmc = new DefaultManagerConnection();		
		dmc.setUsername(userName);
		dmc.setPassword(password);
		dmc.setHostname(ipAddress);
		dmc.setPort(port);

		try {
			dmc.login();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AuthenticationFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dmc.addEventListener(new ManagerEventListenerImpl());
	}

	public boolean sendSms(SmsSession session) {
		System.out.println("Sending sms using AMI "+session);
		SmsSession smsSession = (SmsSession)session;

		//TODO save sms to DB
		MessageSendAction msgSnd = new MessageSendAction();
		String to = smsSession.getCalledPartySipUser();
		if(to==null) {
			System.out.println("TO field can not be null. Returning without sending SMS");
			// TODO send an event SMS_FAILED;
			return false;
		}
		msgSnd.setTo("pjsip:"+to);
		String from = smsSession.getCgpn();
		if(from==null) {
			from = smsSession.getCallingPartySipUser();
		}
		msgSnd.setFrom(from);
		msgSnd.setBody(smsSession.getMessageContent());

		try {
			System.out.println("Sending sms "+msgSnd);
			dmc.sendAction(msgSnd);
			return true;
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	class ManagerEventListenerImpl implements ManagerEventListener {

		public void onManagerEvent(ManagerEvent event) {
			System.out.println("sms= "+event);

			String event_name = event.getClass().getSimpleName();
			System.out.println("sms= "+event_name);
		}
	}
}