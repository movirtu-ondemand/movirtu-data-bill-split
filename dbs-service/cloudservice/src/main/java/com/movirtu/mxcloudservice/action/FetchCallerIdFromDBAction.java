package com.movirtu.mxcloudservice.action;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.dao.manager.UserDetailsManager;
import com.movirtu.mxcloudservice.service.Session;

public class FetchCallerIdFromDBAction extends CallBaseAction{

	public boolean perform(Session session) throws Exception, RestException {
		// TODO Auto-generated method stub
		session.setCallerId(UserDetailsManager.getInstance().getPhoneNumberFromSipUser(session.getCgpn()));
		return true;
	}

	public String getName() {
		// TODO Auto-generated method stub
		return "FetchCallerIdFromDBAction";
	}

}
