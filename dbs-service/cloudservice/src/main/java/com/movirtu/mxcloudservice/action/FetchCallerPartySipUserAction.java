package com.movirtu.mxcloudservice.action;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.dao.manager.UserDetailsManager;
import com.movirtu.mxcloudservice.service.Session;

public class FetchCallerPartySipUserAction extends CallBaseAction {

	public boolean perform(Session session) throws Exception, RestException {
		System.out.println("Inside "+getName());
		
		String callerParty = session.getCgpn();

		if(callerParty == null) {
			return false;
		}

		if(UserDetailsManager.getInstance().isMsisdn(callerParty)) {
			session.setCallingPartySipUser(UserDetailsManager.getInstance().getSipUserFromPhoneNumber(callerParty));
		}

		return true;
	}

	public String getName() {
		return "FetchCallerPartySipUserAction";
	}

}
