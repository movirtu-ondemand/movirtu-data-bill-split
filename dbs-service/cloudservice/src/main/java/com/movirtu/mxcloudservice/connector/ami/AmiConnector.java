package com.movirtu.mxcloudservice.connector.ami;


import java.io.IOException;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.asteriskjava.manager.AuthenticationFailedException;
import org.asteriskjava.manager.DefaultManagerConnection;
import org.asteriskjava.manager.ManagerEventListener;
import org.asteriskjava.manager.TimeoutException;
import org.asteriskjava.manager.event.ManagerEvent;

import ch.loway.oss.ari4java.tools.ARIException;

import com.movirtu.mxcloudservice.connector.AbstractConnector;
import com.movirtu.mxcloudservice.service.Service;
import com.movirtu.mxcloudservice.service.Session;
import com.movirtu.mxcloudservice.service.SessionFactory;
import com.movirtu.mxcloudservice.service.SessionManager;
import com.movirtu.mxcloudservice.service.machine.MachineEvent;

public abstract class AmiConnector extends AbstractConnector {

	private String ipAddress;
	private int port;
	private String applicationName;
	private String userName;
	private String password;
	private int version;
	private int wsMaxPolling;
	private int wsMaxInterval;
	//private MessageQueue messageQueue;
	private SessionFactory sessionFactory;
	protected Service service;
	private BlockingQueue<AmiMessage> internalQueue;
	protected DefaultManagerConnection dmc; 

	protected Logger logger = Logger.getLogger(AmiConnector.class);

	public abstract String generateSessionId(String id);

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}

	public State getState() {
		return state;
	}

	public void connect() {
		System.out.println("Sending sms using userName "+userName);
		System.out.println("Sending sms using password "+password);
		System.out.println("Sending sms using ipAddress "+ipAddress);
		System.out.println("Sending sms using port "+port);

		dmc = new DefaultManagerConnection();		
		dmc.setUsername(userName);
		dmc.setPassword(password);
		dmc.setHostname(ipAddress);
		dmc.setPort(port);

		try {
			dmc.login();
			//		} catch (IllegalStateException e) {
			//			// TODO Auto-generated catch block
			//			e.printStackTrace();
			//		} catch (IOException e) {
			//			// TODO Auto-generated catch block
			//			e.printStackTrace();
			//		} catch (AuthenticationFailedException e) {
			//			// TODO Auto-generated catch block
			//			e.printStackTrace();
			//		} catch (TimeoutException e) {
			//			// TODO Auto-generated catch block
			//			e.printStackTrace();
			//		}
		} catch (Exception ex) {
			System.out.println("ISSUE ");
			ex.printStackTrace();
		}
		dmc.addEventListener(new ManagerEventListenerImpl());
	}

	public void service() {
		try {
			connect();
			changeState(State.RUNNING);
			onInit();
			processInternalEvents();
			processEvents();
			changeState(State.STOPPED);
		} catch(ARIException e) {
			logger.error(e.toString());
			//e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.toString());
			//e.printStackTrace();

		} finally {
			onDestroy();

		}
	}

	protected void onInit() {
		internalQueue = new ArrayBlockingQueue<AmiMessage>(100, true);
		Session session = (Session) SessionManager.getInstance().getOrCreateSession(getName(),sessionFactory,service);
		//getService().execute(session, MachineEvent.INIT);
	}

	protected void onDestroy() {
		Session session = (Session) SessionManager.getInstance().getOrCreateSession(getName(),sessionFactory,service);
		//getService().execute(session, MachineEvent.DESTROY);
	}

	public void processInternalEvents() throws ARIException, InterruptedException {
		while(isRunning()) {
			AmiMessage m = internalQueue.poll(10000, TimeUnit.MILLISECONDS);
			if (m != null) {
				logger.debug("Process internal event : " + m);
				onInternalEvent(m);
			}
		}
	}


	public void processEvents() throws ARIException, InterruptedException {
		//TODO 
	}

	public void enqueueInternalEvents(Object message) {
		logger.debug("Inside AmiConnector: enqueueInternalEvents" +message);
		this.internalQueue.add((AmiMessage) message);
		logger.debug("Leaving AmiConnector: enqueueInternalEvents" +message);
	}

	public abstract void onInternalEvent(AmiMessage m);

	public abstract void onEvent(AmiMessage m);

	public int getWsMaxPolling() {
		return wsMaxPolling;
	}

	public void setWsMaxPolling(int wsMaxPolling) {
		this.wsMaxPolling = wsMaxPolling;
	}

	public int getWsMaxInterval() {
		return wsMaxInterval;
	}

	public void setWsMaxInterval(int wsMaxInterval) {
		this.wsMaxInterval = wsMaxInterval;
	}

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	public synchronized void ping() {
		//TODO 
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	class ManagerEventListenerImpl implements ManagerEventListener {

		public void onManagerEvent(ManagerEvent event) {
			System.out.println("sms= "+event);

			String event_name = event.getClass().getSimpleName();
			System.out.println("sms= "+event_name);
		}
	}
}
