package com.movirtu.mxcloudservice.util;

public class Counter {
	
	private int size;
	private long count;
	private long max=1;
	public Counter(int size) {
		this.size = size;
		count = 0;
		for(;size>1;size--) max = 10 * max;
	}
	
	public String getNext() {
		long cnt = (count++)%max;
		String ret = Long.toString(cnt);
		int remaining = size - ret.length();
		for(;remaining>0;remaining--) ret = "0"+ret;
		return ret;
	}
}
