package com.movirtu.mxcloudservice.action.sms.util;

public enum SmsDirection {

	INCOMING(1), OUTGOING (0);

	private int direction;

	SmsDirection(int state) {
		this.direction=state;
	}

	public int getDirection() {
		return this.direction;
	}
}
