package com.movirtu.diameter.test.postpaid;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.concurrent.CountDownLatch;

import com.movirtu.diameter.charging.apps.postpaid.client.ACRController;

public class TestClientPp {
	public static void menu() {
		System.out.println("Enter 'c'= to connect");
		System.out.println("Enter 's'= to send event ACR");
		System.out.println("Enter 'i'= to send initial ACR");
		System.out.println("Enter 'u'= to send update ACR");
		System.out.println("Enter 't'= to send terminate ACR");
		System.out.println("Enter 'q'= to exit");
		System.out.println("Enter 'x'= to close connection");
		System.out.println("Enter 'p'= to check peer connection");
		System.out.println("Enter 'h'= to show help");
	}

	public static void main(String[] args) {
		try {

			BufferedReader inFromUser = new BufferedReader(
					new InputStreamReader(System.in));

			String aMSISDN, bMSISDN, aIMSI, bIMSI, vLR, configPath;
			boolean useCustomConfig, isSC, useDefaultSC;
			
			/*rohit modification*/
			String apn,sessionIdKey;
			int usuInputOctets=0,usuOutputOctets=0,ccfhVal=1,rsuInputOctets=0,rsuOutputOctets=0;
			/*end of rohit modification*/
			
			
			if (args.length == 16) {

				if (args[0].compareTo("t") == 0) {
					useCustomConfig = true;
				} else {
					useCustomConfig = false;
				}
				configPath = args[1];

				aMSISDN = args[2];
				bMSISDN = args[3];
				aIMSI = args[4];
				bIMSI = args[5];
				vLR = args[6];
				if (args[7].compareTo("t") == 0) {
					isSC = true;
				} else {
					isSC = false;
				}

				if (args[8].compareTo("t") == 0) {
					useDefaultSC = true;
				} else {
					useDefaultSC = false;
				}
				/*rohit modification*/
				apn=args[9];
				sessionIdKey = args[10];
				usuInputOctets = Integer.valueOf(args[11]);
				usuOutputOctets = Integer.valueOf(args[12]);
				ccfhVal = Integer.valueOf(args[13]);
				rsuInputOctets = Integer.valueOf(args[14]);
				rsuOutputOctets = Integer.valueOf(args[15]);
				/*end of rohit modification*/
				// System.out
				// .println("Usage:<aMSISDN> <bMSISDN> <aIMSI> <bIMSI> <vlr> <shortcode=0/1><useDefaultshortcode=0/1>");
				// CCRController.INSTANCE.tearDown();

			} else {
				System.out
						.println("Usage:<use custom config=t/f> <path> <aMSISDN> <bMSISDN> <aIMSI> <bIMSI> <vlr> <shortcode=t/f><useDefaultshortcode=t/f> <apn> <sessionId> <usuInputOctets> <usuOutputOctets> <ccfh = 0/1/2> <gsuInputOctets> <gsuOutputOctets>");

				System.out.println("Enter use custom config=t/f");
				String temp = inFromUser.readLine();
				if (temp.compareTo("t") == 0) {
					useCustomConfig = true;
				} else {
					useCustomConfig = false;
				}
				if (useCustomConfig) {
					System.out.println("Enter config path");
					configPath = inFromUser.readLine();
				}else{
					configPath="NULL";
				}

				System.out.println("Enter aMSISDN");
				aMSISDN = inFromUser.readLine();
				System.out.println("Enter bMSISDN");
				bMSISDN = inFromUser.readLine();
				System.out.println("Enter aIMSI");
				aIMSI = inFromUser.readLine();
				System.out.println("Enter bIMSI");
				bIMSI = inFromUser.readLine();
				System.out.println("Enter vlr");
				vLR = inFromUser.readLine();
				System.out.println("Enter if msisdn is a shortcode =t/f");
				temp = inFromUser.readLine();
				if (temp.compareTo("t") == 0) {
					isSC = true;
				} else {
					isSC = false;
				}
				System.out.println("Enter if use default shortcode =t/f");

				temp = inFromUser.readLine();
				if (temp.compareTo("t") == 0) {
					useDefaultSC = true;
				} else {
					useDefaultSC = false;
				}
				/* rohit modification*/
				System.out.println("Enter apn");
				apn = inFromUser.readLine();
				System.out.println("Enter sessionId");
				sessionIdKey = inFromUser.readLine();
				System.out.println("Enter usuInputOctets");
				String usuInputOctetsStr = inFromUser.readLine();
				usuInputOctets = Integer.valueOf(usuInputOctetsStr);
				System.out.println("Enter usuOutputOctets");
				String usuOutputOctetsStr = inFromUser.readLine();
				usuOutputOctets = Integer.valueOf(usuOutputOctetsStr);
				System.out.println("Enter ccfh");
				String ccfhValStr = inFromUser.readLine();
				ccfhVal = Integer.valueOf(ccfhValStr);
				System.out.println("Enter rsuInputOctets");
				String rsuInputOctetsStr = inFromUser.readLine();
				rsuInputOctets = Integer.valueOf(rsuInputOctetsStr);
				System.out.println("Enter rsuOutputOctets");
				String rsuOutputOctetsStr = inFromUser.readLine();
				rsuOutputOctets = Integer.valueOf(rsuOutputOctetsStr);
				/* end of rohit modification*/
				System.out.println("Entered values: "+useCustomConfig+" "+configPath+" " + aMSISDN + " " + bMSISDN
						+ " " + aIMSI + " " + bIMSI + " " + vLR + " " + isSC
						+ " " + useDefaultSC + " " + apn + " " + sessionIdKey + " " + usuInputOctets + " "
						+ usuOutputOctets + " " + ccfhVal + " " + rsuInputOctets + " " + rsuOutputOctets);

			}

			//set config.xml path
			ACRController.INSTANCE.isCustomConfigPath = useCustomConfig;
			ACRController.INSTANCE.customConfigPath = configPath;
			
			
			// client stack initialize and start connection
			ACRController.INSTANCE.setUp();
			// check if connected
			ACRController.INSTANCE.chkPeerConnection();

			// CCRController.INSTANCE.sendEvent(aMSISDN, bMSISDN, aIMSI,
			// bIMSI, vLR, isSC, useDefaultSC);

			for (;;) {
				menu();
				String input = inFromUser.readLine();

				if (input.compareTo("s") == 0) {
					System.out.println("sending ccr ...");
					ACRController.INSTANCE.sendEvent(aMSISDN, bMSISDN, aIMSI,
							bIMSI, vLR, isSC, useDefaultSC,"200",4, apn,sessionIdKey,usuInputOctets,usuOutputOctets,ccfhVal,rsuInputOctets,rsuOutputOctets);
				} else if(input.compareTo("i") == 0) {
					ACRController.INSTANCE.sendInitial(aMSISDN, bMSISDN, aIMSI,
							bIMSI, vLR, isSC, useDefaultSC,"200",4,apn,sessionIdKey,usuInputOctets,usuOutputOctets,ccfhVal,rsuInputOctets,rsuOutputOctets);
				} else if(input.compareTo("u") == 0) {
					System.out.println("Enter session Id for update<movirtu;<counter>");
					String temp = inFromUser.readLine();
					
					ACRController.INSTANCE.sendInterim(aMSISDN, bMSISDN, aIMSI,
							bIMSI, vLR, isSC, useDefaultSC,"200",4,apn,temp,usuInputOctets,usuOutputOctets,ccfhVal,rsuInputOctets,rsuOutputOctets);
				}  else if(input.compareTo("t") == 0) {
					System.out.println("Enter session Id for terminate<movirtu;<counter>");
					String temp = inFromUser.readLine();
					ACRController.INSTANCE.sendTerminate(aMSISDN, bMSISDN, aIMSI,
							bIMSI, vLR, isSC, useDefaultSC,"200",4,apn,temp,usuInputOctets,usuOutputOctets);
				}
				else if (input.compareTo("c") == 0) {
					ACRController.INSTANCE.start();

				} else if (input.compareTo("q") == 0) {
					ACRController.INSTANCE.tearDown();
					break;

				} else if (input.compareTo("x") == 0) {
					ACRController.INSTANCE.tearDown();

				} else if (input.compareTo("p") == 0) {
					ACRController.INSTANCE.chkPeerConnection();

				} else if (input.compareTo("h") == 0) {
					menu();

				} else {
					// do nothing
				}
			}

			// new CountDownLatch(1).await();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("done.....");

	}
}
