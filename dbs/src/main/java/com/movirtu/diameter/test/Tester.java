package com.movirtu.diameter.test;

import java.io.File;
import java.io.FileInputStream;
import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.jdiameter.api.DisconnectCause;
import org.jdiameter.api.Mode;
import org.jdiameter.api.Peer;
import org.jdiameter.api.PeerState;
import org.jdiameter.api.PeerTable;
import org.jdiameter.api.Stack;

import com.movirtu.diameter.charging.apps.client.CCRClient;
import com.movirtu.diameter.charging.apps.server.Server;

public class Tester {

	private Server serverNode1,serverNode2;

	URI serverNode1ConfigURI;

	public void setUpServer1() throws Exception {
		try {
			this.serverNode1 = new Server();

			this.serverNode1.init(
			getClass().getResourceAsStream("/config-ccaservernode1.xml"),"SERVER1");
			this.serverNode1.start();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	
	public void setUpServer2(String customConfigPath) throws Exception {
		
		try {
			URI serverConfigURI = new URI("file:" + customConfigPath);
			this.serverNode2 = new Server();
			if(customConfigPath!=null) {
				this.serverNode2.init(new FileInputStream(new File(serverConfigURI)),"SERVER2");
			}else {
			this.serverNode2.init(
			getClass().getResourceAsStream("/config-ccaservernode2.xml"),"SERVER2");
			}
			this.serverNode2.start();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void tearDown() {
		if (this.serverNode1 != null) {
			try {
				this.serverNode1.stop(DisconnectCause.REBOOTING);
			} catch (Exception e) {

			}
			this.serverNode1 = null;
		}

		if (this.serverNode2 != null) {
			try {
				this.serverNode2.stop(DisconnectCause.REBOOTING);
			} catch (Exception e) {

			}
			this.serverNode2 = null;
		}
	}

}
