package com.movirtu.diameter.test.b2bgw;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.diameter.charging.apps.B2BGw.client.B2BGwACRController;
import com.movirtu.diameter.charging.apps.B2BGw.client.B2BGwCCRController;

public class TestClientB2BGwCCA implements Runnable{
	public static final boolean isTest = false;
	private static final Logger logger = LoggerFactory
			.getLogger(TestClientB2BGwCCA.class);
	public Thread t;
	public static B2BGwCCRController  clientController = new B2BGwCCRController();
	

	
	public void start() {
		System.out.println("starting client thread");
		if(t == null)
		{
			t = new Thread(this);
			t.start();
		}
	}
	public  void run() {
		try {
			logger.debug("rohit run() inside client thread");
			System.out.println("rohit run() inside client thread");
			BufferedReader inFromUser = new BufferedReader(
					new InputStreamReader(System.in));
						
			// client stack initialize and start connection
			clientController.setUp(null);
			// check if connected
			clientController.chkPeerConnection();

			
			for(;;)
				Thread.sleep(100000000);

			// new CountDownLatch(1).await();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("done.....");

	}
		
}
