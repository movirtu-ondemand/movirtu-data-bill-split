package com.movirtu.diameter.test.b2bgw;

import org.jdiameter.api.InternalException;
import org.jdiameter.api.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.diameter.charging.apps.B2BGw.client.B2BGwSendFromServiceInvocationWrapperForRequest;
import com.movirtu.diameter.charging.apps.B2BGw.server.B2BGwSendFromServiceInvocationWrapperForAnswer;
import com.movirtu.diameter.charging.apps.common.HashMapDS;
import com.movirtu.diameter.charging.apps.common.TransactionInfo;

public class ServiceProcessingDiameterMessagesImpl implements ServiceProcessingDiameterMessages{
	public
	 static final Logger logger = LoggerFactory
			.getLogger(ServiceProcessingDiameterMessagesImpl.class);
	public void serviceProcessingACA(TransactionInfo obj) {
		logger.debug("rohit inside serviceProcessingDiameterMessagesImpl");
		obj.result = 2001;
		
		
		obj.accountInterimInterval = 60;
		try {
			if(TestClientB2BGw.isTest == true) {
				String[] sessIdComp = obj.key.split(";");
				String SearchFirstLegSessIdOn = "movirtu;"+sessIdComp[4];		
				StoreGenSessionId sessIDStorageObj = HashMapDS.INSTANCE.searchInHash(SearchFirstLegSessIdOn);
				obj.key = sessIDStorageObj.SessionId;
				obj.request = sessIDStorageObj.request;
				obj.serverInstListener = sessIDStorageObj.serverInst;
				
				
			}
			B2BGwSendFromServiceInvocationWrapperForAnswer.INSTANCE.sendACAWrapper(obj);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}

	public void serviceProcessingACR(TransactionInfo obj) {
		try {
			logger.debug("inside serviceProcessingACR");
			obj.display();
		} catch (InternalException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
		obj.vSubscriptionIdDataValue = "640000000000090";
		obj.vSubscriptionIdTypeValue = 1;
		if(obj.recordType == 2)
		obj.userProvidedSessionIdKey = "Blackberry" + obj.sessionIdIntComp;
		try {
			B2BGwSendFromServiceInvocationWrapperForRequest.INSTANCE.sendACRWrapper(obj);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	

}
