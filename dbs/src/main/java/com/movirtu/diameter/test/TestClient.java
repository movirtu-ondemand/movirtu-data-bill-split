package com.movirtu.diameter.test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.concurrent.CountDownLatch;

import com.movirtu.diameter.charging.apps.client.CCRController;

public class TestClient {
	public static int sessionNo = 0;
	public static void menu() {
		System.out.println("Enter 'c'= to connect");
		System.out.println("Enter 's'= to send event CCR");
		System.out.println("Enter 'i'= to send initial CCR");
		System.out.println("Enter 'u'= to send update CCR");
		System.out.println("Enter 't'= to send terminate CCR");
		System.out.println("Enter 'q'= to exit");
		System.out.println("Enter 'x'= to close connection");
		System.out.println("Enter 'p'= to check peer connection");
		System.out.println("Enter 'h'= to show help");
	}

	public static void main(String[] args) {
		try {

			BufferedReader inFromUser = new BufferedReader(
					new InputStreamReader(System.in));

			String aMSISDN, bMSISDN, aIMSI, bIMSI, vLR, configPath;
			boolean useCustomConfig, isSC, useDefaultSC;
			
			/*rohit modification*/
			String apn,sessionIdKey;
			int usuInputOctets=0,usuOutputOctets=0,ccfhVal=1,rsuInputOctets=0,rsuOutputOctets=0;
			/*end of rohit modification*/
			
			
			if (args.length == 16) {

				if (args[0].compareTo("t") == 0) {
					useCustomConfig = true;
				} else {
					useCustomConfig = false;
				}
				configPath = args[1];

				aMSISDN = args[2];
				bMSISDN = args[3];
				aIMSI = args[4];
				bIMSI = args[5];
				vLR = args[6];
				if (args[7].compareTo("t") == 0) {
					isSC = true;
				} else {
					isSC = false;
				}

				if (args[8].compareTo("t") == 0) {
					useDefaultSC = true;
				} else {
					useDefaultSC = false;
				}
				/*rohit modification*/
				apn=args[9];
				sessionIdKey = args[10];
				usuInputOctets = Integer.valueOf(args[11]);
				usuOutputOctets = Integer.valueOf(args[12]);
				ccfhVal = Integer.valueOf(args[13]);
				rsuInputOctets = Integer.valueOf(args[14]);
				rsuOutputOctets = Integer.valueOf(args[15]);
				/*end of rohit modification*/
				// System.out
				// .println("Usage:<aMSISDN> <bMSISDN> <aIMSI> <bIMSI> <vlr> <shortcode=0/1><useDefaultshortcode=0/1>");
				// CCRController.INSTANCE.tearDown();

			} else {
				System.out
						.println("Usage:<use custom config=t/f> <path> <aMSISDN> <bMSISDN> <aIMSI> <bIMSI> <vlr> <shortcode=t/f><useDefaultshortcode=t/f> <apn> <sessionId> <usuInputOctets> <usuOutputOctets> <ccfh = 0/1/2> <gsuInputOctets> <gsuOutputOctets>");

				System.out.println("Enter use custom config=t/f");
				String temp = inFromUser.readLine();
				if (temp.compareTo("t") == 0) {
					useCustomConfig = true;
				} else {
					useCustomConfig = false;
				}
				if (useCustomConfig) {
					System.out.println("Enter config path");
					configPath = inFromUser.readLine();
				}else{
					configPath="NULL";
				}

				System.out.println("Enter aMSISDN");
				aMSISDN = inFromUser.readLine();
				System.out.println("Enter bMSISDN");
				bMSISDN = inFromUser.readLine();
				System.out.println("Enter aIMSI");
				aIMSI = inFromUser.readLine();
				System.out.println("Enter bIMSI");
				bIMSI = inFromUser.readLine();
				System.out.println("Enter vlr");
				vLR = inFromUser.readLine();
				System.out.println("Enter if msisdn is a shortcode =t/f");
				temp = inFromUser.readLine();
				if (temp.compareTo("t") == 0) {
					isSC = true;
				} else {
					isSC = false;
				}
				System.out.println("Enter if use default shortcode =t/f");

				temp = inFromUser.readLine();
				if (temp.compareTo("t") == 0) {
					useDefaultSC = true;
				} else {
					useDefaultSC = false;
				}
				/* rohit modification*/
				System.out.println("Enter apn");
				apn = inFromUser.readLine();
				System.out.println("Enter sessionId");
				sessionIdKey = inFromUser.readLine();
				System.out.println("Enter usuInputOctets");
				String usuInputOctetsStr = inFromUser.readLine();
				usuInputOctets = Integer.valueOf(usuInputOctetsStr);
				System.out.println("Enter usuOutputOctets");
				String usuOutputOctetsStr = inFromUser.readLine();
				usuOutputOctets = Integer.valueOf(usuOutputOctetsStr);
				System.out.println("Enter ccfh");
				String ccfhValStr = inFromUser.readLine();
				ccfhVal = Integer.valueOf(ccfhValStr);
				System.out.println("Enter rsuInputOctets");
				String rsuInputOctetsStr = inFromUser.readLine();
				rsuInputOctets = Integer.valueOf(rsuInputOctetsStr);
				System.out.println("Enter rsuOutputOctets");
				String rsuOutputOctetsStr = inFromUser.readLine();
				rsuOutputOctets = Integer.valueOf(rsuOutputOctetsStr);
				/* end of rohit modification*/
				System.out.println("Entered values: "+useCustomConfig+" "+configPath+" " + aMSISDN + " " + bMSISDN
						+ " " + aIMSI + " " + bIMSI + " " + vLR + " " + isSC
						+ " " + useDefaultSC + " " + apn + " " + sessionIdKey + " " + usuInputOctets + " "
						+ usuOutputOctets + " " + ccfhVal + " " + rsuInputOctets + " " + rsuOutputOctets);

			}

			//set config.xml path
			CCRController.INSTANCE.isCustomConfigPath = useCustomConfig;
			CCRController.INSTANCE.customConfigPath = configPath;
			
			
			// client stack initialize and start connection
			CCRController.INSTANCE.setUp();
			// check if connected
			CCRController.INSTANCE.chkPeerConnection();

			// CCRController.INSTANCE.sendEvent(aMSISDN, bMSISDN, aIMSI,
			// bIMSI, vLR, isSC, useDefaultSC);

			for (;;) {
				menu();
				String input = inFromUser.readLine();

				if (input.compareTo("s") == 0) {
					System.out.println("sending ccr ...");
					CCRController.INSTANCE.sendEvent(aMSISDN, bMSISDN, aIMSI,
							bIMSI, vLR, isSC, useDefaultSC,"200",4, apn,sessionIdKey,usuInputOctets,usuOutputOctets,ccfhVal,rsuInputOctets,rsuOutputOctets);
				} else if(input.compareTo("i") == 0) {
					if(TestClient.sessionNo!=0) {
						System.out.println("Enter session Id"); 
						input = inFromUser.readLine();
						sessionIdKey=input;
						System.out.println("Enter APN"); 
						input = inFromUser.readLine();
						apn=input;
					}
					TestClient.sessionNo++;
					CCRController.INSTANCE.sendInitial(aMSISDN, bMSISDN, aIMSI,
							bIMSI, vLR, isSC, useDefaultSC,"200",4,apn,sessionIdKey,usuInputOctets,usuOutputOctets,ccfhVal,rsuInputOctets,rsuOutputOctets);
				} else if(input.compareTo("u") == 0) {
					System.out.println("Enter session Id"); 
					input = inFromUser.readLine();
					sessionIdKey=input;
					System.out.println("Enter APN"); 
					input = inFromUser.readLine();
					apn=input;
					CCRController.INSTANCE.sendInterim(aMSISDN, bMSISDN, aIMSI,
							bIMSI, vLR, isSC, useDefaultSC,"200",4,apn,sessionIdKey,usuInputOctets,usuOutputOctets,ccfhVal,rsuInputOctets,rsuOutputOctets);
				}  else if(input.compareTo("t") == 0) {
					System.out.println("Enter session Id"); 
					input = inFromUser.readLine();
					sessionIdKey=input;
					System.out.println("Enter APN"); 
					input = inFromUser.readLine();
					apn=input;
					CCRController.INSTANCE.sendTerminate(aMSISDN, bMSISDN, aIMSI,
							bIMSI, vLR, isSC, useDefaultSC,"200",4,apn,sessionIdKey,usuInputOctets,usuOutputOctets);
				}
				else if (input.compareTo("c") == 0) {
					CCRController.INSTANCE.start();

				} else if (input.compareTo("q") == 0) {
					CCRController.INSTANCE.tearDown();
					break;

				} else if (input.compareTo("x") == 0) {
					CCRController.INSTANCE.tearDown();

				} else if (input.compareTo("p") == 0) {
					CCRController.INSTANCE.chkPeerConnection();

				} else if (input.compareTo("h") == 0) {
					menu();

				} else {
					// do nothing
				}
			}

			// new CountDownLatch(1).await();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("done.....");

	}
}
