package com.movirtu.diameter.test.b2bgw;

import org.jdiameter.api.InternalException;
import org.jdiameter.api.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.diameter.charging.apps.B2BGw.client.B2BGwCCRController;
import com.movirtu.diameter.charging.apps.common.HashMapDS;
import com.movirtu.diameter.charging.apps.common.PrepaidServiceProcessingDiameterMessages;
import com.movirtu.diameter.charging.apps.common.TransactionInfo;

public class PrepaidServiceProcessingDiameterMessagesImpl implements
		PrepaidServiceProcessingDiameterMessages {
	public static final Logger logger = LoggerFactory
			.getLogger(PrepaidServiceProcessingDiameterMessagesImpl.class);
	B2BGwCCRController ccrController = TestClientB2BGwCCA.clientController;

	public void serviceProcessingCCA(TransactionInfo obj) {
		logger.debug("rohit inside serviceProcessingCCA");
		obj.result = 2001;

		// obj.accountInterimInterval = 60;
		try {
			if (TestClientB2BGwCCA.isTest == true) {
				// String[] sessIdComp = obj.key.split(";");
				// String SearchFirstLegSessIdOn = "movirtu;"+sessIdComp[4];

				StoreGenSessionId sessIDStorageObj = HashMapDS.INSTANCE
						.searchInHash("movirtu");
				if (sessIDStorageObj != null) {
					obj.key = sessIDStorageObj.SessionId;
					obj.request = sessIDStorageObj.request;
					obj.b2bgwServerCCA = sessIDStorageObj.b2bgwServerCCA;
					obj.serverCCASession = sessIDStorageObj.serverCCASession;
					obj.display();
					if (obj.recordType == 1) {
						obj.b2bgwServerCCA.sendInitial(obj);
					} else if (obj.recordType == 2) {
						obj.b2bgwServerCCA.sendInterim(obj);
					} else if (obj.recordType == 3) {
						obj.b2bgwServerCCA.sendTerminate(obj);
					} else
						logger.debug("unexpected record type");

				} else {
					logger.debug("sessIDStorageObj not found");
				}

			} 

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void serviceProcessingCCR(TransactionInfo obj) {
		try {
			logger.debug("inside serviceProcessingCCR");
			if (ccrController == null) {
				ccrController = new B2BGwCCRController();
			}
			obj.display();

			obj.vSubscriptionIdDataValue = "442557867689";
			obj.vSubscriptionIdTypeValue = 0;
			String userProvidedSessionString = "blackberry";
			if (obj.recordType == 1) {
				try {
				obj.key = ccrController.clientNode
						.createSessionIdForSecondLeg(userProvidedSessionString);
				
					obj.display();
					ccrController.sendInitial(obj);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (obj.recordType == 2) {
				try {
					obj.display();
					ccrController.sendInterim(obj);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (obj.recordType == 3) {

				try {
					obj.display();
					ccrController.sendTerminate(obj);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				logger.debug("unexpected scenario");
			}

		} catch (InternalException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public void sessionSupervisionTimerExpiredNotification(String sessionId) {
		try {
			logger.debug("inside sessionSupervisionTimerExpiredNotification");
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public void serviceProcessingRAR(TransactionInfo obj) {
		try {
			logger.debug("inside serviceProcessingRAR");
			StoreGenSessionId sessIDStorageObj = HashMapDS.INSTANCE.searchInHash("movirtu");
			if(sessIDStorageObj!=null) {
				obj.b2bgwServerCCA = sessIDStorageObj.b2bgwServerCCA;
				obj.serverCCASession = sessIDStorageObj.serverCCASession;
			}
			
			obj.b2bgwServerCCA.sendReAuthRequest(obj);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	public void serviceProcessingRAA(TransactionInfo obj) {
		logger.debug("inside serviceProcessingRAA");
		if (ccrController == null) {
			ccrController = new B2BGwCCRController();
		}
		
		StoreGenSessionId sessIDStorageObj = HashMapDS.INSTANCE.searchInHash("blackberry");
		try {
		obj.key = sessIDStorageObj.SessionId;
		ccrController.clientNode.sendReAuthAnswer(obj);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sessionSupervisionTimerStartedNotification(String sessionId) {
		
	}
	public void sessionSupervisionTimerRestartedNotification(String sessionId) {
		
	}
	public void sessionSupervisionTimerStoppedNotification(String sessionId) {
		
	}
	
	public void txTimerExpiredNotification(String sessionId) {
		
	}
	public void grantAccessOnDeliverFailureNotification(
			String sessionId, Message request) {
		
	}
	public void denyAccessOnDeliverFailureNotification(
			String sessionId, Message request) {
		
	}
	public void grantAccessOnTxExpireNotification(String sessionId) {
		
	}
	public void denyAccessOnTxExpireNotification(String sessionId) {
		
	}
	public void grantAccessOnFailureMessageNotification(
			String sessionId) {
		
	}
	public void denyAccessOnFailureMessageNotification(String sessionId) {
		
	}
	public void indicateServiceErrorNotification(String sessionId) {
		
	}
	
}
