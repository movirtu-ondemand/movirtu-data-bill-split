package com.movirtu.diameter.test.b2bgw;

import java.net.URI;

import org.jdiameter.api.DisconnectCause;

import com.movirtu.diameter.charging.apps.B2BGw.client.B2BGwSendFromServiceInvocationWrapperForRequest;
import com.movirtu.diameter.charging.apps.B2BGw.server.B2BGwSendFromServiceInvocationWrapperForAnswer;
import com.movirtu.diameter.charging.apps.B2BGw.server.B2BGwServerAcc;
import com.movirtu.diameter.charging.apps.common.TransactionInfo;

public class TesterB2BGw {

	private B2BGwServerAcc serverNode1,serverNode2;

	URI serverNode1ConfigURI;

	public void setUpServer1() throws Exception {
		try {
			this.serverNode1 = new B2BGwServerAcc(null);

			this.serverNode1.init(
			getClass().getResourceAsStream("/config-server-node-b2bgw.xml"),"SERVER1");
			this.serverNode1.start();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	
	/*public void setUpServer2() throws Exception {
		try {
			this.serverNode2 = B2BGwServerAcc.INSTANCE;

			this.serverNode2.init(
			getClass().getResourceAsStream("/config-server-node2.xml"),"SERVER2");
			this.serverNode2.start();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
*/
	public void tearDown() {
		if (this.serverNode1 != null) {
			try {
				this.serverNode1.stop(DisconnectCause.REBOOTING);
			} catch (Exception e) {

			}
			this.serverNode1 = null;
		}

		if (this.serverNode2 != null) {
			try {
				this.serverNode2.stop(DisconnectCause.REBOOTING);
			} catch (Exception e) {

			}
			this.serverNode2 = null;
		}
	}
	
}
