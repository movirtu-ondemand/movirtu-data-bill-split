package com.movirtu.diameter.test.b2bgw;

import java.io.File;
import java.io.FileInputStream;
import java.net.URI;
import java.net.URISyntaxException;

import org.jdiameter.api.DisconnectCause;

import com.movirtu.diameter.charging.apps.B2BGw.server.B2BGwServerCCA;

public class TesterB2BGwCCA {

	public static B2BGwServerCCA serverNode1 = new B2BGwServerCCA(null);

	URI serverNode1ConfigURI;

	public void setUpServer1(String customConfigPath) throws Exception {
		try {
			
			URI clientConfigURI = null;
			
			if (customConfigPath!=null) {
				
					clientConfigURI = new URI("file:" + customConfigPath);
					
					this.serverNode1.init(new FileInputStream(new File(
							clientConfigURI)),"SERVER1");
				} else {
				this.serverNode1.init(
						getClass().getResourceAsStream("/config-b2bgwccaserver.xml"),"SERVER1");
			}
			
			this.serverNode1.start();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
	}
	
	/*public void setUpServer2() throws Exception {
		try {
			this.serverNode2 = B2BGwServerAcc.INSTANCE;

			this.serverNode2.init(
			getClass().getResourceAsStream("/config-server-node2.xml"),"SERVER2");
			this.serverNode2.start();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
*/
	public void tearDown() {
		if (this.serverNode1 != null) {
			try {
				this.serverNode1.stop(DisconnectCause.REBOOTING);
			} catch (Exception e) {

			}
			this.serverNode1 = null;
		}

		/*if (this.serverNode2 != null) {
			try {
				this.serverNode2.stop(DisconnectCause.REBOOTING);
			} catch (Exception e) {

			}
			this.serverNode2 = null;
		}*/
	}
	
}
