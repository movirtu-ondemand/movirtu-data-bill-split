package com.movirtu.diameter.test.postpaid;

import java.io.File;
import java.io.FileInputStream;
import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.jdiameter.api.DisconnectCause;
import org.jdiameter.api.Mode;
import org.jdiameter.api.Peer;
import org.jdiameter.api.PeerState;
import org.jdiameter.api.PeerTable;
import org.jdiameter.api.Stack;

import com.movirtu.diameter.charging.apps.postpaid.server.ServerAcc;

public class TesterPp {

	private ServerAcc serverNode1,serverNode2;

	URI serverNode1ConfigURI;

	public void setUpServer1() throws Exception {
		try {
			this.serverNode1 = new ServerAcc();

			this.serverNode1.init(
			getClass().getResourceAsStream("/config-server-node1.xml"),"SERVER1");
			this.serverNode1.start();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	
	public void setUpServer2() throws Exception {
		try {
			this.serverNode2 = new ServerAcc();

			this.serverNode2.init(
			getClass().getResourceAsStream("/config-server-node2.xml"),"SERVER2");
			this.serverNode2.start();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void tearDown() {
		if (this.serverNode1 != null) {
			try {
				this.serverNode1.stop(DisconnectCause.REBOOTING);
			} catch (Exception e) {

			}
			this.serverNode1 = null;
		}

		if (this.serverNode2 != null) {
			try {
				this.serverNode2.stop(DisconnectCause.REBOOTING);
			} catch (Exception e) {

			}
			this.serverNode2 = null;
		}
	}

}
