package com.movirtu.diameter.test.b2bgw;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.diameter.charging.apps.B2BGw.client.B2BGwACRController;
import com.movirtu.diameter.charging.apps.B2BGw.client.B2BGwSendFromServiceInvocationWrapperForRequest;

public class TestClientB2BGw implements Runnable{
	public static final boolean isTest = true;
	private static final Logger logger = LoggerFactory
			.getLogger(TestClientB2BGw.class);
	public Thread t;
	/*public static void menu() {
		System.out.println("Enter 'c'= to connect");
		System.out.println("Enter 's'= to send event ACA");
		System.out.println("Enter 'i'= to send initial ACA");
		System.out.println("Enter 'u'= to send update ACA");
		System.out.println("Enter 't'= to send terminate ACA");
		System.out.println("Enter 'q'= to exit");
		System.out.println("Enter 'x'= to close connection");
		System.out.println("Enter 'p'= to check peer connection");
		System.out.println("Enter 'h'= to show help");
	}*/

	
	public void start() {
		System.out.println("starting client thread");
		if(t == null)
		{
			t = new Thread(this);
			t.start();
		}
	}
	public  void run() {
		try {
			logger.debug("rohit run() inside client thread");
			System.out.println("rohit run() inside client thread");
			BufferedReader inFromUser = new BufferedReader(
					new InputStreamReader(System.in));
						
			// client stack initialize and start connection
			B2BGwACRController.INSTANCE.setUp();
			// check if connected
			B2BGwACRController.INSTANCE.chkPeerConnection();

			// CCRController.INSTANCE.sendEvent(aMSISDN, bMSISDN, aIMSI,
			// bIMSI, vLR, isSC, useDefaultSC);
			//String serviceProvidedSessionKey = "Blackberry";
			for(;;)
				Thread.sleep(100000000);
			
			/*for (;;) {
				menu();
				String input = inFromUser.readLine();

				if (input.compareTo("s") == 0) {
					System.out.println("sending acr ...");
					} else if(input.compareTo("i") == 0) {
						TransactionInfo obj = new TransactionInfo();
						obj.userProvidedSessionIdKey = serviceProvidedSessionKey;
						obj.calledStationId = "airtel.gprs";
						obj.cgAddress = "192.168.56.4";
						obj.ggsnAddress = "192.168.56.3";
						obj.isPsInformation = true;
						obj.isVirtualNumber = true;
						obj.recordNumber = 0;
						obj.recordType = 2;
						obj.sgsnAddress = "192.168.56.5";
						obj.subscriptionIdDataValue = "640080000086499";//or can put aIMSI
						obj.subscriptionIdTypeValue = 1;
						B2BGwSendInvocationWrapper.INSTANCE.sendACRWrapper(obj);
					} else if(input.compareTo("u") == 0) {
						TransactionInfo obj = new TransactionInfo();
						
						obj.userProvidedSessionIdKey = serviceProvidedSessionKey;
						obj.calledStationId = "airtel.gprs";
						obj.cgAddress = "192.168.56.4";
						obj.ggsnAddress = "192.168.56.3";
						obj.isPsInformation = true;
						obj.isVirtualNumber = true;
						obj.recordNumber = 0;
						obj.recordType = 3;
						obj.sgsnAddress = "192.168.56.5";
						obj.subscriptionIdDataValue = "640080000086499";//or can put aIMSI
						obj.subscriptionIdTypeValue = 1;
						B2BGwSendInvocationWrapper.INSTANCE.sendACRWrapper(obj);
					B2BGwACRController.INSTANCE.sendInterim(obj);
				}  else if(input.compareTo("t") == 0) {
					TransactionInfo obj = new TransactionInfo();
					System.out.println("enter the update sessionId");
					obj.userProvidedSessionIdKey = serviceProvidedSessionKey;
					obj.calledStationId = "airtel.gprs";
					obj.cgAddress = "192.168.56.4";
					obj.ggsnAddress = "192.168.56.3";
					obj.isPsInformation = true;
					obj.isVirtualNumber = true;
					obj.recordNumber = 0;
					obj.recordType = 2;
					obj.sgsnAddress = "192.168.56.5";
					obj.subscriptionIdDataValue = "640080000086499";//or can put aIMSI
					obj.subscriptionIdTypeValue = 1;
					B2BGwSendInvocationWrapper.INSTANCE.sendACRWrapper(obj);
					B2BGwACRController.INSTANCE.sendTerminate(obj);
				}
				else if (input.compareTo("c") == 0) {
					B2BGwACRController.INSTANCE.start();

				} else if (input.compareTo("q") == 0) {
					B2BGwACRController.INSTANCE.tearDown();
					break;

				} else if (input.compareTo("x") == 0) {
					B2BGwACRController.INSTANCE.tearDown();

				} else if (input.compareTo("p") == 0) {
					B2BGwACRController.INSTANCE.chkPeerConnection();

				} else if (input.compareTo("h") == 0) {
					menu();

				} else {
					// do nothing
				}
			}*/

			// new CountDownLatch(1).await();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("done.....");

	}
		
}
