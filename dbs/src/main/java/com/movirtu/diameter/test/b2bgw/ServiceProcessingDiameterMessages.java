package com.movirtu.diameter.test.b2bgw;

import com.movirtu.diameter.charging.apps.common.TransactionInfo;

public interface ServiceProcessingDiameterMessages {
	//postpaid
	public void serviceProcessingACA(TransactionInfo obj);
	public void serviceProcessingACR(TransactionInfo obj);
	
	
}
