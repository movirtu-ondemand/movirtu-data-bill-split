package com.movirtu.diameter.charging.apps.postpaid.client;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.dellroad.stuff.net.IPv4Util;
import org.jdiameter.api.ApplicationId;
import org.jdiameter.api.Avp;
import org.jdiameter.api.AvpSet;
import org.jdiameter.api.InternalException;
import org.jdiameter.api.acc.ClientAccSession;
import org.jdiameter.api.acc.events.AccountRequest;
import org.jdiameter.common.impl.app.acc.AccountRequestImpl;

public class ACRBuilder {

	private AvpSet acrAvps;
	private AccountRequest acr;

	static final int ACC_REQUEST_TYPE_INITIAL = 2;
	static final int ACC_REQUEST_TYPE_INTERIM = 3;
	static final int ACC_REQUEST_TYPE_TERMINATE = 4;
	static final int ACC_REQUEST_TYPE_EVENT = 1;

	private ClientAccSession session;
	private ApplicationId appID;
	private String serverRealm;
	private String clientRealm;
	private String clientURI;
	private String serverURI;
	private String serviceContextId;
	private String aMSISDN;
	private String bMSISDN;
	private String aIMSI;
	private String bIMSI;
	private String vLR;
	private boolean isShortCode;
	private boolean defaultShortCode;
	private String service_provider_id;
	private int service_identifier;
	private String apn;
	private String sessionIdKey;
	private int usuInputOctets = 0;
	private int usuOutputOctets = 0;
	private int ccfhVal = 0;
	private int rsuInputOctets = 0;
	private int rsuOutputOctets = 0;

	ACRBuilder(ClientAccSession session, ApplicationId appID,
			String clientRealm, String serverRealm, String clientURI,
			String serverURI, String serviceContextId, String aMSISDN,
			String bMSISDN, String aIMSI, String bIMSI, String vLR,
			boolean isShortCode, boolean defaultShortCode,
			String service_provider_id, int service_identifier, String apn,
			String sessionIdKey, int usuIo, int usuOu, int ccfh, int rsuIo,
			int rsuOu) {
		this.session = session;
		this.appID = appID;
		this.clientRealm = clientRealm;
		this.serverRealm = serverRealm;
		this.clientURI = clientURI;
		this.serverURI = serverURI;
		this.serviceContextId = serviceContextId;
		this.aMSISDN = aMSISDN;
		this.bMSISDN = bMSISDN;
		this.aIMSI = aIMSI;
		this.bIMSI = bIMSI;
		this.vLR = vLR;
		this.isShortCode = isShortCode;
		this.defaultShortCode = defaultShortCode;
		this.service_provider_id = service_provider_id;
		this.service_identifier = service_identifier;
		this.apn = apn;
		this.sessionIdKey = sessionIdKey;
		this.usuInputOctets = usuIo;
		this.usuOutputOctets = usuOu;
		this.ccfhVal = ccfh;
		this.rsuInputOctets = rsuIo;
		this.rsuOutputOctets = rsuOu;

	}

	private void defaultACR(int requestType) throws InternalException {

		acr = new AccountRequestImpl(session.getSessions().get(0)
				.createRequest(AccountRequest.code, appID, serverRealm));
		// Create Credit-Control-Request
		// AVPs present by default: Origin-Host, Origin-Realm, Session-Id,
		// Vendor-Specific-Application-Id, Destination-Realm

		acrAvps = acr.getMessage().getAvps();
		// remove VENDOR_SPECIFIC_APPLICATION_ID
		// ccrAvps.removeAvp(Avp.VENDOR_SPECIFIC_APPLICATION_ID);

		// { Origin-Host }
		acrAvps.removeAvp(Avp.ORIGIN_HOST);// one can overwrite default AVP by
											// removing it
		acrAvps.addAvp(Avp.ORIGIN_HOST, clientURI, true, false, true);

		// [ Destination-Host ]
		acrAvps.addAvp(Avp.DESTINATION_HOST, serverURI, true, false, false);

		// { Auth-Application-Id }
		// ccrAvps.addAvp(Avp.AUTH_APPLICATION_ID,
		// appID.getAuthAppId(),appID.getVendorId(), true, false, true);

		// { Accounting-Record-Type }
		acrAvps.addAvp(Avp.ACC_RECORD_TYPE, requestType, true, false);

		// { Accounting-Record-Number }

		acrAvps.addAvp(Avp.ACC_RECORD_NUMBER, 0, true, false, true);

		// [ Requested-Action ]

		// acrAvps.addAvp(Avp.REQUESTED_ACTION, 0, true, false);

		// [ Event-Timestamp ]
		// Calendar c = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
		Calendar c = new GregorianCalendar(1900, 0, 1);

		c.setTimeZone(TimeZone.getTimeZone("UTC"));

		long timestamp = (System.currentTimeMillis() - c.getTimeInMillis()) / 1000;

		acrAvps.addAvp(Avp.EVENT_TIMESTAMP, timestamp, true, false, true);

		// *[ Subscription-Id ]

		// AvpSet subscriptionId = acrAvps.addGroupedAvp(Avp.SUBSCRIPTION_ID,
		// true, false);

		// Subscription-Id-Type AVP
		// subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_TYPE, 0, true, false);//
		// END_USER_E164

		// Subscription-Id-Data AVP
		// subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_DATA, aMSISDN, true, true,
		// false);

		// SERVICE_IDENTIFIER
		// acrAvps.addAvp(439, service_identifier, true, false);

		// [ Requested-Service-Unit ]
		/*
		 * AvpSet rsuAvp = ccrAvps.addGroupedAvp(Avp.REQUESTED_SERVICE_UNIT,
		 * true, false); rsuAvp.addAvp(Avp.CC_SERVICE_SPECIFIC_UNITS, (long) 1,
		 * true, false);
		 * 
		 * // Service-Provider-ID ccrAvps.addAvp(1081, service_provider_id, 193,
		 * true, false, false); // ccrAvps.addAvp(1081, "200", 193, true, false,
		 * false);
		 * 
		 * // Subscription-Id-Location ccrAvps.addAvp(1074, vLR, 193, true,
		 * false, false);
		 * 
		 * // Traffic-Case ccrAvps.addAvp(1082, 20, 193, true, false);
		 * 
		 * // 3GPP-MS-TimeZone // TimeZone tz =
		 * Calendar.getInstance().getTimeZone(); byte[] tz = { (byte) 0x0C,
		 * (byte) 0x00 };
		 * 
		 * ccrAvps.addAvp(Avp.TGPP_MS_TIMEZONE, tz, 10415, true, false); // tigo
		 * // timezone
		 * 
		 * // Service-Parameter-Info [A-Party IMSI] AvpSet svp =
		 * ccrAvps.addGroupedAvp(Avp.SERVICE_PARAMETER_INFO, true, false); //
		 * Service-Parameter-Type svp.addAvp(Avp.SERVICE_PARAMETER_TYPE, 207,
		 * true, false); // Service-Parameter-Value
		 * svp.addAvp(Avp.SERVICE_PARAMETER_VALUE, aIMSI, true, true, false);
		 * 
		 * // Service-Parameter-Info [B-Party IMSI] // svp =
		 * ccrAvps.addGroupedAvp(Avp.SERVICE_PARAMETER_INFO, true, false); //
		 * Service-Parameter-Type // svp.addAvp(Avp.SERVICE_PARAMETER_TYPE, 208,
		 * true, false); // Service-Parameter-Value //
		 * svp.addAvp(Avp.SERVICE_PARAMETER_VALUE, bIMSI, appID.getVendorId(),
		 * // true, true, false);
		 * 
		 * // Other-Party-Id svp = ccrAvps.addGroupedAvp(1075, 193, true,
		 * false); // OtherPartyIdDataTypeAvp svp.addAvp(1078, 0, 193, true,
		 * false);// END_USER_E164 // (0),INTERNATIONAL (4) if (!isShortCode) {
		 * 
		 * // OtherPartyIdDataAvp svp.addAvp(1077, bMSISDN, 193, true, false,
		 * false); // OtherPartyIdNatureAvp svp.addAvp(1076, 1, 193, true,
		 * false);/* Normal Number: International (1), ShortCode: National (2)
		 */
		/*
		 * } else {
		 * 
		 * if (defaultShortCode) { svp.addAvp(1077, "800800", 193, true, false,
		 * false);
		 * 
		 * } else { svp.addAvp(1077, bMSISDN, 193, true, false, false); }
		 * 
		 * svp.addAvp(1076, 2, 193, true, false); }
		 */

	}

	/* rohit modification */
	private void SessionInitiateACR(int requestType) throws InternalException {
		System.out
				.println("inside SessionInitiateACR ACRBuilder.java...appID ="
						+ appID);
		acr = new AccountRequestImpl(session.getSessions().get(0)
				.createRequest(AccountRequest.code, appID, serverRealm));
		// Account-Request
		// AVPs present by default: Origin-Host, Origin-Realm, Session-Id,
		// Vendor-Specific-Application-Id, Destination-Realm

		acrAvps = acr.getMessage().getAvps();
		// remove VENDOR_SPECIFIC_APPLICATION_ID
		// ccrAvps.removeAvp(Avp.VENDOR_SPECIFIC_APPLICATION_ID);

		// { Origin-Host }
		acrAvps.removeAvp(Avp.ORIGIN_HOST);// one can overwrite default AVP by
											// removing it
		acrAvps.addAvp(Avp.ORIGIN_HOST, clientURI, true, false, true);

		// [ Destination-Host ]
		acrAvps.addAvp(Avp.DESTINATION_HOST, serverURI, true, false, false);

		// { Auth-Application-Id }
		// ccrAvps.addAvp(Avp.AUTH_APPLICATION_ID,
		// appID.getAuthAppId(),appID.getVendorId(), true, false, true);

		// { Service-Context-Id }
		// M,V
		/*
		 * acrAvps.addAvp(Avp.SERVICE_CONTEXT_ID, serviceContextId, true, false,
		 * false);
		 */
		// { Accounting-Record-Type }
		acrAvps.addAvp(Avp.ACC_RECORD_TYPE, requestType, true, false);

		// { Accounting-Record-Number }

		acrAvps.addAvp(Avp.ACC_RECORD_NUMBER, 0, true, false, true);

		// [ Requested-Action ]

		// ccrAvps.addAvp(Avp.REQUESTED_ACTION, 0, true, false);

		// [ Event-Timestamp ]
		// Calendar c = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
		Calendar c = new GregorianCalendar(1900, 0, 1);

		c.setTimeZone(TimeZone.getTimeZone("UTC"));

		long timestamp = (System.currentTimeMillis() - c.getTimeInMillis()) / 1000;

		acrAvps.addAvp(Avp.EVENT_TIMESTAMP, timestamp, true, false, true);

		// [Acct-Application-Id]

		// acrAvps.addAvp(Avp.ACCT_APPLICATION_ID, 3, true, false);

		// *[ Subscription-Id ]
		/*
		 * AvpSet subscriptionId = ccrAvps.addGroupedAvp(Avp.SUBSCRIPTION_ID,
		 * true, false);
		 * 
		 * // Subscription-Id-Type AVP
		 * subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_TYPE, 1, true, false);//
		 * END_USER_IMSI
		 * 
		 * // Subscription-Id-Data AVP
		 * subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_DATA, aIMSI, true, true,
		 * false);
		 * 
		 * 
		 * 
		 * // [ Requested-Service-Unit ]
		 * 
		 * /* AvpSet rsuAvp = ccrAvps.addGroupedAvp(Avp.REQUESTED_SERVICE_UNIT,
		 * true, false); rsuAvp.addAvp(Avp.CC_SERVICE_SPECIFIC_UNITS, (long) 1,
		 * true, false);
		 */

		// [Multiple-Services-Credit-Control]
		
		  AvpSet msccAvp =
		  acrAvps.addGroupedAvp(Avp.MULTIPLE_SERVICES_CREDIT_CONTROL
		  ,true,false); AvpSet rsuAvp =
		  msccAvp.addGroupedAvp(Avp.REQUESTED_SERVICE_UNIT, true, false);
		  rsuAvp.addAvp(Avp.CC_INPUT_OCTETS,(long)rsuInputOctets,true,false);
		  rsuAvp.addAvp(Avp.CC_OUTPUT_OCTETS,(long)rsuOutputOctets,true,false);
		  
		  /*// SERVICE_IDENTIFIER ccrAvps.addAvp(439, service_identifier, true,
		 * false);
		 * 
		 * //[Service Information] AvpSet si =
		 * ccrAvps.addGroupedAvp(Avp.SERVICE_INFORMATION, 10415, false,false);
		 * AvpSet psinfo = si.addGroupedAvp(Avp.PS_INFORMATION, 10415,false,
		 * false); // CALLED_STATION_ID
		 * 
		 * //String apn = "airtelgprs.com"; /* String out = null; try { out =
		 * new String(apn.getBytes("UTF-8"), "ISO-8859-1"); } catch
		 * (java.io.UnsupportedEncodingException e) {
		 * System.out.println("failed to convert apn string to UTF8String format"
		 * );
		 * 
		 * }
		 */
		// psinfo.addAvp(30, apn, true,false,true);

		// [Service-Information]

		AvpSet si = acrAvps.addGroupedAvp(Avp.SERVICE_INFORMATION, 10415,
				false, false);
		// [Subscription-Id]
		AvpSet subsId = si.addGroupedAvp(Avp.SUBSCRIPTION_ID, (long) 0, false,
				false);
		subsId.addAvp(Avp.SUBSCRIPTION_ID_TYPE, 1, false, false);
		// subsId.addAvp(Avp.SUBSCRIPTION_ID_DATA, aIMSI, false, false);
		subsId.addAvp(Avp.SUBSCRIPTION_ID_DATA, aIMSI, true);
		// [PS-Information]
		AvpSet psInfo = si.addGroupedAvp(Avp.PS_INFORMATION, 10415, false,
				false);
		//3GPP-charging-id
		
			byte[] ch = {(byte)0x00,(byte)0x00,(byte)0x05,(byte)0xdc};
			Avp chargingId = psInfo.addAvp(Avp.TGPP_CHARGING_ID, ch, 10415,false,false);
		
		// [SGSN-Address]

		String sgsnAddr = "192.168.56.4";
		Inet4Address sgsnAddress = IPv4Util.fromString(sgsnAddr);
		psInfo.addAvp(Avp.SGSN_ADDRESS, sgsnAddress, 10415, false, false);
		// psInfo.addAvp(Avp.SGSN_ADDRESS, sgsnAddr, false, false, true);
		// [GGSN-Address]
		String ggsnAddr = "192.168.56.3";
		Inet4Address ggsnAddress = IPv4Util.fromString(ggsnAddr);
		psInfo.addAvp(Avp.GGSN_ADDRESS, ggsnAddress, 10415, false, false);
		// psInfo.addAvp(Avp.GGSN_ADDRESS, ggsnAddr, false, false, true);
		// [CG-Address]
		String cgAddr = "192.168.56.3";
		Inet4Address cgAddress = IPv4Util.fromString(cgAddr);
		psInfo.addAvp(Avp.CG_ADDRESS, cgAddress, 10415, false, false);
		// psInfo.addAvp(Avp.CG_ADDRESS, cgAddr, false, false, true);
		// [Called-Station-Id]
		psInfo.addAvp(30, apn, false, false, true);
		// [3GPP-MS-Timezone]
		byte[] tz = { (byte) 0x0C, (byte) 0x00 };

		psInfo.addAvp(Avp.TGPP_MS_TIMEZONE, tz, 10415, true, false);
	}

	/* rohit modification */
	private void SessionUpdateACR(int requestType) throws InternalException {

		acr = new AccountRequestImpl(session.getSessions().get(0)
				.createRequest(AccountRequest.code, appID, serverRealm));
		// Create Credit-Control-Request
		// AVPs present by default: Origin-Host, Origin-Realm, Session-Id,
		// Vendor-Specific-Application-Id, Destination-Realm

		acrAvps = acr.getMessage().getAvps();
		// remove VENDOR_SPECIFIC_APPLICATION_ID
		// ccrAvps.removeAvp(Avp.VENDOR_SPECIFIC_APPLICATION_ID);

		// { Origin-Host }
		acrAvps.removeAvp(Avp.ORIGIN_HOST);// one can overwrite default AVP by
											// removing it
		acrAvps.addAvp(Avp.ORIGIN_HOST, clientURI, true, false, true);

		// [ Destination-Host ]
		acrAvps.addAvp(Avp.DESTINATION_HOST, serverURI, true, false, false);

		// { Auth-Application-Id }
		// ccrAvps.addAvp(Avp.AUTH_APPLICATION_ID,
		// appID.getAuthAppId(),appID.getVendorId(), true, false, true);

		// { Service-Context-Id }
		// M,V
		/*
		 * ccrAvps.addAvp(Avp.SERVICE_CONTEXT_ID, serviceContextId, true, false,
		 * false);
		 */
		// { Accounting-Record-Type }
		acrAvps.addAvp(Avp.ACC_RECORD_TYPE, requestType, true, false);

		// { Accounting-Record-Number }

		acrAvps.addAvp(Avp.ACC_RECORD_NUMBER, 0, true, false, true);

		// [ Requested-Action ]

		// ccrAvps.addAvp(Avp.REQUESTED_ACTION, 0, true, false);

		// [ Event-Timestamp ]
		// Calendar c = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
		Calendar c = new GregorianCalendar(1900, 0, 1);

		c.setTimeZone(TimeZone.getTimeZone("UTC"));

		long timestamp = (System.currentTimeMillis() - c.getTimeInMillis()) / 1000;

		acrAvps.addAvp(Avp.EVENT_TIMESTAMP, timestamp, true, false, true);

		// *[ Subscription-Id ]
		/*
		 * AvpSet subscriptionId = ccrAvps.addGroupedAvp(Avp.SUBSCRIPTION_ID,
		 * true, false);
		 * 
		 * // Subscription-Id-Type AVP
		 * subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_TYPE, 1, true, false);//
		 * END_USER_IMSI
		 * 
		 * // Subscription-Id-Data AVP
		 * subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_DATA, aIMSI, true, true,
		 * false);
		 * 
		 * 
		 * 
		 * // [ Requested-Service-Unit ]
		 * 
		 * /* AvpSet rsuAvp = ccrAvps.addGroupedAvp(Avp.REQUESTED_SERVICE_UNIT,
		 * true, false); rsuAvp.addAvp(Avp.CC_SERVICE_SPECIFIC_UNITS, (long) 1,
		 * true, false);
		 */

		// [Multiple-Services-Credit-Control]

		/*
		 * AvpSet msccAvp =
		 * ccrAvps.addGroupedAvp(Avp.MULTIPLE_SERVICES_CREDIT_CONTROL
		 * ,true,false); AvpSet rsuAvp =
		 * msccAvp.addGroupedAvp(Avp.REQUESTED_SERVICE_UNIT, true, false);
		 * rsuAvp.addAvp(Avp.CC_INPUT_OCTETS,(long)rsuInputOctets,true,false);
		 * rsuAvp.addAvp(Avp.CC_OUTPUT_OCTETS,(long)rsuOutputOctets,true,false);
		 * AvpSet usuAvp = msccAvp.addGroupedAvp(Avp.USED_SERVICE_UNIT, true,
		 * false);
		 * usuAvp.addAvp(Avp.CC_INPUT_OCTETS,(long)usuInputOctets,true,false);
		 * usuAvp.addAvp(Avp.CC_OUTPUT_OCTETS,(long)usuOutputOctets,true,false);
		 */

		// SERVICE_IDENTIFIER
		// ccrAvps.addAvp(439, service_identifier, true, false);

		// [Service Information]
		/*
		 * AvpSet si = ccrAvps.addGroupedAvp(Avp.SERVICE_INFORMATION, 10415,
		 * false,false); AvpSet psinfo = si.addGroupedAvp(Avp.PS_INFORMATION,
		 * 10415,false, false);
		 */
		// CALLED_STATION_ID

		// String apn = "airtelgprs.com";
		/*
		 * String out = null; try { out = new String(apn.getBytes("UTF-8"),
		 * "ISO-8859-1"); } catch (java.io.UnsupportedEncodingException e) {
		 * System
		 * .out.println("failed to convert apn string to UTF8String format");
		 * 
		 * }
		 */
		/*
		 * psinfo.addAvp(30, apn, true,false,true);
		 */

		// [Service-Information]
		AvpSet si = acrAvps.addGroupedAvp(Avp.SERVICE_INFORMATION, 10415,
				false, false);
		// [Subscription-Id]
		AvpSet subsId = si.addGroupedAvp(Avp.SUBSCRIPTION_ID, (long) 0, false,
				false);
		subsId.addAvp(Avp.SUBSCRIPTION_ID_TYPE, 1, false, false);
		// subsId.addAvp(Avp.SUBSCRIPTION_ID_DATA, aIMSI, false, false);
		subsId.addAvp(Avp.SUBSCRIPTION_ID_DATA, aIMSI, true);
		// [PS-Information]
		AvpSet psInfo = si.addGroupedAvp(Avp.PS_INFORMATION, 10415, false,
				false);
		// [SGSN-Address]

		String sgsnAddr = "192.168.56.4";
		Inet4Address sgsnAddress = IPv4Util.fromString(sgsnAddr);
		psInfo.addAvp(Avp.SGSN_ADDRESS, sgsnAddress, 10415, false, false);
		// psInfo.addAvp(Avp.SGSN_ADDRESS, sgsnAddr, false, false, true);
		// [GGSN-Address]
		String ggsnAddr = "192.168.56.3";
		Inet4Address ggsnAddress = IPv4Util.fromString(ggsnAddr);
		psInfo.addAvp(Avp.GGSN_ADDRESS, ggsnAddress, 10415, false, false);
		// psInfo.addAvp(Avp.GGSN_ADDRESS, ggsnAddr, false, false, true);
		// [CG-Address]
		String cgAddr = "192.168.56.3";
		Inet4Address cgAddress = IPv4Util.fromString(cgAddr);
		psInfo.addAvp(Avp.CG_ADDRESS, cgAddress, 10415, false, false);
		// psInfo.addAvp(Avp.CG_ADDRESS, cgAddr, false, false, true);
		// [Called-Station-Id]
		psInfo.addAvp(30, apn, false, false, true);
		// [3GPP-MS-Timezone]
		byte[] tz = { (byte) 0x0C, (byte) 0x00 };

		psInfo.addAvp(Avp.TGPP_MS_TIMEZONE, tz, 10415, true, false);

	}

	private void SessionTerminateACR(int requestType) throws InternalException {

		acr = new AccountRequestImpl(session.getSessions().get(0)
				.createRequest(AccountRequest.code, appID, serverRealm));
		// Create Credit-Control-Request
		// AVPs present by default: Origin-Host, Origin-Realm, Session-Id,
		// Vendor-Specific-Application-Id, Destination-Realm

		acrAvps = acr.getMessage().getAvps();
		// remove VENDOR_SPECIFIC_APPLICATION_ID
		// ccrAvps.removeAvp(Avp.VENDOR_SPECIFIC_APPLICATION_ID);

		// { Origin-Host }
		acrAvps.removeAvp(Avp.ORIGIN_HOST);// one can overwrite default AVP by
											// removing it
		acrAvps.addAvp(Avp.ORIGIN_HOST, clientURI, true, false, true);

		// [ Destination-Host ]
		acrAvps.addAvp(Avp.DESTINATION_HOST, serverURI, true, false, false);

		// { Auth-Application-Id }
		// ccrAvps.addAvp(Avp.AUTH_APPLICATION_ID,
		// appID.getAuthAppId(),appID.getVendorId(), true, false, true);

		// { Service-Context-Id }
		// M,V
		acrAvps.addAvp(Avp.SERVICE_CONTEXT_ID, serviceContextId, true, false,
				false);
		// { Accounting-Record-Type }
		acrAvps.addAvp(Avp.ACC_RECORD_TYPE, requestType, true, false);

		// { CC-Request-Number }

		acrAvps.addAvp(Avp.ACC_RECORD_NUMBER, 0, true, false, true);

		// [ Requested-Action ]

		// ccrAvps.addAvp(Avp.REQUESTED_ACTION, 0, true, false);

		// [ Event-Timestamp ]
		// Calendar c = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
		Calendar c = new GregorianCalendar(1900, 0, 1);

		c.setTimeZone(TimeZone.getTimeZone("UTC"));

		long timestamp = (System.currentTimeMillis() - c.getTimeInMillis()) / 1000;

		acrAvps.addAvp(Avp.EVENT_TIMESTAMP, timestamp, true, false, true);

		// *[ Subscription-Id ]

		/*
		 * AvpSet subscriptionId = ccrAvps.addGroupedAvp(Avp.SUBSCRIPTION_ID,
		 * true, false);
		 * 
		 * // Subscription-Id-Type AVP
		 * subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_TYPE, 1, true, false);//
		 * END_USER_IMSI
		 * 
		 * // Subscription-Id-Data AVP
		 * subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_DATA, aIMSI, true, true,
		 * false);
		 * 
		 * 
		 * 
		 * // [ Requested-Service-Unit ]
		 * 
		 * /* AvpSet rsuAvp = ccrAvps.addGroupedAvp(Avp.REQUESTED_SERVICE_UNIT,
		 * true, false); rsuAvp.addAvp(Avp.CC_SERVICE_SPECIFIC_UNITS, (long) 1,
		 * true, false);
		 */

		// [Multiple-Services-Credit-Control]

		/*
		 * AvpSet msccAvp =
		 * ccrAvps.addGroupedAvp(Avp.MULTIPLE_SERVICES_CREDIT_CONTROL
		 * ,true,false);
		 * 
		 * AvpSet usuAvp = msccAvp.addGroupedAvp(Avp.USED_SERVICE_UNIT, true,
		 * false);
		 * usuAvp.addAvp(Avp.CC_INPUT_OCTETS,(long)usuInputOctets,true,false);
		 * usuAvp.addAvp(Avp.CC_OUTPUT_OCTETS,(long)usuOutputOctets,true,false);
		 * 
		 * // SERVICE_IDENTIFIER ccrAvps.addAvp(439, service_identifier, true,
		 * false);
		 * 
		 * //[Service Information] AvpSet si =
		 * ccrAvps.addGroupedAvp(Avp.SERVICE_INFORMATION, 10415, false,false);
		 * AvpSet psinfo = si.addGroupedAvp(Avp.PS_INFORMATION, 10415,false,
		 * false); // CALLED_STATION_ID
		 * 
		 * //String apn = "airtelgprs.com"; /* String out = null; try { out =
		 * new String(apn.getBytes("UTF-8"), "ISO-8859-1"); } catch
		 * (java.io.UnsupportedEncodingException e) {
		 * System.out.println("failed to convert apn string to UTF8String format"
		 * );
		 * 
		 * }
		 */
		/* psinfo.addAvp(30, apn, true,false,true); */

		// [Service-Information]
		AvpSet si = acrAvps.addGroupedAvp(Avp.SERVICE_INFORMATION, 10415,
				false, false);
		// [Subscription-Id]
		AvpSet subsId = si.addGroupedAvp(Avp.SUBSCRIPTION_ID, (long) 0, false,
				false);
		subsId.addAvp(Avp.SUBSCRIPTION_ID_TYPE, 1, false, false);
		// subsId.addAvp(Avp.SUBSCRIPTION_ID_DATA, aIMSI, false, false);
		subsId.addAvp(Avp.SUBSCRIPTION_ID_DATA, aIMSI, true);
		// [PS-Information]
		AvpSet psInfo = si.addGroupedAvp(Avp.PS_INFORMATION, 10415, false,
				false);
		// [SGSN-Address]

		String sgsnAddr = "192.168.56.4";
		Inet4Address sgsnAddress = IPv4Util.fromString(sgsnAddr);
		psInfo.addAvp(Avp.SGSN_ADDRESS, sgsnAddress, 10415, false, false);
		// psInfo.addAvp(Avp.SGSN_ADDRESS, sgsnAddr, false, false, true);
		// [GGSN-Address]
		String ggsnAddr = "192.168.56.3";
		Inet4Address ggsnAddress = IPv4Util.fromString(ggsnAddr);
		psInfo.addAvp(Avp.GGSN_ADDRESS, ggsnAddress, 10415, false, false);
		// psInfo.addAvp(Avp.GGSN_ADDRESS, ggsnAddr, false, false, true);
		// [CG-Address]
		String cgAddr = "192.168.56.3";
		Inet4Address cgAddress = IPv4Util.fromString(cgAddr);
		psInfo.addAvp(Avp.CG_ADDRESS, cgAddress, 10415, false, false);
		// psInfo.addAvp(Avp.CG_ADDRESS, cgAddr, false, false, true);
		// [Called-Station-Id]
		psInfo.addAvp(30, apn, false, false, true);
		// [3GPP-MS-Timezone]
		byte[] tz = { (byte) 0x0C, (byte) 0x00 };

		psInfo.addAvp(Avp.TGPP_MS_TIMEZONE, tz, 10415, true, false);
	}

	/* end of rohit modification */

	public AccountRequest initialACR() throws Exception {
		/* rohit modification */
		SessionInitiateACR(ACC_REQUEST_TYPE_INITIAL);
		/* end of rohit modification */
		return acr;

	}

	public AccountRequest interimACR() throws Exception {

		SessionUpdateACR(ACC_REQUEST_TYPE_INTERIM);
		return acr;

	}

	public AccountRequest terminateACR() throws Exception {

		SessionTerminateACR(ACC_REQUEST_TYPE_TERMINATE);
		return acr;

	}

	public AccountRequest eventCCR() throws Exception {

		defaultACR(ACC_REQUEST_TYPE_EVENT);
		return acr;
	}

}
