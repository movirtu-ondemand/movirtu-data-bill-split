package com.movirtu.diameter.charging.apps.postpaid.client;

import java.net.Inet4Address;

import org.jdiameter.api.Avp;
import org.jdiameter.api.acc.ServerAccSession;

/*stores session related information */
public class SessionInfo {
	public String key;//stores sessionId
	public String sgsnAddress,calledStationId,subscriptionIdDataValue,ggsnAddress,cgAddress;
	public String origHost,destHost;
	//public Inet4Address sgsnAddress;
	//public ServerAccSession serverAccSess;
	public int subscriptionIdTypeValue,recordType,recordNumber,accountInterimInterval;
	public Avp origRealm, destRealm;
	public byte [] tmZone;
	public SessionInfo() {
		System.out.println("inside constructor SessionInfo");

	}
	public SessionInfo(String SessionId) {
		System.out.println("inside constructor SessionInfo");
		key = SessionId;

	}
}
