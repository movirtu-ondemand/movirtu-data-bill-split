package com.movirtu.diameter.charging.apps.common;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import org.dellroad.stuff.net.IPv4Util;
import org.jdiameter.api.Avp;
import org.jdiameter.api.AvpDataException;
import org.jdiameter.api.AvpSet;
import org.jdiameter.api.InternalException;
import org.jdiameter.api.Request;
import org.jdiameter.api.acc.ServerAccSession;
import org.jdiameter.api.acc.events.AccountAnswer;
import org.jdiameter.api.cca.ServerCCASession;
import org.jdiameter.api.cca.events.JCreditControlAnswer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.diameter.charging.apps.B2BGw.server.B2BGwServerAcc;
import com.movirtu.diameter.charging.apps.B2BGw.server.B2BGwServerCCA;

/*stores session related information */
public class TransactionInfo {
	private static final Logger logger = LoggerFactory
			.getLogger(TransactionInfo.class);
	// message objects
	// parameters used in postpaid
	public AccountAnswer answer;
	public String ggsnAddress, cgAddress, sgsnAddress;
	public B2BGwServerAcc serverInstListener;
	public ServerAccSession serverAccSess;
	public byte[] tmZone;
	public boolean isPsInformation; // bitmap purposes
	public int accountInterimInterval, rsuIo, rsuOo, usuIo, usuOo;

	// parameters used both in prepaid and postpaid
	public Request request;// send in ist leg should be responded back while
							// sending response for first leg
	public String key;// stores sessionId generated by jdiameter
	public String userProvidedSessionIdKey;
	public String calledStationId, subscriptionIdDataValue,
			vSubscriptionIdDataValue, origHost, destHostStr, destRealmStr;
	public boolean isVirtualNumber;// if yes than service shall populate
									// vSubscriptionIdDataValue,vSubscriptionIdTypeValue
	public int subscriptionIdTypeValue, recordType, recordNumber,
			vSubscriptionIdTypeValue;
	public int result;
	public Avp origRealm, destRealm, erorMsg, errorReportingHost, failedAvp;

	// parameters used in prepaid
	public B2BGwServerCCA b2bgwServerCCA;
	public ServerCCASession serverCCASession;
	public int creditControlFailureHandling;
	public JCreditControlAnswer prepaidAnswer;
	public int reAuthRequestType,userEquipmentInfoType;
	public boolean isPotentiallyDuplicatePkt;
	public List<MultipleServiceCreditControlClass> msccList = new ArrayList<MultipleServiceCreditControlClass>();
	// Avps used in prepaid

	public Avp chargingId, userName, originStateId, multipleServiceInd,
			framedIpAddress, imsi, pdpType, gprsQosNeg, sgsnAddr, ggsnAddr,
			imsiMccMnc, ggsnMccMnc, nsapi, selectionMode,
			chargingCharacteristics, rat, contextType, bearerUsage, imeiSv,
			ruleBaseId, userLocationInformation;
	public String serviceContextId,userEquipmentInfoDataValue;
	/*-----CCA params---------*/
	public Avp sessionFailover, routeRecord;;

	/*------AvpSet not used------*/
	public AvpSet multipleServiceCreditControl;
	int noOfMultipleServiceCreditControl;
	

	// variables not yet used
	/*
	 * public Avp tarrifTimeChange,totalOctets,ratingGroup,validityTime,
	 * resultSubCode,volumeThreshold,quotaHoldingTime,triggerType; public AvpSet
	 * gsu,trigger;
	 */

	// testing params

	public int sessionIdIntComp;

	public TransactionInfo() {
		// System.out.println("inside constructor SessionInfo");
		key = userProvidedSessionIdKey = sgsnAddress = calledStationId = subscriptionIdDataValue = ggsnAddress = cgAddress = vSubscriptionIdDataValue = origHost = destHostStr = null;
		subscriptionIdTypeValue = vSubscriptionIdTypeValue = 1;
		recordType = 1;
		recordNumber = 0;
		accountInterimInterval = 60;
		isVirtualNumber = true;
		isPsInformation = true;
		rsuIo = rsuOo = usuIo = usuOo = creditControlFailureHandling = reAuthRequestType = -1;
		userEquipmentInfoType = -1;
		rsuIo = rsuOo = usuIo = usuOo = -1;
		chargingId = userName = originStateId = multipleServiceInd = framedIpAddress = imsi = pdpType = gprsQosNeg = sgsnAddr = ggsnAddr = imsiMccMnc = ggsnMccMnc = nsapi = selectionMode = chargingCharacteristics = rat = contextType = bearerUsage = imeiSv = ruleBaseId = sessionFailover = routeRecord = userLocationInformation = erorMsg = errorReportingHost = failedAvp = null;
		serviceContextId = userEquipmentInfoDataValue = null;
		isPotentiallyDuplicatePkt = false;

	}

	public TransactionInfo(String SessionId) {
		// System.out.println("inside constructor SessionInfo");
		key = SessionId;
		userProvidedSessionIdKey = sgsnAddress = calledStationId = subscriptionIdDataValue = ggsnAddress = cgAddress = vSubscriptionIdDataValue = origHost = destHostStr = destRealmStr = null;
		subscriptionIdTypeValue = vSubscriptionIdTypeValue = 1;
		recordType = 1;
		recordNumber = 0;
		userEquipmentInfoType = -1;
		accountInterimInterval = 60;
		isVirtualNumber = false;
		isPsInformation = true;
		rsuIo = rsuOo = usuIo = usuOo = creditControlFailureHandling = reAuthRequestType = -1;
		chargingId = userName = originStateId = multipleServiceInd = framedIpAddress = imsi = pdpType = gprsQosNeg = sgsnAddr = ggsnAddr = imsiMccMnc = ggsnMccMnc = nsapi = selectionMode = chargingCharacteristics = rat = contextType = bearerUsage = imeiSv = ruleBaseId = sessionFailover = routeRecord = userLocationInformation = erorMsg = errorReportingHost = failedAvp = null;
		serviceContextId = userEquipmentInfoDataValue = null;
		isPotentiallyDuplicatePkt = false;

	}

	public void display() throws InternalException {
		try {
			logger.debug(" display");
			logger.debug("sessionId = " + this.key);
			logger.debug("recordType = " + this.recordType);
			logger.debug("recordNumber = " + this.recordNumber);
			logger.debug("serviceContextId = " + this.serviceContextId);
			logger.debug("accountInterimInterval = "
					+ this.accountInterimInterval);
			logger.debug("origHost = " + this.origHost);
			logger.debug("origRealm = " + this.origRealm);
			logger.debug("destRealm = " + this.destRealm);
			logger.debug("destHost = " + this.destHostStr);
			logger.debug("subscriptionIdDataValue = "
					+ this.subscriptionIdDataValue);
			logger.debug("subscriptionIdTypeValue = "
					+ this.subscriptionIdTypeValue);
			logger.debug("calledStationId = " + this.calledStationId);
			logger.debug("ggsnAddress = " + this.ggsnAddress);
			logger.debug("sgsnAddress = " + this.sgsnAddress);
			logger.debug("cgAddress = " + this.cgAddress);
			logger.debug("request = " + this.request);
			logger.debug("serverAccSess = " + this.serverAccSess);
			logger.debug("serverCCASession<interface> = "
					+ this.serverCCASession);
			logger.debug("b2bgwServerCCA<ServerB2BGwCCA instance> = "
					+ this.b2bgwServerCCA);
			logger.debug("userLocationInformation = "
					+ this.userLocationInformation);
			// temporary
			/*this.getChargingChar();
			this.getBearerUsage();
			this.getCalledStationId();
			this.getccfh();
			this.getContextType();
			this.getGgsnAddr();
			this.getGgsnMccMnc();
			this.getImeiSv();
			this.getImsi();
			this.getImsiMccMnc();
			this.getMultipleServiceInd();
			this.getNsapi();
			this.getPdpType();
			this.getChargingId();
			this.getFramedIpAddress();
			this.getMultipleServiceInd();
			this.getOriginStateId();
			this.getRat();
			this.getRecordNumber();
			this.getRecordType();
			this.getSgsnAddr();
			this.getSubscriptionIdDataValue();
			this.getSubscriptionIdTypeValue();
			this.getSelectionMode();
			this.getServiceContextId();
			this.getvSubscriptionIdDataValue();
			this.getvSubscriptionIdTypeValue();
			this.getUserName();*/

		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	public void msccDisplay(MultipleServiceCreditControlClass msccAddOnList) {
		try {
			logger.debug("msccDisplay");
			logger.debug("gsu:tarrifTimeChange = "
					+ msccAddOnList.gsuObj.tariifTimeChange.toString());
			logger.debug("gsu.totalOctets =" + msccAddOnList.gsuObj.totalOctets);
			logger.debug("ratingGroup = " + msccAddOnList.ratingGroup);
			logger.debug("validityTime = " + msccAddOnList.validityTime);
			logger.debug("resultCode =" + msccAddOnList.resultCode);
			logger.debug("volumeQuotaThresholdVoda = "
					+ msccAddOnList.volumeQuotaThresholdVoda);
			// logger.debug("quotaHoldingTimeVoda = " +
			// msccAddOnList.quotaHoldingTimeVoda);
			logger.debug("triggerTypeVoda = " + msccAddOnList.triggerTypeVoda);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getChargingChar() {
		if (this.chargingCharacteristics != null) {
			try {
				/*System.out.println("chargingCharacteristics = "
						+ this.chargingCharacteristics.getUTF8String());*/
				return this.chargingCharacteristics.getUTF8String();
			} catch (AvpDataException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	public int getChargingId() {
		if (this.chargingId != null) {
			try {
				/*System.out.println("chargingId ="
						+ this.chargingId.getInteger32());*/
				return this.chargingId.getInteger32();

			} catch (AvpDataException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return -1;

	}

	public String getUserName() {
		if (this.userName != null) {
			try {
				//System.out.println("userName=" + this.userName.getUTF8String());
				return this.userName.getUTF8String();
			} catch (AvpDataException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return null;
	}

	public long getOriginStateId() {
		if (this.originStateId != null) {
			try {
				/*System.out.println("originStateId="
						+ this.originStateId.getUnsigned32());*/
				return this.originStateId.getUnsigned32();
			} catch (AvpDataException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return -1;
	}

	public int getMultipleServiceInd() {
		if (this.multipleServiceInd != null) {
			try {
				/*System.out.println("multipleServiceInd="
						+ this.multipleServiceInd.getInteger32());*/
				return this.multipleServiceInd.getInteger32();
			} catch (AvpDataException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return -1;
	}

	public String getFramedIpAddress() throws AvpDataException {
		if (this.framedIpAddress != null) {
			/*System.out.println("framedIpAddress="
					+ IPv4Util.toString(IPv4Util.toAddress(this.framedIpAddress
							.getRawData())));*/
			return IPv4Util.toString(IPv4Util.toAddress(this.framedIpAddress
					.getRawData()));

		}
		return null;
	}

	public String getImsi() {
		if (this.imsi != null) {
			try {
				//System.out.println("imsi=" + this.imsi.getUTF8String());
				return this.imsi.getUTF8String();
			} catch (AvpDataException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return null;
	}

	public int getPdpType() {
		if (this.pdpType != null) {
			try {
				//System.out.println("pdpType=" + this.pdpType.getInteger32());
				return this.pdpType.getInteger32();
			} catch (AvpDataException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return -1;
	}

	public String getGprsQosNeg() {
		if (this.gprsQosNeg != null) {
			try {
				/*System.out.println("gprsQosNeg="
						+ this.gprsQosNeg.getUTF8String());*/
				return this.gprsQosNeg.getUTF8String();
			} catch (AvpDataException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return null;
	}

	public String getSgsnAddr() {
		if (this.sgsnAddr != null) {
			try {
				InetAddress ip = this.sgsnAddr.getAddress();
				//System.out.println("sgsnAddr="+ip.toString());
				return ip.toString();	
			
			/*byte sga[] = this.sgsnAddr.getRawData();
			System.out.println("sgsnAddr="
					+ IPv4Util.toString(IPv4Util.toAddress(sga[2])));
			return IPv4Util.toString(IPv4Util.toAddress(sga[2]));*/
				
			} catch (AvpDataException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public String getGgsnAddr() {
		if (this.ggsnAddr != null) {
			try {
				InetAddress ip = this.ggsnAddr.getAddress();
				/*System.out.println("ggsnAddr="+ip.toString());*/
				return ip.toString();
			} catch (AvpDataException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/*byte[] gga = this.ggsnAddr.getRawData(); 
			for (byte b:gga) {
				int i = 0;
				System.out.println("byte["+i+"]="+b);
				i++;
			}
			System.out.println("ggsnAddr="
					+ IPv4Util.toString(IPv4Util.toAddress(gga[2])));
			return IPv4Util.toString(IPv4Util.toAddress(gga[2]));*/
		}
		return null;
	}
	
	public String getImsiMccMnc() {
		if (this.imsiMccMnc != null) {
			try {
				/*System.out.println("imsiMccMnc="
						+ this.imsiMccMnc.getUTF8String());*/
				return this.imsiMccMnc.getUTF8String();
			} catch (AvpDataException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return null;
	}
	public String getGgsnMccMnc() {
		if (this.ggsnMccMnc != null) {
			try {
				/*System.out.println("ggsnMccMnc="
						+ this.ggsnMccMnc.getUTF8String());*/
				return this.ggsnMccMnc.getUTF8String();
			} catch (AvpDataException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return null;
	}
	public String getNsapi() {
		if (this.nsapi != null) {
			try {
				/*System.out.println("nsapi="
						+ this.nsapi.getUTF8String());*/
				return this.nsapi.getUTF8String();
			} catch (AvpDataException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return null;
	}
	public String getSelectionMode() {
		if (this.selectionMode != null) {
			try {
				/*System.out.println("selectionMode="
						+ this.selectionMode.getUTF8String());*/
				return this.selectionMode.getUTF8String();
			} catch (AvpDataException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return null;
	}
	
	public int getRat() {
		if (this.rat != null) {
			try {
				//System.out.println("rat=" + this.rat.getInteger32());
				return this.rat.getInteger32();
			} catch (AvpDataException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return -1;
	}
	public int getContextType() {
		if (this.contextType != null) {
			try {
				//System.out.println("contextType=" + this.contextType.getInteger32());
				return this.contextType.getInteger32();
			} catch (AvpDataException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return -1;
	}
	public int getBearerUsage() {
		if (this.bearerUsage != null) {
			try {
				//System.out.println("bearerUsage=" + this.bearerUsage.getInteger32());
				return this.bearerUsage.getInteger32();
			} catch (AvpDataException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return -1;
	}
	public String getImeiSv() {
		if (this.imeiSv != null) {
			try {
				/*System.out.println("imeiSv="
						+ this.imeiSv.getUTF8String());*/
				return this.imeiSv.getUTF8String();
			} catch (AvpDataException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return null;
	}
	
	public String getRuleBaseId() {
		if (this.ruleBaseId != null) {
			try {
				/*System.out.println("ruleBaseId="
						+ this.ruleBaseId.getUTF8String());*/
				return this.ruleBaseId.getUTF8String();
			} catch (AvpDataException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return null;
	}
	public String getServiceContextId() {
		if (this.serviceContextId != null) {
			//System.out.println("serviceContextId=" + this.serviceContextId);
			return this.serviceContextId;
		}
		return null;
	}
	public int getccfh() {
		System.out.println("creditControlFailureHandling=" + this.creditControlFailureHandling);
		return this.creditControlFailureHandling;
	}
	public int getRecordType() {
		//System.out.println("requestType=" + this.recordType);
		return this.recordType;
	}
	public int getRecordNumber() {
		//System.out.println("recordNumber=" + this.recordNumber);
		return this.recordNumber;
	}
	public String getCalledStationId() {
		//System.out.println("calledStationId=" + this.calledStationId);
		return this.calledStationId;
	}
	public String getSubscriptionIdDataValue() {
		//System.out.println("getSubscriptionIdDataValue=" + this.subscriptionIdDataValue);
		return this.subscriptionIdDataValue;
	}
	
	public int getSubscriptionIdTypeValue() {
		//System.out.println("subscriptionIdTypeValue=" + this.subscriptionIdTypeValue);
		return this.subscriptionIdTypeValue;
	}
	public String getvSubscriptionIdDataValue() {
		//System.out.println("getvSubscriptionIdDataValue=" + this.vSubscriptionIdDataValue);
		return this.vSubscriptionIdDataValue;
	}
	
	public int getvSubscriptionIdTypeValue() {
		//System.out.println("getvSubscriptionIdTypeValue=" + this.vSubscriptionIdTypeValue);
		return this.vSubscriptionIdTypeValue;
	}
	
}
