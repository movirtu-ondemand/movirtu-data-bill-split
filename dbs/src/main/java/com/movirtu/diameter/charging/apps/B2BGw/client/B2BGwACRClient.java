package com.movirtu.diameter.charging.apps.B2BGw.client;

import java.util.List;

import org.jdiameter.api.ApplicationId;
import org.jdiameter.api.IllegalDiameterStateException;
import org.jdiameter.api.InternalException;
import org.jdiameter.api.OverloadException;
import org.jdiameter.api.Peer;
import org.jdiameter.api.PeerState;
import org.jdiameter.api.PeerTable;
import org.jdiameter.api.RouteException;
import org.jdiameter.api.acc.ClientAccSession;
import org.jdiameter.api.acc.events.AccountAnswer;
import org.jdiameter.api.acc.events.AccountRequest;
import org.jdiameter.api.app.AppAnswerEvent;
import org.jdiameter.api.app.AppRequestEvent;
import org.jdiameter.api.app.AppSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.diameter.charging.apps.common.HashMapDS;
import com.movirtu.diameter.charging.apps.common.TransactionInfo;
import com.movirtu.diameter.charging.apps.common.Utils;
import com.movirtu.diameter.test.b2bgw.StoreGenSessionId;
import com.movirtu.diameter.test.b2bgw.TestClientB2BGw;
import com.movirtu.diameter.test.b2bgw.ServiceProcessingDiameterMessages;
import com.movirtu.diameter.test.b2bgw.ServiceProcessingDiameterMessagesImpl;

public class B2BGwACRClient extends B2BGwBaseClientAcc {
	private static final Logger logger = LoggerFactory
			.getLogger(B2BGwACRClient.class);
	protected boolean sentINITIAL;
	protected boolean sentINTERIM;
	protected boolean sentTERMINATE;
	protected boolean sentEVENT;
	protected boolean receiveINITIAL;
	protected boolean receiveINTERIM;
	protected boolean receiveTERMINATE;
	protected boolean receiveEVENT;
	protected ServiceProcessingDiameterMessages serviceProcDiamMsgClient;
	public B2BGwACRClient() {

	}

	private Peer getRemotePeer() throws Exception {
		Peer remotepeer = null;
		List<Peer> peers = getStack().unwrap(PeerTable.class).getPeerTable();
		boolean foundConnected = false;
		if (peers.size() >= 1) {
			for (Peer p : peers) {

				if (p.getState(PeerState.class).equals(PeerState.OKAY)) {
					foundConnected = true;
					remotepeer = p;// use 1st connected peer i.e. overwrite
									// redundancy mode (to-do: other redundancy
									// modes)
					break;
				}
			}

		}

		if (!foundConnected)
			throw new Exception("no connected peer ...unable to send ACR");

		return remotepeer;
	}

	
	public String sendInitial(TransactionInfo obj) throws Exception {
		
		logger.debug(" inside sendInitial B2BGwACRClient");
		try {
		obj.display();
		String sessionID = super.createAccSession(obj.userProvidedSessionIdKey);
		ClientAccSession accsession = super.fetchSession(sessionID);
		Peer localpeer = getStack().getMetaData().getLocalPeer();
		Peer remotepeer = getRemotePeer();

		if (TestClientB2BGw.isTest == true) {
			StoreGenSessionId sessIDStorageObj = new StoreGenSessionId();
			sessIDStorageObj.SessionId = accsession.getSessionId();
			// SessionInfo obj = new SessionInfo(completeSessionId);
			/*
			 * creates a mapping between the userProvidedSessionIdKeyand obj
			 * that contains the sessionId sent in diameter messages
			 */
			HashMapDS.INSTANCE.insertInHash(obj.userProvidedSessionIdKey,
					sessIDStorageObj);
		}

		// private ApplicationId(long vendorId, long authAppId, long acctAppId)
		// createByAuthAppId(long vendorId, long authAppId) {
		// ApplicationId createByAccAppId(long vendorId, long acchAppId) {
		/* temp comment */
		// ApplicationId applicationId = ApplicationId.createByAuthAppId(0, 4);
		/* end of temp comment */
		/* temp change */
		ApplicationId applicationId = ApplicationId.createByAccAppId(0, 3);
		logger.debug("inside B2BGwACRClient.java" + applicationId);
		/* end of temp change */
		// ApplicationId applicationId = null;
		// considering single application only for time-being
		for (ApplicationId appId : remotepeer.getCommonApplications()) {
			// System.out.println("AppicationId= " + appId);
			// applicationId = appId;
			break;

		}

		AccountRequest acr = new B2BGwACRBuilder(accsession, applicationId,
				localpeer.getRealmName(), remotepeer.getRealmName(), localpeer
						.getUri().getFQDN(), remotepeer.getUri().getFQDN(), obj)
				.initialACR();
		logger.debug("AppicationId= " + applicationId + " ClientRealmName= "
				+ localpeer.getRealmName() + " ClientURI="
				+ localpeer.getUri().toString() + " ServerURI="
				+ remotepeer.getUri().toString() + " ServerRealmName = "
				+ remotepeer.getRealmName());
		accsession.sendAccountRequest(acr);
		Utils.printMessage(log, super.stack.getDictionary(), acr.getMessage(),
				true);

		this.sentINITIAL = true;
		return sessionID;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String sendInterim(TransactionInfo obj) throws Exception {

		try {
		ClientAccSession accsession;
		if (TestClientB2BGw.isTest == true) {
			StoreGenSessionId genSessionIdOobj = HashMapDS.INSTANCE
					.searchInHash(obj.userProvidedSessionIdKey);
			accsession = super.fetchSession(genSessionIdOobj.SessionId);
		} else {
			accsession = super.fetchSession(obj.key);
		}
		Peer localpeer = getStack().getMetaData().getLocalPeer();
		Peer remotepeer = getRemotePeer();
		String completeSessionId = accsession.getSessionId();
		logger.debug("session Id = " + completeSessionId);
		// private ApplicationId(long vendorId, long authAppId, long acctAppId)
		// createByAuthAppId(long vendorId, long authAppId) {
		// ApplicationId createByAccAppId(long vendorId, long acchAppId) {

		/* temp comment */
		// ApplicationId applicationId = ApplicationId.createByAuthAppId(0, 4);
		/* end of temp comment */
		/* temp change */
		ApplicationId applicationId = ApplicationId.createByAccAppId(0, 3);
		logger.debug("inside B2BGwACRClient.java sendInterim" + applicationId);
		/* end of temp change */
		
		for (ApplicationId appId : remotepeer.getCommonApplications()) {
			logger.debug("AppicationId= " + appId);
			// applicationId = appId;
			break;

		}

		AccountRequest acr = new B2BGwACRBuilder(accsession, applicationId,
				localpeer.getRealmName(), remotepeer.getRealmName(), localpeer
						.getUri().getFQDN(), remotepeer.getUri().getFQDN(), obj)
				.interimACR();
		logger.debug("AppicationId= " + applicationId + " ClientRealmName= "
				+ localpeer.getRealmName() + " ClientURI="
				+ localpeer.getUri().toString() + " ServerURI="
				+ remotepeer.getUri().toString() + " ServerRealmName = "
				+ remotepeer.getRealmName());
		accsession.sendAccountRequest(acr);
		Utils.printMessage(log, super.stack.getDictionary(), acr.getMessage(),
				true);

		this.sentINTERIM = true;
		return accsession.getSessionId();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String sendTerminate(TransactionInfo obj) throws Exception {

		try {
		ClientAccSession accsession;
		if (TestClientB2BGw.isTest == true) {
			StoreGenSessionId genSessionIdOobj = HashMapDS.INSTANCE
					.searchInHash(obj.userProvidedSessionIdKey);
			accsession = super.fetchSession(genSessionIdOobj.SessionId);
		} else {
			accsession = super.fetchSession(obj.key);
		}
		Peer localpeer = getStack().getMetaData().getLocalPeer();
		Peer remotepeer = getRemotePeer();
		String completeSessionId = accsession.getSessionId();
		logger.debug("session Id = " + completeSessionId);
		// private ApplicationId(long vendorId, long authAppId, long acctAppId)
		// createByAuthAppId(long vendorId, long authAppId) {
		// ApplicationId createByAccAppId(long vendorId, long acchAppId) {

		/* temp comment */
		// ApplicationId applicationId = ApplicationId.createByAuthAppId(0, 4);
		/* end of temp comment */
		/* temp change */
		ApplicationId applicationId = ApplicationId.createByAccAppId(0, 3);
		logger.debug("inside B2BGwACRClient.java sendTerminate" + applicationId);
		/* end of temp change */
		// ApplicationId applicationId = null;
		// considering single application only for time-being
		for (ApplicationId appId : remotepeer.getCommonApplications()) {
			logger.debug("AppicationId= " + appId);
			// applicationId = appId;
			break;

		}

		AccountRequest acr = new B2BGwACRBuilder(accsession, applicationId,
				localpeer.getRealmName(), remotepeer.getRealmName(), localpeer
						.getUri().getFQDN(), remotepeer.getUri().getFQDN(), obj)
				.terminateACR();
		logger.debug("AppicationId= " + applicationId + " ClientRealmName= "
				+ localpeer.getRealmName() + " ClientURI="
				+ localpeer.getUri().toString() + " ServerURI="
				+ remotepeer.getUri().toString() + " ServerRealmName = "
				+ remotepeer.getRealmName());
		accsession.sendAccountRequest(acr);
		Utils.printMessage(log, super.stack.getDictionary(), acr.getMessage(),
				true);

		this.sentINTERIM = true;
		return accsession.getSessionId();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	public TransactionInfo parseACA(AccountAnswer answer)
			throws InternalException {
		try {
			TransactionInfo obj = new TransactionInfo();
			obj.key = answer.getMessage().getSessionId();
			obj.recordType = answer.getAccountingRecordType();
			obj.recordNumber = (int) answer.getAccountingRecordNumber();
			obj.result = answer.getResultCodeAvp().getInteger32();
			//need to extract subscription Id in case of virtual subscriber
			return obj;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.jdiameter.api.acc.ClientAccSessionListener#doAccAnswerEvent(
	 * org.jdiameter.api.acc.ClientAccSession,
	 * org.jdiameter.api.acc.events.AccountRequest,
	 * org.jdiameter.api.acc.events.AccountAnswer)
	 */
	public void doAccAnswerEvent(ClientAccSession session,
			AccountRequest request, AccountAnswer answer)
			throws InternalException, IllegalDiameterStateException,
			RouteException, OverloadException,NullPointerException {
		try {
			TransactionInfo obj;

			if ((obj = parseACA(answer)) != null) {
				obj.answer = answer;
				logger.debug("session id = " + obj.key + " result=>"
						+ obj.result);

				Utils.printMessage(log, super.stack.getDictionary(),
						answer.getMessage(), false);
				switch (obj.recordType) {
				case B2BGwACRBuilder.ACC_REQUEST_TYPE_INITIAL:
					if (receiveINITIAL) {
						fail("Received INITIAL more than once!", null);
					}
					receiveINITIAL = true;

					break;
				case B2BGwACRBuilder.ACC_REQUEST_TYPE_INTERIM:
					if (receiveINTERIM) {
						fail("Received INTERIM more than once!", null);
					}
					receiveINTERIM = true;

					break;
				case B2BGwACRBuilder.ACC_REQUEST_TYPE_TERMINATE:
					if (receiveTERMINATE) {
						fail("Received TERMINATE more than once!", null);
					}
					receiveTERMINATE = true;

					break;
				case B2BGwACRBuilder.ACC_REQUEST_TYPE_EVENT:
					if (receiveEVENT) {
						fail("Received EVENT more than once!", null);
					}
					receiveEVENT = true;
					break;

				default:

				}
				//durgesh method invocation
				if(serviceProcDiamMsgClient ==null) {
					serviceProcDiamMsgClient = new ServiceProcessingDiameterMessagesImpl();
				}
				serviceProcDiamMsgClient.serviceProcessingACA(obj);
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jdiameter.api.cca.ClientCCASessionListener#doReAuthRequest(org.jdiameter
	 * .api.cca.ClientCCASession, org.jdiameter.api.auth.events.ReAuthRequest)
	 */
	/*
	 * public void doReAuthRequest(ClientCCASession session, ReAuthRequest
	 * request) throws InternalException, IllegalDiameterStateException,
	 * RouteException, OverloadException {
	 * fail("Received \"ReAuthRequest\" event, request[" + request +
	 * "], on session[" + session + "]", null); }
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jdiameter.api.cca.ClientCCASessionListener#doOtherEvent(org.jdiameter
	 * .api.app.AppSession, org.jdiameter.api.app.AppRequestEvent,
	 * org.jdiameter.api.app.AppAnswerEvent)
	 */

	public void doOtherEvent(AppSession session, AppRequestEvent request,
			AppAnswerEvent answer) throws InternalException,
			IllegalDiameterStateException, RouteException, OverloadException {
		fail("Received \"Other\" event, request[" + request + "], answer["
				+ answer + "], on session[" + session + "]", null);

	}

	// ------------ getters for some vars;

	public boolean isSentINITIAL() {
		return sentINITIAL;
	}

	public boolean isSentEVENT() {
		return sentEVENT;
	}

	public boolean isReceiveEVENT() {
		return receiveEVENT;
	}

	public boolean isSentINTERIM() {
		return sentINTERIM;
	}

	public boolean isSentTERMINATE() {
		return sentTERMINATE;
	}

	public boolean isReceiveINITIAL() {
		return receiveINITIAL;
	}

	public boolean isReceiveINTERIM() {
		return receiveINTERIM;
	}

	public boolean isReceiveTERMINATE() {
		return receiveTERMINATE;
	}

}
