package com.movirtu.diameter.charging.apps.common;

import org.jdiameter.api.Message;
import org.jdiameter.api.cca.ClientCCASession;

import com.movirtu.diameter.charging.apps.common.TransactionInfo;

public interface PrepaidServiceProcessingDiameterMessages {
	
	//prepaid
	public void serviceProcessingCCR(TransactionInfo obj);
	public void serviceProcessingCCA(TransactionInfo obj);
	public void serviceProcessingRAR(TransactionInfo obj);
	public void serviceProcessingRAA(TransactionInfo obj);
	//triggered from server class
	public void sessionSupervisionTimerExpiredNotification(String sessionId);
	public void sessionSupervisionTimerStartedNotification(String sessionId);
	public void sessionSupervisionTimerRestartedNotification(String sessionId);
	public void sessionSupervisionTimerStoppedNotification(String sessionId);
	//triggered from client class
	public void txTimerExpiredNotification(String sessionId); 
	public void grantAccessOnDeliverFailureNotification(
			String sessionId, Message request); 
	public void denyAccessOnDeliverFailureNotification(
			String sessionId, Message request);
	public void grantAccessOnTxExpireNotification(String sessionId); 
	public void denyAccessOnTxExpireNotification(String sessionId); 
	public void grantAccessOnFailureMessageNotification(
			String sessionId); 
	public void denyAccessOnFailureMessageNotification(String sessionId); 
	public void indicateServiceErrorNotification(String sessionId); 

}
