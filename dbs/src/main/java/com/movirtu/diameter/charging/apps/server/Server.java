/*
 * JBoss, Home of Professional Open Source
 * Copyright 2011, Red Hat, Inc. and/or its affiliates, and individual
 * contributors as indicated by the @authors tag. All rights reserved.
 * See the copyright.txt in the distribution for a full listing
 * of individual contributors.
 * 
 * This copyrighted material is made available to anyone wishing to use,
 * modify, copy, or redistribute it subject to the terms and conditions
 * of the GNU General Public License, v. 2.0.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License,
 * v. 2.0 along with this distribution; if not, write to the Free 
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
package com.movirtu.diameter.charging.apps.server;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.jdiameter.api.Answer;
import org.jdiameter.api.Avp;
import org.jdiameter.api.AvpSet;
import org.jdiameter.api.IllegalDiameterStateException;
import org.jdiameter.api.InternalException;
import org.jdiameter.api.NetworkReqListener;
import org.jdiameter.api.OverloadException;
import org.jdiameter.api.Request;
import org.jdiameter.api.RouteException;
import org.jdiameter.api.app.AppAnswerEvent;
import org.jdiameter.api.app.AppRequestEvent;
import org.jdiameter.api.app.AppSession;
import org.jdiameter.api.auth.events.ReAuthAnswer;
import org.jdiameter.api.auth.events.ReAuthRequest;
import org.jdiameter.api.cca.ServerCCASession;
import org.jdiameter.api.cca.events.JCreditControlAnswer;
import org.jdiameter.api.cca.events.JCreditControlRequest;
import org.jdiameter.client.api.ISessionFactory;
import org.jdiameter.common.impl.app.cca.JCreditControlAnswerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.diameter.charging.apps.client.CCRController;
import com.movirtu.diameter.charging.apps.common.Utils;
import com.movirtu.diameter.test.TestServer;

/**
 * Base implementation of Server
 * 
 * @author <a href="mailto:brainslog@gmail.com"> Alexandre Mendonca </a>
 * @author <a href="mailto:baranowb@gmail.com"> Bartosz Baranowski </a>
 */
public class Server extends AbstractServer {

	protected boolean sentINITIAL;
	protected boolean sentINTERIM;
	protected boolean sentTERMINATE;
	protected boolean sentEVENT;
	protected boolean receiveINITIAL;
	protected boolean receiveINTERIM;
	protected boolean receiveTERMINATE;
	protected boolean receiveEVENT;

	protected JCreditControlRequest request;
	protected Logger logger = LoggerFactory.getLogger(Server.class);

	// ------- send methods to trigger answer

	public void sendInitial() throws Exception {
		if (!this.receiveINITIAL || this.request == null) {
			fail("Did not receive INITIAL or answer already sent.", null);
			throw new Exception("Request: " + this.request);
		}

		try {
			logger.debug("inside sendInitial Server.java");
			JCreditControlAnswer answer = new JCreditControlAnswerImpl(
					(Request) request.getMessage(), 2001);

			AvpSet reqSet = request.getMessage().getAvps();

			AvpSet set = answer.getMessage().getAvps();
			set.removeAvp(Avp.DESTINATION_HOST);
			set.removeAvp(Avp.DESTINATION_REALM);
			set.addAvp(reqSet.getAvp(Avp.CC_REQUEST_TYPE),
					reqSet.getAvp(Avp.CC_REQUEST_NUMBER),
					reqSet.getAvp(Avp.AUTH_APPLICATION_ID));

			// CC-Session-Failover enumerated

			// failover-supported
			set.addAvp(Avp.CC_SESSION_FAILOVER, 1, true, false, true);

			// Service-Context-Id

			// [Multiple-Services-Credit-Control-1]
			if (TestServer.ratingGroup1 == 1) {
				AvpSet msccAvp1 = set.addGroupedAvp(
						Avp.MULTIPLE_SERVICES_CREDIT_CONTROL, true, false);

				// Granted-Service-Unit
				AvpSet gsuAvp = msccAvp1.addGroupedAvp(
						Avp.GRANTED_SERVICE_UNIT, true, false);

				// Tariff-Time-Change time type
				Calendar c = new GregorianCalendar(1900, 0, 1);

				c.setTimeZone(TimeZone.getTimeZone("UTC"));

				long timestamp = (System.currentTimeMillis() - c
						.getTimeInMillis()) / 1000;

				gsuAvp.addAvp(Avp.TARIFF_TIME_CHANGE, timestamp, true, false,
						true);

				// CC-Total-Octets unsigned64
				long totalOct = 2147482624;
				gsuAvp.addAvp(Avp.CC_TOTAL_OCTETS, totalOct, true, false, false);

				// Rating-Group unsigned32
				msccAvp1.addAvp(Avp.RATING_GROUP, 0, true, false, true);

				// Validity-Time unsigned32
				
				msccAvp1.addAvp(Avp.VALIDITY_TIME, TestServer.validityTime, true, false,
						true);
				// Result-Code
				int resultCode = 2001;
				msccAvp1.addAvp(Avp.RESULT_CODE, resultCode, true, false, true);

				// Volume-Quota-Threshold
				int volQotaThresh = 2621440;
				msccAvp1.addAvp(268, volQotaThresh, 12645, true, false, true);

				// Quota-Holding-Time
				int quotaHoldTime = 800;
				byte[] holdTime = { (byte) 0x00, (byte) 0x00, (byte) 0x03,
						(byte) 0x20 };
				System.out.println("holdtime changes");
				msccAvp1.addAvp(258, holdTime, 12645, true, false);

				// Trigger
				AvpSet trigger = msccAvp1
						.addGroupedAvp(265, 12645, true, false);
				// Trigger-Type
				trigger.addAvp(266, 1, 12645, true, false, true);
			}// end of mscc 1

			// [Multiple-Services-Credit-Control-2]
			if (TestServer.ratingGroup2 == 1) {
				AvpSet msccAvp2 = set.addGroupedAvp(
						Avp.MULTIPLE_SERVICES_CREDIT_CONTROL, true, false);

				// Granted-Service-Unit
				AvpSet gsuAvp = msccAvp2.addGroupedAvp(
						Avp.GRANTED_SERVICE_UNIT, true, false);

				// Tariff-Time-Change time type
				Calendar c = new GregorianCalendar(1900, 0, 1);

				c.setTimeZone(TimeZone.getTimeZone("UTC"));

				long timestamp = (System.currentTimeMillis() - c
						.getTimeInMillis()) / 1000;

				gsuAvp.addAvp(Avp.TARIFF_TIME_CHANGE, timestamp, true, false,
						true);

				// CC-Total-Octets unsigned64
				long totalOct = 2147482624;
				gsuAvp.addAvp(Avp.CC_TOTAL_OCTETS, totalOct, true, false, false);

				// Rating-Group unsigned32
				msccAvp2.addAvp(Avp.RATING_GROUP, 1, true, false, true);

				// Validity-Time unsigned32
				
				msccAvp2.addAvp(Avp.VALIDITY_TIME, TestServer.validityTime, true, false,
						true);
				// Result-Code
				int resultCode = 2001;
				msccAvp2.addAvp(Avp.RESULT_CODE, resultCode, true, false, true);

				// Volume-Quota-Threshold
				int volQotaThresh = 2621440;
				msccAvp2.addAvp(268, volQotaThresh, 12645, true, false, true);

				// Quota-Holding-Time
				int quotaHoldTime = 800;
				byte[] holdTime = { (byte) 0x00, (byte) 0x00, (byte) 0x03,
						(byte) 0x20 };
				System.out.println("holdtime changes");
				msccAvp2.addAvp(258, holdTime, 12645, true, false);

				// Trigger
				AvpSet trigger = msccAvp2
						.addGroupedAvp(265, 12645, true, false);
				// Trigger-Type
				trigger.addAvp(266, 1, 12645, true, false, true);
			}// end of mscc 2

			// [Multiple-Services-Credit-Control-3]
			if (TestServer.ratingGroup3 == 1) {
				AvpSet msccAvp3 = set.addGroupedAvp(
						Avp.MULTIPLE_SERVICES_CREDIT_CONTROL, true, false);

				// Granted-Service-Unit
				AvpSet gsuAvp = msccAvp3.addGroupedAvp(
						Avp.GRANTED_SERVICE_UNIT, true, false);

				// Tariff-Time-Change time type
				Calendar c = new GregorianCalendar(1900, 0, 1);

				c.setTimeZone(TimeZone.getTimeZone("UTC"));

				long timestamp = (System.currentTimeMillis() - c
						.getTimeInMillis()) / 1000;

				gsuAvp.addAvp(Avp.TARIFF_TIME_CHANGE, timestamp, true, false,
						true);

				// CC-Total-Octets unsigned64
				long totalOct = 2147482624;
				gsuAvp.addAvp(Avp.CC_TOTAL_OCTETS, totalOct, true, false, false);

				// Rating-Group unsigned32
				msccAvp3.addAvp(Avp.RATING_GROUP, 2, true, false, true);

				// Validity-Time unsigned32
				int validityTime = 900;
				msccAvp3.addAvp(Avp.VALIDITY_TIME, TestServer.validityTime, true, false,
						true);
				// Result-Code
				int resultCode = 2001;
				msccAvp3.addAvp(Avp.RESULT_CODE, resultCode, true, false, true);

				// Volume-Quota-Threshold
				int volQotaThresh = 2621440;
				msccAvp3.addAvp(268, volQotaThresh, 12645, true, false, true);

				// Quota-Holding-Time
				int quotaHoldTime = 800;
				byte[] holdTime = { (byte) 0x00, (byte) 0x00, (byte) 0x03,
						(byte) 0x20 };
				//System.out.println("holdtime changes");
				msccAvp3.addAvp(258, holdTime, 12645, true, false);

				// Trigger
				AvpSet trigger = msccAvp3
						.addGroupedAvp(265, 12645, true, false);
				// Trigger-Type
				trigger.addAvp(266, 1, 12645, true, false, true);
			}

			// [CCFH]

			set.addAvp(Avp.CREDIT_CONTROL_FAILURE_HANDLING, 1, false, false);

			// Route-Record
			String routeRec = "AH-OCS-OCS1";
			set.addAvp(Avp.ROUTE_RECORD, routeRec, true, false, true);

			// Rulebase-Id

			String ruleBaseId = "GPd_H_pp";
			set.addAvp(262, ruleBaseId, 12645, true, false, true);

			super.serverCCASession.sendCreditControlAnswer(answer);

			sentINITIAL = true;
			request = null;
			Utils.printMessage(log, super.stack.getDictionary(),
					answer.getMessage(), true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sendInterim() throws Exception {
		if (!this.receiveINTERIM || this.request == null) {
			fail("Did not receive INTERIM or answer already sent.", null);
			throw new Exception("Request: " + this.request);
		}
		try {
			logger.debug("inside sendInterim Server.java");
			JCreditControlAnswerImpl answer = new JCreditControlAnswerImpl(
					(Request) request.getMessage(), 2001);

			AvpSet reqSet = request.getMessage().getAvps();

			AvpSet set = answer.getMessage().getAvps();
			set.removeAvp(Avp.DESTINATION_HOST);
			set.removeAvp(Avp.DESTINATION_REALM);
			set.addAvp(reqSet.getAvp(Avp.CC_REQUEST_TYPE),
					reqSet.getAvp(Avp.CC_REQUEST_NUMBER),
					reqSet.getAvp(Avp.AUTH_APPLICATION_ID));

			// CC-Session-Failover enumerated

			// failover-supported
			set.addAvp(Avp.CC_SESSION_FAILOVER, 1, true, false, true);

			// Service-Context-Id

			// [Multiple-Services-Credit-Control-1]
			if (TestServer.ratingGroup1 == 1) {
				AvpSet msccAvp1 = set.addGroupedAvp(
						Avp.MULTIPLE_SERVICES_CREDIT_CONTROL, true, false);

				// Granted-Service-Unit
				AvpSet gsuAvp = msccAvp1.addGroupedAvp(
						Avp.GRANTED_SERVICE_UNIT, true, false);

				// Tariff-Time-Change time type
				Calendar c = new GregorianCalendar(1900, 0, 1);

				c.setTimeZone(TimeZone.getTimeZone("UTC"));

				long timestamp = (System.currentTimeMillis() - c
						.getTimeInMillis()) / 1000;

				gsuAvp.addAvp(Avp.TARIFF_TIME_CHANGE, timestamp, true, false,
						true);

				// CC-Total-Octets unsigned64
				long totalOct = 2147482624;
				gsuAvp.addAvp(Avp.CC_TOTAL_OCTETS, totalOct, true, false, false);

				// Rating-Group unsigned32
				msccAvp1.addAvp(Avp.RATING_GROUP, 0, true, false, true);

				// Validity-Time unsigned32
				
				msccAvp1.addAvp(Avp.VALIDITY_TIME, TestServer.validityTime, true, false,
						true);
				// Result-Code
				int resultCode = 2001;
				msccAvp1.addAvp(Avp.RESULT_CODE, resultCode, true, false, true);

				// Volume-Quota-Threshold
				int volQotaThresh = 2621440;
				msccAvp1.addAvp(268, volQotaThresh, 12645, true, false, true);

				// Quota-Holding-Time
				int quotaHoldTime = 800;
				byte[] holdTime = { (byte) 0x00, (byte) 0x00, (byte) 0x03,
						(byte) 0x20 };
				System.out.println("holdtime changes");
				msccAvp1.addAvp(258, holdTime, 12645, true, false);

				// Trigger
				AvpSet trigger = msccAvp1
						.addGroupedAvp(265, 12645, true, false);
				// Trigger-Type
				trigger.addAvp(266, 1, 12645, true, false, true);
			}// end of mscc 1

			// [Multiple-Services-Credit-Control-2]
			if (TestServer.ratingGroup2 == 1) {
				AvpSet msccAvp2 = set.addGroupedAvp(
						Avp.MULTIPLE_SERVICES_CREDIT_CONTROL, true, false);

				// Granted-Service-Unit
				AvpSet gsuAvp = msccAvp2.addGroupedAvp(
						Avp.GRANTED_SERVICE_UNIT, true, false);

				// Tariff-Time-Change time type
				Calendar c = new GregorianCalendar(1900, 0, 1);

				c.setTimeZone(TimeZone.getTimeZone("UTC"));

				long timestamp = (System.currentTimeMillis() - c
						.getTimeInMillis()) / 1000;

				gsuAvp.addAvp(Avp.TARIFF_TIME_CHANGE, timestamp, true, false,
						true);

				// CC-Total-Octets unsigned64
				long totalOct = 2147482624;
				gsuAvp.addAvp(Avp.CC_TOTAL_OCTETS, totalOct, true, false, false);

				// Rating-Group unsigned32
				msccAvp2.addAvp(Avp.RATING_GROUP, 1, true, false, true);

				// Validity-Time unsigned32
				
				msccAvp2.addAvp(Avp.VALIDITY_TIME, TestServer.validityTime, true, false,
						true);
				// Result-Code
				int resultCode = 2001;
				msccAvp2.addAvp(Avp.RESULT_CODE, resultCode, true, false, true);

				// Volume-Quota-Threshold
				int volQotaThresh = 2621440;
				msccAvp2.addAvp(268, volQotaThresh, 12645, true, false, true);

				// Quota-Holding-Time
				int quotaHoldTime = 800;
				byte[] holdTime = { (byte) 0x00, (byte) 0x00, (byte) 0x03,
						(byte) 0x20 };
				System.out.println("holdtime changes");
				msccAvp2.addAvp(258, holdTime, 12645, true, false);

				// Trigger
				AvpSet trigger = msccAvp2
						.addGroupedAvp(265, 12645, true, false);
				// Trigger-Type
				trigger.addAvp(266, 1, 12645, true, false, true);
			}// end of mscc 2

			// [Multiple-Services-Credit-Control-3]
			if (TestServer.ratingGroup3 == 1) {
				AvpSet msccAvp3 = set.addGroupedAvp(
						Avp.MULTIPLE_SERVICES_CREDIT_CONTROL, true, false);

				// Granted-Service-Unit
				AvpSet gsuAvp = msccAvp3.addGroupedAvp(
						Avp.GRANTED_SERVICE_UNIT, true, false);

				// Tariff-Time-Change time type
				Calendar c = new GregorianCalendar(1900, 0, 1);

				c.setTimeZone(TimeZone.getTimeZone("UTC"));

				long timestamp = (System.currentTimeMillis() - c
						.getTimeInMillis()) / 1000;

				gsuAvp.addAvp(Avp.TARIFF_TIME_CHANGE, timestamp, true, false,
						true);

				// CC-Total-Octets unsigned64
				long totalOct = 2147482624;
				gsuAvp.addAvp(Avp.CC_TOTAL_OCTETS, totalOct, true, false, false);

				// Rating-Group unsigned32
				msccAvp3.addAvp(Avp.RATING_GROUP, 2, true, false, true);

				// Validity-Time unsigned32
				
				msccAvp3.addAvp(Avp.VALIDITY_TIME, TestServer.validityTime, true, false,
						true);
				// Result-Code
				int resultCode = 2001;
				msccAvp3.addAvp(Avp.RESULT_CODE, resultCode, true, false, true);

				// Volume-Quota-Threshold
				int volQotaThresh = 2621440;
				msccAvp3.addAvp(268, volQotaThresh, 12645, true, false, true);

				// Quota-Holding-Time
				int quotaHoldTime = 800;
				byte[] holdTime = { (byte) 0x00, (byte) 0x00, (byte) 0x03,
						(byte) 0x20 };
				System.out.println("holdtime changes");
				msccAvp3.addAvp(258, holdTime, 12645, true, false);

				// Trigger
				AvpSet trigger = msccAvp3
						.addGroupedAvp(265, 12645, true, false);
				// Trigger-Type
				trigger.addAvp(266, 1, 12645, true, false, true);
			}

			// [CCFH]

			set.addAvp(Avp.CREDIT_CONTROL_FAILURE_HANDLING, 1, false, false);

			// Route-Record
			String routeRec = "AH-OCS-OCS1";
			set.addAvp(Avp.ROUTE_RECORD, routeRec, true, false, true);

			super.serverCCASession.sendCreditControlAnswer(answer);
			sentINTERIM = true;
			request = null;
			Utils.printMessage(log, super.stack.getDictionary(),
					answer.getMessage(), true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sendTerminate() throws Exception {
		if (!this.receiveTERMINATE || this.request == null) {
			fail("Did not receive TERMINATE or answer already sent.", null);
			throw new Exception("Request: " + this.request);
		}
		try {
			logger.debug("inside sendTerminate Server.java");
			JCreditControlAnswerImpl answer = new JCreditControlAnswerImpl(
					(Request) request.getMessage(), 2001);

			AvpSet reqSet = request.getMessage().getAvps();

			AvpSet set = answer.getMessage().getAvps();
			set.removeAvp(Avp.DESTINATION_HOST);
			set.removeAvp(Avp.DESTINATION_REALM);
			set.addAvp(reqSet.getAvp(Avp.CC_REQUEST_TYPE),
					reqSet.getAvp(Avp.CC_REQUEST_NUMBER),
					reqSet.getAvp(Avp.AUTH_APPLICATION_ID));

			// CC-Session-Failover enumerated

			// failover-supported
			set.addAvp(Avp.CC_SESSION_FAILOVER, 1, true, false, true);

			// Route-Record
			String routeRec = "AH-OCS-OCS1";
			set.addAvp(Avp.ROUTE_RECORD, routeRec, true, false, true);

			// Rulebase-Id

			String ruleBaseId = "GPd_H_pp";
			set.addAvp(262, ruleBaseId, 12645, true, false, true);

			super.serverCCASession.sendCreditControlAnswer(answer);
			sentTERMINATE = true;
			request = null;
			Utils.printMessage(log, super.stack.getDictionary(),
					answer.getMessage(), true);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void sendEvent() throws Exception {
		System.out.println("rohit sendEvent");
		if (!this.receiveEVENT || this.request == null) {
			fail("Did not receive EVENT or answer already sent.", null);
			throw new Exception("Request: " + this.request);
		}
		JCreditControlAnswerImpl answer = new JCreditControlAnswerImpl(
				(Request) request.getMessage(), 2001);

		AvpSet reqSet = request.getMessage().getAvps();

		AvpSet set = answer.getMessage().getAvps();
		set.removeAvp(Avp.DESTINATION_HOST);
		set.removeAvp(Avp.DESTINATION_REALM);
		set.addAvp(reqSet.getAvp(Avp.CC_REQUEST_TYPE),
				reqSet.getAvp(Avp.CC_REQUEST_NUMBER),
				reqSet.getAvp(Avp.AUTH_APPLICATION_ID));

		super.serverCCASession.sendCreditControlAnswer(answer);
		sentEVENT = true;
		request = null;
		Utils.printMessage(log, super.stack.getDictionary(),
				answer.getMessage(), true);
	}

	// ------- initial, this will be triggered for first msg.

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jdiameter.api.NetworkReqListener#processRequest(org.jdiameter.api
	 * .Request)
	 */
	@Override
	public Answer processRequest(Request request) {
		try {
			logger.debug(
					"checking if code reaches processRequest, request code = {}",
					request.getCommandCode());
			if (request.getCommandCode() != 272) {
				fail("Received Request with code not equal 272!. Code["
						+ request.getCommandCode() + "]", null);
				return null;
			}
			int reqType = request.getAvps().getAvp(Avp.CC_REQUEST_TYPE)
					.getInteger32();
			if (super.serverCCASession == null || (reqType == 1)) {
				try {
					logger.debug("rohit inside processRequest Server.java");
					super.serverCCASession = ((ISessionFactory) this.sessionFactory)
							.getNewAppSession(request.getSessionId(),
									getApplicationId(), ServerCCASession.class,
									(Object) null);
					logger.debug("processRequest Server");
					((NetworkReqListener) this.serverCCASession)
							.processRequest(request);
				} catch (Exception e) {
					fail(null, e);
				}
			} else {
				// do fail?
				fail("Received Request in base listener, not in app specific!",
						null);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	// ------------- specific, app session listener.

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jdiameter.api.cca.ServerCCASessionListener#doCreditControlRequest
	 * (org.jdiameter.api.cca.ServerCCASession,
	 * org.jdiameter.api.cca.events.JCreditControlRequest)
	 */
	public void doCreditControlRequest(ServerCCASession session,
			JCreditControlRequest request) throws InternalException,
			IllegalDiameterStateException, RouteException, OverloadException {

		try {
			Utils.printMessage(log, super.stack.getDictionary(),
					request.getMessage(), false);
			logger.debug("inside doCreditControlRequest Sever.java");
			// INITIAL_REQUEST 1,
			// UPDATE_REQUEST 2,
			// TERMINATION_REQUEST, 3
			// EVENT_REQUEST 4

			switch (request.getRequestTypeAVPValue()) {
			case CC_REQUEST_TYPE_INITIAL:
				if (receiveINITIAL) {
					fail("Received INITIAL more than once!", null);
				}
				receiveINITIAL = true;
				this.request = request;
				/* rohit modification */
				try {
					this.sendInitial();
				} catch (Exception e) {
					fail(null, e);
				}
				/* end of rohit modification */
				break;

			case CC_REQUEST_TYPE_INTERIM:
				if (receiveINTERIM) {
					fail("Received INTERIM more than once!", null);
				}
				receiveINTERIM = true;
				this.request = request;
				/* rohit modification */
				try {
					this.sendInterim();
				} catch (Exception e) {
					fail(null, e);
				}
				/* end of rohit modification */
				break;

			case CC_REQUEST_TYPE_TERMINATE:
				if (receiveTERMINATE) {
					fail("Received TERMINATE more than once!", null);
				}
				receiveTERMINATE = true;
				this.request = request;
				try {
					this.sendTerminate();
				} catch (Exception e) {
					fail(null, e);
				}
				break;

			case CC_REQUEST_TYPE_EVENT:
				if (receiveEVENT) {
					fail("Received EVENT more than once!", null);
				}
				System.out.println("rohit doCreditControlRequest event type");
				receiveEVENT = true;
				this.request = request;
				/* rohit modification */
				try {
					this.sendEvent();
				} catch (Exception e) {
					fail(null, e);
				}
				/* end of rohit modification */

				break;

			default:
				fail("No REQ type present?: "
						+ request.getRequestTypeAVPValue(), null);
			}
		} catch (Exception e) {
			fail(null, e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jdiameter.api.cca.ServerCCASessionListener#doReAuthAnswer(org.jdiameter
	 * .api.cca.ServerCCASession, org.jdiameter.api.auth.events.ReAuthRequest,
	 * org.jdiameter.api.auth.events.ReAuthAnswer)
	 */
	public void doReAuthAnswer(ServerCCASession session, ReAuthRequest request,
			ReAuthAnswer answer) throws InternalException,
			IllegalDiameterStateException, RouteException, OverloadException {
		fail("Received \"ReAuthAnswer\" event, request[" + request
				+ "], on session[" + session + "]", null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jdiameter.api.cca.ServerCCASessionListener#doOtherEvent(org.jdiameter
	 * .api.app.AppSession, org.jdiameter.api.app.AppRequestEvent,
	 * org.jdiameter.api.app.AppAnswerEvent)
	 */
	public void doOtherEvent(AppSession session, AppRequestEvent request,
			AppAnswerEvent answer) throws InternalException,
			IllegalDiameterStateException, RouteException, OverloadException {
		fail("Received \"Other\" event, request[" + request + "], answer["
				+ answer + "], on session[" + session + "]", null);
	}

	public boolean isSentINITIAL() {
		return sentINITIAL;
	}

	public boolean isSentINTERIM() {
		return sentINTERIM;
	}

	public boolean isSentTERMINATE() {
		return sentTERMINATE;
	}

	public boolean isReceiveINITIAL() {
		return receiveINITIAL;
	}

	public boolean isReceiveINTERIM() {
		return receiveINTERIM;
	}

	public boolean isReceiveTERMINATE() {
		return receiveTERMINATE;
	}

	public boolean isSentEVENT() {
		return sentEVENT;
	}

	public boolean isReceiveEVENT() {
		return receiveEVENT;
	}

	public JCreditControlRequest getRequest() {
		return request;
	}

}
