package com.movirtu.diameter.charging.apps.postpaid.client;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.jdiameter.api.Answer;
import org.jdiameter.api.ApplicationId;
import org.jdiameter.api.IllegalDiameterStateException;
import org.jdiameter.api.InternalException;
import org.jdiameter.api.Message;
import org.jdiameter.api.Mode;
import org.jdiameter.api.Request;
import org.jdiameter.api.acc.ClientAccSession;
import org.jdiameter.api.acc.ClientAccSessionListener;
import org.jdiameter.api.cca.ServerCCASession;
import org.jdiameter.client.api.ISessionFactory;
import org.jdiameter.common.api.app.acc.ClientAccSessionState;
import org.jdiameter.common.api.app.acc.IClientAccActionContext;
import org.jdiameter.common.impl.app.acc.AccSessionFactoryImpl;

import com.movirtu.diameter.charging.apps.common.StateChange;
import com.movirtu.diameter.charging.apps.common.TBase;

public abstract class BaseClientAcc extends TBase implements
		ClientAccSessionListener, IClientAccActionContext {

	protected int ccRequestNumber = 0;
	protected List<StateChange<ClientAccSessionState>> stateChanges = new ArrayList<StateChange<ClientAccSessionState>>();

	public void init(InputStream configStream, String clientID)
			throws Exception {
		try {
			//comment changes
			 /* super.init(configStream, clientID,
					ApplicationId.createByAuthAppId(0, 4));*/
			/*end of comment changes */
			/*end of comment changes*/
			/*temp changes*/
			super.init(configStream, clientID,
					ApplicationId.createByAccAppId(0, 3));
			/*end of temp changes*/		
			AccSessionFactoryImpl accountingSessionFactory = new AccSessionFactoryImpl(
					this.sessionFactory);
			((ISessionFactory) sessionFactory).registerAppFacory(
					ServerCCASession.class, accountingSessionFactory);
			((ISessionFactory) sessionFactory).registerAppFacory(
					ClientAccSession.class, accountingSessionFactory);

			accountingSessionFactory.setStateListener(this);
			accountingSessionFactory.setClientSessionListener(this);
			accountingSessionFactory.setClientContextListener(this);

		} finally {
			try {
				configStream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	public void start() throws IllegalDiameterStateException, InternalException {
		stack.start();
		
	}

	public void start(Mode mode, long timeOut, TimeUnit timeUnit)
			throws IllegalDiameterStateException, InternalException {
		stack.start(mode, timeOut, timeUnit);
	}

	public void stop(long timeOut, TimeUnit timeUnit, int disconnectCause)
			throws IllegalDiameterStateException, InternalException {
		stack.stop(timeOut, timeUnit, disconnectCause);
	}

	public void stop(int disconnectCause) {
		stack.stop(disconnectCause);
	}

	
	public void interimIntervalElapses(ClientAccSession appSession,Request interimRequest) throws InternalException {
		
	}

	public boolean failedSendRecord(ClientAccSession appSession,Request accRequest) throws InternalException {
		return true;
	}
	
	public void disconnectUserOrDev(ClientAccSession appSession,Request sessionTermRequest) throws InternalException {
		
	}
	
	/*
	public long getDefaultTxTimerValue() {
		return 10;
	}

	public int getDefaultDDFHValue() {
		// DDFH_CONTINUE: 1
		return 1;
	}

	public int getDefaultCCFHValue() {
		// CCFH_CONTINUE: 1
		return 1;
	}

	// ----------- should not be called..

	public void receivedSuccessMessage(Request request, Answer answer) {
		fail("Received \"SuccessMessage\" event, request[" + request
				+ "], answer[" + answer + "]", null);

	}

	public void timeoutExpired(Request request) {
		fail("Received \"Timoeout\" event, request[" + request + "]", null);

	}

	public Answer processRequest(Request request) {
		fail("Received \"Request\" event, request[" + request + "]", null);
		return null;
	}

	public void txTimerExpired(ClientAccSession session) {
		// NOP
	}

	public void grantAccessOnDeliverFailure(
			ClientAccSession clientAccSessionImpl, Message request) {

	}

	public void denyAccessOnDeliverFailure(
			ClientAccSession clientAccSessionImpl, Message request) {

	}

	public void grantAccessOnTxExpire(ClientAccSession clientCCASessionImpl) {

	}

	public void denyAccessOnTxExpire(ClientAccSession clientCCASessionImpl) {
		// NOP
	}

	public void grantAccessOnFailureMessage(
			ClientAccSession clientCCASessionImpl) {
		// NOP
	}

	public void denyAccessOnFailureMessage(ClientAccSession clientCCASessionImpl) {
		// NOP
	}

	public void indicateServiceError(ClientAccSession clientCCASessionImpl) {
		// NOP
	}

	*/

	public ClientAccSession fetchSession(String sessionId)
			throws InternalException {
		ClientAccSession clientAccSession = stack.getSession(sessionId, ClientAccSession.class);
		return clientAccSession;
	}

	protected String createAccSession(String sessionName) throws InternalException {

		ClientAccSession clientAccSession = ((ISessionFactory) this.sessionFactory)
				.getNewAppSession(this.sessionFactory.getSessionId(sessionName),
						getApplicationId(), ClientAccSession.class,
						(Object) null);

		return clientAccSession.getSessionId();
	}

	public List<StateChange<ClientAccSessionState>> getStateChanges() {
		return stateChanges;
	}

	
}
