package com.movirtu.diameter.charging.apps.client;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.jdiameter.api.Answer;
import org.jdiameter.api.ApplicationId;
import org.jdiameter.api.IllegalDiameterStateException;
import org.jdiameter.api.InternalException;
import org.jdiameter.api.Message;
import org.jdiameter.api.Mode;
import org.jdiameter.api.Request;
import org.jdiameter.api.cca.ClientCCASession;
import org.jdiameter.api.cca.ClientCCASessionListener;
import org.jdiameter.api.cca.ServerCCASession;
import org.jdiameter.client.api.ISessionFactory;
import org.jdiameter.common.api.app.cca.ClientCCASessionState;
import org.jdiameter.common.api.app.cca.IClientCCASessionContext;
import org.jdiameter.common.impl.app.cca.CCASessionFactoryImpl;

import com.movirtu.diameter.charging.apps.common.StateChange;
import com.movirtu.diameter.charging.apps.common.TBase;

public abstract class BaseClient extends TBase implements
		ClientCCASessionListener, IClientCCASessionContext {

	protected int ccRequestNumber = 0;
	protected List<StateChange<ClientCCASessionState>> stateChanges = new ArrayList<StateChange<ClientCCASessionState>>();

	public void init(InputStream configStream, String clientID)
			throws Exception {
		try {
			super.init(configStream, clientID,
					ApplicationId.createByAuthAppId(0, 4));
			CCASessionFactoryImpl creditControlSessionFactory = new CCASessionFactoryImpl(
					this.sessionFactory);
			((ISessionFactory) sessionFactory).registerAppFacory(
					ServerCCASession.class, creditControlSessionFactory);
			((ISessionFactory) sessionFactory).registerAppFacory(
					ClientCCASession.class, creditControlSessionFactory);

			creditControlSessionFactory.setStateListener(this);
			creditControlSessionFactory.setClientSessionListener(this);
			creditControlSessionFactory.setClientContextListener(this);

		} finally {
			try {
				configStream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	public void start() throws IllegalDiameterStateException, InternalException {
		stack.start();
		
	}

	public void start(Mode mode, long timeOut, TimeUnit timeUnit)
			throws IllegalDiameterStateException, InternalException {
		stack.start(mode, timeOut, timeUnit);
	}

	public void stop(long timeOut, TimeUnit timeUnit, int disconnectCause)
			throws IllegalDiameterStateException, InternalException {
		stack.stop(timeOut, timeUnit, disconnectCause);
	}

	public void stop(int disconnectCause) {
		stack.stop(disconnectCause);
	}

	public long getDefaultTxTimerValue() {
		return 10;
	}

	public int getDefaultDDFHValue() {
		// DDFH_CONTINUE: 1
		return 1;
	}

	public int getDefaultCCFHValue() {
		// CCFH_CONTINUE: 1
		return 1;
	}

	// ----------- should not be called..

	public void receivedSuccessMessage(Request request, Answer answer) {
		fail("Received \"SuccessMessage\" event, request[" + request
				+ "], answer[" + answer + "]", null);

	}

	public void timeoutExpired(Request request) {
		fail("Received \"Timoeout\" event, request[" + request + "]", null);

	}

	public Answer processRequest(Request request) {
		fail("Received \"Request\" event, request[" + request + "]", null);
		return null;
	}

	public void txTimerExpired(ClientCCASession session) {
		// NOP
	}

	public void grantAccessOnDeliverFailure(
			ClientCCASession clientCCASessionImpl, Message request) {

	}

	public void denyAccessOnDeliverFailure(
			ClientCCASession clientCCASessionImpl, Message request) {

	}

	public void grantAccessOnTxExpire(ClientCCASession clientCCASessionImpl) {

	}

	public void denyAccessOnTxExpire(ClientCCASession clientCCASessionImpl) {
		// NOP
	}

	public void grantAccessOnFailureMessage(
			ClientCCASession clientCCASessionImpl) {
		// NOP
	}

	public void denyAccessOnFailureMessage(ClientCCASession clientCCASessionImpl) {
		// NOP
	}

	public void indicateServiceError(ClientCCASession clientCCASessionImpl) {
		// NOP
	}

	

	public ClientCCASession fetchSession(String sessionId)
			throws InternalException {
		ClientCCASession clientCCASession = stack.getSession(sessionId, ClientCCASession.class);
		return clientCCASession;
	}

	protected String createCCASession(String sessionName) throws InternalException {

		ClientCCASession clientCCASession = ((ISessionFactory) this.sessionFactory)
				.getNewAppSession(this.sessionFactory.getSessionId(sessionName),
						getApplicationId(), ClientCCASession.class,
						(Object) null);

		return clientCCASession.getSessionId();
	}

	public List<StateChange<ClientCCASessionState>> getStateChanges() {
		return stateChanges;
	}

	
}
