package com.movirtu.diameter.charging.apps.B2BGw.client;

import java.util.List;

import org.jdiameter.api.ApplicationId;
import org.jdiameter.api.Avp;
import org.jdiameter.api.AvpDataException;
import org.jdiameter.api.AvpSet;
import org.jdiameter.api.IllegalDiameterStateException;
import org.jdiameter.api.InternalException;
import org.jdiameter.api.Message;
import org.jdiameter.api.OverloadException;
import org.jdiameter.api.Peer;
import org.jdiameter.api.PeerState;
import org.jdiameter.api.PeerTable;
import org.jdiameter.api.Request;
import org.jdiameter.api.RouteException;
import org.jdiameter.api.app.AppAnswerEvent;
import org.jdiameter.api.app.AppRequestEvent;
import org.jdiameter.api.app.AppSession;
import org.jdiameter.api.auth.events.ReAuthRequest;
import org.jdiameter.api.cca.ClientCCASession;
import org.jdiameter.api.cca.events.JCreditControlAnswer;
import org.jdiameter.api.cca.events.JCreditControlRequest;
import org.jdiameter.common.impl.app.auth.ReAuthAnswerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.diameter.charging.apps.common.GrantedServiceUnit;
import com.movirtu.diameter.charging.apps.common.HashMapDS;
import com.movirtu.diameter.charging.apps.common.MultipleServiceCreditControlClass;
import com.movirtu.diameter.charging.apps.common.PrepaidServiceProcessingDiameterMessages;
import com.movirtu.diameter.charging.apps.common.TransactionInfo;
import com.movirtu.diameter.charging.apps.common.Utils;
import com.movirtu.diameter.test.b2bgw.PrepaidServiceProcessingDiameterMessagesImpl;
import com.movirtu.diameter.test.b2bgw.StoreGenSessionId;
import com.movirtu.diameter.test.b2bgw.TestClientB2BGwCCA;

public class B2BGwCCRClient extends B2BGwBaseClientCCA {
	protected Logger logger = LoggerFactory.getLogger(B2BGwCCRClient.class);
	protected PrepaidServiceProcessingDiameterMessages serviceProcDiamMsgClient;
	protected boolean sentINITIAL;
	protected boolean sentINTERIM;
	protected boolean sentTERMINATE;
	protected boolean sentEVENT;
	protected boolean receiveINITIAL;
	protected boolean receiveINTERIM;
	protected boolean receiveTERMINATE;
	protected boolean receiveEVENT;

	public B2BGwCCRClient() {

	}

	public B2BGwCCRClient(
			PrepaidServiceProcessingDiameterMessages serviceProcDiamMsgClient) {
		this.serviceProcDiamMsgClient = serviceProcDiamMsgClient;
	}

	private Peer getRemotePeer() throws Exception {
		Peer remotepeer = null;
		List<Peer> peers = getStack().unwrap(PeerTable.class).getPeerTable();
		boolean foundConnected = false;
		if (peers.size() >= 1) {
			for (Peer p : peers) {

				if (p.getState(PeerState.class).equals(PeerState.OKAY)) {
					foundConnected = true;
					remotepeer = p;// use 1st connected peer i.e. overwrite
									// redundancy mode (to-do: other redundancy
									// modes)
					break;
				}
			}

		}

		if (!foundConnected)
			throw new Exception("no connected peer ...unable to send CCR");

		return remotepeer;
	}

	/*
	 * old code public String sendInitial() throws Exception { String sessionID
	 * = super.createCCASession("Movirtu"); ClientCCASession ccasession =
	 * super.fetchSession(sessionID); /* JCreditControlRequest ccr = new
	 * CCRBuilder(ccasession, super.getApplicationId(),
	 * super.getClientRealmName(), super.getServerRealmName(),
	 * super.getClientURI(), super.getServerURI(),
	 * "SCAP_V.2.0@ericsson.com").initialCCR();
	 * ccasession.sendCreditControlRequest(ccr); Utils.printMessage(log,
	 * super.stack.getDictionary(), ccr.getMessage(), true);
	 */
	/*
	 * this.sentINITIAL = true; return sessionID; }
	 */
	/* end of old code */

	public String sendInitial(TransactionInfo obj) throws Exception {
		try {
			String sessionID = null;
			ClientCCASession clientCCASession = null;
			obj.display();
			Peer localpeer = getStack().getMetaData().getLocalPeer();
			Peer remotepeer = getRemotePeer();

			if (TestClientB2BGwCCA.isTest == true) {
				StoreGenSessionId sessIDStorageObj = new StoreGenSessionId();
				sessionID = createSessionIdForSecondLeg(obj.userProvidedSessionIdKey);
				clientCCASession = super.fetchSession(sessionID);
				sessIDStorageObj.SessionId = sessionID;

				// SessionInfo obj = new SessionInfo(completeSessionId);
				/*
				 * creates a mapping between the userProvidedSessionIdKeyand obj
				 * that contains the sessionId sent in diameter messages
				 */
				HashMapDS.INSTANCE.insertInHash(obj.userProvidedSessionIdKey,
						sessIDStorageObj);
			} else {
				sessionID = obj.key;
				clientCCASession = super.fetchSession(sessionID);
			}

			// private ApplicationId(long vendorId, long authAppId, long
			// acctAppId)
			// createByAuthAppId(long vendorId, long authAppId) {
			// ApplicationId createByAccAppId(long vendorId, long acchAppId) {

			ApplicationId applicationId = ApplicationId.createByAuthAppId(0, 4);
			// ApplicationId applicationId = null;
			// considering single application only for time-being
			for (ApplicationId appId : remotepeer.getCommonApplications()) {
				// System.out.println("AppicationId= " + appId);
				// applicationId = appId;
				break;

			}

			JCreditControlRequest ccr = new B2BGwCCRBuilder(clientCCASession,
					applicationId, localpeer.getRealmName(),
					remotepeer.getRealmName(), localpeer.getUri().getFQDN(),
					remotepeer.getUri().getFQDN(), obj.serviceContextId, obj)
					.initialCCR();
			/*
			 * System.out.println("AppicationId= " + applicationId +
			 * " ClientRealmName= " + localpeer.getRealmName() + " ClientURI=" +
			 * localpeer.getUri().toString() + " ServerURI=" +
			 * remotepeer.getUri().toString() + " ServerRealmName = " +
			 * remotepeer.getRealmName());
			 */
			clientCCASession.sendCreditControlRequest(ccr);
			Utils.printMessage(log, super.stack.getDictionary(),
					ccr.getMessage(), true);

			this.sentINITIAL = true;
			return sessionID;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String sendInterim(TransactionInfo obj) throws Exception {

		String sessionID = null;
		ClientCCASession clientCCASession = null;
		try {
			if (TestClientB2BGwCCA.isTest == true) {
				StoreGenSessionId sessIDStorageObj = HashMapDS.INSTANCE
						.searchInHash(obj.userProvidedSessionIdKey);
				sessionID = sessIDStorageObj.SessionId;
				clientCCASession = super.fetchSession(sessionID);
			} else {
				sessionID = obj.key;// second leg key generated by jdiameter &
									// provided by service
				clientCCASession = super.fetchSession(sessionID);
			}
			// TransactionInfo obj =
			// HashMapDS.INSTANCE.searchInHash(userProvidedSessionIdKey);
			// ClientCCASession ccasession = super.fetchSession(obj.key);
			Peer localpeer = getStack().getMetaData().getLocalPeer();
			Peer remotepeer = getRemotePeer();

			ApplicationId applicationId = ApplicationId.createByAuthAppId(0, 4);
			// ApplicationId applicationId = null;
			// considering single application only for time-being
			for (ApplicationId appId : remotepeer.getCommonApplications()) {
				// System.out.println("AppicationId= " + appId);
				// applicationId = appId;
				break;

			}

			JCreditControlRequest ccr = new B2BGwCCRBuilder(clientCCASession,
					applicationId, localpeer.getRealmName(),
					remotepeer.getRealmName(), localpeer.getUri().getFQDN(),
					remotepeer.getUri().getFQDN(), obj.serviceContextId, obj)
					.interimCCR();
			/*
			 * System.out.println("AppicationId= " + applicationId +
			 * " ClientRealmName= " + localpeer.getRealmName() + " ClientURI=" +
			 * localpeer.getUri().toString() + " ServerURI=" +
			 * remotepeer.getUri().toString() + " ServerRealmName = " +
			 * remotepeer.getRealmName());
			 */
			clientCCASession.sendCreditControlRequest(ccr);
			/*
			 * Utils.printMessage(log, super.stack.getDictionary(),
			 * ccr.getMessage(), true);
			 */

			this.sentINTERIM = true;
			return clientCCASession.getSessionId();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String sendTerminate(TransactionInfo obj) throws Exception {

		// String sessionID = super.createCCASession("Movirtu");
		try {
			String sessionID = null;
			ClientCCASession clientCCASession = null;

			if (TestClientB2BGwCCA.isTest == true) {
				StoreGenSessionId sessIDStorageObj = HashMapDS.INSTANCE
						.searchInHash(obj.userProvidedSessionIdKey);
				sessionID = sessIDStorageObj.SessionId;
				clientCCASession = super.fetchSession(sessionID);
			} else {
				sessionID = obj.key;// second leg key generated by jdiameter &
									// provided by service
				clientCCASession = super.fetchSession(sessionID);
			}
			// TransactionInfo obj =
			// HashMapDS.INSTANCE.searchInHash(userProvidedSessionIdKey);
			// ClientCCASession ccasession = super.fetchSession(obj.key);
			Peer localpeer = getStack().getMetaData().getLocalPeer();
			Peer remotepeer = getRemotePeer();

			ApplicationId applicationId = ApplicationId.createByAuthAppId(0, 4);
			// ApplicationId applicationId = null;
			// considering single application only for time-being
			for (ApplicationId appId : remotepeer.getCommonApplications()) {
				// System.out.println("AppicationId= " + appId);
				// applicationId = appId;
				break;

			}

			JCreditControlRequest ccr = new B2BGwCCRBuilder(clientCCASession,
					applicationId, localpeer.getRealmName(),
					remotepeer.getRealmName(), localpeer.getUri().getFQDN(),
					remotepeer.getUri().getFQDN(), obj.serviceContextId, obj)
					.terminateCCR();
			/*
			 * System.out.println("AppicationId= " + applicationId +
			 * " ClientRealmName= " + localpeer.getRealmName() + " ClientURI=" +
			 * localpeer.getUri().toString() + " ServerURI=" +
			 * remotepeer.getUri().toString() + " ServerRealmName = " +
			 * remotepeer.getRealmName());
			 */
			clientCCASession.sendCreditControlRequest(ccr);
			/*
			 * Utils.printMessage(log, super.stack.getDictionary(),
			 * ccr.getMessage(), true);
			 */

			this.sentINTERIM = true;
			return clientCCASession.getSessionId();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void sendReAuthAnswer(TransactionInfo obj) throws Exception {
		try {
			ReAuthAnswerImpl answer = new ReAuthAnswerImpl(obj.request,
					obj.result);

			logger.debug("inside sendInitialCCA1,answer = {} ", answer);

			AvpSet set = answer.getMessage().getAvps();

			Peer localpeer = getStack().getMetaData().getLocalPeer();
			Peer remotepeer = getRemotePeer();

			//AvpSet set = answer.getAvps();
			// Session-Id
			obj.key = set.getAvp(Avp.SESSION_ID).getUTF8String();

			// Result-code
			if (set.getAvp(Avp.RESULT_CODE) != null)
				obj.result = set.getAvp(Avp.RESULT_CODE).getInteger32();

			// Orig-Host String

			Avp origHost = set.getAvp(Avp.ORIGIN_HOST);
			if (origHost != null)
				obj.origHost = origHost.getUTF8String();

			// Orig-realm AVP
			Avp origRealm = set.getAvp(Avp.ORIGIN_REALM);
			if (origRealm != null)
				obj.origRealm = origRealm;
			// System.out.println("origRealm = " + obj.origRealm);
			// User-Name

			Avp userName = set.getAvp(Avp.USER_NAME);
			if (userName != null)
				obj.userName = userName;
			// Origin-State-Id
			Avp originStateId = set.getAvp(Avp.ORIGIN_STATE_ID);
			if (originStateId != null)
				obj.originStateId = originStateId;
			// Error-Message
			Avp errorMsg = set.getAvp(Avp.ERROR_MESSAGE);
			if (errorMsg != null) {
				obj.erorMsg = errorMsg;
			}
			// Error-Reporting-Host
			Avp errorReportingHost = set.getAvp(Avp.ERROR_REPORTING_HOST);
			if (errorReportingHost != null)
				obj.errorReportingHost = errorReportingHost;
			// Failed-Avp
			Avp failedAvp = set.getAvp(Avp.FAILED_AVP);
			if (failedAvp != null)
				obj.failedAvp = failedAvp;

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String sendEvent(TransactionInfo obj) throws Exception {

		return null;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jdiameter.api.cca.ClientCCASessionListener#doCreditControlAnswer(
	 * org.jdiameter.api.cca.ClientCCASession,
	 * org.jdiameter.api.cca.events.JCreditControlRequest,
	 * org.jdiameter.api.cca.events.JCreditControlAnswer)
	 */
	public void doCreditControlAnswer(ClientCCASession session,
			JCreditControlRequest request, JCreditControlAnswer answer)
			throws InternalException, IllegalDiameterStateException,
			RouteException, OverloadException {
		try {

			TransactionInfo obj;
			if ((obj = parseCCA(answer)) != null) {
				obj.prepaidAnswer = answer;
			}
			//for CDR purposes
			this.collateMSCCInformation(obj);
			/*
			 * System.out.println("session id = " +
			 * answer.getMessage().getSessionId() + " result=>" +
			 * answer.getResultCodeAvp().getInteger32());
			 */
			logger.debug("inside doCreditControlAnswer sessionId= {}", answer
					.getMessage().getSessionId());
			/*
			 * Utils.printMessage(log, super.stack.getDictionary(),
			 * answer.getMessage(), false);
			 */
			switch (answer.getRequestTypeAVPValue()) {
			case B2BGwCCRBuilder.CC_REQUEST_TYPE_INITIAL:
				if (receiveINITIAL) {
					fail("Received INITIAL more than once!", null);
				}
				receiveINITIAL = true;

				break;
			case B2BGwCCRBuilder.CC_REQUEST_TYPE_INTERIM:
				if (receiveINTERIM) {
					fail("Received INTERIM more than once!", null);
				}
				receiveINTERIM = true;

				break;
			case B2BGwCCRBuilder.CC_REQUEST_TYPE_TERMINATE:
				if (receiveTERMINATE) {
					fail("Received TERMINATE more than once!", null);
				}
				receiveTERMINATE = true;

				break;
			case B2BGwCCRBuilder.CC_REQUEST_TYPE_EVENT:
				if (receiveEVENT) {
					fail("Received EVENT more than once!", null);
				}
				receiveEVENT = true;
				break;

			default:

			}

			if (serviceProcDiamMsgClient == null) {
				serviceProcDiamMsgClient = new PrepaidServiceProcessingDiameterMessagesImpl();
			}
			serviceProcDiamMsgClient.serviceProcessingCCA(obj);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jdiameter.api.cca.ClientCCASessionListener#doReAuthRequest(org.jdiameter
	 * .api.cca.ClientCCASession, org.jdiameter.api.auth.events.ReAuthRequest)
	 */
	public void doReAuthRequest(ClientCCASession session, ReAuthRequest request)
			throws InternalException, IllegalDiameterStateException,
			RouteException, OverloadException {
		try {
			TransactionInfo obj = null;
			if (request.getCommandCode() != 258) {
				logger.debug("received unexpected command code = {}",
						request.getCommandCode());
				return;
			}
			ClientCCASession clientCCASession = null;
			String sessionId = request.getMessage().getSessionId();
			obj = parseRAR((Request) request.getMessage());
			if (TestClientB2BGwCCA.isTest == true) {
				StoreGenSessionId sessIDStorageObj = HashMapDS.INSTANCE
						.searchInHash(obj.userProvidedSessionIdKey);
				sessIDStorageObj.request = (Request) request.getMessage();
				obj.key = sessionId;

			} else {
				clientCCASession = super.fetchSession(sessionId);
				obj.key = sessionId;
				obj.request = (Request) request.getMessage();
			}
			if (serviceProcDiamMsgClient == null) {
				serviceProcDiamMsgClient = new PrepaidServiceProcessingDiameterMessagesImpl();
			}
			serviceProcDiamMsgClient.serviceProcessingRAR(obj);

		} catch (Exception e) {
			e.printStackTrace();
		}

		/*
		 * fail("Received \"ReAuthRequest\" event, request[" + request +
		 * "], on session[" + session + "]", null);
		 */

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jdiameter.api.cca.ClientCCASessionListener#doOtherEvent(org.jdiameter
	 * .api.app.AppSession, org.jdiameter.api.app.AppRequestEvent,
	 * org.jdiameter.api.app.AppAnswerEvent)
	 */
	public void doOtherEvent(AppSession session, AppRequestEvent request,
			AppAnswerEvent answer) throws InternalException,
			IllegalDiameterStateException, RouteException, OverloadException {
		fail("Received \"Other\" event, request[" + request + "], answer["
				+ answer + "], on session[" + session + "]", null);

	}

	// ------------ getters for some vars;

	public boolean isSentINITIAL() {
		return sentINITIAL;
	}

	public boolean isSentEVENT() {
		return sentEVENT;
	}

	public boolean isReceiveEVENT() {
		return receiveEVENT;
	}

	public boolean isSentINTERIM() {
		return sentINTERIM;
	}

	public boolean isSentTERMINATE() {
		return sentTERMINATE;
	}

	public boolean isReceiveINITIAL() {
		return receiveINITIAL;
	}

	public boolean isReceiveINTERIM() {
		return receiveINTERIM;
	}

	public boolean isReceiveTERMINATE() {
		return receiveTERMINATE;
	}

	public String createSessionIdForSecondLeg(String userProvidedSessionString) {
		String sessionId = null;
		try {
			sessionId = super.createCCASession(userProvidedSessionString);
		} catch (InternalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return sessionId;
	}

	public TransactionInfo parseCCA(JCreditControlAnswer answer)
			throws InternalException {
		try {
			TransactionInfo obj = new TransactionInfo();
			// Session-Id
			obj.key = answer.getMessage().getSessionId();
			// Record-Type
			obj.recordType = answer.getRequestTypeAVPValue();
			// Record-Number
			obj.recordNumber = (int) answer.getMessage().getAvps()
					.getAvp(Avp.CC_REQUEST_NUMBER).getInteger32();
			// Result-Code
			obj.result = answer.getResultCodeAvp().getInteger32();
			// CC-Session-Failover
			Avp sessionFailover = answer.getMessage().getAvps()
					.getAvp(Avp.CC_SESSION_FAILOVER);
			if (sessionFailover != null) {
				obj.sessionFailover = sessionFailover;
			}

			// need to add multiple service credit control multiple possible
			// AvpSets

			// MULTIPLE-SERVICES-CREDIT-CONTROL

			AvpSet mscc = answer.getMessage().getAvps()
					.getAvps(Avp.MULTIPLE_SERVICES_CREDIT_CONTROL);
			if (mscc != null) {
				obj.multipleServiceCreditControl = mscc;
			}
			
			// Credit-Control-Failure-Handling
			Avp creditControlFailureHandlingAvp = answer.getMessage().getAvps()
					.getAvp(Avp.CREDIT_CONTROL_FAILURE_HANDLING);
			if (creditControlFailureHandlingAvp != null) {
				obj.creditControlFailureHandling = creditControlFailureHandlingAvp
						.getInteger32();
			}
			// Route-Record
			Avp routeRecord = answer.getMessage().getAvps().getAvp(282);
			if (routeRecord != null) {
				obj.routeRecord = routeRecord;
			}
			// Rulebase-Id
			Avp ruleBaseId = answer.getMessage().getAvps().getAvp(262);
			if (ruleBaseId != null) {
				obj.ruleBaseId = ruleBaseId;
			}
			// need to extract subscription Id in case of virtual subscriber
			return obj;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public TransactionInfo parseRAR(Request request) {
		try {
			AvpSet avpSet = request.getAvps();
			TransactionInfo obj = new TransactionInfo();
			// Session-Id
			String sessId = avpSet.getAvp(Avp.SESSION_ID).getUTF8String();

			// Orig-Host String

			Avp origHost = avpSet.getAvp(Avp.ORIGIN_HOST);
			if (origHost != null)
				obj.origHost = origHost.getUTF8String();
			// System.out.println("origHost = " + obj.origHost);

			// Dest-Host String
			Avp destHost = avpSet.getAvp(Avp.DESTINATION_HOST);
			if (destHost != null)
				obj.destHostStr = destHost.getUTF8String();
			

			// Orig-realm AVP
			Avp origRealm = avpSet.getAvp(Avp.ORIGIN_REALM);
			if (origRealm != null)
				obj.origRealm = origRealm;
			// System.out.println("origRealm = " + obj.origRealm);

			// Dest-Realm --AVP
			Avp destRealm = avpSet.getAvp(Avp.DESTINATION_REALM);
			if (destRealm != null)
				obj.destRealm = destRealm;
			// RE-Auth-Request-Type ENUMERATED
			Avp reAuthReqType = avpSet.getAvp(Avp.RE_AUTH_REQUEST_TYPE);
			if (reAuthReqType != null)
				obj.reAuthRequestType = reAuthReqType.getInteger32();

			// User-Name AVP
			Avp userName = avpSet.getAvp(Avp.USER_NAME);
			if (userName != null) {
				obj.userName = userName;
			}
			// Origin-State-Id AVP
			Avp originStateId = avpSet.getAvp(Avp.ORIGIN_STATE_ID);
			if (originStateId != null) {
				obj.originStateId = originStateId;
			}
			return obj;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void collateGSUInformation(AvpSet gsu,MultipleServiceCreditControlClass msccAddOnList) {
		try {
			msccAddOnList.gsuObj = new GrantedServiceUnit();
			if(msccAddOnList.gsuObj!=null) {
			msccAddOnList.gsuObj.totalOctets = gsu.getAvp(Avp.CC_TOTAL_OCTETS).getInteger64();
			msccAddOnList.gsuObj.tariifTimeChange = gsu.getAvp(Avp.TARIFF_TIME_CHANGE).getTime();
			}
		} catch (AvpDataException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public  void collateMSCCInformation(TransactionInfo obj) {
		try {
			if (obj.multipleServiceCreditControl != null) {
				for (Avp m : obj.multipleServiceCreditControl) {
					AvpSet msccSet = m.getGrouped();
					MultipleServiceCreditControlClass msccAddOnList = new MultipleServiceCreditControlClass();
					Avp gsu = msccSet.getAvp(Avp.GRANTED_SERVICE_UNIT);
					if (gsu != null) {
						AvpSet gsuSet = gsu.getGrouped();
						this.collateGSUInformation(gsuSet, msccAddOnList);
					}
					// rating-group
					msccAddOnList.ratingGroup = msccSet
							.getAvp(Avp.RATING_GROUP).getInteger32();
					// validity-time
					msccAddOnList.validityTime = msccSet.getAvp(
							Avp.VALIDITY_TIME).getInteger32();
					// result code
					msccAddOnList.resultCode = msccSet.getAvp(Avp.RESULT_CODE)
							.getInteger32();
					// volume quota threshold
					msccAddOnList.volumeQuotaThresholdVoda = msccSet
							.getAvp(268,12645).getInteger32();
					// quota holding time
					//msccAddOnList.quotaHoldingTimeVoda = msccSet.getAvp(258,12645).getInteger32();
					//trigger-type
					Avp trigger = msccSet.getAvp(265);
					if (trigger != null) {
						AvpSet triggerSet = trigger.getGrouped();
						msccAddOnList.triggerTypeVoda = triggerSet.getAvp(266)
								.getInteger32();
					}
					obj.msccList.add(msccAddOnList);
					obj.msccDisplay(msccAddOnList);
				}
				logger.debug("obj.msccList size="+obj.msccList.size());
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	public void txTimerExpired(ClientCCASession session) {
		String sessionId = session.getSessionId();
		try {
			logger.debug("inside txTimerExpired, sessionId = {}", sessionId);
			if (serviceProcDiamMsgClient == null) {
				serviceProcDiamMsgClient = new PrepaidServiceProcessingDiameterMessagesImpl();
			}
			serviceProcDiamMsgClient.txTimerExpiredNotification(sessionId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void grantAccessOnDeliverFailure(
			ClientCCASession clientCCASessionImpl, Message request) {
		String sessionId = clientCCASessionImpl.getSessionId();
		try {
			logger.debug("inside grantAccessOnDeliverFailure, sessionId = {}",
					sessionId);
			if (serviceProcDiamMsgClient == null) {
				serviceProcDiamMsgClient = new PrepaidServiceProcessingDiameterMessagesImpl();
			}
			serviceProcDiamMsgClient.grantAccessOnDeliverFailureNotification(
					sessionId, request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void denyAccessOnDeliverFailure(
			ClientCCASession clientCCASessionImpl, Message request) {
		String sessionId = clientCCASessionImpl.getSessionId();
		try {
			logger.debug("inside denyAccessOnDeliverFailure, sessionId = {}",
					sessionId);
			if (serviceProcDiamMsgClient == null) {
				serviceProcDiamMsgClient = new PrepaidServiceProcessingDiameterMessagesImpl();
			}
			serviceProcDiamMsgClient.denyAccessOnDeliverFailureNotification(
					sessionId, request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void grantAccessOnTxExpire(ClientCCASession clientCCASessionImpl) {
		String sessionId = clientCCASessionImpl.getSessionId();
		try {
			logger.debug("inside grantAccessOnTxExpire, sessionId = {}",
					sessionId);
			if (serviceProcDiamMsgClient == null) {
				serviceProcDiamMsgClient = new PrepaidServiceProcessingDiameterMessagesImpl();
			}
			serviceProcDiamMsgClient
					.grantAccessOnTxExpireNotification(sessionId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void denyAccessOnTxExpire(ClientCCASession clientCCASessionImpl) {
		String sessionId = clientCCASessionImpl.getSessionId();
		try {
			logger.debug("inside denyAccessOnTxExpire, sessionId = {}",
					sessionId);
			if (serviceProcDiamMsgClient == null) {
				serviceProcDiamMsgClient = new PrepaidServiceProcessingDiameterMessagesImpl();
			}
			serviceProcDiamMsgClient
					.denyAccessOnTxExpireNotification(sessionId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void grantAccessOnFailureMessage(
			ClientCCASession clientCCASessionImpl) {
		String sessionId = clientCCASessionImpl.getSessionId();
		try {
			logger.debug("inside grantAccessOnFailureMessage, sessionId = {}",
					sessionId);
			if (serviceProcDiamMsgClient == null) {
				serviceProcDiamMsgClient = new PrepaidServiceProcessingDiameterMessagesImpl();
			}
			serviceProcDiamMsgClient
					.grantAccessOnFailureMessageNotification(sessionId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void denyAccessOnFailureMessage(ClientCCASession clientCCASessionImpl) {
		String sessionId = clientCCASessionImpl.getSessionId();
		try {
			logger.debug("inside denyAccessOnFailureMessage, sessionId = {}",
					sessionId);
			if (serviceProcDiamMsgClient == null) {
				serviceProcDiamMsgClient = new PrepaidServiceProcessingDiameterMessagesImpl();
			}
			serviceProcDiamMsgClient
					.denyAccessOnFailureMessageNotification(sessionId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void indicateServiceError(ClientCCASession clientCCASessionImpl) {
		String sessionId = clientCCASessionImpl.getSessionId();
		try {
			logger.debug("inside indicateServiceError, sessionId = {}",
					sessionId);
			if (serviceProcDiamMsgClient == null) {
				serviceProcDiamMsgClient = new PrepaidServiceProcessingDiameterMessagesImpl();
			}
			serviceProcDiamMsgClient
					.indicateServiceErrorNotification(sessionId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
