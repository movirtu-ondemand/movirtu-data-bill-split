/*
 * JBoss, Home of Professional Open Source
 * Copyright 2011, Red Hat, Inc. and/or its affiliates, and individual
 * contributors as indicated by the @authors tag. All rights reserved.
 * See the copyright.txt in the distribution for a full listing
 * of individual contributors.
 * 
 * This copyrighted material is made available to anyone wishing to use,
 * modify, copy, or redistribute it subject to the terms and conditions
 * of the GNU General Public License, v. 2.0.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License,
 * v. 2.0 along with this distribution; if not, write to the Free 
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
package com.movirtu.diameter.charging.apps.postpaid.server;

import java.net.Inet4Address;
import java.net.InetAddress;

import org.dellroad.stuff.net.IPv4Util;
import org.jdiameter.api.Answer;
import org.jdiameter.api.Avp;
import org.jdiameter.api.AvpDataException;
import org.jdiameter.api.AvpSet;
import org.jdiameter.api.IllegalDiameterStateException;
import org.jdiameter.api.InternalException;
import org.jdiameter.api.NetworkReqListener;
import org.jdiameter.api.OverloadException;
import org.jdiameter.api.Request;
import org.jdiameter.api.RouteException;
import org.jdiameter.api.acc.ServerAccSession;
import org.jdiameter.api.acc.events.AccountAnswer;
import org.jdiameter.api.acc.events.AccountRequest;
import org.jdiameter.api.app.AppAnswerEvent;
import org.jdiameter.api.app.AppRequestEvent;
import org.jdiameter.api.app.AppSession;
import org.jdiameter.client.api.ISessionFactory;
import org.jdiameter.common.impl.app.acc.AccountAnswerImpl;
import org.jdiameter.server.impl.app.acc.ServerAccSessionImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.diameter.charging.apps.common.Utils;
import com.movirtu.diameter.charging.apps.postpaid.client.HashMapDS;
import com.movirtu.diameter.charging.apps.postpaid.client.SessionInfo;

/**
 * Base implementation of Server
 * 
 * @author <a href="mailto:brainslog@gmail.com"> Alexandre Mendonca </a>
 * @author <a href="mailto:baranowb@gmail.com"> Bartosz Baranowski </a>
 */
public class ServerAcc extends AbstractServerAcc {
	private static final Logger logger = LoggerFactory
			.getLogger(ServerAcc.class);
	protected boolean sentINITIAL;
	protected boolean sentINTERIM;
	protected boolean sentTERMINATE;
	protected boolean sentEVENT;
	protected boolean receiveINITIAL;
	protected boolean receiveINTERIM;
	protected boolean receiveTERMINATE;
	protected boolean receiveEVENT;

	protected AccountRequest request;

	public  ServerAcc() {
		
		
	}
	
	// ------- send methods to trigger answer

	public void sendInitial() throws Exception {
		if (!this.receiveINITIAL || this.request == null) {
			fail("Did not receive INITIAL or answer already sent.", null);
			throw new Exception("Request: " + this.request);
		}
		try {
		AccountAnswer answer = new AccountAnswerImpl(
				(Request) request.getMessage(), 2001);

		AvpSet reqSet = request.getMessage().getAvps();

		AvpSet set = answer.getMessage().getAvps();
		set.removeAvp(Avp.DESTINATION_HOST);
		set.removeAvp(Avp.DESTINATION_REALM);
		set.addAvp(reqSet.getAvp(Avp.ACC_RECORD_TYPE),
				reqSet.getAvp(Avp.ACC_RECORD_NUMBER));
		set.addAvp(Avp.ACCT_INTERIM_INTERVAL, 60, false, false);

		/* rohit modification */
		// [Multiple-Services-Credit-Control]
		
		  AvpSet msccAvp =
		  set.addGroupedAvp(Avp.MULTIPLE_SERVICES_CREDIT_CONTROL,true,false);
		  AvpSet gsuAvp = msccAvp.addGroupedAvp(Avp.GRANTED_SERVICE_UNIT, true,
		  false); gsuAvp.addAvp(Avp.CC_INPUT_OCTETS,(long)100,true,false);
		  gsuAvp.addAvp(Avp.CC_OUTPUT_OCTETS,(long)100,true,false);
		  msccAvp.addAvp(Avp.VALIDITY_TIME, 60,false, false);
		  
		  
		  
		 /* //[CCFH]
		 * 
		 * set.addAvp(Avp.CREDIT_CONTROL_FAILURE_HANDLING,1,false,false);
		 */
		/* end of rohit modification */
		super.serverAccSession.sendAccountAnswer(answer);

		sentINITIAL = true;
		request = null;
		Utils.printMessage(log, super.stack.getDictionary(),
				answer.getMessage(), true);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public void sendInterim() throws Exception {
		if (!this.receiveINTERIM || this.request == null) {
			fail("Did not receive INTERIM or answer already sent.", null);
			throw new Exception("Request: " + this.request);
		}
		logger.debug(" rohit sending sendInterim");
		AccountAnswerImpl answer = new AccountAnswerImpl(
				(Request) request.getMessage(), 2001);

		AvpSet reqSet = request.getMessage().getAvps();

		AvpSet set = answer.getMessage().getAvps();
		set.removeAvp(Avp.DESTINATION_HOST);
		set.removeAvp(Avp.DESTINATION_REALM);
		set.addAvp(reqSet.getAvp(Avp.ACC_RECORD_TYPE),
				reqSet.getAvp(Avp.ACC_RECORD_NUMBER),
				reqSet.getAvp(Avp.AUTH_APPLICATION_ID));
		set.addAvp(Avp.ACCT_INTERIM_INTERVAL, 60, false, false);
		/* rohit modification */
		// [Multiple-Services-Credit-Control]
		/*
		 * AvpSet msccAvp =
		 * set.addGroupedAvp(Avp.MULTIPLE_SERVICES_CREDIT_CONTROL,true,false);
		 * AvpSet gsuAvp = msccAvp.addGroupedAvp(Avp.GRANTED_SERVICE_UNIT, true,
		 * false); gsuAvp.addAvp(Avp.CC_INPUT_OCTETS,(long)100,true,false);
		 * gsuAvp.addAvp(Avp.CC_OUTPUT_OCTETS,(long)100,true,false);
		 * 
		 * /* end of rohit modification
		 */
		logger.debug("rohit inside sendInterim super.serverAccSession = "
				+ super.serverAccSession);
		super.serverAccSession.sendAccountAnswer(answer);
		sentINTERIM = true;
		request = null;
		Utils.printMessage(log, super.stack.getDictionary(),
				answer.getMessage(), true);
	}

	public void sendTerminate() throws Exception {
		if (!this.receiveTERMINATE || this.request == null) {
			fail("Did not receive TERMINATE or answer already sent.", null);
			throw new Exception("Request: " + this.request);
		}

		AccountAnswerImpl answer = new AccountAnswerImpl(
				(Request) request.getMessage(), 2001);

		AvpSet reqSet = request.getMessage().getAvps();

		AvpSet set = answer.getMessage().getAvps();
		set.removeAvp(Avp.DESTINATION_HOST);
		set.removeAvp(Avp.DESTINATION_REALM);
		set.addAvp(reqSet.getAvp(Avp.ACC_RECORD_TYPE),
				reqSet.getAvp(Avp.ACC_RECORD_NUMBER));
		set.addAvp(Avp.ACCT_INTERIM_INTERVAL, 60, false, false);
		super.serverAccSession.sendAccountAnswer(answer);
		super.serverAccSession = null;
		sentTERMINATE = true;
		request = null;
		Utils.printMessage(log, super.stack.getDictionary(),
				answer.getMessage(), true);

	}

	public void sendEvent() throws Exception {
		System.out.println("rohit sendEvent");
		if (!this.receiveEVENT || this.request == null) {
			fail("Did not receive EVENT or answer already sent.", null);
			throw new Exception("Request: " + this.request);
		}
		AccountAnswerImpl answer = new AccountAnswerImpl(
				(Request) request.getMessage(), 2001);

		AvpSet reqSet = request.getMessage().getAvps();

		AvpSet set = answer.getMessage().getAvps();
		set.removeAvp(Avp.DESTINATION_HOST);
		set.removeAvp(Avp.DESTINATION_REALM);
		set.addAvp(reqSet.getAvp(Avp.ACC_RECORD_TYPE),
				reqSet.getAvp(Avp.ACC_RECORD_NUMBER),
				reqSet.getAvp(Avp.AUTH_APPLICATION_ID));
		set.addAvp(Avp.ACCT_INTERIM_INTERVAL, 60, false, false);
		super.serverAccSession.sendAccountAnswer(answer);
		sentEVENT = true;
		request = null;
		Utils.printMessage(log, super.stack.getDictionary(),
				answer.getMessage(), true);
	}

	// ------- initial, this will be triggered for first msg.

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jdiameter.api.NetworkReqListener#processRequest(org.jdiameter.api
	 * .Request)
	 */
	@Override
	public Answer processRequest(Request request) {
		logger.debug("rohit processRequest ServerAcc.java acr11");
		if (request.getCommandCode() != 271) {
			fail("Received Request with code not equal 271!. Code["
					+ request.getCommandCode() + "]", null);
			return null;
		}
		int recordType = 0;
		try {
			recordType = request.getAvps().getAvp(Avp.ACC_RECORD_TYPE)
					.getInteger32();

		} catch (AvpDataException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String sessId;
		try {
			sessId = request.getAvps().getAvp(Avp.SESSION_ID).getUTF8String();
			if (recordType == ACC_REQUEST_TYPE_INITIAL) {
				SessionInfo obj = new SessionInfo();
				obj.key = sessId;
				logger.debug("sessId = " + sessId);
				HashMapDS.INSTANCE.insertInHash(sessId, obj);
				this.getServiceInformation(request, obj);
				this.showSessionInfoValues(obj);
			} else {
				SessionInfo obj = HashMapDS.INSTANCE.searchInHash(sessId);
				this.getServiceInformation(request, obj);
				this.showSessionInfoValues(obj);
			}

				logger.debug("rohit processRequest ServerAcc.java acr12");

				if(super.serverAccSession == null|| recordType ==  ACC_REQUEST_TYPE_INTERIM || recordType == ACC_REQUEST_TYPE_TERMINATE) {
				super.serverAccSession = ((ISessionFactory) this.sessionFactory)
						.getNewAppSession(request.getSessionId(),
								getApplicationId(), ServerAccSession.class,
								(Object) null);
				logger.debug("processRequest ServerAcc");
				((NetworkReqListener) super.serverAccSession)
						.processRequest(request);
				}
		 
		} catch (AvpDataException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InternalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	// ------------- get service-information parameters.

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jdiameter.api.cca.ServerCCASessionListener#doCreditControlRequest
	 * (org.jdiameter.api.cca.ServerCCASession,
	 * org.jdiameter.api.cca.events.JCreditControlRequest)
	 */

	public void getServiceInformation(Request request, SessionInfo obj)
			throws InternalException {

		try {
			// AvpSet avpSet = request.getMessage().getAvps();
			AvpSet avpSet = request.getAvps();
			String sessId = avpSet.getAvp(Avp.SESSION_ID).getUTF8String();
			System.out.println("sessId = " + sessId);

			// service-information
			Avp serviceInfo = avpSet.getAvp(Avp.SERVICE_INFORMATION);
			AvpSet serviceInfoAvpSet = serviceInfo.getGrouped();
			// Subscription-Id-data and Subscription-Id-type
			Avp subscriptionId = serviceInfoAvpSet.getAvp(Avp.SUBSCRIPTION_ID);
			AvpSet subscriptionIdAvpSet = subscriptionId.getGrouped();
			obj.subscriptionIdTypeValue = subscriptionIdAvpSet.getAvp(
					Avp.SUBSCRIPTION_ID_TYPE).getInteger32();
			obj.subscriptionIdDataValue = subscriptionIdAvpSet.getAvp(
					Avp.SUBSCRIPTION_ID_DATA).getUTF8String();
			// System.out.println("SubscriptionIdData = "+obj.subscriptionIdData);
			System.out.println("SubscriptionIdDataValue = "
					+ obj.subscriptionIdDataValue);
			System.out.println("SubscriptionIdTypeValue = "
					+ obj.subscriptionIdTypeValue);
			// PS-Information
			Avp psInfo = serviceInfoAvpSet.getAvp(Avp.PS_INFORMATION);
			AvpSet psInfoAvpSet = psInfo.getGrouped();
			Avp sgsnAddr = psInfoAvpSet.getAvp(Avp.SGSN_ADDRESS);

			// obj.sgsnAddress = IPv4Util.toString(sgsnAddr.getAddress());
			InetAddress tempSgsnAddr = (InetAddress) sgsnAddr.getAddress();
			obj.sgsnAddress = IPv4Util.toString((Inet4Address) tempSgsnAddr);
			// InetAddress i =
			// IPv4Util.toString((InetAddress)(sgsnAddr.getAddress()));;
			// Inet4Address sgsnIn4Address = sgsnAddr.getAddress();
			// obj.ss = sgsnAddr.getAddress();
			// obj.sgsnAddress = sgsnIn4Address.getHostAddress();
			System.out.println("SGSN-Address = " + sgsnAddr);
			System.out.println("SGSN-AddressValue = " + obj.sgsnAddress);
			Avp ggsnAddr = psInfoAvpSet.getAvp(Avp.GGSN_ADDRESS);

			InetAddress tempGgsnAddr = (InetAddress) ggsnAddr.getAddress();
			obj.ggsnAddress = IPv4Util.toString((Inet4Address) tempGgsnAddr);
			System.out.println("GGSN-Address = " + ggsnAddr);
			System.out.println("GGSN-AddressValue = " + obj.ggsnAddress);
			Avp cgAddr = psInfoAvpSet.getAvp(Avp.CG_ADDRESS);
			InetAddress tempCgAddr = (InetAddress) ggsnAddr.getAddress();
			obj.cgAddress = IPv4Util.toString((Inet4Address) tempCgAddr);

			System.out.println("CG-Address = " + cgAddr);
			System.out.println("CG-AddressValue = " + obj.cgAddress);
			Avp calledStationId = psInfoAvpSet.getAvp(30);
			obj.calledStationId = calledStationId.getUTF8String();
			System.out.println("calledStationId = " + calledStationId);
			System.out.println("calledStationIdValue = "
					+ calledStationId.getUTF8String());

			Avp msTmZone = psInfoAvpSet.getAvp(Avp.TGPP_MS_TIMEZONE, 10415);

			System.out.println("3GPP-MS-TIMEZONE = " + msTmZone);
			System.out.println("3GPP-MS-TIMEZONE getUTF8String = "
					+ msTmZone.getUTF8String());
			System.out.println("3GPP-MS-TIMEZONE getOctetString= "
					+ msTmZone.getOctetString());

			obj.tmZone = msTmZone.getRawData();
			int len = obj.tmZone.length;

			System.out.println("len= " + len);
			System.out.println("obj.tmZone[0] = " + obj.tmZone[0]);
			System.out.println("obj.tmZone[1] = " + obj.tmZone[1]);

			obj.recordType = avpSet.getAvp(Avp.ACC_RECORD_TYPE).getInteger32();
			System.out.println("recordType = " + obj.recordType);
			obj.recordNumber = avpSet.getAvp(Avp.ACC_RECORD_NUMBER)
					.getInteger32();
			System.out.println("recordNumber = " + obj.recordNumber);
			
			obj.origHost = avpSet.getAvp(Avp.ORIGIN_HOST).getUTF8String();
			System.out.println("origHost = " + obj.origHost);
			
			avpSet.getAvp(Avp.DESTINATION_HOST).getUTF8String();
			System.out.println("destHost = " + obj.destHost);
			
			obj.accountInterimInterval = avpSet.getAvp(
					Avp.ACCT_INTERIM_INTERVAL).getInteger32();
			 System.out.println("accountInterimInterval = " +
			  obj.accountInterimInterval);
			
			obj.origRealm = avpSet.getAvp(Avp.ORIGIN_REALM);
			//System.out.println("origRealm = " + obj.origRealm);
			
			
			 
			obj.destRealm = avpSet.getAvp(Avp.DESTINATION_REALM);
			
			//System.out.println("destRealm = " + obj.destRealm);
			
			 
			// obj.destHost =
			// 
			
			 

		} catch (Exception e) {
			fail(null, e);
		}

	}

	// print SessionInfo stored values

	public void showSessionInfoValues(SessionInfo obj) throws InternalException {
		try {
			logger.debug("rohit showSessionInfoValues");
			logger.debug("sessionId = " + obj.key);
			logger.debug("recordType = " + obj.recordType);
			logger.debug("recordNumber = " + obj.recordNumber);
			logger.debug("accountInterimInterval = "
					+ obj.accountInterimInterval);
			logger.debug("origHost = " + obj.origHost);
			logger.debug("origRealm = " + obj.origRealm);
			logger.debug("destRealm = " + obj.destRealm);
			logger.debug("destHost = " + obj.destHost);
			logger.debug("subscriptionIdDataValue = "
					+ obj.subscriptionIdDataValue);
			logger.debug("subscriptionIdTypeValue = "
					+ obj.subscriptionIdTypeValue);
			logger.debug("calledStationId = " + obj.calledStationId);
			logger.debug("ggsnAddress = " + obj.ggsnAddress);
			logger.debug("sgsnAddress = " + obj.sgsnAddress);
			logger.debug("cgAddress = " + obj.cgAddress);

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	// ------------- specific, app session listener.

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jdiameter.api.cca.ServerCCASessionListener#doCreditControlRequest
	 * (org.jdiameter.api.cca.ServerCCASession,
	 * org.jdiameter.api.cca.events.JCreditControlRequest)
	 */
	public void doAccRequestEvent(ServerAccSession session,
			AccountRequest request) throws InternalException,
			IllegalDiameterStateException, RouteException, OverloadException {

		try {
			Utils.printMessage(log, super.stack.getDictionary(),
					request.getMessage(), false);
			// INITIAL_REQUEST 2,
			// UPDATE_REQUEST 3,
			// TERMINATION_REQUEST, 4
			// EVENT_REQUEST 1

			switch (request.getAccountingRecordType()) {
			case ACC_REQUEST_TYPE_INITIAL:
				if (receiveINITIAL) {
					fail("Received INITIAL more than once!", null);
				}
				receiveINITIAL = true;
				this.request = request;
				/* rohit modification */
				try {
					// this.getServiceInformation(request);
					this.sendInitial();
				} catch (Exception e) {
					fail(null, e);
				}
				/* end of rohit modification */
				break;

			case ACC_REQUEST_TYPE_INTERIM:
				if (receiveINTERIM) {
					fail("Received INTERIM more than once!", null);
				}
				receiveINTERIM = true;
				this.request = request;
				/* rohit modification */
				try {
					this.sendInterim();
				} catch (Exception e) {
					fail(null, e);
				}
				/* end of rohit modification */
				break;

			case ACC_REQUEST_TYPE_TERMINATE:
				if (receiveTERMINATE) {
					fail("Received TERMINATE more than once!", null);
				}
				receiveTERMINATE = true;
				this.request = request;
				try {
					this.sendTerminate();
				} catch (Exception e) {
					fail(null, e);
				}
				break;

			case ACC_REQUEST_TYPE_EVENT:
				if (receiveEVENT) {
					fail("Received EVENT more than once!", null);
				}
				System.out.println("rohit doAccountingRequest event type");
				receiveEVENT = true;
				this.request = request;
				/* rohit modification */
				try {
					this.sendEvent();
				} catch (Exception e) {
					fail(null, e);
				}
				/* end of rohit modification */

				break;

			default:
				fail("No REQ type present?: "
						+ request.getAccountingRecordType(), null);
			}
		} catch (Exception e) {
			fail(null, e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jdiameter.api.cca.ServerCCASessionListener#doReAuthAnswer(org.jdiameter
	 * .api.cca.ServerCCASession, org.jdiameter.api.auth.events.ReAuthRequest,
	 * org.jdiameter.api.auth.events.ReAuthAnswer)
	 */
	/*
	 * public void doReAuthAnswer(ServerCCASession session, ReAuthRequest
	 * request, ReAuthAnswer answer) throws InternalException,
	 * IllegalDiameterStateException, RouteException, OverloadException {
	 * fail("Received \"ReAuthAnswer\" event, request[" + request +
	 * "], on session[" + session + "]", null); }
	 * 
	 * /* (non-Javadoc)
	 * 
	 * @see
	 * org.jdiameter.api.cca.ServerCCASessionListener#doOtherEvent(org.jdiameter
	 * .api.app.AppSession, org.jdiameter.api.app.AppRequestEvent,
	 * org.jdiameter.api.app.AppAnswerEvent)
	 */
	public void doOtherEvent(AppSession session, AppRequestEvent request,
			AppAnswerEvent answer) throws InternalException,
			IllegalDiameterStateException, RouteException, OverloadException {
		fail("Received \"Other\" event, request[" + request + "], answer["
				+ answer + "], on session[" + session + "]", null);
	}

	public boolean isSentINITIAL() {
		return sentINITIAL;
	}

	public boolean isSentINTERIM() {
		return sentINTERIM;
	}

	public boolean isSentTERMINATE() {
		return sentTERMINATE;
	}

	public boolean isReceiveINITIAL() {
		return receiveINITIAL;
	}

	public boolean isReceiveINTERIM() {
		return receiveINTERIM;
	}

	public boolean isReceiveTERMINATE() {
		return receiveTERMINATE;
	}

	public boolean isSentEVENT() {
		return sentEVENT;
	}

	public boolean isReceiveEVENT() {
		return receiveEVENT;
	}

	public AccountRequest getRequest() {
		return request;
	}

}
