package com.movirtu.diameter.charging.apps.B2BGw.client;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.jdiameter.api.ApplicationId;
import org.jdiameter.api.Avp;
import org.jdiameter.api.AvpSet;
import org.jdiameter.api.InternalException;
import org.jdiameter.api.cca.ClientCCASession;
import org.jdiameter.api.cca.events.JCreditControlRequest;
import org.jdiameter.common.impl.app.cca.JCreditControlRequestImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.diameter.charging.apps.common.TransactionInfo;

public class B2BGwCCRBuilder {
	protected Logger logger = LoggerFactory.getLogger(B2BGwCCRBuilder.class);
	private AvpSet ccrAvps;
	private JCreditControlRequest ccr;

	public static final int CC_REQUEST_TYPE_INITIAL = 1;
	public static final int CC_REQUEST_TYPE_INTERIM = 2;
	public static final int CC_REQUEST_TYPE_TERMINATE = 3;
	public static final int CC_REQUEST_TYPE_EVENT = 4;

	private ClientCCASession session;
	private ApplicationId appID;
	private String serverRealm;
	private String clientRealm;
	private String clientURI;
	private String serverURI;
	private String serviceContextId;
	TransactionInfo obj;

	B2BGwCCRBuilder(ClientCCASession session, ApplicationId appID,
			String clientRealm, String serverRealm, String clientURI,
			String serverURI, String serviceContextId, TransactionInfo objTrans) {
		this.session = session;
		this.appID = appID;
		this.clientRealm = clientRealm;
		this.serverRealm = serverRealm;
		this.clientURI = clientURI;
		this.serverURI = serverURI;
		this.serviceContextId = serviceContextId;
		this.obj = objTrans;

	}

	private void defaultCCR(int requestType) throws InternalException {

	}

	private void SessionInitiateCCR(int requestType) throws InternalException {
		try {
			logger.debug(
					"inside sessionInitiateCCR session.getSessions().get(0) = {}",
					session.getSessions().get(0));
			logger.debug(
					"inside sessionInitiateCCR session.getSessions().size = {}",
					session.getSessions().size());

			ccr = new JCreditControlRequestImpl(session
					.getSessions()
					.get(0)
					.createRequest(JCreditControlRequest.code, appID,
							serverRealm));
			// Create Credit-Control-Request
			// AVPs present by default: Origin-Host, Origin-Realm, Session-Id,
			// Vendor-Specific-Application-Id, Destination-Realm

			ccrAvps = ccr.getMessage().getAvps();
			// remove VENDOR_SPECIFIC_APPLICATION_ID
			// ccrAvps.removeAvp(Avp.VENDOR_SPECIFIC_APPLICATION_ID);

			// { Origin-Host }
			ccrAvps.removeAvp(Avp.ORIGIN_HOST);// one can overwrite default AVP
												// by
												// removing it
			ccrAvps.addAvp(Avp.ORIGIN_HOST, clientURI, true, false, true);

			// [ Destination-Host ]
			ccrAvps.addAvp(Avp.DESTINATION_HOST, serverURI, true, false, false);

			// { Auth-Application-Id }
			// ccrAvps.addAvp(Avp.AUTH_APPLICATION_ID,
			// appID.getAuthAppId(),appID.getVendorId(), true, false, true);

			// { Service-Context-Id }
			// M
			ccrAvps.addAvp(Avp.SERVICE_CONTEXT_ID, obj.serviceContextId, true,
					false, true);

			// { CC-Request-Type }
			ccrAvps.addAvp(Avp.CC_REQUEST_TYPE, obj.recordType, true, false);

			// { CC-Request-Number }

			ccrAvps.addAvp(Avp.CC_REQUEST_NUMBER, obj.recordNumber, true,
					false, true);

			// [Called-Station-Id]
			if (obj.calledStationId != null)
				// ccrAvps.addAvp(obj.calledStationId);
				ccrAvps.addAvp(30, obj.calledStationId, true, false, true);

			// User-Name
			if (obj.userName != null) {
				// ccrAvps.addAvp(Avp.USER_NAME, obj.userName, true, false,
				// true);
				ccrAvps.addAvp(obj.userName);
			}
			// Origin-State-Id

			if (obj.originStateId != null) {
				// ccrAvps.addAvp(Avp.ORIGIN_STATE_ID, originStateId, true,
				// false);
				ccrAvps.addAvp(obj.originStateId);
			}

			// [ Event-Timestamp ]
			// Calendar c = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
			Calendar c = new GregorianCalendar(1900, 0, 1);

			c.setTimeZone(TimeZone.getTimeZone("UTC"));

			long timestamp = (System.currentTimeMillis() - c.getTimeInMillis()) / 1000;

			ccrAvps.addAvp(Avp.EVENT_TIMESTAMP, timestamp, true, false, true);

			// *[ Subscription-Id ]

			AvpSet subscriptionId = ccrAvps.addGroupedAvp(Avp.SUBSCRIPTION_ID,
					true, false);

			// Subscription-Id-Type AVP
			if (obj.isVirtualNumber == true) {
				subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_TYPE,
						obj.vSubscriptionIdTypeValue, true, false);// END_USER_E164(MSISDN)

				// Subscription-Id-Data AVP
				subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_DATA,
						obj.vSubscriptionIdDataValue, true, false, false);
			} else {
				subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_TYPE,
						obj.subscriptionIdTypeValue, true, false);// END_USER_E164(MSISDN)

				// Subscription-Id-Data AVP
				subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_DATA,
						obj.subscriptionIdDataValue, true, false, false);
			}
			// Multiple-Services-Indicator

			if (obj.multipleServiceInd != null) {

				ccrAvps.addAvp(obj.multipleServiceInd);
				
			}

			// [Multiple-Services-Credit-Control]
			for (Avp m : obj.multipleServiceCreditControl) {
				ccrAvps.addAvp(m);
			}

			// Framed-IP-Address
			if (obj.framedIpAddress != null) {
				// ccrAvps.addAvp(8, framedIp, true, false);
				ccrAvps.addAvp(obj.framedIpAddress);
			}
			// 3GPP-IMSI
			if (obj.imsi != null) {
				ccrAvps.addAvp(obj.imsi);
				// ccrAvps.addAvp(Avp.TGPP_IMSI, aIMSI, 10415, true, false,
				// true);
			}
			// 3GPP-Charging-Id

			if (obj.chargingId != null) {
				ccrAvps.addAvp(obj.chargingId);
				// ccrAvps.addAvp(Avp.TGPP_CHARGING_ID, chargingId, 10415, true,
				// false);
			}
			// 3GPP-PDP-Type
			if (obj.pdpType != null) {
				// ccrAvps.addAvp(Avp.TGPP_PDP_TYPE, 0, 10415, true, false,
				// true);
				ccrAvps.addAvp(obj.pdpType);
			}
			// 3GPP-GPRS-QoS-Negotiated-Profile
			if (obj.gprsQosNeg != null) {
				// ccrAvps.addAvp(5, qosNeg, 10415, true, false, true);
				ccrAvps.addAvp(obj.gprsQosNeg);
			}
			// 3GPP-SGSN-Address
			if (obj.sgsnAddr != null) {

				/*
				 * String sgsnAddr = "192.168.56.4"; Inet4Address sgsnAddress =
				 * IPv4Util.fromString(sgsnAddr);
				 * ccrAvps.addAvp(Avp.SGSN_ADDRESS, sgsnAddress, 10415, true,
				 * false);
				 */
				ccrAvps.addAvp(obj.sgsnAddr);
			}

			// 3GPP-GGSN-Address

			if (obj.ggsnAddr != null) {
				ccrAvps.addAvp(obj.ggsnAddr);

				/*
				 * String ggsnAddr = "192.168.56.3"; Inet4Address ggsnAddress =
				 * IPv4Util.fromString(ggsnAddr);
				 * ccrAvps.addAvp(Avp.GGSN_ADDRESS, ggsnAddress, 10415, true,
				 * true);
				 */
			}
			// 3GPP-IMSI-MCC-MNC

			if (obj.imsiMccMnc != null) {

				/*
				 * String imsiMccMnc = "23415";
				 * ccrAvps.addAvp(Avp.TGPP_IMSI_MCC_MNC, imsiMccMnc, 10415,
				 * true, false, true);
				 */

				ccrAvps.addAvp(obj.imsiMccMnc);
			}

			// 3GPP-GGSN-MCC-MNC
			if (obj.ggsnMccMnc != null) {
				ccrAvps.addAvp(obj.ggsnMccMnc);
			}
			/*
			 * String ggsnMccMnc = "23415";
			 * ccrAvps.addAvp(Avp.TGPP_GGSN_MCC_MNC, ggsnMccMnc, 10415, true,
			 * false, true);
			 */

			// 3GPP-NSAPI
			if (obj.nsapi != null) {

				// ccrAvps.addAvp(Avp.TGPP_NSAPI, nsapi, 10415, true, false,
				// true);
				ccrAvps.addAvp(obj.nsapi);
			}
			// selection mode

			if (obj.selectionMode != null) {
				/*
				 * ccrAvps.addAvp(Avp.TGPP_SELECTION_MODE, selMode, 10415, true,
				 * false, true);
				 */
				ccrAvps.addAvp(obj.selectionMode);

			}
			// 3GPP-Charging-Characteristics

			if (obj.chargingCharacteristics != null) {
				/*
				 * ccrAvps.addAvp(Avp.TGPP_CHARGING_CHARACTERISTICS, chargChar,
				 * 10415, true, false, true);
				 */
				ccrAvps.addAvp(obj.chargingCharacteristics);
			}
			// User-Location-Information
			if (obj.userLocationInformation != null) {
				ccrAvps.addAvp(obj.userLocationInformation);
			}
			// Radio-Access-Technology

			if (obj.rat != null) {
				// ccrAvps.addAvp(260, rat, 12645, true, false, true);
				ccrAvps.addAvp(obj.rat);
			}
			// Context-Type
			if (obj.contextType != null) {
				// ccrAvps.addAvp(256, contextTyp, 12645, true, false, true);
				ccrAvps.addAvp(obj.contextType);
			}
			// Bearer-Usage
			if (obj.bearerUsage != null) {
				// ccrAvps.addAvp(1000, bearerUsage, 10415, true, false, true);
				ccrAvps.addAvp(obj.bearerUsage);
			}
			// 3GPP-IMEISV

			if (obj.imeiSv != null) {
				// ccrAvps.addAvp(20, imeiSv, 10415, true, false, false);
				ccrAvps.addAvp(obj.imeiSv);
			}
			// Rulebase-Id

			if (obj.ruleBaseId != null) {
				// ccrAvps.addAvp(262, ruleBaseId, 12645, true, false, true);
				ccrAvps.addAvp(obj.ruleBaseId);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void SessionUpdateCCR(int requestType) throws InternalException {
		try {
			ccr = new JCreditControlRequestImpl(session
					.getSessions()
					.get(0)
					.createRequest(JCreditControlRequest.code, appID,
							serverRealm));
			// Create Credit-Control-Request
			// AVPs present by default: Origin-Host, Origin-Realm, Session-Id,
			// Vendor-Specific-Application-Id, Destination-Realm

			ccrAvps = ccr.getMessage().getAvps();
			// remove VENDOR_SPECIFIC_APPLICATION_ID
			// ccrAvps.removeAvp(Avp.VENDOR_SPECIFIC_APPLICATION_ID);

			// { Origin-Host }
			ccrAvps.removeAvp(Avp.ORIGIN_HOST);// one can overwrite default AVP
												// by
												// removing it
			ccrAvps.addAvp(Avp.ORIGIN_HOST, clientURI, true, false, true);

			// [ Destination-Host ]
			ccrAvps.addAvp(Avp.DESTINATION_HOST, serverURI, true, false, false);

			// { CC-Request-Type }
			ccrAvps.addAvp(Avp.CC_REQUEST_TYPE, obj.recordType, true, false);

			// { CC-Request-Number }

			ccrAvps.addAvp(Avp.CC_REQUEST_NUMBER, obj.recordNumber, true,
					false, true);

			// { Auth-Application-Id }
			// ccrAvps.addAvp(Avp.AUTH_APPLICATION_ID,
			// appID.getAuthAppId(),appID.getVendorId(), true, false, true);

			// { Service-Context-Id }
			// M,V
			ccrAvps.addAvp(Avp.SERVICE_CONTEXT_ID, obj.serviceContextId, true,
					false, false);

			// [Called-Station-Id]
			if (obj.calledStationId != null)
				// ccrAvps.addAvp(obj.calledStationId);
				ccrAvps.addAvp(30, obj.calledStationId, true, false, true);

			// User-Name
			if (obj.userName != null) {
				// ccrAvps.addAvp(Avp.USER_NAME, obj.userName, true, false,
				// true);
				ccrAvps.addAvp(obj.userName);
			}
			// Origin-State-Id

			if (obj.originStateId != null) {
				// ccrAvps.addAvp(Avp.ORIGIN_STATE_ID, originStateId, true,
				// false);
				ccrAvps.addAvp(obj.originStateId);
			}

			// [ Event-Timestamp ]
			// Calendar c = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
			Calendar c = new GregorianCalendar(1900, 0, 1);

			c.setTimeZone(TimeZone.getTimeZone("UTC"));

			long timestamp = (System.currentTimeMillis() - c.getTimeInMillis()) / 1000;

			ccrAvps.addAvp(Avp.EVENT_TIMESTAMP, timestamp, true, false, true);

			// *[ Subscription-Id ]

			AvpSet subscriptionId = ccrAvps.addGroupedAvp(Avp.SUBSCRIPTION_ID,
					true, false);

			// Subscription-Id-Type AVP
			if (obj.isVirtualNumber == true) {
				subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_TYPE,
						obj.vSubscriptionIdTypeValue, true, false);// END_USER_E164(MSISDN)

				// Subscription-Id-Data AVP
				subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_DATA,
						obj.vSubscriptionIdDataValue, true, false, false);
			} else {
				subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_TYPE,
						obj.subscriptionIdTypeValue, true, false);// END_USER_E164(MSISDN)

				// Subscription-Id-Data AVP
				subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_DATA,
						obj.subscriptionIdDataValue, true, false, false);
			}
			// Multiple-Services-Indicator

			if (obj.multipleServiceInd != null) {

				ccrAvps.addAvp(obj.multipleServiceInd);
				/*
				 * ccrAvps.addAvp(Avp.MULTIPLE_SERVICES_INDICATOR, 1, true,
				 * false, true);
				 */
			}

			// [Multiple-Services-Credit-Control]
			for (Avp m : obj.multipleServiceCreditControl) {
				ccrAvps.addAvp(m);
			}

			// Framed-IP-Address
			if (obj.framedIpAddress != null) {
				// ccrAvps.addAvp(8, framedIp, true, false);
				ccrAvps.addAvp(obj.framedIpAddress);
			}
			// 3GPP-IMSI
			if (obj.imsi != null) {
				ccrAvps.addAvp(obj.imsi);
				// ccrAvps.addAvp(Avp.TGPP_IMSI, aIMSI, 10415, true, false,
				// true);
			}
			// 3GPP-Charging-Id

			if (obj.chargingId != null) {
				ccrAvps.addAvp(obj.chargingId);
				// ccrAvps.addAvp(Avp.TGPP_CHARGING_ID, chargingId, 10415, true,
				// false);
			}
			// 3GPP-PDP-Type
			if (obj.pdpType != null) {
				// ccrAvps.addAvp(Avp.TGPP_PDP_TYPE, 0, 10415, true, false,
				// true);
				ccrAvps.addAvp(obj.pdpType);
			}
			// 3GPP-GPRS-QoS-Negotiated-Profile
			if (obj.gprsQosNeg != null) {
				// ccrAvps.addAvp(5, qosNeg, 10415, true, false, true);
				ccrAvps.addAvp(obj.gprsQosNeg);
			}
			// 3GPP-SGSN-Address
			if (obj.sgsnAddr != null) {

				/*
				 * String sgsnAddr = "192.168.56.4"; Inet4Address sgsnAddress =
				 * IPv4Util.fromString(sgsnAddr);
				 * ccrAvps.addAvp(Avp.SGSN_ADDRESS, sgsnAddress, 10415, true,
				 * false);
				 */
				ccrAvps.addAvp(obj.sgsnAddr);
			}

			// 3GPP-GGSN-Address

			if (obj.ggsnAddr != null) {
				ccrAvps.addAvp(obj.ggsnAddr);

				/*
				 * String ggsnAddr = "192.168.56.3"; Inet4Address ggsnAddress =
				 * IPv4Util.fromString(ggsnAddr);
				 * ccrAvps.addAvp(Avp.GGSN_ADDRESS, ggsnAddress, 10415, true,
				 * true);
				 */
			}
			// 3GPP-IMSI-MCC-MNC

			if (obj.imsiMccMnc != null) {

				/*
				 * String imsiMccMnc = "23415";
				 * ccrAvps.addAvp(Avp.TGPP_IMSI_MCC_MNC, imsiMccMnc, 10415,
				 * true, false, true);
				 */

				ccrAvps.addAvp(obj.imsiMccMnc);
			}

			// 3GPP-GGSN-MCC-MNC
			if (obj.ggsnMccMnc != null) {
				ccrAvps.addAvp(obj.ggsnMccMnc);
			}
			/*
			 * String ggsnMccMnc = "23415";
			 * ccrAvps.addAvp(Avp.TGPP_GGSN_MCC_MNC, ggsnMccMnc, 10415, true,
			 * false, true);
			 */

			// 3GPP-NSAPI
			if (obj.nsapi != null) {

				// ccrAvps.addAvp(Avp.TGPP_NSAPI, nsapi, 10415, true, false,
				// true);
				ccrAvps.addAvp(obj.nsapi);
			}
			// selection mode

			if (obj.selectionMode != null) {
				/*
				 * ccrAvps.addAvp(Avp.TGPP_SELECTION_MODE, selMode, 10415, true,
				 * false, true);
				 */
				ccrAvps.addAvp(obj.selectionMode);

			}
			// 3GPP-Charging-Characteristics

			if (obj.chargingCharacteristics != null) {
				/*
				 * ccrAvps.addAvp(Avp.TGPP_CHARGING_CHARACTERISTICS, chargChar,
				 * 10415, true, false, true);
				 */
				ccrAvps.addAvp(obj.chargingCharacteristics);
			}
			// User-Location-Information
			if (obj.userLocationInformation != null) {
				ccrAvps.addAvp(obj.userLocationInformation);
			}

			// Radio-Access-Technology

			if (obj.rat != null) {
				// ccrAvps.addAvp(260, rat, 12645, true, false, true);
				ccrAvps.addAvp(obj.rat);
			}
			// Context-Type
			if (obj.contextType != null) {
				// ccrAvps.addAvp(256, contextTyp, 12645, true, false, true);
				ccrAvps.addAvp(obj.contextType);
			}
			// Bearer-Usage
			if (obj.bearerUsage != null) {
				// ccrAvps.addAvp(1000, bearerUsage, 10415, true, false, true);
				ccrAvps.addAvp(obj.bearerUsage);
			}
			// 3GPP-IMEISV

			if (obj.imeiSv != null) {
				// ccrAvps.addAvp(20, imeiSv, 10415, true, false, false);
				ccrAvps.addAvp(obj.imeiSv);
			}
			// Rulebase-Id

			if (obj.ruleBaseId != null) {
				// ccrAvps.addAvp(262, ruleBaseId, 12645, true, false, true);
				ccrAvps.addAvp(obj.ruleBaseId);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void SessionTerminateCCR(int requestType) throws InternalException {
		try {
			ccr = new JCreditControlRequestImpl(session
					.getSessions()
					.get(0)
					.createRequest(JCreditControlRequest.code, appID,
							serverRealm));
			// Create Credit-Control-Request
			// AVPs present by default: Origin-Host, Origin-Realm, Session-Id,
			// Vendor-Specific-Application-Id, Destination-Realm

			ccrAvps = ccr.getMessage().getAvps();
			// remove VENDOR_SPECIFIC_APPLICATION_ID
			// ccrAvps.removeAvp(Avp.VENDOR_SPECIFIC_APPLICATION_ID);

			// { Origin-Host }
			ccrAvps.removeAvp(Avp.ORIGIN_HOST);// one can overwrite default AVP
												// by
												// removing it
			ccrAvps.addAvp(Avp.ORIGIN_HOST, clientURI, true, false, true);

			// [ Destination-Host ]
			ccrAvps.addAvp(Avp.DESTINATION_HOST, serverURI, true, false, false);

			// { Auth-Application-Id }
			// ccrAvps.addAvp(Avp.AUTH_APPLICATION_ID,
			// appID.getAuthAppId(),appID.getVendorId(), true, false, true);

			// { Service-Context-Id }
			// M,V
			ccrAvps.addAvp(Avp.SERVICE_CONTEXT_ID, obj.serviceContextId, true,
					false, false);
			// { CC-Request-Type }
			ccrAvps.addAvp(Avp.CC_REQUEST_TYPE, requestType, true, false);

			// { CC-Request-Number }

			ccrAvps.addAvp(Avp.CC_REQUEST_NUMBER, 0, true, false, true);

			// [Called-Station-Id]
			if (obj.calledStationId != null)
				// ccrAvps.addAvp(obj.calledStationId);
				ccrAvps.addAvp(30, obj.calledStationId, true, false, true);

			// User-Name
			if (obj.userName != null) {
				// ccrAvps.addAvp(Avp.USER_NAME, obj.userName, true, false,
				// true);
				ccrAvps.addAvp(obj.userName);
			}
			// Origin-State-Id

			if (obj.originStateId != null) {
				// ccrAvps.addAvp(Avp.ORIGIN_STATE_ID, originStateId, true,
				// false);
				ccrAvps.addAvp(obj.originStateId);
			}

			// [ Event-Timestamp ]
			// Calendar c = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
			Calendar c = new GregorianCalendar(1900, 0, 1);

			c.setTimeZone(TimeZone.getTimeZone("UTC"));

			long timestamp = (System.currentTimeMillis() - c.getTimeInMillis()) / 1000;

			ccrAvps.addAvp(Avp.EVENT_TIMESTAMP, timestamp, true, false, true);

			// *[ Subscription-Id ]

			AvpSet subscriptionId = ccrAvps.addGroupedAvp(Avp.SUBSCRIPTION_ID,
					true, false);

			// Subscription-Id-Type AVP
			if (obj.isVirtualNumber == true) {
				subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_TYPE,
						obj.vSubscriptionIdTypeValue, true, false);// END_USER_E164(MSISDN)

				// Subscription-Id-Data AVP
				subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_DATA,
						obj.vSubscriptionIdDataValue, true, false, false);
			} else {
				subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_TYPE,
						obj.subscriptionIdTypeValue, true, false);// END_USER_E164(MSISDN)

				// Subscription-Id-Data AVP
				subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_DATA,
						obj.subscriptionIdDataValue, true, false, false);
			}
			// Multiple-Services-Indicator

			if (obj.multipleServiceInd != null) {

				ccrAvps.addAvp(obj.multipleServiceInd);
				/*
				 * ccrAvps.addAvp(Avp.MULTIPLE_SERVICES_INDICATOR, 1, true,
				 * false, true);
				 */
			}

			// [Multiple-Services-Credit-Control]
			for (Avp m : obj.multipleServiceCreditControl) {
				ccrAvps.addAvp(m);
			}

			// Framed-IP-Address
			if (obj.framedIpAddress != null) {
				// ccrAvps.addAvp(8, framedIp, true, false);
				ccrAvps.addAvp(obj.framedIpAddress);
			}
			// 3GPP-IMSI
			if (obj.imsi != null) {
				ccrAvps.addAvp(obj.imsi);
				// ccrAvps.addAvp(Avp.TGPP_IMSI, aIMSI, 10415, true, false,
				// true);
			}
			// 3GPP-Charging-Id

			if (obj.chargingId != null) {
				ccrAvps.addAvp(obj.chargingId);
				// ccrAvps.addAvp(Avp.TGPP_CHARGING_ID, chargingId, 10415, true,
				// false);
			}
			// 3GPP-PDP-Type
			if (obj.pdpType != null) {
				// ccrAvps.addAvp(Avp.TGPP_PDP_TYPE, 0, 10415, true, false,
				// true);
				ccrAvps.addAvp(obj.pdpType);
			}
			// 3GPP-GPRS-QoS-Negotiated-Profile
			if (obj.gprsQosNeg != null) {
				// ccrAvps.addAvp(5, qosNeg, 10415, true, false, true);
				ccrAvps.addAvp(obj.gprsQosNeg);
			}
			// 3GPP-SGSN-Address
			if (obj.sgsnAddr != null) {

				/*
				 * String sgsnAddr = "192.168.56.4"; Inet4Address sgsnAddress =
				 * IPv4Util.fromString(sgsnAddr);
				 * ccrAvps.addAvp(Avp.SGSN_ADDRESS, sgsnAddress, 10415, true,
				 * false);
				 */
				ccrAvps.addAvp(obj.sgsnAddr);
			}

			// 3GPP-GGSN-Address

			if (obj.ggsnAddr != null) {
				ccrAvps.addAvp(obj.ggsnAddr);

				/*
				 * String ggsnAddr = "192.168.56.3"; Inet4Address ggsnAddress =
				 * IPv4Util.fromString(ggsnAddr);
				 * ccrAvps.addAvp(Avp.GGSN_ADDRESS, ggsnAddress, 10415, true,
				 * true);
				 */
			}
			// 3GPP-IMSI-MCC-MNC

			if (obj.imsiMccMnc != null) {

				/*
				 * String imsiMccMnc = "23415";
				 * ccrAvps.addAvp(Avp.TGPP_IMSI_MCC_MNC, imsiMccMnc, 10415,
				 * true, false, true);
				 */

				ccrAvps.addAvp(obj.imsiMccMnc);
			}

			// 3GPP-GGSN-MCC-MNC
			if (obj.ggsnMccMnc != null) {
				ccrAvps.addAvp(obj.ggsnMccMnc);
			}
			/*
			 * String ggsnMccMnc = "23415";
			 * ccrAvps.addAvp(Avp.TGPP_GGSN_MCC_MNC, ggsnMccMnc, 10415, true,
			 * false, true);
			 */

			// 3GPP-NSAPI
			if (obj.nsapi != null) {

				// ccrAvps.addAvp(Avp.TGPP_NSAPI, nsapi, 10415, true, false,
				// true);
				ccrAvps.addAvp(obj.nsapi);
			}
			// selection mode

			if (obj.selectionMode != null) {
				/*
				 * ccrAvps.addAvp(Avp.TGPP_SELECTION_MODE, selMode, 10415, true,
				 * false, true);
				 */
				ccrAvps.addAvp(obj.selectionMode);

			}
			// 3GPP-Charging-Characteristics

			if (obj.chargingCharacteristics != null) {
				/*
				 * ccrAvps.addAvp(Avp.TGPP_CHARGING_CHARACTERISTICS, chargChar,
				 * 10415, true, false, true);
				 */
				ccrAvps.addAvp(obj.chargingCharacteristics);
			}
			// User-Location-Information
			if (obj.userLocationInformation != null) {
				ccrAvps.addAvp(obj.userLocationInformation);
			}

			// Radio-Access-Technology

			if (obj.rat != null) {
				// ccrAvps.addAvp(260, rat, 12645, true, false, true);
				ccrAvps.addAvp(obj.rat);
			}
			// Context-Type
			if (obj.contextType != null) {
				// ccrAvps.addAvp(256, contextTyp, 12645, true, false, true);
				ccrAvps.addAvp(obj.contextType);
			}
			// Bearer-Usage
			if (obj.bearerUsage != null) {
				// ccrAvps.addAvp(1000, bearerUsage, 10415, true, false, true);
				ccrAvps.addAvp(obj.bearerUsage);
			}
			// 3GPP-IMEISV

			if (obj.imeiSv != null) {
				// ccrAvps.addAvp(20, imeiSv, 10415, true, false, false);
				ccrAvps.addAvp(obj.imeiSv);
			}
			// Rulebase-Id

			if (obj.ruleBaseId != null) {
				// ccrAvps.addAvp(262, ruleBaseId, 12645, true, false, true);
				ccrAvps.addAvp(obj.ruleBaseId);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	public JCreditControlRequest initialCCR() throws Exception {
		SessionInitiateCCR(CC_REQUEST_TYPE_INITIAL);
		return ccr;

	}

	public JCreditControlRequest interimCCR() throws Exception {

		SessionUpdateCCR(CC_REQUEST_TYPE_INTERIM);
		return ccr;

	}

	public JCreditControlRequest terminateCCR() throws Exception {

		SessionTerminateCCR(CC_REQUEST_TYPE_TERMINATE);
		return ccr;

	}

	public JCreditControlRequest eventCCR() throws Exception {

		defaultCCR(CC_REQUEST_TYPE_EVENT);
		return ccr;
	}

}
