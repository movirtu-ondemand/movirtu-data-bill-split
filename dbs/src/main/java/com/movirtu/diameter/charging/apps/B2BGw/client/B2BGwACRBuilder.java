package com.movirtu.diameter.charging.apps.B2BGw.client;

import java.net.Inet4Address;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.dellroad.stuff.net.IPv4Util;
import org.jdiameter.api.ApplicationId;
import org.jdiameter.api.Avp;
import org.jdiameter.api.AvpSet;
import org.jdiameter.api.InternalException;
import org.jdiameter.api.acc.ClientAccSession;
import org.jdiameter.api.acc.events.AccountRequest;
import org.jdiameter.common.impl.app.acc.AccountRequestImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.diameter.charging.apps.common.TransactionInfo;
import com.movirtu.diameter.charging.apps.common.Utils;
public class B2BGwACRBuilder {
	private static final Logger logger = LoggerFactory.getLogger(B2BGwACRBuilder.class);
	private AvpSet acrAvps;
	private AccountRequest acr;

	static final int ACC_REQUEST_TYPE_INITIAL = 2;
	static final int ACC_REQUEST_TYPE_INTERIM = 3;
	static final int ACC_REQUEST_TYPE_TERMINATE = 4;
	static final int ACC_REQUEST_TYPE_EVENT = 1;

	private ClientAccSession session;
	private ApplicationId appID;
	private String serverRealm;
	private String clientRealm;
	private String clientURI;
	private String serverURI;
	private TransactionInfo obj;


	B2BGwACRBuilder(ClientAccSession session, ApplicationId appID,
			String clientRealm, String serverRealm, String clientURI,
			String serverURI, TransactionInfo objTrans)
	{
		this.session = session;
		this.appID = appID;
		this.clientRealm = clientRealm;
		this.serverRealm = serverRealm;
		this.clientURI = clientURI;
		this.serverURI = serverURI;
		this .obj = objTrans;

	}

	

	private void SessionInitiateACR(int requestType) throws InternalException {
		logger.debug("inside SessionInitiateACR B2BGwACRBuilder.java...appID ="
						+ appID);
		this.obj.display();
		acr = new AccountRequestImpl(session.getSessions().get(0)
				.createRequest(AccountRequest.code, appID, serverRealm));
		// Account-Request
		// AVPs present by default: Origin-Host, Origin-Realm, Session-Id,
		// Vendor-Specific-Application-Id, Destination-Realm

		acrAvps = acr.getMessage().getAvps();
		// remove VENDOR_SPECIFIC_APPLICATION_ID
		// ccrAvps.removeAvp(Avp.VENDOR_SPECIFIC_APPLICATION_ID);

		// { Origin-Host }
		acrAvps.removeAvp(Avp.ORIGIN_HOST);// one can overwrite default AVP by
											// removing it
		acrAvps.addAvp(Avp.ORIGIN_HOST, clientURI, true, false, true);

		// [ Destination-Host ]
		acrAvps.addAvp(Avp.DESTINATION_HOST, serverURI, true, false, false);

		// { Auth-Application-Id }
		// ccrAvps.addAvp(Avp.AUTH_APPLICATION_ID,
		// appID.getAuthAppId(),appID.getVendorId(), true, false, true);

		// { Service-Context-Id }
		// M,V
		/*
		 * acrAvps.addAvp(Avp.SERVICE_CONTEXT_ID, serviceContextId, true, false,
		 * false);
		 */
		// { Accounting-Record-Type }
		acrAvps.addAvp(Avp.ACC_RECORD_TYPE, requestType, true, false);

		// { Accounting-Record-Number }

		acrAvps.addAvp(Avp.ACC_RECORD_NUMBER, obj.recordNumber, true, false, true);

		// [ Requested-Action ]

		// ccrAvps.addAvp(Avp.REQUESTED_ACTION, 0, true, false);

		// [ Event-Timestamp ]
		// Calendar c = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
		Calendar c = new GregorianCalendar(1900, 0, 1);

		c.setTimeZone(TimeZone.getTimeZone("UTC"));

		long timestamp = (System.currentTimeMillis() - c.getTimeInMillis()) / 1000;

		acrAvps.addAvp(Avp.EVENT_TIMESTAMP, timestamp, true, false, true);

		// [Acct-Application-Id]

		// acrAvps.addAvp(Avp.ACCT_APPLICATION_ID, 3, true, false);

		// *[ Subscription-Id ]
		/*
		 * AvpSet subscriptionId = ccrAvps.addGroupedAvp(Avp.SUBSCRIPTION_ID,
		 * true, false);
		 * 
		 * // Subscription-Id-Type AVP
		 * subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_TYPE, 1, true, false);//
		 * END_USER_IMSI
		 * 
		 * // Subscription-Id-Data AVP
		 * subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_DATA, aIMSI, true, true,
		 * false);
		 * 
		 * 
		 * 
		 * // [ Requested-Service-Unit ]
		 * 
		 * /* AvpSet rsuAvp = ccrAvps.addGroupedAvp(Avp.REQUESTED_SERVICE_UNIT,
		 * true, false); rsuAvp.addAvp(Avp.CC_SERVICE_SPECIFIC_UNITS, (long) 1,
		 * true, false);
		 */

		// [Multiple-Services-Credit-Control]
		
		  AvpSet msccAvp =
		  acrAvps.addGroupedAvp(Avp.MULTIPLE_SERVICES_CREDIT_CONTROL
		  ,true,false);
		  AvpSet rsuAvp = msccAvp.addGroupedAvp(Avp.REQUESTED_SERVICE_UNIT, true, false);
		  rsuAvp.addAvp(Avp.CC_INPUT_OCTETS,(long)obj.rsuIo,true,false);
		  rsuAvp.addAvp(Avp.CC_OUTPUT_OCTETS,(long)obj.rsuOo,true,false);
		  
		  /* SERVICE_IDENTIFIER ccrAvps.addAvp(439, service_identifier, true,
		  false);
		  
		 /* //[Service Information] AvpSet si =
		 * ccrAvps.addGroupedAvp(Avp.SERVICE_INFORMATION, 10415, false,false);
		 * AvpSet psinfo = si.addGroupedAvp(Avp.PS_INFORMATION, 10415,false,
		 * false); // CALLED_STATION_ID
		 * 
		 * //String apn = "airtelgprs.com"; /* String out = null; try { out =
		 * new String(apn.getBytes("UTF-8"), "ISO-8859-1"); } catch
		 * (java.io.UnsupportedEncodingException e) {
		 * System.out.println("failed to convert apn string to UTF8String format"
		 * );
		 * 
		 * }
		 */
		// psinfo.addAvp(30, apn, true,false,true);

		// [Service-Information]

		AvpSet si = acrAvps.addGroupedAvp(Avp.SERVICE_INFORMATION, 10415,
				false, false);
		// [Subscription-Id]
		AvpSet subsId = si.addGroupedAvp(Avp.SUBSCRIPTION_ID, (long) 0, false,
				false);
		if(obj.isVirtualNumber) {
		subsId.addAvp(Avp.SUBSCRIPTION_ID_TYPE, obj.vSubscriptionIdTypeValue, false, false);
		
		subsId.addAvp(Avp.SUBSCRIPTION_ID_DATA, obj.vSubscriptionIdDataValue, true);
		}
		else {
			subsId.addAvp(Avp.SUBSCRIPTION_ID_TYPE, obj.subscriptionIdTypeValue, false, false);
			
			subsId.addAvp(Avp.SUBSCRIPTION_ID_DATA, obj.subscriptionIdDataValue, true);
		}
		// [PS-Information]
		if(obj.isPsInformation == true) {
		AvpSet psInfo = si.addGroupedAvp(Avp.PS_INFORMATION, 10415, false,
				false);
		//3gpp-charging-id
		if(obj.chargingId!=null) {
			byte[] tempB = obj.chargingId.getRawData();
			psInfo.addAvp(Avp.TGPP_CHARGING_ID, obj.chargingId.getRawData(),10415,false,false);
			logger.debug("charging-id = {}",Utils.bytesToHexString(tempB));
		}
		//3GPP-GPRS-Negotiated-QoS-Profile 
		
		// [SGSN-Address]

		if(!(obj.sgsnAddress.length()==0)) {
		Inet4Address sgsnAddress = IPv4Util.fromString(obj.sgsnAddress);
		psInfo.addAvp(Avp.SGSN_ADDRESS, sgsnAddress, 10415, false, false);
		}
		// psInfo.addAvp(Avp.SGSN_ADDRESS, sgsnAddr, false, false, true);
		// [GGSN-Address]
		if(!(obj.ggsnAddress.length()==0)) {
		Inet4Address ggsnAddress = IPv4Util.fromString(obj.ggsnAddress);
		psInfo.addAvp(Avp.GGSN_ADDRESS, ggsnAddress, 10415, false, false);
		}
		// psInfo.addAvp(Avp.GGSN_ADDRESS, ggsnAddr, false, false, true);
		// [CG-Address]
		if(!(obj.cgAddress.length()==0)) {
		Inet4Address cgAddress = IPv4Util.fromString(obj.cgAddress);
		psInfo.addAvp(Avp.CG_ADDRESS, cgAddress, 10415, false, false);
		}
		// psInfo.addAvp(Avp.CG_ADDRESS, cgAddr, false, false, true);
		// [Called-Station-Id]
		if(obj.calledStationId.length() !=0)
		psInfo.addAvp(30, obj.calledStationId, false, false, true);
		// [3GPP-MS-Timezone]
		byte[] tz = { (byte) 0x0C, (byte) 0x00 };

		psInfo.addAvp(Avp.TGPP_MS_TIMEZONE, tz, 10415, true, false);
		}
	}

	private void SessionUpdateACR(int requestType) throws InternalException {

		acr = new AccountRequestImpl(session.getSessions().get(0)
				.createRequest(AccountRequest.code, appID, serverRealm));
		// Create Credit-Control-Request
		// AVPs present by default: Origin-Host, Origin-Realm, Session-Id,
		// Vendor-Specific-Application-Id, Destination-Realm

		acrAvps = acr.getMessage().getAvps();
		// remove VENDOR_SPECIFIC_APPLICATION_ID
		// ccrAvps.removeAvp(Avp.VENDOR_SPECIFIC_APPLICATION_ID);

		// { Origin-Host }
		acrAvps.removeAvp(Avp.ORIGIN_HOST);// one can overwrite default AVP by
											// removing it
		acrAvps.addAvp(Avp.ORIGIN_HOST, clientURI, true, false, true);

		// [ Destination-Host ]
		acrAvps.addAvp(Avp.DESTINATION_HOST, serverURI, true, false, false);

		// { Auth-Application-Id }
		// ccrAvps.addAvp(Avp.AUTH_APPLICATION_ID,
		// appID.getAuthAppId(),appID.getVendorId(), true, false, true);

		// { Service-Context-Id }
		// M,V
		/*
		 * ccrAvps.addAvp(Avp.SERVICE_CONTEXT_ID, serviceContextId, true, false,
		 * false);
		 */
		// { Accounting-Record-Type }
		acrAvps.addAvp(Avp.ACC_RECORD_TYPE, requestType, true, false);

		// { Accounting-Record-Number }

		acrAvps.addAvp(Avp.ACC_RECORD_NUMBER, obj.recordNumber, true, false, true);

		// [ Requested-Action ]

		// ccrAvps.addAvp(Avp.REQUESTED_ACTION, 0, true, false);

		// [ Event-Timestamp ]
		// Calendar c = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
		Calendar c = new GregorianCalendar(1900, 0, 1);

		c.setTimeZone(TimeZone.getTimeZone("UTC"));

		long timestamp = (System.currentTimeMillis() - c.getTimeInMillis()) / 1000;

		acrAvps.addAvp(Avp.EVENT_TIMESTAMP, timestamp, true, false, true);

		// *[ Subscription-Id ]
		/*
		 * AvpSet subscriptionId = ccrAvps.addGroupedAvp(Avp.SUBSCRIPTION_ID,
		 * true, false);
		 * 
		 * // Subscription-Id-Type AVP
		 * subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_TYPE, 1, true, false);//
		 * END_USER_IMSI
		 * 
		 * // Subscription-Id-Data AVP
		 * subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_DATA, aIMSI, true, true,
		 * false);
		 * 
		 * 
		 * 
		 * // [ Requested-Service-Unit ]
		 * 
		 * /* AvpSet rsuAvp = ccrAvps.addGroupedAvp(Avp.REQUESTED_SERVICE_UNIT,
		 * true, false); rsuAvp.addAvp(Avp.CC_SERVICE_SPECIFIC_UNITS, (long) 1,
		 * true, false);
		 */

		// [Multiple-Services-Credit-Control]

		
		  AvpSet msccAvp =
		  acrAvps.addGroupedAvp(Avp.MULTIPLE_SERVICES_CREDIT_CONTROL
		  ,true,false); AvpSet rsuAvp =
		  msccAvp.addGroupedAvp(Avp.REQUESTED_SERVICE_UNIT, true, false);
		  rsuAvp.addAvp(Avp.CC_INPUT_OCTETS,(long)obj.rsuIo,true,false);
		  rsuAvp.addAvp(Avp.CC_OUTPUT_OCTETS,(long)obj.rsuOo,true,false);
		  AvpSet usuAvp = msccAvp.addGroupedAvp(Avp.USED_SERVICE_UNIT, true,
		  false);
		  usuAvp.addAvp(Avp.CC_INPUT_OCTETS,(long)obj.usuIo,true,false);
		  usuAvp.addAvp(Avp.CC_OUTPUT_OCTETS,(long)obj.usuOo,true,false);
		 

		// SERVICE_IDENTIFIER
		// ccrAvps.addAvp(439, service_identifier, true, false);

		// [Service Information]
		/*
		 * AvpSet si = ccrAvps.addGroupedAvp(Avp.SERVICE_INFORMATION, 10415,
		 * false,false); AvpSet psinfo = si.addGroupedAvp(Avp.PS_INFORMATION,
		 * 10415,false, false);
		 */
		// CALLED_STATION_ID

		// String apn = "airtelgprs.com";
		/*
		 * String out = null; try { out = new String(apn.getBytes("UTF-8"),
		 * "ISO-8859-1"); } catch (java.io.UnsupportedEncodingException e) {
		 * System
		 * .out.println("failed to convert apn string to UTF8String format");
		 * 
		 * }
		 */
		/*
		 * psinfo.addAvp(30, apn, true,false,true);
		 */

		// [Service-Information]

				AvpSet si = acrAvps.addGroupedAvp(Avp.SERVICE_INFORMATION, 10415,
						false, false);
				// [Subscription-Id]
				AvpSet subsId = si.addGroupedAvp(Avp.SUBSCRIPTION_ID, (long) 0, false,
						false);
				if(obj.isVirtualNumber) {
				subsId.addAvp(Avp.SUBSCRIPTION_ID_TYPE, obj.vSubscriptionIdTypeValue, false, false);
				// subsId.addAvp(Avp.SUBSCRIPTION_ID_DATA, aIMSI, false, false);
				subsId.addAvp(Avp.SUBSCRIPTION_ID_DATA, obj.vSubscriptionIdDataValue, true);
				}
				else {
					subsId.addAvp(Avp.SUBSCRIPTION_ID_TYPE, obj.subscriptionIdTypeValue, false, false);
					// subsId.addAvp(Avp.SUBSCRIPTION_ID_DATA, aIMSI, false, false);
					subsId.addAvp(Avp.SUBSCRIPTION_ID_DATA, obj.subscriptionIdDataValue, true);
				}
				// [PS-Information]
				if(obj.isPsInformation == true) {
				AvpSet psInfo = si.addGroupedAvp(Avp.PS_INFORMATION, 10415, false,
						false);
				// [SGSN-Address]

				if(!(obj.sgsnAddress.length()==0)) {
				Inet4Address sgsnAddress = IPv4Util.fromString(obj.sgsnAddress);
				psInfo.addAvp(Avp.SGSN_ADDRESS, sgsnAddress, 10415, false, false);
				}
				// psInfo.addAvp(Avp.SGSN_ADDRESS, sgsnAddr, false, false, true);
				// [GGSN-Address]
				if(!(obj.ggsnAddress.length()==0)) {
				Inet4Address ggsnAddress = IPv4Util.fromString(obj.ggsnAddress);
				psInfo.addAvp(Avp.GGSN_ADDRESS, ggsnAddress, 10415, false, false);
				}
				// psInfo.addAvp(Avp.GGSN_ADDRESS, ggsnAddr, false, false, true);
				// [CG-Address]
				if(!(obj.cgAddress.length()==0)) {
				Inet4Address cgAddress = IPv4Util.fromString(obj.cgAddress);
				psInfo.addAvp(Avp.CG_ADDRESS, cgAddress, 10415, false, false);
				}
				// psInfo.addAvp(Avp.CG_ADDRESS, cgAddr, false, false, true);
				// [Called-Station-Id]
				if(obj.calledStationId.length() == 0)
				psInfo.addAvp(30, obj.calledStationId, false, false, true);
				// [3GPP-MS-Timezone]
				byte[] tz = { (byte) 0x0C, (byte) 0x00 };

				psInfo.addAvp(Avp.TGPP_MS_TIMEZONE, tz, 10415, true, false);
				}

	}

	private void SessionTerminateACR(int requestType) throws InternalException {

		acr = new AccountRequestImpl(session.getSessions().get(0)
				.createRequest(AccountRequest.code, appID, serverRealm));
		// Create Credit-Control-Request
		// AVPs present by default: Origin-Host, Origin-Realm, Session-Id,
		// Vendor-Specific-Application-Id, Destination-Realm

		acrAvps = acr.getMessage().getAvps();
		// remove VENDOR_SPECIFIC_APPLICATION_ID
		// ccrAvps.removeAvp(Avp.VENDOR_SPECIFIC_APPLICATION_ID);

		// { Origin-Host }
		acrAvps.removeAvp(Avp.ORIGIN_HOST);// one can overwrite default AVP by
											// removing it
		acrAvps.addAvp(Avp.ORIGIN_HOST, clientURI, true, false, true);

		// [ Destination-Host ]
		acrAvps.addAvp(Avp.DESTINATION_HOST, serverURI, true, false, false);

		// { Auth-Application-Id }
		// ccrAvps.addAvp(Avp.AUTH_APPLICATION_ID,
		// appID.getAuthAppId(),appID.getVendorId(), true, false, true);

		// { Service-Context-Id }
		// M,V
		/*acrAvps.addAvp(Avp.SERVICE_CONTEXT_ID, serviceContextId, true, false,
				false);*/
		// { Accounting-Record-Type }
		acrAvps.addAvp(Avp.ACC_RECORD_TYPE, requestType, true, false);

		// { CC-Request-Number }

		acrAvps.addAvp(Avp.ACC_RECORD_NUMBER, obj.recordNumber, true, false, true);

		// [ Requested-Action ]

		// ccrAvps.addAvp(Avp.REQUESTED_ACTION, 0, true, false);

		// [ Event-Timestamp ]
		// Calendar c = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
		Calendar c = new GregorianCalendar(1900, 0, 1);

		c.setTimeZone(TimeZone.getTimeZone("UTC"));

		long timestamp = (System.currentTimeMillis() - c.getTimeInMillis()) / 1000;

		acrAvps.addAvp(Avp.EVENT_TIMESTAMP, timestamp, true, false, true);
		
		// *[ Subscription-Id ]

		/*
		 * AvpSet subscriptionId = ccrAvps.addGroupedAvp(Avp.SUBSCRIPTION_ID,
		 * true, false);
		 * 
		 * // Subscription-Id-Type AVP
		 * subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_TYPE, 1, true, false);//
		 * END_USER_IMSI
		 * 
		 * // Subscription-Id-Data AVP
		 * subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_DATA, aIMSI, true, true,
		 * false);
		 * 
		 * 
		 * 
		 * // [ Requested-Service-Unit ]
		 * 
		 * /* AvpSet rsuAvp = ccrAvps.addGroupedAvp(Avp.REQUESTED_SERVICE_UNIT,
		 * true, false); rsuAvp.addAvp(Avp.CC_SERVICE_SPECIFIC_UNITS, (long) 1,
		 * true, false);
		 */

		// [Multiple-Services-Credit-Control]

		
		  AvpSet msccAvp =
		  acrAvps.addGroupedAvp(Avp.MULTIPLE_SERVICES_CREDIT_CONTROL
		  ,true,false);
		  
		  AvpSet usuAvp = msccAvp.addGroupedAvp(Avp.USED_SERVICE_UNIT, true,
		  false);
		  usuAvp.addAvp(Avp.CC_INPUT_OCTETS,(long)obj.usuIo,true,false);
		  usuAvp.addAvp(Avp.CC_OUTPUT_OCTETS,(long)obj.usuOo,true,false);
		  
		  /* SERVICE_IDENTIFIER ccrAvps.addAvp(439, service_identifier, true,
		  false);
		 * 
		 * //[Service Information] AvpSet si =
		 * ccrAvps.addGroupedAvp(Avp.SERVICE_INFORMATION, 10415, false,false);
		 * AvpSet psinfo = si.addGroupedAvp(Avp.PS_INFORMATION, 10415,false,
		 * false); // CALLED_STATION_ID
		 * 
		 * //String apn = "airtelgprs.com"; /* String out = null; try { out =
		 * new String(apn.getBytes("UTF-8"), "ISO-8859-1"); } catch
		 * (java.io.UnsupportedEncodingException e) {
		 * System.out.println("failed to convert apn string to UTF8String format"
		 * );
		 * 
		 * }
		 */
		/* psinfo.addAvp(30, apn, true,false,true); */

		// [Service-Information]

				AvpSet si = acrAvps.addGroupedAvp(Avp.SERVICE_INFORMATION, 10415,
						false, false);
				// [Subscription-Id]
				AvpSet subsId = si.addGroupedAvp(Avp.SUBSCRIPTION_ID, (long) 0, false,
						false);
				if(obj.isVirtualNumber) {
				subsId.addAvp(Avp.SUBSCRIPTION_ID_TYPE, obj.vSubscriptionIdTypeValue, false, false);
				// subsId.addAvp(Avp.SUBSCRIPTION_ID_DATA, aIMSI, false, false);
				subsId.addAvp(Avp.SUBSCRIPTION_ID_DATA, obj.vSubscriptionIdDataValue, true);
				}
				else {
					subsId.addAvp(Avp.SUBSCRIPTION_ID_TYPE, obj.subscriptionIdTypeValue, false, false);
					// subsId.addAvp(Avp.SUBSCRIPTION_ID_DATA, aIMSI, false, false);
					subsId.addAvp(Avp.SUBSCRIPTION_ID_DATA, obj.subscriptionIdDataValue, true);
				}
				// [PS-Information]
				if(obj.isPsInformation == true) {
				AvpSet psInfo = si.addGroupedAvp(Avp.PS_INFORMATION, 10415, false,
						false);
				
				// [SGSN-Address]

				if(!(obj.sgsnAddress.length()==0)) {
				Inet4Address sgsnAddress = IPv4Util.fromString(obj.sgsnAddress);
				psInfo.addAvp(Avp.SGSN_ADDRESS, sgsnAddress, 10415, false, false);
				}
				// psInfo.addAvp(Avp.SGSN_ADDRESS, sgsnAddr, false, false, true);
				// [GGSN-Address]
				if(!(obj.ggsnAddress.length()==0)) {
				Inet4Address ggsnAddress = IPv4Util.fromString(obj.ggsnAddress);
				psInfo.addAvp(Avp.GGSN_ADDRESS, ggsnAddress, 10415, false, false);
				}
				// psInfo.addAvp(Avp.GGSN_ADDRESS, ggsnAddr, false, false, true);
				// [CG-Address]
				if(!(obj.cgAddress.length()==0)) {
				Inet4Address cgAddress = IPv4Util.fromString(obj.cgAddress);
				psInfo.addAvp(Avp.CG_ADDRESS, cgAddress, 10415, false, false);
				}
				// psInfo.addAvp(Avp.CG_ADDRESS, cgAddr, false, false, true);
				// [Called-Station-Id]
				if(obj.calledStationId.length() == 0)
				psInfo.addAvp(30, obj.calledStationId, false, false, true);
				// [3GPP-MS-Timezone]
				byte[] tz = { (byte) 0x0C, (byte) 0x00 };

				psInfo.addAvp(Avp.TGPP_MS_TIMEZONE, tz, 10415, true, false);
				}
	}


	public AccountRequest initialACR() throws Exception {
		SessionInitiateACR(ACC_REQUEST_TYPE_INITIAL);
		return acr;

	}

	public AccountRequest interimACR() throws Exception {

		SessionUpdateACR(ACC_REQUEST_TYPE_INTERIM);
		return acr;

	}

	public AccountRequest terminateACR() throws Exception {

		SessionTerminateACR(ACC_REQUEST_TYPE_TERMINATE);
		return acr;

	}

	public AccountRequest eventCCR() throws Exception {

		//defaultACR(ACC_REQUEST_TYPE_EVENT);
		return acr;
	}

}
