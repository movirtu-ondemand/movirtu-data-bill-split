package com.movirtu.diameter.charging.apps.common;

import java.util.HashMap;

import com.movirtu.diameter.test.b2bgw.StoreGenSessionId;

public class HashMapDS {
	public static HashMap<String, StoreGenSessionId> hm = new HashMap<String, StoreGenSessionId>();

	public HashMapDS() {
		System.out.println("called demoHash constructor");
	}

	public static final HashMapDS INSTANCE = new HashMapDS();

	public void insertInHash(String key, StoreGenSessionId obj) {
		synchronized (this) {
			hm.put(key, obj);
		}
	}

	public StoreGenSessionId searchInHash(String key) {
		synchronized (this) {
			StoreGenSessionId obj = (StoreGenSessionId) hm.get(key);
			return obj;
		}
	}

	public boolean deleteFromHash(String key) {
		synchronized (this) {
			if (hm.containsKey(key) == true) {
				hm.remove(key);
				return true;
			} else {
				return false;
			}
		}
	}

}
