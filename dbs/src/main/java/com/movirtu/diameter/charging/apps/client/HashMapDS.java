package com.movirtu.diameter.charging.apps.client;
import java.util.*;
public class HashMapDS {
	public static HashMap<String , SessionInfo> hm= new HashMap<String , SessionInfo>();
	public HashMapDS() {
		System.out.println("called demoHash constructor");
	}
	public static final HashMapDS INSTANCE = new HashMapDS();
	public void insertInHash(String key,SessionInfo obj) {
		hm.put(key,obj);
	}
	public SessionInfo searchInHash(String key) {
		SessionInfo obj = (SessionInfo)hm.get(key);
		return obj;
	}
	public boolean deleteFromHash(String key) {
		if(hm.containsKey(key) == true) {
			hm.remove(key);
			return true;
		}
		else {
			return false;
		}
	}

}

