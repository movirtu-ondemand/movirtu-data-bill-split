/*
 * JBoss, Home of Professional Open Source
 * Copyright 2011, Red Hat, Inc. and/or its affiliates, and individual
 * contributors as indicated by the @authors tag. All rights reserved.
 * See the copyright.txt in the distribution for a full listing
 * of individual contributors.
 * 
 * This copyrighted material is made available to anyone wishing to use,
 * modify, copy, or redistribute it subject to the terms and conditions
 * of the GNU General Public License, v. 2.0.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License,
 * v. 2.0 along with this distribution; if not, write to the Free 
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
package com.movirtu.diameter.charging.apps.B2BGw.server;

import java.util.List;
import java.util.concurrent.ScheduledFuture;

import org.jdiameter.api.Answer;
import org.jdiameter.api.ApplicationId;
import org.jdiameter.api.Avp;
import org.jdiameter.api.AvpSet;
import org.jdiameter.api.IllegalDiameterStateException;
import org.jdiameter.api.InternalException;
import org.jdiameter.api.NetworkReqListener;
import org.jdiameter.api.OverloadException;
import org.jdiameter.api.Peer;
import org.jdiameter.api.PeerState;
import org.jdiameter.api.PeerTable;
import org.jdiameter.api.Request;
import org.jdiameter.api.RouteException;
import org.jdiameter.api.app.AppAnswerEvent;
import org.jdiameter.api.app.AppRequestEvent;
import org.jdiameter.api.app.AppSession;
import org.jdiameter.api.auth.events.ReAuthAnswer;
import org.jdiameter.api.auth.events.ReAuthRequest;
import org.jdiameter.api.cca.ServerCCASession;
import org.jdiameter.api.cca.events.JCreditControlAnswer;
import org.jdiameter.api.cca.events.JCreditControlRequest;
import org.jdiameter.client.api.ISessionFactory;
import org.jdiameter.common.impl.app.auth.ReAuthRequestImpl;
import org.jdiameter.common.impl.app.cca.JCreditControlAnswerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.diameter.charging.apps.B2BGw.client.B2BGwCCRBuilder;
import com.movirtu.diameter.charging.apps.common.HashMapDS;
import com.movirtu.diameter.charging.apps.common.PrepaidServiceProcessingDiameterMessages;
import com.movirtu.diameter.charging.apps.common.TransactionInfo;
import com.movirtu.diameter.test.b2bgw.PrepaidServiceProcessingDiameterMessagesImpl;
import com.movirtu.diameter.test.b2bgw.StoreGenSessionId;
import com.movirtu.diameter.test.b2bgw.TestClientB2BGwCCA;

/**
 * Base implementation of Server
 * 
 * @author <a href="mailto:brainslog@gmail.com"> Alexandre Mendonca </a>
 * @author <a href="mailto:baranowb@gmail.com"> Bartosz Baranowski </a>
 */
public class B2BGwServerCCA extends B2BGwAbstractServerCCA {

	protected PrepaidServiceProcessingDiameterMessages serviceProcDiamMsgServer;

	public B2BGwServerCCA() {
		this.serviceProcDiamMsgServer = null;
	}

	public B2BGwServerCCA(
			PrepaidServiceProcessingDiameterMessages serviceProcDiamMsgServer) {
		this.serviceProcDiamMsgServer = serviceProcDiamMsgServer;
	}

	protected boolean sentINITIAL;
	protected boolean sentINTERIM;
	protected boolean sentTERMINATE;
	protected boolean sentEVENT;
	protected boolean receiveINITIAL;
	protected boolean receiveINTERIM;
	protected boolean receiveTERMINATE;
	protected boolean receiveEVENT;

	protected JCreditControlRequest request;
	protected Logger logger = LoggerFactory.getLogger(B2BGwServerCCA.class);

	// ------- send methods to trigger answer

	public void sendInitial(TransactionInfo obj) throws Exception {
		/*
		 * if (!this.receiveINITIAL || this.request == null) { //
		 * fail("Did not receive INITIAL or answer already sent.", null); throw
		 * new Exception("Request: " + this.request); }
		 */

		try {

			JCreditControlAnswer answerIstLeg = new JCreditControlAnswerImpl(
					obj.request, obj.result);

			logger.debug("inside sendInitialCCA1,answer = {} ",
					answerIstLeg);

			AvpSet set = answerIstLeg.getMessage().getAvps();

			set.removeAvp(Avp.DESTINATION_HOST);
			set.removeAvp(Avp.DESTINATION_REALM);
			set.addAvp(Avp.CC_REQUEST_TYPE, obj.recordType, true, false, true);
			set.addAvp(Avp.CC_REQUEST_NUMBER, obj.recordNumber, true, false,
					true);
			set.addAvp(Avp.AUTH_APPLICATION_ID, 4, true, false, true);
			// CC-Session-Failover enumerated

			// failover-supported
			set.addAvp(Avp.CC_SESSION_FAILOVER, 1, true, false, true);

			// [Multiple-Services-Credit-Control]
			if(obj.multipleServiceCreditControl!=null) {
				for (Avp m : obj.multipleServiceCreditControl) {
					set.addAvp(m);
				}
			}

			// [CCFH]
			if (obj.creditControlFailureHandling != -1)
				set.addAvp(Avp.CREDIT_CONTROL_FAILURE_HANDLING,
						obj.creditControlFailureHandling, false, false);

			// Route-Record
			if (obj.routeRecord != null) {
				// set.addAvp(Avp.ROUTE_RECORD, routeRec, true, false, true);
				set.addAvp(obj.routeRecord);
			}
			// Rulebase-Id
			if (obj.ruleBaseId != null) {
				// String ruleBaseId = "GPd_H_pp";
				// set.addAvp(262, ruleBaseId, 12645, true, false, true);
				set.addAvp(obj.ruleBaseId);
			}
			obj.serverCCASession.sendCreditControlAnswer(answerIstLeg);
			// super.serverCCASession.sendCreditControlAnswer(answerIstLeg);
			sentINITIAL = true;
			request = null;
			/*
			 * Utils.printMessage(log, super.stack.getDictionary(),
			 * answer.getMessage(), true);
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sendInterim(TransactionInfo obj) throws Exception {
		/*
		 * if (!this.receiveINTERIM || this.request == null) {
		 * fail("Did not receive INTERIM or answer already sent.", null); throw
		 * new Exception("Request: " + this.request); }
		 */
		try {
			JCreditControlAnswer answerIstLeg = new JCreditControlAnswerImpl(
					obj.request, obj.result);

			logger.debug("inside sendInitialCCA1,answer = {} ",
					answerIstLeg);

			AvpSet set = answerIstLeg.getMessage().getAvps();

			set.addAvp(Avp.CC_REQUEST_TYPE, obj.recordType, true, false, true);
			set.addAvp(Avp.CC_REQUEST_NUMBER, obj.recordNumber, true, false,
					true);
			set.addAvp(Avp.AUTH_APPLICATION_ID, 4, true, false, true);

			// CC-Session-Failover enumerated

			// failover-supported
			set.addAvp(Avp.CC_SESSION_FAILOVER, 1, true, false, true);

			// [Multiple-Services-Credit-Control]
			if(obj.multipleServiceCreditControl!=null) {
				for (Avp m : obj.multipleServiceCreditControl) {
					set.addAvp(m);
				}
			}

			// [CCFH]
			if (obj.creditControlFailureHandling != -1)
				set.addAvp(Avp.CREDIT_CONTROL_FAILURE_HANDLING,
						obj.creditControlFailureHandling, false, false);

			// Route-Record
			if (obj.routeRecord != null) {
				// set.addAvp(Avp.ROUTE_RECORD, routeRec, true, false, true);
				set.addAvp(obj.routeRecord);
			}
			// Rulebase-Id
			if (obj.ruleBaseId != null) {
				// String ruleBaseId = "GPd_H_pp";
				// set.addAvp(262, ruleBaseId, 12645, true, false, true);
				set.addAvp(obj.ruleBaseId);
			}
			obj.serverCCASession.sendCreditControlAnswer(answerIstLeg);
			// super.serverCCASession.sendCreditControlAnswer(answerIstLeg);
			sentINTERIM = true;
			request = null;
			/*
			 * Utils.printMessage(log, super.stack.getDictionary(),
			 * answer.getMessage(), true);
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sendTerminate(TransactionInfo obj) throws Exception {
		/*
		 * if (!this.receiveTERMINATE || this.request == null) {
		 * fail("Did not receive TERMINATE or answer already sent.", null);
		 * throw new Exception("Request: " + this.request); }
		 */
		try {
			JCreditControlAnswer answerIstLeg = new JCreditControlAnswerImpl(
					obj.request, obj.result);

			logger.debug("inside sendInitialCCA1,answer = {} ",
					answerIstLeg);

			AvpSet set = answerIstLeg.getMessage().getAvps();

			set.addAvp(Avp.CC_REQUEST_TYPE, obj.recordType, true, false, true);
			set.addAvp(Avp.CC_REQUEST_NUMBER, obj.recordNumber, true, false,
					true);
			set.addAvp(Avp.AUTH_APPLICATION_ID, 4, true, false, true);

			// CC-Session-Failover enumerated

			// failover-supported
			set.addAvp(Avp.CC_SESSION_FAILOVER, 1, true, false, true);

			// [Multiple-Services-Credit-Control]
			if(obj.multipleServiceCreditControl!=null) {
				for (Avp m : obj.multipleServiceCreditControl) {
					set.addAvp(m);
				}
			}

			// [CCFH]
			if (obj.creditControlFailureHandling != -1)
				set.addAvp(Avp.CREDIT_CONTROL_FAILURE_HANDLING,
						obj.creditControlFailureHandling, false, false);

			// Route-Record
			if (obj.routeRecord != null) {
				// set.addAvp(Avp.ROUTE_RECORD, routeRec, true, false, true);
				set.addAvp(obj.routeRecord);
			}
			// Rulebase-Id
			if (obj.ruleBaseId != null) {
				// String ruleBaseId = "GPd_H_pp";
				// set.addAvp(262, ruleBaseId, 12645, true, false, true);
				set.addAvp(obj.ruleBaseId);
			}
			obj.serverCCASession.sendCreditControlAnswer(answerIstLeg);
			sentTERMINATE = true;
			request = null;
			/*
			 * Utils.printMessage(log, super.stack.getDictionary(),
			 * answer.getMessage(), true);
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void sendEvent(TransactionInfo obj) throws Exception {
		

	}

	public void sendReAuthRequest(TransactionInfo obj) throws Exception {
		try {

			Peer localpeer = getStack().getMetaData().getLocalPeer();
			Peer remotepeer = getRemotePeer();
			if (TestClientB2BGwCCA.isTest == true) {
				// String[] sessIdComp = obj.key.split(";");
				// String SearchFirstLegSessIdOn = "movirtu;"+sessIdComp[4];

				StoreGenSessionId sessIDStorageObj = HashMapDS.INSTANCE
						.searchInHash("movirtu");
				if (sessIDStorageObj != null) {
					obj.key = sessIDStorageObj.SessionId;
					obj.b2bgwServerCCA = sessIDStorageObj.b2bgwServerCCA;
					obj.serverCCASession = sessIDStorageObj.serverCCASession;
					obj.display();
				}
			}
			ReAuthRequest rar = null;
			//Session-Id and Destination realm added by default
			ApplicationId appID = ApplicationId.createByAuthAppId(0, 4);
			String destRealmStr = localpeer.getRealmName();
			rar = new ReAuthRequestImpl(obj.serverCCASession
					.getSessions()
					.get(0)
					.createRequest(ReAuthRequest.code, appID,
							destRealmStr));
			
			AvpSet avpSet = request.getMessage().getAvps();
			

			// Orig-Host String
			String origHostStr = localpeer.getUri().getFQDN();
			if (obj.origHost != null)
				avpSet.addAvp(Avp.ORIGIN_HOST, origHostStr, true, false, true);

			// System.out.println("origHost = " + obj.origHost);

			// Dest-Host String
			String destHostStr = remotepeer.getUri().getFQDN();
			if (destHostStr != null)
				avpSet.addAvp(Avp.ORIGIN_HOST, destHostStr, true, false, true);
			// System.out.println("destHost = " + obj.destHost);

			// Orig-realm AVP
			String origRealmStr = localpeer.getRealmName();
			if (obj.origRealm != null)
				avpSet.addAvp(Avp.ORIGIN_HOST, origRealmStr, true, false, true);
			// System.out.println("origRealm = " + obj.origRealm);

			
			// RE-Auth-Request-Type ENUMERATED

			if (obj.reAuthRequestType != -1)
				avpSet.addAvp(Avp.RE_AUTH_REQUEST_TYPE, obj.reAuthRequestType,
						true, false, true);

			// User-Name AVP

			if (obj.userName != null) {
				avpSet.addAvp(obj.userName);
			}
			// Origin-State-Id AVP

			if (obj.originStateId != null) {
				avpSet.addAvp(obj.originStateId);
			}

			obj.request = (Request) request.getMessage();
			obj.serverCCASession.sendReAuthRequest(rar);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// ------- initial, this will be triggered for first msg.

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jdiameter.api.NetworkReqListener#processRequest(org.jdiameter.api
	 * .Request)
	 */
	@Override
	public Answer processRequest(Request request) {
		try {
			if (request.getCommandCode() != 272) {
				fail("Received Request with code not equal 272!. Code["
						+ request.getCommandCode() + "]", null);
				return null;
			}

			logger.debug(
					"processRequest ServerCCA.java ccr11 super.serverCCASession = {}",
					super.serverCCASession);
			String sessId = request.getAvps().getAvp(Avp.SESSION_ID)
					.getUTF8String();
			TransactionInfo obj = new TransactionInfo();

			obj.key = sessId;
			obj.request = request;
			logger.debug("processRequest request = {}", obj.request);
			// durgesh provided invocation
			if (serviceProcDiamMsgServer == null) {
				serviceProcDiamMsgServer = new PrepaidServiceProcessingDiameterMessagesImpl();
			}
			this.parseCCR(request, obj);
			
			B2BGwServerCCA b2bgwServerInst = null;

			logger.debug(
					"processRequest ServerCCA.java ccr12 super.serverCCASession = {}",
					super.serverCCASession);

			try {
				if (obj.recordType == B2BGwCCRBuilder.CC_REQUEST_TYPE_INITIAL
						|| super.serverCCASession == null) {

					b2bgwServerInst = new B2BGwServerCCA(
							this.serviceProcDiamMsgServer);
					b2bgwServerInst.serverCCASession = ((ISessionFactory) this.sessionFactory)
							.getNewAppSession(request.getSessionId(),
									getApplicationId(), ServerCCASession.class,
									(Object) null);
				}
				/*
				 * To do  the process request should be invoked through the
				 * new instance serverInst it may be required that a map is kept
				 * of sessionId and serverInst for fetching
				 * serveInst.serverCCASession
				 */
				logger.debug("processRequest B2BGwServerCCA");
				((NetworkReqListener) b2bgwServerInst.serverCCASession)
						.processRequest(request);
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (TestClientB2BGwCCA.isTest == true) {
				StoreGenSessionId sessIDStorageObj = new StoreGenSessionId();
				sessIDStorageObj.SessionId = sessId;
				sessIDStorageObj.request = request;
				// sessIDStorageObj.serverInstCCA = serverInst;
				sessIDStorageObj.b2bgwServerCCA = b2bgwServerInst;
				sessIDStorageObj.serverCCASession = b2bgwServerInst.serverCCASession;
				String[] sessionIdComponents = sessId.split(";");
				// sessIDStorageObj.sessIdIntComponent =
				// Integer.valueOf(sessionIdComponents[4]);
				// storing user provided sessionId stored for leg 1
				// String UserProvidedSessionId = "movirtu;" +
				// "sessionIdComponents[4]";
				String UserProvidedSessionId = "movirtu";
				HashMapDS.INSTANCE.insertInHash(UserProvidedSessionId,
						sessIDStorageObj);
				// obj.sessionIdIntComp = sessIDStorageObj.sessIdIntComponent;
			}
			// durgesh this would be required for sending CCA towards
			// PCRF.Service should return this in obj while sending
			// sendACAWrapper
			obj.serverCCASession = b2bgwServerInst.serverCCASession;
			obj.b2bgwServerCCA = b2bgwServerInst;
			/*
			 * } catch (Exception e) { fail(null, e); }
			 */
			// }

			logger.debug("before invoking serviceProcessingCCR");

			serviceProcDiamMsgServer.serviceProcessingCCR(obj);

			logger.debug("after invoking serviceProcessingCCR");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	// ------------- specific, app session listener.

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jdiameter.api.cca.ServerCCASessionListener#doCreditControlRequest
	 * (org.jdiameter.api.cca.ServerCCASession,
	 * org.jdiameter.api.cca.events.JCreditControlRequest)
	 */
	public void doCreditControlRequest(ServerCCASession session,
			JCreditControlRequest request) throws InternalException,
			IllegalDiameterStateException, RouteException, OverloadException {

		try {
			// this section of code only hit when
			// request_type==2||request_type==3
			int requestType = request.getMessage().getAvps()
					.getAvp((Avp.CC_REQUEST_TYPE)).getInteger32();
			logger.debug(
					"inside doCreditControlRequest doCCR1 B2BGwServerCCA.java, requestType = {}",
					requestType);
			if (requestType != 1) {

				String sessId = request.getMessage().getAvps()
						.getAvp(Avp.SESSION_ID).getUTF8String();
				logger.debug("doCCR2 sessId = {}", sessId);
				TransactionInfo obj = new TransactionInfo();

				obj.key = sessId;
				
				// service provided invocation
				if (serviceProcDiamMsgServer == null) {
					serviceProcDiamMsgServer = new PrepaidServiceProcessingDiameterMessagesImpl();
				}
				
				this.parseCCR((Request) request.getMessage(), obj);

				if (TestClientB2BGwCCA.isTest == true) {
					String UserProvidedSessionId = "movirtu";
					StoreGenSessionId sessIDStorageObj = HashMapDS.INSTANCE
							.searchInHash(UserProvidedSessionId);
					sessIDStorageObj.request = (Request) request.getMessage();
					// sessIDStorageObj.serverInstCCA = serverInst;
					sessIDStorageObj.b2bgwServerCCA = this;
					// sessIDStorageObj.serverCCASession =
					// this.serverCCASession;
					
					

				}
				// durgesh this would be required for sending CCA towards
				// PCRF.Service should return this in obj while sending
				// sendACAWrapper
				// obj.serverCCASession = this.serverCCASession;
				obj.b2bgwServerCCA = this;
				obj.request = (Request)request.getMessage();
				/*
				 * } catch (Exception e) { fail(null, e); }
				 */
				// }

				logger.debug("before invoking serviceProcessingCCR");

				serviceProcDiamMsgServer.serviceProcessingCCR(obj);

				logger.debug("after invoking serviceProcessingCCR");

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jdiameter.api.cca.ServerCCASessionListener#doReAuthAnswer(org.jdiameter
	 * .api.cca.ServerCCASession, org.jdiameter.api.auth.events.ReAuthRequest,
	 * org.jdiameter.api.auth.events.ReAuthAnswer)
	 */
	public void doReAuthAnswer(ServerCCASession session, ReAuthRequest request,
			ReAuthAnswer answer) throws InternalException,
			IllegalDiameterStateException, RouteException, OverloadException {

		try {

			TransactionInfo obj = null;
			if ((obj = parseRAA((Answer)answer.getMessage())) != null) {
				if (serviceProcDiamMsgServer == null) {
					serviceProcDiamMsgServer = new PrepaidServiceProcessingDiameterMessagesImpl();
				}
				if (TestClientB2BGwCCA.isTest == true) {
					StoreGenSessionId sessIDStorageObj = HashMapDS.INSTANCE
							.searchInHash("movirtu");
					if (sessIDStorageObj != null) {
						obj.key = sessIDStorageObj.SessionId;
						obj.display();
					}
				}
				serviceProcDiamMsgServer.serviceProcessingRAA(obj);
			} 
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jdiameter.api.cca.ServerCCASessionListener#doOtherEvent(org.jdiameter
	 * .api.app.AppSession, org.jdiameter.api.app.AppRequestEvent,
	 * org.jdiameter.api.app.AppAnswerEvent)
	 */
	public void doOtherEvent(AppSession session, AppRequestEvent request,
			AppAnswerEvent answer) throws InternalException,
			IllegalDiameterStateException, RouteException, OverloadException {
		fail("Received \"Other\" event, request[" + request + "], answer["
				+ answer + "], on session[" + session + "]", null);
	}

	public boolean isSentINITIAL() {
		return sentINITIAL;
	}

	public boolean isSentINTERIM() {
		return sentINTERIM;
	}

	public boolean isSentTERMINATE() {
		return sentTERMINATE;
	}

	public boolean isReceiveINITIAL() {
		return receiveINITIAL;
	}

	public boolean isReceiveINTERIM() {
		return receiveINTERIM;
	}

	public boolean isReceiveTERMINATE() {
		return receiveTERMINATE;
	}

	public boolean isSentEVENT() {
		return sentEVENT;
	}

	public boolean isReceiveEVENT() {
		return receiveEVENT;
	}

	public JCreditControlRequest getRequest() {
		return request;
	}

	public void parseCCR(Request request, TransactionInfo obj)
			throws InternalException {

		try {

			logger.debug("inside parseCCR1");
			// AvpSet avpSet = request.getMessage().getAvps();
			AvpSet avpSet = request.getAvps();
			
			String sessId = avpSet.getAvp(Avp.SESSION_ID).getUTF8String();

			// Orig-Host
			obj.origHost = avpSet.getAvp(Avp.ORIGIN_HOST).getUTF8String();
			// System.out.println("origHost = " + obj.origHost);
			obj.display();
			// Dest-Host
			//avpSet.getAvp(Avp.DESTINATION_HOST).getUTF8String();
			logger.debug("inside parseccr21");
			// Orig-realm
			obj.origRealm = avpSet.getAvp(Avp.ORIGIN_REALM);
			
			// Dest-Realm
			obj.destRealm = avpSet.getAvp(Avp.DESTINATION_REALM);

			// CC-Request-Type

			obj.recordType = avpSet.getAvp(Avp.CC_REQUEST_TYPE).getInteger32();
			
			// CC-Request-Number

			obj.recordNumber = avpSet.getAvp(Avp.CC_REQUEST_NUMBER)
					.getInteger32();
			
			// service-context-id
			
			obj.serviceContextId = avpSet.getAvp(Avp.SERVICE_CONTEXT_ID)
					.getUTF8String();
			
			// Called-Station-Id
			Avp calledStationId = avpSet.getAvp(30);
			if (calledStationId != null) {
				obj.calledStationId = calledStationId.getUTF8String();
			}
			// User-Name
			Avp userName = avpSet.getAvp(Avp.USER_NAME);
			if (userName != null) {
				obj.userName = userName;
			}
			// Origin-State-Id
			Avp originStateId = avpSet.getAvp(Avp.ORIGIN_STATE_ID);
			if (originStateId != null) {
				obj.originStateId = originStateId;
			}
			obj.display();
			logger.debug("inside parseccr22");
			// Subscription-Id
			AvpSet subsIdSet = avpSet.getAvps(Avp.SUBSCRIPTION_ID);
			if(subsIdSet.size()>1) {
				Avp subsId = subsIdSet.getAvp(Avp.SUBSCRIPTION_ID);
				AvpSet subsIdSubSet = subsId.getGrouped();
				obj.subscriptionIdTypeValue = subsIdSubSet.getAvp(
						Avp.SUBSCRIPTION_ID_TYPE).getInteger32();
				obj.subscriptionIdDataValue = subsIdSubSet.getAvp(
						Avp.SUBSCRIPTION_ID_DATA).getUTF8String();
			}else {
				Avp subscriptionId = avpSet.getAvp(Avp.SUBSCRIPTION_ID);
				if (subscriptionId != null) {
				AvpSet subscriptionIdAvpSet = subscriptionId.getGrouped();
				obj.subscriptionIdTypeValue = subscriptionIdAvpSet.getAvp(
						Avp.SUBSCRIPTION_ID_TYPE).getInteger32();
				obj.subscriptionIdDataValue = subscriptionIdAvpSet.getAvp(
						Avp.SUBSCRIPTION_ID_DATA).getUTF8String();
				
				}
			}
			obj.display();
			logger.debug("inside parseccr23");
			// Multiple-Service-Indicator

			Avp multipleServiceInd = avpSet
					.getAvp(Avp.MULTIPLE_SERVICES_INDICATOR);
			if (multipleServiceInd != null)
				obj.multipleServiceInd = multipleServiceInd;
			// MULTIPLE-SERVICES-CREDIT-CONTROL

			AvpSet mscc = avpSet.getAvps(Avp.MULTIPLE_SERVICES_CREDIT_CONTROL);
			if (mscc != null) {
				obj.multipleServiceCreditControl = mscc;
			}

			// Framed-IP-Address

			Avp framedIpAddress = avpSet.getAvp(8);
			if (framedIpAddress != null)
				obj.framedIpAddress = framedIpAddress;

			// IMSI
			Avp imsi = avpSet.getAvp(Avp.TGPP_IMSI);
			if (imsi != null) {
				obj.imsi = imsi;
			}

			// 3GPP-Charging-Id
			Avp chargingId = avpSet.getAvp(Avp.TGPP_CHARGING_ID);
			if (chargingId != null) {
				obj.chargingId = chargingId;
			}

			// 3GPP-PDP-Type
			Avp pdpType = avpSet.getAvp(Avp.TGPP_PDP_TYPE);
			if (pdpType != null) {
				obj.pdpType = pdpType;
			}

			// 3GPP-GPRS-QoS-Negotiated-Profile
			Avp gprsQosNeg = avpSet.getAvp(5);
			if (gprsQosNeg != null) {
				obj.gprsQosNeg = gprsQosNeg;
			}

			// 3GPP-SGSN-Address

			Avp sgsnAddr = avpSet.getAvp(Avp.SGSN_ADDRESS);
			if (sgsnAddr != null) {
				obj.sgsnAddr = sgsnAddr;
			}

			// 3GPP-GGSN-Address

			Avp ggsnAddr = avpSet.getAvp(Avp.GGSN_ADDRESS);
			if (ggsnAddr != null) {
				obj.ggsnAddr = ggsnAddr;
			}

			// 3GPP-IMSI-MCC-MNC

			Avp imsiMccMnc = avpSet.getAvp(Avp.TGPP_IMSI_MCC_MNC);
			if (imsiMccMnc != null) {
				obj.imsiMccMnc = imsiMccMnc;
			}

			// 3GPP-GGSN-MCC-MNC

			Avp ggsnMccMnc = avpSet.getAvp(Avp.TGPP_GGSN_MCC_MNC);
			if (ggsnMccMnc != null) {
				obj.ggsnMccMnc = ggsnMccMnc;
			}

			// 3GPP-NSAPI

			Avp nsapi = avpSet.getAvp(Avp.TGPP_NSAPI);
			if (nsapi != null) {
				obj.nsapi = nsapi;
			}

			// 3GPP-Selection-Mode

			Avp selectionMode = avpSet.getAvp(Avp.TGPP_SELECTION_MODE);
			if (selectionMode != null) {
				obj.selectionMode = selectionMode;
			}

			// 3GPP-Charging-Characteristics

			Avp chargingCharacteristics = avpSet
					.getAvp(Avp.TGPP_CHARGING_CHARACTERISTICS);
			if (chargingCharacteristics != null) {
				obj.chargingCharacteristics = chargingCharacteristics;
			}

			// User-Location-Information
			Avp userLocationInformation = avpSet.getAvp(267);
			if (userLocationInformation != null) {
				obj.userLocationInformation = userLocationInformation;
			}

			// Radio-Access-Technology

			Avp rat = avpSet.getAvp(260);
			if (rat != null) {
				obj.rat = rat;
			}

			// Context-Type

			Avp contextType = avpSet.getAvp(256);
			if (contextType != null) {
				obj.contextType = contextType;
			}

			// Bearer-Usage

			Avp bearerUsage = avpSet.getAvp(1000);
			if (bearerUsage != null) {
				obj.bearerUsage = bearerUsage;
			}

			// 3GPP-IMEISV

			Avp imeiSv = avpSet.getAvp(20);
			if (imeiSv != null) {
				obj.imeiSv = imeiSv;
			}

			// Rulebase-Id
			Avp ruleBaseId = avpSet.getAvp(262);
			if (ruleBaseId != null) {
				obj.ruleBaseId = ruleBaseId;
			}
			
			//User-Equipment-Info
			Avp userEquipmentInfo = avpSet.getAvp(Avp.USER_EQUIPMENT_INFO);
			if (userEquipmentInfo != null) {
				AvpSet userEquipmentInfoSet = userEquipmentInfo.getGrouped();
				obj.userEquipmentInfoType = userEquipmentInfoSet.getAvp(
						Avp.USER_EQUIPMENT_INFO_TYPE).getInteger32();
				obj.userEquipmentInfoDataValue = userEquipmentInfoSet.getAvp(
						Avp.USER_EQUIPMENT_INFO_VALUE).getUTF8String();

			}
			logger.debug("before exiting parseCCR4");
			obj.display();

		} catch (Exception e) {	
			e.printStackTrace();
			fail(null, e);
		}

	}

	public void parseCCR(JCreditControlRequest request, TransactionInfo obj)
			throws InternalException {

		try {

			logger.debug("inside parseCCR1");
			AvpSet avpSet = request.getMessage().getAvps();
			String sessId = avpSet.getAvp(Avp.SESSION_ID).getUTF8String();

			// Orig-Host
			obj.origHost = avpSet.getAvp(Avp.ORIGIN_HOST).getUTF8String();
			// System.out.println("origHost = " + obj.origHost);

			// Dest-Host
			avpSet.getAvp(Avp.DESTINATION_HOST).getUTF8String();
			// System.out.println("destHost = " + obj.destHost);

			obj.origRealm = avpSet.getAvp(Avp.ORIGIN_REALM);
			// System.out.println("origRealm = " + obj.origRealm);

			obj.destRealm = avpSet.getAvp(Avp.DESTINATION_REALM);

			// CC-Request-Type

			obj.recordType = avpSet.getAvp(Avp.CC_REQUEST_TYPE).getInteger32();
			
			// System.out.println("recordType = " + obj.recordType);
			// CC-Request-Number

			obj.recordNumber = avpSet.getAvp(Avp.CC_REQUEST_NUMBER)
					.getInteger32();
			// service-context-id
			obj.serviceContextId = avpSet.getAvp(Avp.SERVICE_CONTEXT_ID)
					.toString();

			// Called-Station-Id
			Avp calledStationId = avpSet.getAvp(30);
			if (calledStationId != null) {
				obj.calledStationId = calledStationId.getUTF8String();

			}
			// User-Name
			Avp userName = avpSet.getAvp(Avp.USER_NAME);
			if (userName != null) {
				obj.userName = userName;
			}
			// Origin-State-Id
			Avp originStateId = avpSet.getAvp(Avp.ORIGIN_STATE_ID);
			if (originStateId != null) {
				obj.originStateId = originStateId;
			}
			// Subscription-Id
			Avp subscriptionId = avpSet.getAvp(Avp.SUBSCRIPTION_ID);
			if (subscriptionId != null) {
				AvpSet subscriptionIdAvpSet = subscriptionId.getGrouped();
				obj.subscriptionIdTypeValue = subscriptionIdAvpSet.getAvp(
						Avp.SUBSCRIPTION_ID_TYPE).getInteger32();
				obj.subscriptionIdDataValue = subscriptionIdAvpSet.getAvp(
						Avp.SUBSCRIPTION_ID_DATA).getUTF8String();
				
			}
			// Multiple-Service-Indicator

			Avp multipleServiceInd = avpSet
					.getAvp(Avp.MULTIPLE_SERVICES_INDICATOR);
			if (multipleServiceInd != null)
				obj.multipleServiceInd = multipleServiceInd;

			// MULTIPLE-SERVICES-CREDIT-CONTROL

			AvpSet mscc = avpSet.getAvps(Avp.MULTIPLE_SERVICES_CREDIT_CONTROL);
			if (mscc != null) {
				obj.multipleServiceCreditControl = mscc;
			}
			// Framed-IP-Address

			Avp framedIpAddress = avpSet.getAvp(8);
			if (framedIpAddress != null)
				obj.framedIpAddress = framedIpAddress;

			// IMSI
			Avp imsi = avpSet.getAvp(Avp.TGPP_IMSI);
			if (imsi != null) {
				obj.imsi = imsi;
			}

			// 3GPP-Charging-Id
			Avp chargingId = avpSet.getAvp(Avp.TGPP_CHARGING_ID);
			if (chargingId != null) {
				obj.chargingId = chargingId;
			}

			// 3GPP-PDP-Type
			Avp pdpType = avpSet.getAvp(Avp.TGPP_PDP_TYPE);
			if (pdpType != null) {
				obj.pdpType = pdpType;
			}

			// 3GPP-GPRS-QoS-Negotiated-Profile
			Avp gprsQosNeg = avpSet.getAvp(5);
			if (gprsQosNeg != null) {
				obj.gprsQosNeg = gprsQosNeg;
			}

			// 3GPP-SGSN-Address

			Avp sgsnAddr = avpSet.getAvp(Avp.SGSN_ADDRESS);
			if (sgsnAddr != null) {
				obj.sgsnAddr = sgsnAddr;
			}

			// 3GPP-GGSN-Address

			Avp ggsnAddr = avpSet.getAvp(Avp.GGSN_ADDRESS);
			if (ggsnAddr != null) {
				obj.ggsnAddr = ggsnAddr;
			}

			// 3GPP-IMSI-MCC-MNC

			Avp imsiMccMnc = avpSet.getAvp(Avp.TGPP_IMSI_MCC_MNC);
			if (imsiMccMnc != null) {
				obj.imsiMccMnc = imsiMccMnc;
			}

			// 3GPP-GGSN-MCC-MNC

			Avp ggsnMccMnc = avpSet.getAvp(Avp.TGPP_GGSN_MCC_MNC);
			if (ggsnMccMnc != null) {
				obj.ggsnMccMnc = ggsnMccMnc;
			}

			// 3GPP-NSAPI

			Avp nsapi = avpSet.getAvp(Avp.TGPP_NSAPI);
			if (nsapi != null) {
				obj.nsapi = nsapi;
			}

			// 3GPP-Selection-Mode

			Avp selectionMode = avpSet.getAvp(Avp.TGPP_SELECTION_MODE);
			if (selectionMode != null) {
				obj.selectionMode = selectionMode;
			}

			// 3GPP-Charging-Characteristics

			Avp chargingCharacteristics = avpSet
					.getAvp(Avp.TGPP_CHARGING_CHARACTERISTICS);
			if (chargingCharacteristics != null) {
				obj.chargingCharacteristics = chargingCharacteristics;
			}

			// Radio-Access-Technology

			Avp rat = avpSet.getAvp(260);
			if (rat != null) {
				obj.rat = rat;
			}

			// Context-Type

			Avp contextType = avpSet.getAvp(256);
			if (contextType != null) {
				obj.contextType = contextType;
			}

			// Bearer-Usage

			Avp bearerUsage = avpSet.getAvp(1000);
			if (bearerUsage != null) {
				obj.bearerUsage = bearerUsage;
			}

			// 3GPP-IMEISV

			Avp imeiSv = avpSet.getAvp(20);
			if (imeiSv != null) {
				obj.imeiSv = imeiSv;
			}

			// Rulebase-Id
			Avp ruleBaseId = avpSet.getAvp(262);
			if (ruleBaseId != null) {
				obj.ruleBaseId = ruleBaseId;
			}
			
			logger.debug("before exiting parseCCR4");
			obj.display();
			// System.out.println("destRealm = " + obj.destRealm);

			// obj.destHost =
			//

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public TransactionInfo parseRAA(Answer answer)
			throws InternalException {
		try {
				TransactionInfo obj = new TransactionInfo();
				AvpSet set = answer.getAvps();
				// Session-Id
				obj.key = set.getAvp(Avp.SESSION_ID).getUTF8String();

				//Result-code
				if(set.getAvp(Avp.RESULT_CODE)!=null)
					obj.result = set.getAvp(Avp.RESULT_CODE).getInteger32();
			
				// Orig-Host String
						
				Avp origHost = set.getAvp(Avp.ORIGIN_HOST);
				if(origHost!=null)
					obj.origHost = origHost.getUTF8String();

				// Orig-realm AVP
				Avp origRealm  = set.getAvp(Avp.ORIGIN_REALM);
				if(origRealm!=null)
					obj.origRealm = origRealm;
						// System.out.println("origRealm = " + obj.origRealm);
				//User-Name
				
				Avp userName = set.getAvp(Avp.USER_NAME);
				if(userName!=null)
					obj.userName = userName;
				//Origin-State-Id
				Avp originStateId = set.getAvp(Avp.ORIGIN_STATE_ID);
				if(originStateId!=null)
					obj.originStateId = originStateId;
				//Error-Message
				Avp errorMsg = set.getAvp(Avp.ERROR_MESSAGE);
				if(errorMsg!=null) {
					obj.erorMsg = errorMsg;
				}
				//Error-Reporting-Host
				Avp errorReportingHost = set.getAvp(Avp.ERROR_REPORTING_HOST);
				if(errorReportingHost!=null)
					obj.errorReportingHost = errorReportingHost;
				//Failed-Avp
				Avp failedAvp = set.getAvp(Avp.FAILED_AVP);
				if(failedAvp!=null)
					obj.failedAvp = failedAvp;
				return obj;
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private Peer getRemotePeer() throws Exception {
		Peer remotepeer = null;
		List<Peer> peers = getStack().unwrap(PeerTable.class).getPeerTable();
		boolean foundConnected = false;
		if (peers.size() >= 1) {
			for (Peer p : peers) {

				if (p.getState(PeerState.class).equals(PeerState.OKAY)) {
					foundConnected = true;
					remotepeer = p;// use 1st connected peer i.e. overwrite
									// redundancy mode (to-do: other redundancy
									// modes)
					break;
				}
			}

		}

		if (!foundConnected)
			throw new Exception("no connected peer ...unable to send CCR");

		return remotepeer;
	}

	public void sessionSupervisionTimerExpired(ServerCCASession session) {
		logger.debug(
				"inside sessionSupervisionTimerExpired, b2bgwSeverCCA = {}",
				this);
		logger.debug(
				"inside sessionSupervisionTimerExpired,ServerCCASession = {} ",
				session);
		String sessionId = session.getSessionId();
		logger.debug(
				"inside sessionSupervisionTimerExpired,session.getSessionId()  = {}",
				sessionId);
		if (serviceProcDiamMsgServer == null) {
			serviceProcDiamMsgServer = new PrepaidServiceProcessingDiameterMessagesImpl();
		}
		serviceProcDiamMsgServer
				.sessionSupervisionTimerExpiredNotification(sessionId);
	}

	public void sessionSupervisionTimerStarted(ServerCCASession session,
			ScheduledFuture future) {
		String sessionId = session.getSessionId();
		logger.debug(
				"inside sessionSupervisionTimerStarted,session.getSessionId()  = {}",
				sessionId);
		if (serviceProcDiamMsgServer == null) {
			serviceProcDiamMsgServer = new PrepaidServiceProcessingDiameterMessagesImpl();
		}
		serviceProcDiamMsgServer
				.sessionSupervisionTimerStartedNotification(sessionId);
	}

	public void sessionSupervisionTimerReStarted(ServerCCASession session,
			ScheduledFuture future) {
		String sessionId = session.getSessionId();
		logger.debug(
				"inside sessionSupervisionTimerReStarted,session.getSessionId()  = {}",
				sessionId);
		if (serviceProcDiamMsgServer == null) {
			serviceProcDiamMsgServer = new PrepaidServiceProcessingDiameterMessagesImpl();
		}
		serviceProcDiamMsgServer
				.sessionSupervisionTimerRestartedNotification(sessionId);
	}

	public void sessionSupervisionTimerStopped(ServerCCASession session,
			ScheduledFuture future) {
		String sessionId = session.getSessionId();
		logger.debug(
				"inside sessionSupervisionTimerStopped,session.getSessionId()  = {}",
				sessionId);
		if (serviceProcDiamMsgServer == null) {
			serviceProcDiamMsgServer = new PrepaidServiceProcessingDiameterMessagesImpl();
		}
		serviceProcDiamMsgServer
				.sessionSupervisionTimerStoppedNotification(sessionId);
	}

}
