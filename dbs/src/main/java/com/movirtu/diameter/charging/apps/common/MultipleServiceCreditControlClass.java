package com.movirtu.diameter.charging.apps.common;

import org.jdiameter.api.Avp;
import org.jdiameter.api.AvpSet;


public class MultipleServiceCreditControlClass {
	
	public GrantedServiceUnit gsuObj;
	public int ratingGroup;
	public int validityTime;
	public int  resultCode;
	public int volumeQuotaThresholdVoda;
	public int quotaHoldingTimeVoda;
	public int triggerTypeVoda;
	
	public void MultipleServiceCreditControlClass() {
		ratingGroup = validityTime = resultCode = volumeQuotaThresholdVoda = -1;
		quotaHoldingTimeVoda = triggerTypeVoda = 0;
	}
}
