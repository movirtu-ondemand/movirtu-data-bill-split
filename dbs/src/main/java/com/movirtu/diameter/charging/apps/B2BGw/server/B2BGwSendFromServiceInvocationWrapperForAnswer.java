package com.movirtu.diameter.charging.apps.B2BGw.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.diameter.charging.apps.common.TransactionInfo;

public class B2BGwSendFromServiceInvocationWrapperForAnswer {
	public static final B2BGwSendFromServiceInvocationWrapperForAnswer INSTANCE = new B2BGwSendFromServiceInvocationWrapperForAnswer();
	private static final Logger logger = LoggerFactory
			.getLogger(B2BGwSendFromServiceInvocationWrapperForAnswer.class);

	public void sendACAWrapper(TransactionInfo obj) throws Exception {
		try {
			//logger.debug("inside sendACAWrapper B2BGwServerAcc.INSTANCE.serverAccSession.getSessionId().toString() = {}",B2BGwServerAcc.INSTANCE.serverAccSession.getSessionId().toString());
			logger.debug("inside sendACAWrapper");
			if (obj.recordType == B2BGwAbstractServerAcc.ACC_REQUEST_TYPE_INITIAL) {
				logger.debug("before sendInitialACA...B2BGwServerAcc.INSTANCE = {} ",B2BGwServerAcc.INSTANCE);
				logger.debug("rohit sendACAWrapper request = {}",obj.request);
				
				obj.serverInstListener.sendInitialACA(obj);
				logger.debug("after sendInitialACA");
			}else if (obj.recordType == B2BGwAbstractServerAcc.ACC_REQUEST_TYPE_INTERIM)
				obj.serverInstListener.sendInterimACA(obj);
			else if (obj.recordType == B2BGwAbstractServerAcc.ACC_REQUEST_TYPE_TERMINATE)
				obj.serverInstListener.sendTerminateACA(obj);
			else {
				logger.debug("Unexpected record type");
			}
		} catch (Exception e) {
			StackTraceElement[] trace = e.getStackTrace();
			System.err.println(trace[0].toString());
		}

	}
}