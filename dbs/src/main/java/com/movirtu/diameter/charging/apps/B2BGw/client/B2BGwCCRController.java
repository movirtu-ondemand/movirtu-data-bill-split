package com.movirtu.diameter.charging.apps.B2BGw.client;

import java.io.File;
import java.io.FileInputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.jdiameter.api.DisconnectCause;
import org.jdiameter.api.IllegalDiameterStateException;
import org.jdiameter.api.InternalException;
import org.jdiameter.api.Mode;
import org.jdiameter.api.Peer;
import org.jdiameter.api.PeerState;
import org.jdiameter.api.PeerTable;
import org.jdiameter.api.Stack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.diameter.charging.apps.common.TransactionInfo;

public class B2BGwCCRController {
	//public static final B2BGwCCRController INSTANCE = new B2BGwCCRController();
	public B2BGwCCRClient clientNode;
	protected Logger logger = LoggerFactory.getLogger(B2BGwCCRController.class);

	private URI clientConfigURI;
	public boolean isCustomConfigPath;
	public String customConfigPath;

	public B2BGwCCRController() {
		clientNode = null;
		isCustomConfigPath = false;
		customConfigPath = null;
	}

	public void setUp(String configPath) throws Exception {
		if(configPath!=null) {
			isCustomConfigPath = true;
			customConfigPath = configPath;
		}
		if (isCustomConfigPath) {
			try {
				clientConfigURI = new URI("file:" + customConfigPath);
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		}

		if (clientNode == null) {
			clientNode = new B2BGwCCRClient();
			if (!isCustomConfigPath || customConfigPath == null) {
				clientNode.init(
						// new FileInputStream(new File(this.clientConfigURI)),
						getClass().getResourceAsStream("/config-b2bgwccaclient.xml"),
						"CLIENT");
			} else {
				clientNode.init(new FileInputStream(new File(
						this.clientConfigURI)),

				"CLIENT");
			}
			start();
		} else {
			logger.warn("Diameter client already started.. you need to tear down to restart");
		}

	}

	public void start() throws IllegalDiameterStateException, InternalException {
		if (clientNode != null) {
			this.clientNode.start(Mode.ANY_PEER, 10, TimeUnit.SECONDS);
		} else {
			logger.warn("No Diameter client running .. you need to setup one first..");
		}
	}

	public void chkPeerConnection() throws Exception {
		System.out
				.println("checking peers.... supporting one connectin time-being..");
		Stack stack = this.clientNode.getStack();
		List<Peer> peers = stack.unwrap(PeerTable.class).getPeerTable();
		if (peers.size() == 1) {
			// do nothing .. everything ok
			// System.out.println(peers.get(0).getRealmName());
		} else if (peers.size() > 1) {
			// works better with replicated, since disconnected peers are
			// also listed
			boolean foundConnected = false;
			for (Peer p : peers) {

				if (p.getState(PeerState.class).equals(PeerState.OKAY)) {
					if (foundConnected) {
						throw new Exception("Wrong number of connected peers: "
								+ peers);
					}
					foundConnected = true;
				}
			}
		} else {
			throw new Exception("Wrong number of connected peers: " + peers);
		}

	}

	public void tearDown() {

		if (this.clientNode != null) {

			try {
				this.clientNode.stop(DisconnectCause.REBOOTING);
			} catch (Exception e) {

			}
			this.clientNode = null;
		} else {

			logger.warn("No Diameter client running .. you need to setup one first..");

		}

	}

	public void sendEvent(TransactionInfo obj) throws Exception {
		if (clientNode != null) {
			logger.info("sending CCR ..");
			clientNode.sendEvent(obj);
		} else {
			logger.warn("Diameter client not running ..");
		}

	}
	
	public void sendInitial(TransactionInfo obj) throws Exception {
		if (clientNode != null) {
			logger.info("sending Initial CCR ..");
			clientNode.sendInitial(obj);
		} else {
			logger.warn("Diameter client not running ..");
		}

	}
	
	public void sendInterim(TransactionInfo obj) throws Exception {
		if (clientNode != null) {
			logger.info("sending Update CCR ..");
			clientNode.sendInterim(obj);
		} else {
			logger.warn("Diameter client not running ..");
		}

	}
	
	public void sendTerminate(TransactionInfo obj) throws Exception {
		if (clientNode != null) {
			logger.info("sending Terminate CCR ..");
			clientNode.sendTerminate(obj);
		} else {
			logger.warn("Diameter client not running ..");
		}

	}
}
