/*
 * JBoss, Home of Professional Open Source
 * Copyright 2011, Red Hat, Inc. and/or its affiliates, and individual
 * contributors as indicated by the @authors tag. All rights reserved.
 * See the copyright.txt in the distribution for a full listing
 * of individual contributors.
 * 
 * This copyrighted material is made available to anyone wishing to use,
 * modify, copy, or redistribute it subject to the terms and conditions
 * of the GNU General Public License, v. 2.0.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License,
 * v. 2.0 along with this distribution; if not, write to the Free 
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
package com.movirtu.diameter.charging.apps.B2BGw.server;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.List;

import org.dellroad.stuff.net.IPv4Util;
import org.jdiameter.api.Answer;
import org.jdiameter.api.Avp;
import org.jdiameter.api.AvpDataException;
import org.jdiameter.api.AvpSet;
import org.jdiameter.api.IllegalDiameterStateException;
import org.jdiameter.api.InternalException;
import org.jdiameter.api.NetworkReqListener;
import org.jdiameter.api.OverloadException;
import org.jdiameter.api.Peer;
import org.jdiameter.api.PeerState;
import org.jdiameter.api.PeerTable;
import org.jdiameter.api.Request;
import org.jdiameter.api.RouteException;
import org.jdiameter.api.acc.ServerAccSession;
import org.jdiameter.api.acc.events.AccountAnswer;
import org.jdiameter.api.acc.events.AccountRequest;
import org.jdiameter.api.app.AppAnswerEvent;
import org.jdiameter.api.app.AppRequestEvent;
import org.jdiameter.api.app.AppSession;
import org.jdiameter.client.api.ISessionFactory;
import org.jdiameter.common.impl.app.acc.AccountAnswerImpl;
import org.jdiameter.server.impl.app.acc.ServerAccSessionImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.diameter.charging.apps.common.HashMapDS;
import com.movirtu.diameter.charging.apps.common.TransactionInfo;
import com.movirtu.diameter.charging.apps.common.Utils;
import com.movirtu.diameter.test.b2bgw.StoreGenSessionId;
import com.movirtu.diameter.test.b2bgw.TestClientB2BGw;
import com.movirtu.diameter.test.b2bgw.ServiceProcessingDiameterMessages;
import com.movirtu.diameter.test.b2bgw.ServiceProcessingDiameterMessagesImpl;

/**
 * Base implementation of Server
 * 
 * @author <a href="mailto:brainslog@gmail.com"> Alexandre Mendonca </a>
 * @author <a href="mailto:baranowb@gmail.com"> Bartosz Baranowski </a>
 */
public class B2BGwServerAcc extends B2BGwAbstractServerAcc {
	private static final Logger logger = LoggerFactory
			.getLogger(B2BGwServerAcc.class);
	public static final B2BGwServerAcc INSTANCE = new B2BGwServerAcc();
	protected ServiceProcessingDiameterMessages serviceProcDiamMsgServer;
	
	public B2BGwServerAcc() {
		this.serviceProcDiamMsgServer = null;
	}
	public B2BGwServerAcc(ServiceProcessingDiameterMessages serviceProcDiamMsgServer) {
		this.serviceProcDiamMsgServer = serviceProcDiamMsgServer;
	}
	
	private Peer getRemotePeer() throws Exception {
		Peer remotepeer = null;
		List<Peer> peers = getStack().unwrap(PeerTable.class).getPeerTable();
		boolean foundConnected = false;
		if (peers.size() >= 1) {
			for (Peer p : peers) {

				if (p.getState(PeerState.class).equals(PeerState.OKAY)) {
					foundConnected = true;
					remotepeer = p;// use 1st connected peer i.e. overwrite
									// redundancy mode (to-do: other redundancy
									// modes)
					break;
				}
			}

		}

		if (!foundConnected)
			throw new Exception("no connected peer ...unable to send ACR");

		return remotepeer;
	}
	// ------- send methods to trigger answer

	public void sendInitialACA(TransactionInfo obj) throws Exception {
		logger.debug(" rohit inside sendInitialACA1 request = {}",obj.request);
		
		
		try {
			logger.debug("sendInitialACA   getStack = {}",getStack());
		AccountAnswer answerIstLeg = new AccountAnswerImpl(
			obj.request, obj.result);
		logger.debug(" rohit inside sendInitialACA2,answer = {} ",answerIstLeg);
		AvpSet set = answerIstLeg.getMessage().getAvps();
		set.addAvp(Avp.ACC_RECORD_TYPE, obj.recordType, true, false);
		set.addAvp(Avp.ACC_RECORD_NUMBER, obj.recordNumber, true, false);		
		set.addAvp(Avp.ACCT_INTERIM_INTERVAL, obj.accountInterimInterval, false, false);
		super.serverAccSession.sendAccountAnswer(answerIstLeg);
		/*Utils.printMessage(log, super.stack.getDictionary(),
				answerIstLeg.getMessage(), true);*/
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	public void sendInterimACA(TransactionInfo obj) throws Exception {
		
		logger.debug(" rohit sending sendInterimACA");
		
		try {
			
		AccountAnswer answerIstLeg = new AccountAnswerImpl(
			obj.request, obj.result);
		logger.debug(" rohit inside sendInitialACA2,answer = {} ",answerIstLeg);
		
		

		AvpSet set = answerIstLeg.getMessage().getAvps();
		
		set.addAvp(Avp.ACC_RECORD_TYPE, obj.recordType, true, false);
		
		
		set.addAvp(Avp.ACC_RECORD_NUMBER, obj.recordNumber, true, false);		
		set.addAvp(Avp.ACCT_INTERIM_INTERVAL, obj.accountInterimInterval, false, false);
		
		super.serverAccSession.sendAccountAnswer(answerIstLeg);
		/*Utils.printMessage(log, super.stack.getDictionary(),
				answerIstLeg.getMessage(), true);*/
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	public void sendTerminateACA(TransactionInfo obj) throws Exception {
		
		logger.debug(" rohit sending sendTerminateACA");
		try {
		AccountAnswer answerIstLeg = new AccountAnswerImpl(
			obj.request, obj.result);
		logger.debug(" rohit inside sendInitialACA2,answer = {} ",answerIstLeg);
		
		AvpSet set = answerIstLeg.getMessage().getAvps();
		
		//set.addAvp(Avp.SESSION_ID,obj.key,true,false,true);
		set.addAvp(Avp.ACC_RECORD_TYPE, obj.recordType, true, false);
		set.addAvp(Avp.ACC_RECORD_NUMBER, obj.recordNumber, true, false);		
		set.addAvp(Avp.ACCT_INTERIM_INTERVAL, obj.accountInterimInterval, false, false);
		super.serverAccSession.sendAccountAnswer(answerIstLeg);
		
		/*Utils.printMessage(log, super.stack.getDictionary(),
				answerIstLeg.getMessage(), true);*/
		}
		catch(Exception e) {
			e.printStackTrace();
		}

	}

	public void sendEvent() throws Exception {
		
	}

	// ------- initial, this will be triggered for first msg.

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jdiameter.api.NetworkReqListener#processRequest(org.jdiameter.api
	 * .Request)
	 */
	@Override
	public Answer processRequest(Request request) {
		logger.debug("rohit processRequest ServerAcc.java acr11");
		if (request.getCommandCode() != 271) {
			fail("Received Request with code not equal 271!. Code["
					+ request.getCommandCode() + "]", null);
			return null;
		}
		int recordType = 0;
		try {
			recordType = request.getAvps().getAvp(Avp.ACC_RECORD_TYPE)
					.getInteger32();

		} catch (AvpDataException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String sessId;
		try {
			sessId = request.getAvps().getAvp(Avp.SESSION_ID).getUTF8String();
			
			TransactionInfo obj = new TransactionInfo();
			
			obj.key = sessId;
			obj.request = request;
			logger.debug("rohit processRequest request = {}",obj.request);
			// durgesh this would be required for sending ACA towards PCRF.Service should return this in obj while sending sendACAWrapper
			obj.serverAccSess = this.serverAccSession;
			this.parseACR(request, obj);
			this.showTransactionInfoValues(obj);
			B2BGwServerAcc serverInst = new B2BGwServerAcc(this.serviceProcDiamMsgServer);
			
			logger.debug("rohit processRequest ServerAcc.java acr12");
				
				/*super.serverAccSession = ((ISessionFactory) this.sessionFactory)
						.getNewAppSession(request.getSessionId(),
								getApplicationId(), ServerAccSession.class,
								(Object) null);*/
				
			serverInst.serverAccSession = ((ISessionFactory) this.sessionFactory)
						.getNewAppSession(request.getSessionId(),
								getApplicationId(), ServerAccSession.class,
								(Object) serverInst);
			logger.debug("processRequest B2BGwServerAcc");
			((NetworkReqListener) serverInst.serverAccSession)
						.processRequest(request);
				
			//durgesh provided invocation
			if(serviceProcDiamMsgServer ==null) {
				serviceProcDiamMsgServer = new ServiceProcessingDiameterMessagesImpl();
			}
			
			if (TestClientB2BGw.isTest == true) {
				StoreGenSessionId sessIDStorageObj = new StoreGenSessionId();
				sessIDStorageObj.SessionId = sessId;
				sessIDStorageObj.request = request;
				sessIDStorageObj.serverInst = serverInst;
				String[] sessionIdComponents = sessId.split(";");
				sessIDStorageObj.sessIdIntComponent = Integer.valueOf(sessionIdComponents[4]);
				//storing user provided sessionId stored for leg 1
				String UserProvidedSessionId = "movirtu;" + "sessionIdComponents[4]";
				HashMapDS.INSTANCE.insertInHash(UserProvidedSessionId,
						sessIDStorageObj);
				obj.sessionIdIntComp = sessIDStorageObj.sessIdIntComponent;
			}
			obj.serverInstListener = serverInst;
			this.showTransactionInfoValues(obj);
			logger.debug("before invoking serviceProcessingACR");
			serviceProcDiamMsgServer.serviceProcessingACR(obj);
			this.showTransactionInfoValues(obj);
			logger.debug("after invoking serviceProcessingACR");
			return null;
		} catch (AvpDataException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InternalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	// ------------- get service-information parameters.

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jdiameter.api.cca.ServerCCASessionListener#doCreditControlRequest
	 * (org.jdiameter.api.cca.ServerCCASession,
	 * org.jdiameter.api.cca.events.JCreditControlRequest)
	 */

	public void parseACR(Request request, TransactionInfo obj)
			throws InternalException {

		try {
			// AvpSet avpSet = request.getMessage().getAvps();
			AvpSet avpSet = request.getAvps();
			String sessId = avpSet.getAvp(Avp.SESSION_ID).getUTF8String();
			System.out.println("sessId = " + sessId);

			// service-information
			Avp serviceInfo = avpSet.getAvp(Avp.SERVICE_INFORMATION);
			if (serviceInfo != null) {
				AvpSet serviceInfoAvpSet = serviceInfo.getGrouped();
				// Subscription-Id-data and Subscription-Id-type
				Avp subscriptionId = serviceInfoAvpSet
						.getAvp(Avp.SUBSCRIPTION_ID);
				if (subscriptionId != null) {
					AvpSet subscriptionIdAvpSet = subscriptionId.getGrouped();
					obj.subscriptionIdTypeValue = subscriptionIdAvpSet.getAvp(
							Avp.SUBSCRIPTION_ID_TYPE).getInteger32();
					obj.subscriptionIdDataValue = subscriptionIdAvpSet.getAvp(
							Avp.SUBSCRIPTION_ID_DATA).getUTF8String();
					// System.out.println("SubscriptionIdData = "+obj.subscriptionIdData);
					System.out.println("SubscriptionIdDataValue = "
							+ obj.subscriptionIdDataValue);
					System.out.println("SubscriptionIdTypeValue = "
							+ obj.subscriptionIdTypeValue);
				}
				// PS-Information
				Avp psInfo = serviceInfoAvpSet.getAvp(Avp.PS_INFORMATION);
				if (psInfo != null) {
					obj.isPsInformation = true;
					AvpSet psInfoAvpSet = psInfo.getGrouped();
					//3gpp-charging-Id
					Avp chargingId = psInfoAvpSet.getAvp(Avp.TGPP_CHARGING_ID);
					if(chargingId !=null) {
						obj.chargingId = chargingId;
					}
					//SGSN-Address
					Avp sgsnAddr = psInfoAvpSet.getAvp(Avp.SGSN_ADDRESS);
					if (sgsnAddr != null) {
						// obj.sgsnAddress =
						// IPv4Util.toString(sgsnAddr.getAddress());
						InetAddress tempSgsnAddr = (InetAddress) sgsnAddr
								.getAddress();
						obj.sgsnAddress = IPv4Util
								.toString((Inet4Address) tempSgsnAddr);
						// InetAddress i =
						// IPv4Util.toString((InetAddress)(sgsnAddr.getAddress()));;
						// Inet4Address sgsnIn4Address = sgsnAddr.getAddress();
						// obj.ss = sgsnAddr.getAddress();
						// obj.sgsnAddress = sgsnIn4Address.getHostAddress();
						System.out.println("SGSN-Address = " + sgsnAddr);
						System.out.println("SGSN-AddressValue = "
								+ obj.sgsnAddress);
					}
					Avp ggsnAddr = psInfoAvpSet.getAvp(Avp.GGSN_ADDRESS);
					if (ggsnAddr != null) {
						InetAddress tempGgsnAddr = (InetAddress) ggsnAddr
								.getAddress();
						obj.ggsnAddress = IPv4Util
								.toString((Inet4Address) tempGgsnAddr);
						System.out.println("GGSN-Address = " + ggsnAddr);
						System.out.println("GGSN-AddressValue = "
								+ obj.ggsnAddress);
					}
					Avp cgAddr = psInfoAvpSet.getAvp(Avp.CG_ADDRESS);
					if (cgAddr != null) {
						InetAddress tempCgAddr = (InetAddress) cgAddr
								.getAddress();

						obj.cgAddress = IPv4Util
								.toString((Inet4Address) tempCgAddr);

						System.out.println("CG-Address = " + cgAddr);
						System.out
								.println("CG-AddressValue = " + obj.cgAddress);
					}
					Avp calledStationId = psInfoAvpSet.getAvp(30);
					if (calledStationId != null) {
						obj.calledStationId = calledStationId.getUTF8String();
						System.out.println("calledStationId = "
								+ calledStationId);
						System.out.println("calledStationIdValue = "
								+ calledStationId.getUTF8String());
					}
					Avp msTmZone = psInfoAvpSet.getAvp(Avp.TGPP_MS_TIMEZONE,
							10415);
					if (msTmZone != null) {
						System.out.println("3GPP-MS-TIMEZONE = " + msTmZone);
						System.out.println("3GPP-MS-TIMEZONE getUTF8String = "
								+ msTmZone.getUTF8String());
						System.out.println("3GPP-MS-TIMEZONE getOctetString= "
								+ msTmZone.getOctetString());

						obj.tmZone = msTmZone.getRawData();
						int len = obj.tmZone.length;

						System.out.println("len= " + len);
						System.out.println("obj.tmZone[0] = " + obj.tmZone[0]);
						System.out.println("obj.tmZone[1] = " + obj.tmZone[1]);
					}
				}
			}
			obj.recordType = avpSet.getAvp(Avp.ACC_RECORD_TYPE).getInteger32();
			System.out.println("recordType = " + obj.recordType);
			obj.recordNumber = avpSet.getAvp(Avp.ACC_RECORD_NUMBER)
					.getInteger32();
			System.out.println("recordNumber = " + obj.recordNumber);

			obj.origHost = avpSet.getAvp(Avp.ORIGIN_HOST).getUTF8String();
			System.out.println("origHost = " + obj.origHost);

			avpSet.getAvp(Avp.DESTINATION_HOST).getUTF8String();
			System.out.println("destHost = " + obj.destHostStr);

			obj.accountInterimInterval = avpSet.getAvp(
					Avp.ACCT_INTERIM_INTERVAL).getInteger32();
			System.out.println("accountInterimInterval = "
					+ obj.accountInterimInterval);

			obj.origRealm = avpSet.getAvp(Avp.ORIGIN_REALM);
			// System.out.println("origRealm = " + obj.origRealm);

			obj.destRealm = avpSet.getAvp(Avp.DESTINATION_REALM);

			// System.out.println("destRealm = " + obj.destRealm);

			// obj.destHost =
			//

		} catch (Exception e) {
			fail(null, e);
		}

	}

	// print SessionInfo stored values

	public void showTransactionInfoValues(TransactionInfo obj)
			throws InternalException {
		try {
			logger.debug("rohit showTransactionInfoValues");
			logger.debug("sessionId = " + obj.key);
			logger.debug("recordType = " + obj.recordType);
			logger.debug("recordNumber = " + obj.recordNumber);
			logger.debug("accountInterimInterval = "
					+ obj.accountInterimInterval);
			logger.debug("origHost = " + obj.origHost);
			logger.debug("origRealm = " + obj.origRealm);
			logger.debug("destRealm = " + obj.destRealm);
			logger.debug("destHost = " + obj.destHostStr);
			logger.debug("subscriptionIdDataValue = "
					+ obj.subscriptionIdDataValue);
			logger.debug("subscriptionIdTypeValue = "
					+ obj.subscriptionIdTypeValue);
			logger.debug("calledStationId = " + obj.calledStationId);
			logger.debug("ggsnAddress = " + obj.ggsnAddress);
			logger.debug("sgsnAddress = " + obj.sgsnAddress);
			logger.debug("cgAddress = " + obj.cgAddress);
			logger.debug("request = " + obj.request);
			logger.debug("serverAccSess = " + obj.serverAccSess);

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	// ------------- specific, app session listener.

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jdiameter.api.cca.ServerCCASessionListener#doCreditControlRequest
	 * (org.jdiameter.api.cca.ServerCCASession,
	 * org.jdiameter.api.cca.events.JCreditControlRequest)
	 */
	public void doAccRequestEvent(ServerAccSession session,
			AccountRequest request) throws InternalException,
			IllegalDiameterStateException, RouteException, OverloadException {

		logger.debug("rohit dummy function");
		

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jdiameter.api.cca.ServerCCASessionListener#doReAuthAnswer(org.jdiameter
	 * .api.cca.ServerCCASession, org.jdiameter.api.auth.events.ReAuthRequest,
	 * org.jdiameter.api.auth.events.ReAuthAnswer)
	 */
	/*
	 * public void doReAuthAnswer(ServerCCASession session, ReAuthRequest
	 * request, ReAuthAnswer answer) throws InternalException,
	 * IllegalDiameterStateException, RouteException, OverloadException {
	 * fail("Received \"ReAuthAnswer\" event, request[" + request +
	 * "], on session[" + session + "]", null); }
	 * 
	 * /* (non-Javadoc)
	 * 
	 * @see
	 * org.jdiameter.api.cca.ServerCCASessionListener#doOtherEvent(org.jdiameter
	 * .api.app.AppSession, org.jdiameter.api.app.AppRequestEvent,
	 * org.jdiameter.api.app.AppAnswerEvent)
	 */
	public void doOtherEvent(AppSession session, AppRequestEvent request,
			AppAnswerEvent answer) throws InternalException,
			IllegalDiameterStateException, RouteException, OverloadException {
		fail("Received \"Other\" event, request[" + request + "], answer["
				+ answer + "], on session[" + session + "]", null);
	}

	

}
