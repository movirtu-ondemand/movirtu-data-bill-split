package com.movirtu.diameter.charging.apps.client;

import java.net.Inet4Address;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.dellroad.stuff.net.IPv4Util;
import org.jdiameter.api.ApplicationId;
import org.jdiameter.api.Avp;
import org.jdiameter.api.AvpSet;
import org.jdiameter.api.InternalException;
import org.jdiameter.api.cca.ClientCCASession;
import org.jdiameter.api.cca.events.JCreditControlRequest;
import org.jdiameter.common.impl.app.cca.JCreditControlRequestImpl;

public class CCRBuilder {

	private AvpSet ccrAvps;
	private JCreditControlRequest ccr;

	static final int CC_REQUEST_TYPE_INITIAL = 1;
	static final int CC_REQUEST_TYPE_INTERIM = 2;
	static final int CC_REQUEST_TYPE_TERMINATE = 3;
	static final int CC_REQUEST_TYPE_EVENT = 4;

	private ClientCCASession session;
	private ApplicationId appID;
	private String serverRealm;
	private String clientRealm;
	private String clientURI;
	private String serverURI;
	private String serviceContextId;
	private String aMSISDN;
	private String bMSISDN;
	private String aIMSI;
	private String bIMSI;
	private String vLR;
	private boolean isShortCode;
	private boolean defaultShortCode;
	private String service_provider_id;
	private int service_identifier;
	private String apn;
	private String sessionIdKey;
	private int usuInputOctets = 0;
	private int usuOutputOctets = 0;
	private int ccfhVal = 0;
	private int rsuInputOctets = 0;
	private int rsuOutputOctets = 0;

	CCRBuilder(ClientCCASession session, ApplicationId appID,
			String clientRealm, String serverRealm, String clientURI,
			String serverURI, String serviceContextId, String aMSISDN,
			String bMSISDN, String aIMSI, String bIMSI, String vLR,
			boolean isShortCode, boolean defaultShortCode,
			String service_provider_id, int service_identifier, String apn,
			String sessionIdKey, int usuIo, int usuOu, int ccfh, int rsuIo,
			int rsuOu) {
		this.session = session;
		this.appID = appID;
		this.clientRealm = clientRealm;
		this.serverRealm = serverRealm;
		this.clientURI = clientURI;
		this.serverURI = serverURI;
		this.serviceContextId = serviceContextId;
		this.aMSISDN = aMSISDN;
		this.bMSISDN = bMSISDN;
		this.aIMSI = aIMSI;
		this.bIMSI = bIMSI;
		this.vLR = vLR;
		this.isShortCode = isShortCode;
		this.defaultShortCode = defaultShortCode;
		this.service_provider_id = service_provider_id;
		this.service_identifier = service_identifier;
		this.apn = apn;
		this.sessionIdKey = sessionIdKey;
		this.usuInputOctets = usuIo;
		this.usuOutputOctets = usuOu;
		this.ccfhVal = ccfh;
		this.rsuInputOctets = rsuIo;
		this.rsuOutputOctets = rsuOu;

	}

	private void defaultCCR(int requestType) throws InternalException {

		ccr = new JCreditControlRequestImpl(session.getSessions().get(0)
				.createRequest(JCreditControlRequest.code, appID, serverRealm));
		// Create Credit-Control-Request
		// AVPs present by default: Origin-Host, Origin-Realm, Session-Id,
		// Vendor-Specific-Application-Id, Destination-Realm

		ccrAvps = ccr.getMessage().getAvps();
		// remove VENDOR_SPECIFIC_APPLICATION_ID
		// ccrAvps.removeAvp(Avp.VENDOR_SPECIFIC_APPLICATION_ID);

		// { Origin-Host }
		ccrAvps.removeAvp(Avp.ORIGIN_HOST);// one can overwrite default AVP by
											// removing it
		ccrAvps.addAvp(Avp.ORIGIN_HOST, clientURI, true, false, true);

		// [ Destination-Host ]
		ccrAvps.addAvp(Avp.DESTINATION_HOST, serverURI, true, false, false);

		// { Auth-Application-Id }
		// ccrAvps.addAvp(Avp.AUTH_APPLICATION_ID,
		// appID.getAuthAppId(),appID.getVendorId(), true, false, true);

		// { Service-Context-Id }
		// M,V
		ccrAvps.addAvp(Avp.SERVICE_CONTEXT_ID, serviceContextId, true, false,
				false);
		// { CC-Request-Type }
		ccrAvps.addAvp(Avp.CC_REQUEST_TYPE, requestType, true, false);

		// { CC-Request-Number }

		ccrAvps.addAvp(Avp.CC_REQUEST_NUMBER, 0, true, false, true);

		// [ Requested-Action ]

		ccrAvps.addAvp(Avp.REQUESTED_ACTION, 0, true, false);

		// [ Event-Timestamp ]
		// Calendar c = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
		Calendar c = new GregorianCalendar(1900, 0, 1);

		c.setTimeZone(TimeZone.getTimeZone("UTC"));

		long timestamp = (System.currentTimeMillis() - c.getTimeInMillis()) / 1000;

		ccrAvps.addAvp(Avp.EVENT_TIMESTAMP, timestamp, true, false, true);

		// *[ Subscription-Id ]

		AvpSet subscriptionId = ccrAvps.addGroupedAvp(Avp.SUBSCRIPTION_ID,
				true, false);

		// Subscription-Id-Type AVP
		subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_TYPE, 0, true, false);// END_USER_E164

		// Subscription-Id-Data AVP
		subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_DATA, aMSISDN, true, true,
				false);

		// SERVICE_IDENTIFIER
		ccrAvps.addAvp(439, service_identifier, true, false);

		// [ Requested-Service-Unit ]

		AvpSet rsuAvp = ccrAvps.addGroupedAvp(Avp.REQUESTED_SERVICE_UNIT, true,
				false);
		rsuAvp.addAvp(Avp.CC_SERVICE_SPECIFIC_UNITS, (long) 1, true, false);

		// Service-Provider-ID
		ccrAvps.addAvp(1081, service_provider_id, 193, true, false, false);
		// ccrAvps.addAvp(1081, "200", 193, true, false, false);

		// Subscription-Id-Location
		ccrAvps.addAvp(1074, vLR, 193, true, false, false);

		// Traffic-Case
		ccrAvps.addAvp(1082, 20, 193, true, false);

		// 3GPP-MS-TimeZone
		// TimeZone tz = Calendar.getInstance().getTimeZone();
		byte[] tz = { (byte) 0x0C, (byte) 0x00 };

		ccrAvps.addAvp(Avp.TGPP_MS_TIMEZONE, tz, 10415, true, false); // tigo
																		// timezone

		// Service-Parameter-Info [A-Party IMSI]
		AvpSet svp = ccrAvps.addGroupedAvp(Avp.SERVICE_PARAMETER_INFO, true,
				false);
		// Service-Parameter-Type
		svp.addAvp(Avp.SERVICE_PARAMETER_TYPE, 207, true, false);
		// Service-Parameter-Value
		svp.addAvp(Avp.SERVICE_PARAMETER_VALUE, aIMSI, true, true, false);

		// Service-Parameter-Info [B-Party IMSI]
		// svp = ccrAvps.addGroupedAvp(Avp.SERVICE_PARAMETER_INFO, true, false);
		// Service-Parameter-Type
		// svp.addAvp(Avp.SERVICE_PARAMETER_TYPE, 208, true, false);
		// Service-Parameter-Value
		// svp.addAvp(Avp.SERVICE_PARAMETER_VALUE, bIMSI, appID.getVendorId(),
		// true, true, false);

		// Other-Party-Id
		svp = ccrAvps.addGroupedAvp(1075, 193, true, false);
		// OtherPartyIdDataTypeAvp
		svp.addAvp(1078, 0, 193, true, false);// END_USER_E164
		// (0),INTERNATIONAL (4)
		if (!isShortCode) {

			// OtherPartyIdDataAvp
			svp.addAvp(1077, bMSISDN, 193, true, false, false);
			// OtherPartyIdNatureAvp
			svp.addAvp(1076, 1, 193, true, false);/*
												 * Normal Number: International
												 * (1), ShortCode: National (2)
												 */
		} else {

			if (defaultShortCode) {
				svp.addAvp(1077, "800800", 193, true, false, false);

			} else {
				svp.addAvp(1077, bMSISDN, 193, true, false, false);
			}

			svp.addAvp(1076, 2, 193, true, false);
		}

	}

	private void SessionInitiateCCR(int requestType) throws InternalException {
		try {
			ccr = new JCreditControlRequestImpl(session
					.getSessions()
					.get(0)
					.createRequest(JCreditControlRequest.code, appID,
							serverRealm));
			// Create Credit-Control-Request
			// AVPs present by default: Origin-Host, Origin-Realm, Session-Id,
			// Vendor-Specific-Application-Id, Destination-Realm

			ccrAvps = ccr.getMessage().getAvps();
			// remove VENDOR_SPECIFIC_APPLICATION_ID
			// ccrAvps.removeAvp(Avp.VENDOR_SPECIFIC_APPLICATION_ID);

			// { Origin-Host }
			ccrAvps.removeAvp(Avp.ORIGIN_HOST);// one can overwrite default AVP
												// by
												// removing it
			ccrAvps.addAvp(Avp.ORIGIN_HOST, clientURI, true, false, true);

			// [ Destination-Host ]
			
			ccrAvps.addAvp(Avp.DESTINATION_HOST, serverURI, true, false, false);

			// { Auth-Application-Id }
			// ccrAvps.addAvp(Avp.AUTH_APPLICATION_ID,
			// appID.getAuthAppId(),appID.getVendorId(), true, false, true);

			// { Service-Context-Id }
			// M,V
			ccrAvps.addAvp(Avp.SERVICE_CONTEXT_ID, serviceContextId, true,
					false, true);
			// { CC-Request-Type }
			ccrAvps.addAvp(Avp.CC_REQUEST_TYPE, requestType, true, false);

			// { CC-Request-Number }

			ccrAvps.addAvp(Avp.CC_REQUEST_NUMBER, 0, true, false, true);

			// [Called-Station-Id]

			ccrAvps.addAvp(30, apn, true, false, true);

			// User-Name

			ccrAvps.addAvp(Avp.USER_NAME, "movirtu_subscriber", true, false,
					true);

			// Origin-State-Id

			byte[] originStateId = { (byte) 0xae, (byte) 0x07, (byte) 0xb0,
					(byte) 0xb7 };
			ccrAvps.addAvp(Avp.ORIGIN_STATE_ID, originStateId, true, false);

			// [ Requested-Action ]

			// ccrAvps.addAvp(Avp.REQUESTED_ACTION, 0, true, false);

			// [ Event-Timestamp ]
			// Calendar c = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
			Calendar c = new GregorianCalendar(1900, 0, 1);

			c.setTimeZone(TimeZone.getTimeZone("UTC"));

			long timestamp = (System.currentTimeMillis() - c.getTimeInMillis()) / 1000;

			ccrAvps.addAvp(Avp.EVENT_TIMESTAMP, timestamp, true, false, true);

			// *[ Subscription-Id-1 ]

			AvpSet subscriptionId = ccrAvps.addGroupedAvp(Avp.SUBSCRIPTION_ID,
					true, false);

			// Subscription-Id-Type AVP
			subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_TYPE, 0, true, false);// END_USER_E164(MSISDN)

			// Subscription-Id-Data AVP
			subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_DATA, aMSISDN, true,
					false, false);
			// *[ Subscription-Id-2 ]

			AvpSet subscriptionId2 = ccrAvps.addGroupedAvp(Avp.SUBSCRIPTION_ID,
					true, false);

			// Subscription-Id-Type AVP
			subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_TYPE, 1, true, false);// END_USER_E164(MSISDN)

			// Subscription-Id-Data AVP
			String hardCodeImsi = "234159152409836";
			subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_DATA, hardCodeImsi, true,
					false, false);
			
			// Multiple-Services-Indicator
			ccrAvps.addAvp(Avp.MULTIPLE_SERVICES_INDICATOR, 1, true, false,
					true);

			// [Multiple-Services-Credit-Control-1]

			AvpSet msccAvp = ccrAvps.addGroupedAvp(
					Avp.MULTIPLE_SERVICES_CREDIT_CONTROL, true, false);
			// Requested-Service-Unit
			AvpSet rsuAvp = msccAvp.addGroupedAvp(Avp.REQUESTED_SERVICE_UNIT,
					true, false);

			// Rating -Group
			msccAvp.addAvp(Avp.RATING_GROUP, 1, true, false, true);

			// [Multiple-Services-Credit-Control-2]

			AvpSet msccAvp2 = ccrAvps.addGroupedAvp(
					Avp.MULTIPLE_SERVICES_CREDIT_CONTROL, true, false);
			// Requested-Service-Unit
			AvpSet rsuAvp2 = msccAvp2.addGroupedAvp(Avp.REQUESTED_SERVICE_UNIT,
					true, false);

			// Rating -Group
			msccAvp2.addAvp(Avp.RATING_GROUP, 2, true, false, true);

			// [Multiple-Services-Credit-Control-3]

			AvpSet msccAvp3 = ccrAvps.addGroupedAvp(
					Avp.MULTIPLE_SERVICES_CREDIT_CONTROL, true, false);
			// Requested-Service-Unit
			AvpSet rsuAvp3 = msccAvp3.addGroupedAvp(Avp.REQUESTED_SERVICE_UNIT,
					true, false);

			// Rating -Group
			msccAvp3.addAvp(Avp.RATING_GROUP, 3, true, false, true);
			
			// Framed-IP-Address
			byte[] framedIp = { (byte) 0x0a, (byte) 0x3b, (byte) 0x10,
					(byte) 0xc0 };
			ccrAvps.addAvp(8, framedIp, true, false);

			
			// 3GPP-IMSI

			ccrAvps.addAvp(Avp.TGPP_IMSI, aIMSI, 10415, true, false, true);

			// 3GPP-Charging-Id

			byte[] chargingId = { (byte) 0x04, (byte) 0x00, (byte) 0x00,
					(byte) 0x70 };
			ccrAvps.addAvp(Avp.TGPP_CHARGING_ID, chargingId, 10415, true, false);

			// 3GPP-PDP-Type

			ccrAvps.addAvp(Avp.TGPP_PDP_TYPE, 0, 10415, true, false, true);

			// 3GPP-GPRS-QoS-Negotiated-Profile
			String qosNeg = "08-4807000021C000002710";
			ccrAvps.addAvp(5, qosNeg, 10415, true, false, true);

			// 3GPP-SGSN-Address
			

			String sgsnAddr = "192.168.56.4";
			Inet4Address sgsnAddress = IPv4Util.fromString(sgsnAddr);
			ccrAvps.addAvp(Avp.SGSN_ADDRESS, sgsnAddress, 10415, true, false);

			// 3GPP-GGSN-Address

			String ggsnAddr = "192.168.56.3";
			Inet4Address ggsnAddress = IPv4Util.fromString(ggsnAddr);
			ccrAvps.addAvp(Avp.GGSN_ADDRESS, ggsnAddress, 10415, true, true);

			// 3GPP-IMSI-MCC-MNC

			String imsiMccMnc = "23415";
			ccrAvps.addAvp(Avp.TGPP_IMSI_MCC_MNC, imsiMccMnc, 10415, true,
					false, true);

			// 3GPP-GGSN-MCC-MNC
			String ggsnMccMnc = "23415";
			ccrAvps.addAvp(Avp.TGPP_GGSN_MCC_MNC, ggsnMccMnc, 10415, true,
					false, true);

			// 3GPP-NSAPI

			String nsapi = "5";
			ccrAvps.addAvp(Avp.TGPP_NSAPI, nsapi, 10415, true, false, true);

			// selection mode

			String selMode = "0";
			ccrAvps.addAvp(Avp.TGPP_SELECTION_MODE, selMode, 10415, true,
					false, true);

			// 3GPP-Charging-Characteristics

			String chargChar = "0800";
			ccrAvps.addAvp(Avp.TGPP_CHARGING_CHARACTERISTICS, chargChar, 10415,
					true, false, true);

			// User-Location-Information

			// Radio-Access-Technology
			int rat = 6;
			ccrAvps.addAvp(260, rat, 12645, true, false, true);

			// Context-Type
			int contextTyp = 0;
			ccrAvps.addAvp(256, contextTyp, 12645, true, false, true);

			// Bearer-Usage
			int bearerUsage = 0;
			ccrAvps.addAvp(1000, bearerUsage, 10415, true, false, true);

			// 3GPP-IMEISV

			String imeiSv = "3535750569046801";
			ccrAvps.addAvp(20, imeiSv, 10415, true, false, false);

			// Rulebase-Id

			String ruleBaseId = "wap_ONLINE";
			ccrAvps.addAvp(262, ruleBaseId, 12645, true, false, true);
			
			//User-Equipment-Info
			System.out.println("adding user equipment info");
			AvpSet userequipmentInfo = ccrAvps.addGroupedAvp(Avp.USER_EQUIPMENT_INFO,
					true, false);

			// Subscription-Id-Type AVP
			userequipmentInfo.addAvp(Avp.USER_EQUIPMENT_INFO_TYPE, 0, true, true);// END_USER_E164(MSISDN)

			// Subscription-Id-Data AVP
			String hardCodeImeiSv = "3569780654855803";
			userequipmentInfo.addAvp(Avp.USER_EQUIPMENT_INFO_VALUE, hardCodeImeiSv, true,
					false, false);

			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void SessionUpdateCCR(int requestType) throws InternalException {
		try {
		ccr = new JCreditControlRequestImpl(session.getSessions().get(0)
				.createRequest(JCreditControlRequest.code, appID, serverRealm));
		// Create Credit-Control-Request
		// AVPs present by default: Origin-Host, Origin-Realm, Session-Id,
		// Vendor-Specific-Application-Id, Destination-Realm

		ccrAvps = ccr.getMessage().getAvps();
		// remove VENDOR_SPECIFIC_APPLICATION_ID
		// ccrAvps.removeAvp(Avp.VENDOR_SPECIFIC_APPLICATION_ID);

		// { Origin-Host }
		ccrAvps.removeAvp(Avp.ORIGIN_HOST);// one can overwrite default AVP by
											// removing it
		ccrAvps.addAvp(Avp.ORIGIN_HOST, clientURI, true, false, true);

		// [ Destination-Host ]
		ccrAvps.addAvp(Avp.DESTINATION_HOST, serverURI, true, false, false);

		// { Auth-Application-Id }
		// ccrAvps.addAvp(Avp.AUTH_APPLICATION_ID,
		// appID.getAuthAppId(),appID.getVendorId(), true, false, true);

		// { Service-Context-Id }
		// M,V
		ccrAvps.addAvp(Avp.SERVICE_CONTEXT_ID, serviceContextId, true, false,
				false);
		// { CC-Request-Type }
		ccrAvps.addAvp(Avp.CC_REQUEST_TYPE, requestType, true, false);

		// { CC-Request-Number }

		ccrAvps.addAvp(Avp.CC_REQUEST_NUMBER, 0, true, false, true);

		// [Called-Station-Id]

		ccrAvps.addAvp(30, apn, true, false, true);

		// User-Name

		ccrAvps.addAvp(Avp.USER_NAME, "movirtu_subscriber", true, false, true);

		// Origin-State-Id

		byte[] originStateId = { (byte) 0xae, (byte) 0x07, (byte) 0xb0,
				(byte) 0xb7 };
		ccrAvps.addAvp(Avp.ORIGIN_STATE_ID, originStateId, true, false);

		// [ Event-Timestamp ]
		// Calendar c = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
		Calendar c = new GregorianCalendar(1900, 0, 1);

		c.setTimeZone(TimeZone.getTimeZone("UTC"));

		long timestamp = (System.currentTimeMillis() - c.getTimeInMillis()) / 1000;

		ccrAvps.addAvp(Avp.EVENT_TIMESTAMP, timestamp, true, false, true);

		// *[ Subscription-Id ]

		AvpSet subscriptionId = ccrAvps.addGroupedAvp(Avp.SUBSCRIPTION_ID,
				true, false);

		// Subscription-Id-Type AVP
		subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_TYPE, 0, true, false);// END_USER_E164(MSISDN)

		// Subscription-Id-Data AVP
		subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_DATA, aMSISDN, true, false,
				false);

		// Multiple-Services-Indicator
		ccrAvps.addAvp(Avp.MULTIPLE_SERVICES_INDICATOR, 1, true, false, true);

		// [ Requested-Service-Unit ]

		/*
		 * AvpSet rsuAvp = ccrAvps.addGroupedAvp(Avp.REQUESTED_SERVICE_UNIT,
		 * true, false); rsuAvp.addAvp(Avp.CC_SERVICE_SPECIFIC_UNITS, (long) 1,
		 * true, false);
		 */

		// [Multiple-Services-Credit-Control-1]

		AvpSet msccAvp = ccrAvps.addGroupedAvp(
				Avp.MULTIPLE_SERVICES_CREDIT_CONTROL, true, false);
		// Requested-Service-Unit
		AvpSet rsuAvp = msccAvp.addGroupedAvp(Avp.REQUESTED_SERVICE_UNIT, true,
				false);

		// Rating -Group
		msccAvp.addAvp(Avp.RATING_GROUP, 1, true, false, true);

		// [Multiple-Services-Credit-Control-2]

		AvpSet msccAvp2 = ccrAvps.addGroupedAvp(
				Avp.MULTIPLE_SERVICES_CREDIT_CONTROL, true, false);
		// Requested-Service-Unit
		AvpSet rsuAvp2 = msccAvp2.addGroupedAvp(Avp.REQUESTED_SERVICE_UNIT,
				true, false);

		// Rating -Group
		msccAvp2.addAvp(Avp.RATING_GROUP, 2, true, false, true);

		// [Multiple-Services-Credit-Control-3]

		AvpSet msccAvp3 = ccrAvps.addGroupedAvp(
				Avp.MULTIPLE_SERVICES_CREDIT_CONTROL, true, false);
		// Requested-Service-Unit
		AvpSet rsuAvp3 = msccAvp3.addGroupedAvp(Avp.REQUESTED_SERVICE_UNIT,
				true, false);

		// Rating -Group
		msccAvp3.addAvp(Avp.RATING_GROUP, 3, true, false, true);

		// Framed-IP-Address
		byte[] framedIp = { (byte) 0x0a, (byte) 0x3b, (byte) 0x10, (byte) 0xc0 };
		ccrAvps.addAvp(8, framedIp, true, false);

		// 3GPP-IMSI

		ccrAvps.addAvp(Avp.TGPP_IMSI, aIMSI, 10415, true, false, true);

		// 3GPP-Charging-Id

		byte[] chargingId = { (byte) 0x04, (byte) 0x00, (byte) 0x00,
				(byte) 0x70 };
		ccrAvps.addAvp(Avp.TGPP_CHARGING_ID, chargingId, 10415, true, false);

		// 3GPP-PDP-Type

		ccrAvps.addAvp(Avp.TGPP_PDP_TYPE, 0, 10415, true, false, true);

		// 3GPP-GPRS-QoS-Negotiated-Profile
		String qosNeg = "08-4807000021C000002710";
		ccrAvps.addAvp(5, qosNeg, 10415, true, false, true);

		// 3GPP-SGSN-Address
		/*
		 * byte[] sgsnIp = {(byte)0xc0,(byte)0xa8,(byte)0x38,(byte)0x04};
		 * ccrAvps.addAvp(Avp.SGSN_ADDRESS, sgsnIp,true,false);
		 */

		String sgsnAddr = "192.168.56.4";
		Inet4Address sgsnAddress = IPv4Util.fromString(sgsnAddr);
		ccrAvps.addAvp(Avp.SGSN_ADDRESS, sgsnAddress, 10415, true, false);

		// 3GPP-GGSN-Address

		/*
		 * byte[] ggsnIp = {(byte)0xc0,(byte)0xa8,(byte)0x38,(byte)0x03};
		 * ccrAvps.addAvp(Avp.GGSN_ADDRESS, ggsnIp,true,false);
		 */

		String ggsnAddr = "192.168.56.3";
		Inet4Address ggsnAddress = IPv4Util.fromString(ggsnAddr);
		ccrAvps.addAvp(Avp.GGSN_ADDRESS, ggsnAddress, 10415, true, true);

		// 3GPP-IMSI-MCC-MNC

		String imsiMccMnc = "23415";
		ccrAvps.addAvp(Avp.TGPP_IMSI_MCC_MNC, imsiMccMnc, 10415, true, false,
				true);

		// 3GPP-GGSN-MCC-MNC
		String ggsnMccMnc = "23415";
		ccrAvps.addAvp(Avp.TGPP_GGSN_MCC_MNC, ggsnMccMnc, 10415, true, false,
				true);

		// 3GPP-NSAPI

		String nsapi = "5";
		ccrAvps.addAvp(Avp.TGPP_NSAPI, nsapi, 10415, true, false, true);

		// selection mode

		String selMode = "0";
		ccrAvps.addAvp(Avp.TGPP_SELECTION_MODE, selMode, 10415, true, false,
				true);

		// 3GPP-Charging-Characteristics

		String chargChar = "0800";
		ccrAvps.addAvp(Avp.TGPP_CHARGING_CHARACTERISTICS, chargChar, 10415,
				true, false, true);

		// User-Location-Information

		// Radio-Access-Technology
		int rat = 6;
		ccrAvps.addAvp(260, rat, 12645, true, false, true);

		// Context-Type
		int contextTyp = 0;
		ccrAvps.addAvp(256, contextTyp, 12645, true, false, true);

		// Bearer-Usage
		int bearerUsage = 0;
		ccrAvps.addAvp(1000, bearerUsage, 10415, true, false, true);

		// 3GPP-IMEISV

		String imeiSv = "3535750569046801";
		ccrAvps.addAvp(20, imeiSv, 10415, true, false, false);

		// Rulebase-Id

		String ruleBaseId = "wap_ONLINE";
		ccrAvps.addAvp(262, ruleBaseId, 12645, true, false, true);

		// [Service Information]
		/*AvpSet si = ccrAvps.addGroupedAvp(Avp.SERVICE_INFORMATION, 10415,
				false, false);
		AvpSet psinfo = si.addGroupedAvp(Avp.PS_INFORMATION, 10415, false,
				false);
		// CALLED_STATION_ID

		// String apn = "airtelgprs.com";
		/*
		 * String out = null; try { out = new String(apn.getBytes("UTF-8"),
		 * "ISO-8859-1"); } catch (java.io.UnsupportedEncodingException e) {
		 * System
		 * .out.println("failed to convert apn string to UTF8String format");
		 * 
		 * }
		 */
		/*psinfo.addAvp(30, apn, true, false, true);*/
		
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void SessionTerminateCCR(int requestType) throws InternalException {
		try {
		ccr = new JCreditControlRequestImpl(session.getSessions().get(0)
				.createRequest(JCreditControlRequest.code, appID, serverRealm));
		// Create Credit-Control-Request
		// AVPs present by default: Origin-Host, Origin-Realm, Session-Id,
		// Vendor-Specific-Application-Id, Destination-Realm

		ccrAvps = ccr.getMessage().getAvps();
		// remove VENDOR_SPECIFIC_APPLICATION_ID
		// ccrAvps.removeAvp(Avp.VENDOR_SPECIFIC_APPLICATION_ID);

		// { Origin-Host }
		ccrAvps.removeAvp(Avp.ORIGIN_HOST);// one can overwrite default AVP by
											// removing it
		ccrAvps.addAvp(Avp.ORIGIN_HOST, clientURI, true, false, true);

		// [ Destination-Host ]
		ccrAvps.addAvp(Avp.DESTINATION_HOST, serverURI, true, false, false);

		// { Auth-Application-Id }
		// ccrAvps.addAvp(Avp.AUTH_APPLICATION_ID,
		// appID.getAuthAppId(),appID.getVendorId(), true, false, true);

		// { Service-Context-Id }
		// M,V
		ccrAvps.addAvp(Avp.SERVICE_CONTEXT_ID, serviceContextId, true, false,
				false);
		// { CC-Request-Type }
		ccrAvps.addAvp(Avp.CC_REQUEST_TYPE, requestType, true, false);

		// { CC-Request-Number }

		ccrAvps.addAvp(Avp.CC_REQUEST_NUMBER, 0, true, false, true);

		// [Called-Station-Id]

				ccrAvps.addAvp(30, apn, true, false, true);

				// User-Name

				ccrAvps.addAvp(Avp.USER_NAME, "movirtu_subscriber", true, false, true);

				// Origin-State-Id

				byte[] originStateId = { (byte) 0xae, (byte) 0x07, (byte) 0xb0,
						(byte) 0xb7 };
				ccrAvps.addAvp(Avp.ORIGIN_STATE_ID, originStateId, true, false);

				// [ Event-Timestamp ]
				// Calendar c = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
				Calendar c = new GregorianCalendar(1900, 0, 1);

				c.setTimeZone(TimeZone.getTimeZone("UTC"));

				long timestamp = (System.currentTimeMillis() - c.getTimeInMillis()) / 1000;

				ccrAvps.addAvp(Avp.EVENT_TIMESTAMP, timestamp, true, false, true);

				// *[ Subscription-Id ]

				AvpSet subscriptionId = ccrAvps.addGroupedAvp(Avp.SUBSCRIPTION_ID,
						true, false);

				// Subscription-Id-Type AVP
				subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_TYPE, 0, true, false);// END_USER_E164(MSISDN)

				// Subscription-Id-Data AVP
				subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_DATA, aMSISDN, true, false,
						false);

				// Multiple-Services-Indicator
				ccrAvps.addAvp(Avp.MULTIPLE_SERVICES_INDICATOR, 1, true, false, true);

				// [ Requested-Service-Unit ]

				/*
				 * AvpSet rsuAvp = ccrAvps.addGroupedAvp(Avp.REQUESTED_SERVICE_UNIT,
				 * true, false); rsuAvp.addAvp(Avp.CC_SERVICE_SPECIFIC_UNITS, (long) 1,
				 * true, false);
				 */

				// [Multiple-Services-Credit-Control-1]

				AvpSet msccAvp = ccrAvps.addGroupedAvp(
						Avp.MULTIPLE_SERVICES_CREDIT_CONTROL, true, false);
				// Requested-Service-Unit
				AvpSet rsuAvp = msccAvp.addGroupedAvp(Avp.REQUESTED_SERVICE_UNIT, true,
						false);

				// Rating -Group
				msccAvp.addAvp(Avp.RATING_GROUP, 1, true, false, true);

				// [Multiple-Services-Credit-Control-2]

				AvpSet msccAvp2 = ccrAvps.addGroupedAvp(
						Avp.MULTIPLE_SERVICES_CREDIT_CONTROL, true, false);
				// Requested-Service-Unit
				AvpSet rsuAvp2 = msccAvp2.addGroupedAvp(Avp.REQUESTED_SERVICE_UNIT,
						true, false);

				// Rating -Group
				msccAvp2.addAvp(Avp.RATING_GROUP, 2, true, false, true);

				// [Multiple-Services-Credit-Control-3]

				AvpSet msccAvp3 = ccrAvps.addGroupedAvp(
						Avp.MULTIPLE_SERVICES_CREDIT_CONTROL, true, false);
				// Requested-Service-Unit
				AvpSet rsuAvp3 = msccAvp3.addGroupedAvp(Avp.REQUESTED_SERVICE_UNIT,
						true, false);

				// Rating -Group
				msccAvp3.addAvp(Avp.RATING_GROUP, 3, true, false, true);

				// Framed-IP-Address
				byte[] framedIp = { (byte) 0x0a, (byte) 0x3b, (byte) 0x10, (byte) 0xc0 };
				ccrAvps.addAvp(8, framedIp, true, false);

				// 3GPP-IMSI

				ccrAvps.addAvp(Avp.TGPP_IMSI, aIMSI, 10415, true, false, true);

				// 3GPP-Charging-Id

				byte[] chargingId = { (byte) 0x04, (byte) 0x00, (byte) 0x00,
						(byte) 0x70 };
				ccrAvps.addAvp(Avp.TGPP_CHARGING_ID, chargingId, 10415, true, false);

				// 3GPP-PDP-Type

				ccrAvps.addAvp(Avp.TGPP_PDP_TYPE, 0, 10415, true, false, true);

				// 3GPP-GPRS-QoS-Negotiated-Profile
				String qosNeg = "08-4807000021C000002710";
				ccrAvps.addAvp(5, qosNeg, 10415, true, false, true);

				// 3GPP-SGSN-Address
				/*
				 * byte[] sgsnIp = {(byte)0xc0,(byte)0xa8,(byte)0x38,(byte)0x04};
				 * ccrAvps.addAvp(Avp.SGSN_ADDRESS, sgsnIp,true,false);
				 */

				String sgsnAddr = "192.168.56.4";
				Inet4Address sgsnAddress = IPv4Util.fromString(sgsnAddr);
				ccrAvps.addAvp(Avp.SGSN_ADDRESS, sgsnAddress, 10415, true, false);

				// 3GPP-GGSN-Address

				/*
				 * byte[] ggsnIp = {(byte)0xc0,(byte)0xa8,(byte)0x38,(byte)0x03};
				 * ccrAvps.addAvp(Avp.GGSN_ADDRESS, ggsnIp,true,false);
				 */

				String ggsnAddr = "192.168.56.3";
				Inet4Address ggsnAddress = IPv4Util.fromString(ggsnAddr);
				ccrAvps.addAvp(Avp.GGSN_ADDRESS, ggsnAddress, 10415, true, true);

				// 3GPP-IMSI-MCC-MNC

				String imsiMccMnc = "23415";
				ccrAvps.addAvp(Avp.TGPP_IMSI_MCC_MNC, imsiMccMnc, 10415, true, false,
						true);

				// 3GPP-GGSN-MCC-MNC
				String ggsnMccMnc = "23415";
				ccrAvps.addAvp(Avp.TGPP_GGSN_MCC_MNC, ggsnMccMnc, 10415, true, false,
						true);

				// 3GPP-NSAPI

				String nsapi = "5";
				ccrAvps.addAvp(Avp.TGPP_NSAPI, nsapi, 10415, true, false, true);

				// selection mode

				String selMode = "0";
				ccrAvps.addAvp(Avp.TGPP_SELECTION_MODE, selMode, 10415, true, false,
						true);

				// 3GPP-Charging-Characteristics

				String chargChar = "0800";
				ccrAvps.addAvp(Avp.TGPP_CHARGING_CHARACTERISTICS, chargChar, 10415,
						true, false, true);

				// User-Location-Information

				// Radio-Access-Technology
				int rat = 6;
				ccrAvps.addAvp(260, rat, 12645, true, false, true);

				// Context-Type
				int contextTyp = 0;
				ccrAvps.addAvp(256, contextTyp, 12645, true, false, true);

				// Bearer-Usage
				int bearerUsage = 0;
				ccrAvps.addAvp(1000, bearerUsage, 10415, true, false, true);

				// 3GPP-IMEISV

				String imeiSv = "3535750569046801";
				ccrAvps.addAvp(20, imeiSv, 10415, true, false, false);

				// Rulebase-Id

				String ruleBaseId = "wap_ONLINE";
				ccrAvps.addAvp(262, ruleBaseId, 12645, true, false, true);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	public JCreditControlRequest initialCCR() throws Exception {
		SessionInitiateCCR(CC_REQUEST_TYPE_INITIAL);
		return ccr;

	}

	public JCreditControlRequest interimCCR() throws Exception {

		SessionUpdateCCR(CC_REQUEST_TYPE_INTERIM);
		return ccr;

	}

	public JCreditControlRequest terminateCCR() throws Exception {

		SessionTerminateCCR(CC_REQUEST_TYPE_TERMINATE);
		return ccr;

	}

	public JCreditControlRequest eventCCR() throws Exception {

		defaultCCR(CC_REQUEST_TYPE_EVENT);
		return ccr;
	}

}
