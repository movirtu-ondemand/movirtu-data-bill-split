package com.movirtu.diameter.charging.apps.B2BGw.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.diameter.charging.apps.common.TransactionInfo;

public class B2BGwSendFromServiceInvocationWrapperForRequest {
	public static final B2BGwSendFromServiceInvocationWrapperForRequest INSTANCE = new B2BGwSendFromServiceInvocationWrapperForRequest();
	private static final Logger logger = LoggerFactory
			.getLogger(B2BGwSendFromServiceInvocationWrapperForRequest.class);

	public void sendACRWrapper(TransactionInfo obj) throws Exception {
		try {
			logger.debug("inside sendACRWrapper");
			obj.display();
			if (obj.recordType == B2BGwACRBuilder.ACC_REQUEST_TYPE_INITIAL) 
				B2BGwACRController.INSTANCE.sendInitial(obj);
			else if (obj.recordType == B2BGwACRBuilder.ACC_REQUEST_TYPE_INTERIM)
				B2BGwACRController.INSTANCE.sendInterim(obj);
			else if (obj.recordType == B2BGwACRBuilder.ACC_REQUEST_TYPE_TERMINATE)
				B2BGwACRController.INSTANCE.sendTerminate(obj);
			else {
				logger.debug("Unexpected record type");
			}
		} catch (Exception e) {
			StackTraceElement[] trace = e.getStackTrace();
			System.err.println(trace[0].toString());
		}

	}
}
